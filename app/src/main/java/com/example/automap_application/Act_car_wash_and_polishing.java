package com.example.automap_application;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class Act_car_wash_and_polishing extends AppCompatActivity {

    private static final String TAG = "Act_car_wash_and_polish";
    LinearLayout c_body_wash,c_dry_clean,card_clean_by_neno_ceramic,c_publishing;

    ImageView back;
    ArrayList<String> object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_car_wash_and_polishing);

        c_body_wash=findViewById(R.id.card_body_wash);
        c_dry_clean=findViewById(R.id.card_dry_clean);
        c_publishing=findViewById(R.id.card_polishing);
        card_clean_by_neno_ceramic=findViewById(R.id.card_clean_by_neno_ceramic);

        back=findViewById(R.id.back_car_wash_polishing);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<String>) args.getSerializable("ARRAYLIST");
        Log.e(TAG, "object==>: "+object );

        c_body_wash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(0);
                Intent intent = new Intent(Act_car_wash_and_polishing.this, Act_body_wash.class);
                intent.putExtra("string",string);
                startActivity(intent);
            }
        });

        c_dry_clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(1);
                Intent intent = new Intent(Act_car_wash_and_polishing.this, Act_dry_clean.class);
                intent.putExtra("string",string);
                startActivity(intent);
            }
        });

        c_publishing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(2);
                Intent intent = new Intent(Act_car_wash_and_polishing.this, Act_Polishing.class);
                intent.putExtra("string",string);
                startActivity(intent);
            }
        });

        card_clean_by_neno_ceramic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // String string = object.get(3);
                Intent intent = new Intent(Act_car_wash_and_polishing.this, Act_clean_by_neno_ceramic.class);
                intent.putExtra("string","clean by neno ceramic");
                startActivity(intent);
            }
        });
    }
}
