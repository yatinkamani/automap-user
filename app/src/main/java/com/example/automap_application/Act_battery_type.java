package com.example.automap_application;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.Serializable;
import java.util.ArrayList;

public class Act_battery_type extends AppCompatActivity implements View.OnClickListener {

    LinearLayout card_12v_battery,card_electric_battery,card_hybrid_battery;
    ImageView back;

    ArrayList<String> object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_battery_type);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<String>) args.getSerializable("ARRAYLIST");

        back = findViewById(R.id.back_battery);
        card_12v_battery = findViewById(R.id.card_12v_battery);
        card_electric_battery = findViewById(R.id.card_electric_battery);
        card_hybrid_battery = findViewById(R.id.card_hybrid_battery);

        back.setOnClickListener(this);
        card_12v_battery.setOnClickListener(this);
        card_electric_battery.setOnClickListener(this);
        card_hybrid_battery.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == back){
            onBackPressed();
        } else if (view == card_12v_battery){
            Intent intent = new Intent(Act_battery_type.this, Act_battery.class);
            intent.putExtra("enum_id",object.get(0));
            intent.putExtra(getString(R.string.battery),getString(R.string.battery_12v));
            startActivity(intent);
        } else if (view == card_electric_battery){
            Intent intent = new Intent(Act_battery_type.this, Act_battery.class);
            intent.putExtra("enum_id",object.get(1));
            intent.putExtra(getString(R.string.battery),getString(R.string.electric_battery));
            startActivity(intent);
        } else if (view == card_hybrid_battery){
            Intent intent = new Intent(Act_battery_type.this, Act_battery.class);
            intent.putExtra("enum_id",object.get(2));
            intent.putExtra(getString(R.string.battery),getString(R.string.hybrid_battery));
            startActivity(intent);
        }
    }
}
