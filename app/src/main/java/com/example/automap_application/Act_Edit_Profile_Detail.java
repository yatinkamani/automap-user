package com.example.automap_application;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.City_Data_Model;
import com.example.automap_application.Model.Contry_spinner_model;
import com.example.automap_application.Model.Edit_User_Model;
import com.example.automap_application.Model.VolleyMultipartRequest;
import com.example.automap_application.RetrofitApi.ApiConfig;
import com.example.automap_application.RetrofitApi.AppConfig;
import com.example.automap_application.RetrofitApi.UpdateProfile.Result;
import com.example.automap_application.RetrofitApi.UpdateProfile.ServerResponse;
import com.example.automap_application.extra.API;
import com.example.automap_application.extra.APIResponse;
import com.example.automap_application.listeners.PermissionCallback;
import com.example.automap_application.listeners.PermissionChecker;
import com.github.dhaval2404.imagepicker.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Act_Edit_Profile_Detail extends AppCompatActivity implements PermissionCallback {

    EditText e_nm, e_email, e_country, e_state, e_city, e_address, e_pin, e_con_no;
    int pos;

    boolean LoginPage = false;
    CardView c_edit;

    TextView tv_lat, tv_long;
    Button upload;
    private Bitmap bitmap;
    private File path;

    ImageView imageView;
    String user_id, name, email_id, country_id, state, city, address, pin, con_no;

    ImageView ed_pro_back;

    Edit_User_Model ed_model = new Edit_User_Model();
    private ArrayList<Edit_User_Model> editModel;
    LinearLayout ll_account;

    SharedPreferences prefs;
    TextView txt_upload_photo;

    Spinner spinner_country;
    private ArrayList<Contry_spinner_model> goodModelArrayList;
    private ArrayList<String> names = new ArrayList<String>();

    int PICK_IMAGE_CAMERA = 100, PICK_IMAGE_GALLERY = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__edit__profile__detail);

        Intent intent = getIntent();
        ll_account = (LinearLayout) findViewById(R.id.ll_account);
        ll_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ll_account.setVisibility(View.GONE);

        ed_pro_back = (ImageView) findViewById(R.id.ed_pro_back);
        ed_pro_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        name = intent.getStringExtra(getString(R.string.pref_user_name));
        email_id = intent.getStringExtra(getString(R.string.pref_email));
        con_no = intent.getStringExtra(getString(R.string.contact_no));
        LoginPage = intent.getBooleanExtra("LoginPage", false);

        if (LoginPage) {
            ed_pro_back.setVisibility(View.GONE);
        }

        imageView = findViewById(R.id.image);
        upload = findViewById(R.id.btn_upload);
        e_nm = findViewById(R.id.ed_E_nm);
        e_email = findViewById(R.id.ed_E_email);
        e_country = findViewById(R.id.ed_E_country);
        e_state = findViewById(R.id.ed_E_state);
        e_city = findViewById(R.id.ed_E_city);
        e_address = findViewById(R.id.ed_E_address);
        e_pin = findViewById(R.id.ed_E_pincode);
        e_con_no = findViewById(R.id.ed_E_contect);
        c_edit = findViewById(R.id.edit_E_card);
        txt_upload_photo = findViewById(R.id.txt_upload_photo);
        spinner_country = (Spinner) findViewById(R.id.Sp_select_contry);
        tv_lat = findViewById(R.id.tv_let);
        tv_long = findViewById(R.id.tv_long);

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);
        name = prefs.getString(getString(R.string.pref_user_name), "");
        email_id = prefs.getString(getString(R.string.pref_email), "");
        con_no = prefs.getString(getString(R.string.contact_no), "");
        country_id = prefs.getString(getString(R.string.pref_country_id), "");

        PermissionChecker.askForPermission(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, this);

        getUserProfile();
        if (Constant.i == 1) {
            user_id = prefs.getString(getString(R.string.pref_user_id), "");
        } else {
            user_id = intent.getStringExtra(getString(R.string.pref_user_id));
            if (name != null) {
                e_nm.setText(name);
                e_email.setText(email_id);
                e_country.setText(country_id);
                e_con_no.setText(con_no);
            } else {
                e_nm.setText("");
                e_email.setText("");
                e_country.setText("");
                e_con_no.setText("");
            }
        }

        if (hasImage(imageView)) {
            txt_upload_photo.setVisibility(View.VISIBLE);
        } else {
            txt_upload_photo.setVisibility(View.GONE);
        }

        // select user profile image
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);*/
//                selectImage();
                ImagePicker.Companion.with(Act_Edit_Profile_Detail.this)
                        .compress(512)
                        .maxResultSize(620, 620)
                        .crop(3f, 3f)
                        .start();
            }
        });

        c_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadBitmap(bitmap);
//                UpdateProfile2(bitmap);
            }
        });

        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pos = i;
                country_id = goodModelArrayList.get(i).getCon_id();
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString("SPINNER_COUNTRY", spinner_country.getSelectedItem().toString());
                prefEditor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });
        retrieveJSON2();
    }

    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(Act_Edit_Profile_Detail.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }

        return hasImage;
    }

    String user_image = null;
    String currency;

    // get user profile detail and set view
    public void getUserProfile() {
        ProgressDialog dialog = new ProgressDialog(Act_Edit_Profile_Detail.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        RequestQueue queue = Volley.newRequestQueue(Act_Edit_Profile_Detail.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_user_profile",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", " " + response);
                        String name = "";
                        String email = "";
                        String contact_no = "";
                        String country_id = "";
                        String state = "";
                        String city = "";
                        String address = "";
                        String pinCode = "";

                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray result = obj.getJSONArray("result");
                            for (int i = 0; i < result.length(); i++) {

                                JSONObject mainJsonObject = result.getJSONObject(i);
                                name = mainJsonObject.getString("name");
                                email = mainJsonObject.getString("email");
                                contact_no = mainJsonObject.getString("contact_no");
                                country_id = mainJsonObject.getString("country_id");
//                                state = mainJsonObject.getString("state");
                                city = mainJsonObject.getString("city_id");
                                address = mainJsonObject.getString("address");
                                pinCode = mainJsonObject.getString("pincode");
                                user_image = mainJsonObject.getString("user_image");

                                JSONObject country_data = mainJsonObject.optJSONObject("country_data");
                                currency = country_data.optString("currency", "");
                            }

                            SharedPreferences.Editor editor = prefs.edit();
//                            editor.putString("CURRENCY", currency);
                            editor.putString(getString(R.string.pref_user_image), user_image);
                            editor.apply();

                            e_nm.setText(name);
                            e_email.setText(email);
                            e_country.setText(country_id);
//                            if (!state.equals("null"))
//                                e_state.setText(state);
                            if (!city.equals("null"))
                                e_city.setText(city);
                            if (!address.equals("null"))
                                e_address.setText(address);
                            if (!pinCode.equals("null"))
                                e_pin.setText(pinCode);
                            e_con_no.setText(contact_no);

                            if (!user_image.equals("") && !user_image.equals("null")) {
                                txt_upload_photo.setVisibility(View.GONE);
                            } else {
                                txt_upload_photo.setVisibility(View.VISIBLE);
                            }

                            Glide.with(getApplicationContext()).asBitmap().thumbnail(0.01f).load(user_image).listener(new RequestListener<Bitmap>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                    txt_upload_photo.setVisibility(View.VISIBLE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                    txt_upload_photo.setVisibility(View.GONE);
                                    imageView.setImageBitmap(resource);
                                    return true;
                                }
                            }).into(new BitmapImageViewTarget(imageView) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    imageView.setImageBitmap(resource);
                                }
                            });

                            thread.start();

                            currency = prefs.getString("CURRENCY", " ");

                            Log.e("currency", " " + currency);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                Log.e("Tag Error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", user_id);
                return map;

            }
        };
        queue.add(request);
    }

    Thread thread = new Thread(new Runnable() {

        @Override
        public void run() {
            try {
                bitmap = getBitmapFromURL(user_image);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });

    @org.jetbrains.annotations.Nullable
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && data != null) {

            //getting the image Uri
            Uri imageUri = data.getData();
            try {
                //getting bitmap object from uri
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                path = new File(picturePath);

                //displaying selected image to image_view

                imageView.setImageBitmap(bitmap);

                //calling the method uploadBitmap to upload image

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK && data != null) {

            File destination;
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                path = new File(destination.getAbsolutePath());
                imageView.setImageBitmap(bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultCode == Activity.RESULT_OK && data != null) {

            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                imageView.setImageBitmap(bitmap);
                this.bitmap = bitmap;
                path = ImagePicker.Companion.getFile(data);
                txt_upload_photo.setVisibility(View.GONE);
            } catch (Exception e) {
                Log.e("Tag Error", e.toString());
                e.printStackTrace();
            }
        }
    }

    ProgressDialog progress;

    // update user detail with image
    private void uploadBitmap(final Bitmap bitmap) {

        //getting the tag from the edit_text

        name = e_nm.getText().toString();
        email_id = e_email.getText().toString();
        //country_id = e_country.getText().toString();
        state = e_state.getText().toString();
        city = e_city.getText().toString();
        address = e_address.getText().toString();
        pin = e_pin.getText().toString();
        con_no = e_con_no.getText().toString();

        if (TextUtils.isEmpty(name)) {
            e_nm.setError(getString(R.string.name));
        } /*else if (TextUtils.isEmpty(email_id)) {
            e_email.setError("enter email");
        }*/ /*else if (TextUtils.isEmpty(state)) {
            e_state.setError("enter state");
        } else if (TextUtils.isEmpty(city)) {
            e_city.setError("enter city");
        } else if (TextUtils.isEmpty(address)) {
            e_address.setError("enter address");
        } else if (TextUtils.isEmpty(pin)) {
            e_pin.setError("enter pin");
        } */ else if (TextUtils.isEmpty(con_no)) {
            e_con_no.setError(getString(R.string.contact_no));
        } /*else if (!hasImage(imageView)) {
            Toast.makeText(Act_Edit_Profile_Detail.this, "Please select profile image..", Toast.LENGTH_SHORT).show();
        } */ else {
            progress = new ProgressDialog(Act_Edit_Profile_Detail.this);
            progress.setMessage(getString(R.string.updating_your_profile_please_wait));
            progress.setCancelable(false);
            progress.show();
            //  our custom volley request
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "user_profile_update",
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            progress.dismiss();

                            Log.e("response", " " + new String(response.data));
                            try {
                                JSONObject obj = new JSONObject(new String(response.data));

                                ed_model.setMessage(obj.getString("message"));
                                Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();

                                if (obj.optString("status").equals("true")) {

                                    editModel = new ArrayList<>();

                                    JSONArray data_array = obj.getJSONArray("result");

                                    for (int i = 0; i < data_array.length(); i++) {
                                        JSONObject data_obj = data_array.getJSONObject(i);
                                        ed_model.setName(data_obj.getString("name"));
                                        ed_model.setEmail(data_obj.getString("email"));
                                        ed_model.setCountry_id("");
                                        ed_model.setContact_no(data_obj.getString("contact_no"));
                                        ed_model.setState("");
                                        ed_model.setCity("");
                                        ed_model.setAddress("");
                                        ed_model.setPincode("");
                                        ed_model.setLatitude("");
                                        ed_model.setLongitude("");
                                        ed_model.setStatus_d("");

                                        editModel.add(ed_model);

                                    /*if (obj.optString("status").equals("true")) {
                                        Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();
                                    }*/

                                        if (data_obj.has("user_image") && data_obj.getString("user_image").equals("null")) {
                                            ed_model.setUser_image(data_obj.getString("user_image"));
                                        }

                                        SharedPreferences.Editor edit = prefs.edit();
                                        if (bitmap != null) {

                                            Bitmap realImage = BitmapFactory.decodeStream(bitmap2InputStream(bitmap));
                                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                            realImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                            byte[] b = baos.toByteArray();
                                            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                                            edit.putString(getString(R.string.pref_user_image), encodedImage);
                                        }

                                        edit.putString(getString(R.string.pref_user_id), user_id);
                                        edit.putString(getString(R.string.pref_contatct), ed_model.getContact_no());
                                        edit.putString(getString(R.string.pref_email), ed_model.getEmail());
                                        edit.putString(getString(R.string.pref_user_name), ed_model.getName());
                                        Log.e("Tag Path ppp", ""+path);
                                        edit.putString(getString(R.string.pref_user_image), ""+path);
                                        edit.apply();
                                    }
                                    Toast.makeText(Act_Edit_Profile_Detail.this, ed_model.getMessage(), Toast.LENGTH_SHORT).show();
                                    if (Constant.i == 1) {
                                        Intent intent = new Intent(Act_Edit_Profile_Detail.this, Navigation_Activity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(Act_Edit_Profile_Detail.this, Act_car_details.class);
                                        startActivity(intent);
                                    }
                                }
                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progress.dismiss();
                    Log.e("volley_error", " " + error.getMessage());

                    //Toast.makeText(Act_Edit_Profile_Detail.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {

                /*
                 * If you want to add more parameters with the image
                 * you can do it here
                 * here we have only one parameter with the image
                 * which is tags
                 * */
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", user_id);
                    params.put("name", name);
                    params.put("email", email_id);
                    params.put("contact_no", con_no);
                    params.put("language", prefs.getString(getString(R.string.pref_language), "ar"));
//                    params.put("country_id", country_id);
//                    params.put("state", state);
//                    params.put("city", city);
//                    params.put("address", address);
//                    params.put("pincode", pin);
//                    params.put("latitude", "22.281137");
//                    params.put("longitude", "70.794300");
                    Log.e("params", " " + params);

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("USER_NAME", name);
                    editor.apply();

                    return params;
                }

                /*
                 * Here we are passing image by renaming it with a unique name
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    long image_name = System.currentTimeMillis();
                    if (bitmap != null) {

                        params.put("user_image", new DataPart(image_name + ".png", getFileDataFromDrawable(bitmap), "image/*"));

                        Bitmap realImage = BitmapFactory.decodeStream(bitmap2InputStream(bitmap));
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        realImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] b = baos.toByteArray();

                        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                        SharedPreferences.Editor edit = prefs.edit();
                        edit.putString(getString(R.string.pref_user_image), encodedImage);
                        edit.apply();
                    }
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };

            //adding the request to volley
            Volley.newRequestQueue(this).add(volleyMultipartRequest);
            volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 100000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 100000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        }
    }

    private void UpdateProfile2(Bitmap bitmap) {

        name = e_nm.getText().toString();
        email_id = e_email.getText().toString();
        // country_id = e_country.getText().toString();
        state = e_state.getText().toString();
        city = e_city.getText().toString();
        address = e_address.getText().toString();
        pin = e_pin.getText().toString();
        con_no = e_con_no.getText().toString();

        if (TextUtils.isEmpty(name)) {
            e_nm.setError(getString(R.string.name));
        } /*else if (TextUtils.isEmpty(email_id)) {
            e_email.setError("enter email");
        } else if (TextUtils.isEmpty(state)) {
            e_state.setError("enter state");
        } else if (TextUtils.isEmpty(city)) {
            e_city.setError("enter city");
        } else if (TextUtils.isEmpty(address)) {
            e_address.setError("enter address");
        } else if (TextUtils.isEmpty(pin)) {
            e_pin.setError("enter pin");
        } else if (TextUtils.isEmpty(con_no)) {
            e_con_no.setError("enter contact no");
        } else if (!hasImage(imageView)) {
            Toast.makeText(Act_Edit_Profile_Detail.this, "Please select profile image..", Toast.LENGTH_SHORT).show();
        }*/ else {
            progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.updating_your_profile_please_wait));
            progress.setCancelable(false);
            progress.show();
            File file = new File(getRealPathFromURIPath(getImageUri(this, bitmap), this));
//            Log.e("pppp", "UpdateProfile2: "+file );
            RequestBody mFile = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("user_image", file.getName(), mFile);
//            Log.d("ppp", "UpdateProfile2: "+fileToUpload);
            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), user_id);
            RequestBody names = RequestBody.create(MediaType.parse("text/plain"), name);
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), email_id);
            RequestBody contact_no = RequestBody.create(MediaType.parse("text/plain"), con_no);
//            RequestBody country_idd = RequestBody.create(MediaType.parse("text/plain"), country_id);
//            RequestBody state_e = RequestBody.create(MediaType.parse("text/plain"), state);
//            RequestBody city_y = RequestBody.create(MediaType.parse("text/plain"), city);
            RequestBody address = RequestBody.create(MediaType.parse("text/plain"), "Dummy");
//            RequestBody pinCode = RequestBody.create(MediaType.parse("text/plain"), pin);
//            RequestBody latitude = RequestBody.create(MediaType.parse("text/plain"), "22.281137");
//            RequestBody longitude = RequestBody.create(MediaType.parse("text/plain"), "70.794300");

            ApiConfig config = AppConfig.getRetrofit().create(ApiConfig.class);
            config.UpdateProfile(id, names, email, contact_no, address, fileToUpload).enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(@NonNull Call<ServerResponse> call, @NonNull retrofit2.Response<ServerResponse> response) {
                    if (response.isSuccessful()) {
                        Log.e("pp_pp_pp", "onResponse: " + response.body());
                        if (response.body() != null) {
                            if (Objects.requireNonNull(response.body()).getStatus().equalsIgnoreCase("true")) {

                                ed_model.setMessage(Objects.requireNonNull(response.body()).getMessage());
                                Result result = Objects.requireNonNull(response.body()).getResult();
                                editModel = new ArrayList<>();
                                ed_model.setName(result.getName());
                                ed_model.setEmail(result.getEmail());
                                ed_model.setCountry_id("");
                                ed_model.setContact_no(result.getContact_no());
                                ed_model.setState("");
                                ed_model.setCity("");
                                ed_model.setAddress("");
                                ed_model.setPincode("");
                                ed_model.setLatitude("");
                                ed_model.setLongitude("");
                                if (result.getUser_image() != null) {
                                    ed_model.setUser_image(result.getUser_image());
                                }
                                ed_model.setStatus_d(Objects.requireNonNull(response.body()).getStatus());
                                Toast.makeText(getApplicationContext(), Objects.requireNonNull(response.body()).getMessage(), Toast.LENGTH_LONG).show();

                                editModel.add(ed_model);

                                if (Constant.i == 1) {
                                    Intent intent = new Intent(Act_Edit_Profile_Detail.this, Navigation_Activity.class);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(Act_Edit_Profile_Detail.this, Act_car_details.class);
                                    intent.putExtra("Page", "profile");
                                    startActivity(intent);
                                }

                                Bitmap realImage = BitmapFactory.decodeStream(bitmap2InputStream(bitmap));
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                realImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                byte[] b = baos.toByteArray();

                                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                                SharedPreferences.Editor edit = prefs.edit();
                                edit.putString(getString(R.string.pref_user_image), encodedImage);
                                edit.putString(getString(R.string.pref_user_id), user_id);
                                edit.putString(getString(R.string.pref_contatct), ed_model.getContact_no());
                                edit.putString(getString(R.string.pref_email), ed_model.getEmail());
                                edit.putString(getString(R.string.pref_user_name), ed_model.getName());
                                edit.apply();
                            }
                        }
                    }
                    progress.dismiss();
                }

                @Override
                public void onFailure(@NonNull Call<ServerResponse> call, @NonNull Throwable t) {

                    if (t instanceof SocketTimeoutException) {
                        Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                    } else if (t instanceof SocketException) {
                        Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "" + t, Toast.LENGTH_LONG).show();
                    }
                    progress.dismiss();
                }
            });
        }
    }

    public static InputStream bitmap2InputStream(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        InputStream is = new ByteArrayInputStream(baos.toByteArray());
        return is;
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    // get country data
    private void retrieveJSON2() {

        /*StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject obj = new JSONObject(response);
                            if (obj.optString("status").equals("true")) {
                                goodModelArrayList = new ArrayList<>();
                                JSONObject object = obj.getJSONObject("result");
                                JSONArray dataArray = object.getJSONArray("country");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    Contry_spinner_model playerModel = new Contry_spinner_model();
                                    JSONObject data_obj = dataArray.getJSONObject(i);

                                    playerModel.setCon_id(data_obj.getString("country_id"));
                                    playerModel.setCon_code(data_obj.getString("country_code"));
                                    playerModel.setCon_name(data_obj.getString("country_name"));
                                    playerModel.setCon_currency(data_obj.getString("currency"));
                                    playerModel.setCon_language(data_obj.getString("language"));
                                    playerModel.setCon_ph_code(data_obj.getString("phone_code"));
                                    goodModelArrayList.add(playerModel);
                                }

                                for (int i = 0; i < goodModelArrayList.size(); i++) {
                                    names.add(goodModelArrayList.get(i).getCon_name().toString());
                                }
                                String country = prefs.getString("SPINNER_COUNTRY", " ");

                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Edit_Profile_Detail.this, android.R.layout.simple_spinner_dropdown_item, names);
                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                spinner_country.setAdapter(spinnerArrayAdapter);

                                for (int i = 0; i < goodModelArrayList.size(); i++) {
                                    if (country.equals(spinner_country.getItemAtPosition(i).toString())) {
                                        spinner_country.setSelection(i);
                                        break;
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);*/

        API api = new API(getApplicationContext(), new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {
                        goodModelArrayList = new ArrayList<>();
                        JSONObject object = obj.getJSONObject("result");
                        JSONArray dataArray = object.getJSONArray("country");
                        for (int i = 0; i < dataArray.length(); i++) {
                            Contry_spinner_model playerModel = new Contry_spinner_model();
                            JSONObject data_obj = dataArray.getJSONObject(i);

                            playerModel.setCon_id(data_obj.getString("country_id"));
                            playerModel.setCon_code(data_obj.getString("country_code"));
                            playerModel.setCon_name(data_obj.getString("country_name"));
                            playerModel.setCon_currency(data_obj.getString("currency"));
                            playerModel.setCon_language(data_obj.getString("language"));
                            playerModel.setCon_ph_code(data_obj.getString("phone_code"));
                            goodModelArrayList.add(playerModel);
                        }

                        for (int i = 0; i < goodModelArrayList.size(); i++) {
                            names.add(goodModelArrayList.get(i).getCon_name().toString());
                        }
                        String country = prefs.getString("SPINNER_COUNTRY", " ");

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Edit_Profile_Detail.this, android.R.layout.simple_spinner_dropdown_item, names);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        spinner_country.setAdapter(spinnerArrayAdapter);

                        for (int i = 0; i < goodModelArrayList.size(); i++) {
                            if (country.equals(spinner_country.getItemAtPosition(i).toString())) {
                                spinner_country.setSelection(i);
                                break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
            }
        });

        api.execute(200,Constant.URL + "get_country",null,false);
    }

    @Override
    public void permissionGranted() {

    }

    @Override
    public void permissionRefused() {

    }

}
