package com.example.automap_application.listeners;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.Random;

public class PermissionRequest {
    private static Random random;
    private final int requestCode;
    private List<String> permissions;
    private PermissionCallback permissionCallback;

    /*
     * Instantiates a new Permission request.
     * @param permissions the permissions
     * @param permissionCallback the permission callback
     */

    PermissionRequest(List<String> permissions, PermissionCallback permissionCallback) {
        this.permissions = permissions;
        this.permissionCallback = permissionCallback;
        if (random == null) {
            random = new Random();
        }
        this.requestCode = random.nextInt(255);
    }

    /**
     * Gets request code.
     * @return the request code
     */

    public int getRequestCode() {
        return requestCode;
    }

    @Override
    public boolean equals(@Nullable Object object) {
        return object instanceof PermissionRequest && ((PermissionRequest) object).requestCode == this.requestCode;
    }

    @Override
    public int hashCode() {
        return requestCode;
    }
}