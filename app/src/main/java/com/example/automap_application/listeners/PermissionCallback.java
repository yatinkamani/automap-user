package com.example.automap_application.listeners;


public interface PermissionCallback {
    /**
     * Permission granted.
     */
    void permissionGranted();

    /**
     * Permission refused.
     */
    void permissionRefused();


}