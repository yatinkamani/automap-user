package com.example.automap_application.listeners;

import android.location.Location;

public interface GetUserLocationListener {

    void getUserLocation(Location location);
    void onNetworkConnectionChanged(boolean isConnected);
}
