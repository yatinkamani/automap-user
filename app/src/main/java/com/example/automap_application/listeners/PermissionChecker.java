package com.example.automap_application.listeners;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Arrays;

public class PermissionChecker {

    private static final int GRANTED = 0;
    static final int DENIED = 1;
    static final int BLOCKED_OR_NEVER_ASKED = 2;

    @NonNull
    private static final ArrayList<PermissionRequest> PERMISSION_REQUESTS = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    private PermissionChecker() {
    }

    /*
     * Init.
     * @param context the context
     */

    public static void init(@NonNull Context context) {
          PermissionChecker.context = context;
    }

    /*
     * Ask for permission.
     * @param activity           the activity
     * @param permissions        the permissions
     * @param permissionCallback the permission callback
     */

    public static void askForPermission(@NonNull Activity activity, @NonNull String[] permissions, @Nullable PermissionCallback permissionCallback) {
        if (!hasPermissions(activity, permissions)) {
            if (permissionCallback == null) {
                return;
            }
            if (hasPermission(activity, permissions)) {
                permissionCallback.permissionGranted();
                return;
            }
            PermissionRequest permissionRequest = new PermissionRequest(new ArrayList<>(Arrays.asList(permissions)), permissionCallback);
            PERMISSION_REQUESTS.add(permissionRequest);
            ActivityCompat.requestPermissions(activity, permissions, permissionRequest.getRequestCode());
        }
    }

    private static boolean hasPermissions(@Nullable Context context, @Nullable String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /*
     * Returns true if the Activity has access to a all given permission.
     */

    private static boolean hasPermission(@NonNull Activity activity, @NonNull String[] permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /*
     * Not that needed method but if we override others it is good to keep same.
     * @param permissionName the permission name
     * @return the boolean
     * @throws RuntimeException the runtime exception
     */

    static boolean checkPermission(String permissionName) throws RuntimeException {
        if (context == null) {
            throw new RuntimeException("Before comparing permissions you need to call PermissionChecker.init" + "(context)");
        }
            return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context, permissionName);
    }

    /* Gets permission status.
     * @param activity the activity
     * @param androidPermissionName the android permission name
     * @return the permission status
     */

    static int getPermissionStatus(Activity activity, String androidPermissionName) {
        if (ContextCompat.checkSelfPermission(activity, androidPermissionName) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, androidPermissionName)) {
                return BLOCKED_OR_NEVER_ASKED;
            }
            return DENIED;
        }
        return GRANTED;
    }
}