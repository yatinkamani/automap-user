package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Active_user_model;
import com.example.automap_application.Model.Check_user_Model;
import com.example.automap_application.Model.Login_Model;
import com.example.automap_application.RetrofitApi.ApiConfig;
import com.example.automap_application.RetrofitApi.AppConfig;
import com.example.automap_application.newPackage.Helper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Act_Login extends AppCompatActivity {

    public static final String LOGIN_PREF = "Login_preference";
    SharedPreferences sharedpreferences;

    TextView btn_new_user;
    TextView btn_login, forgot;
    EditText ed_email, ed_pass;
    String st_email, st_pass;

    private ArrayList<Active_user_model> Active_user_model;
    Active_user_model active_user_model = new Active_user_model();
    private ArrayList<Check_user_Model> check_model;

    Check_user_Model playerModel = new Check_user_Model();
    private ArrayList<Login_Model> loginModel;
    ProgressDialog progressDialog;

    Login_Model login_model = new Login_Model();
    ImageView login_back;
    String DEVICE_TOKEN;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__login);

        // firebase token
        FirebaseApp.initializeApp(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        @SuppressLint("HardwareIds")
        String d_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    DEVICE_TOKEN = d_id;
                    return;
                }
                String token = task.getResult().getToken();
                DEVICE_TOKEN = token;
                Log.d("DEVICE_TOKEN", DEVICE_TOKEN);
            }
        });
        InitializeToken();
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Event.LOGIN, "Login Screen");
        bundle.putString(FirebaseAnalytics.Event.CAMPAIGN_DETAILS, Settings.Global.getString(getContentResolver(), "device_name"));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        login_back = (ImageView) findViewById(R.id.login_back);
        login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sharedpreferences = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        Intent intent = getIntent();
        setDefault();
        onNewIntent(intent);

        btn_login = findViewById(R.id.btn_Login);
        btn_new_user = findViewById(R.id.Btn_new_user);
        forgot = findViewById(R.id.TV_forgot);
        ed_email = findViewById(R.id.Ed_L_uid);
        ed_pass = findViewById(R.id.Ed_L_pwd);

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Act_Login.this, Act_forgot_password.class);
                startActivity(intent1);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Tag trim email", ed_email.getText().toString());
                progressDialog = new ProgressDialog(Act_Login.this);
                st_email = ed_email.getText().toString().trim();
                st_pass = ed_pass.getText().toString();

                Log.e("Tag trim email", st_email);

                if (TextUtils.isEmpty(st_email)) {
                    ed_email.setError(getString(R.string.enter_contact));
                } else if (!isMobile(st_email)) {
                    ed_email.setError(getString(R.string.contact_invalide));
                } else if (!Helper.isNetworkAvailable(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), R.string.internet_not_connect, Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(st_pass)) {
                    ed_pass.setError(getString(R.string.enter_password));
                } else {
                    progressDialog.setMessage(getString(R.string.loading));
                    progressDialog.show();
//                    LoginApi(); // Retrofit Library
                    Login();// Volley Library
                }
            }
        });

        btn_new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i_new = new Intent(Act_Login.this, Act_Registration.class);
                startActivity(i_new);
            }
        });
    }


    // set default language
    public void setDefault(){
        Locale locale = Locale.getDefault();

        locale.getLanguage();
        Log.e("Tag Language", getResources().getConfiguration().locale.getLanguage());
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(getString(R.string.pref_language), getResources().getConfiguration().locale.getLanguage()).apply();
    }

    public void InitializeToken(){
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken();
                // Used to get firebase token until its null so it will save you from null pointer exeption
                while(DEVICE_TOKEN == null) {
                    DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken();
                }
                Log.d("DEVICE_TOKEN", DEVICE_TOKEN);
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
            }
        }.execute();
    }

    // login user
    public void Login() {
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"user_login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    loginModel = new ArrayList<>();
                    login_model.setMessege(obj.getString("message"));
                    if (obj.optString("status").equals("true")) {

                        login_model.setStatus_up(obj.getString("status"));
                        login_model.setMessege(obj.getString("message"));

                        JSONArray dataArray = obj.getJSONArray("result");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data_obj = dataArray.getJSONObject(i);
                            login_model.setUser_id(data_obj.getString("user_id"));
                            login_model.setName(data_obj.getString("name"));
                            login_model.setEmail(data_obj.getString("email"));
                            login_model.setContect_no(data_obj.getString("contact_no"));
                            login_model.setCountry_id(data_obj.getString("country_id"));
//                            login_model.setState(data_obj.getString("state"));
//                            login_model.setCity(data_obj.getString("city"));
                            login_model.setAddress(data_obj.getString("address"));
                            login_model.setLati(data_obj.getString("latitude"));
                            login_model.setLongi(data_obj.getString("longitude"));
//                            login_model.setPin(data_obj.getString("pinCode"));
                            login_model.setUser_image(data_obj.getString("user_image"));
                            login_model.setStatus(data_obj.getString("status"));
                            login_model.setReg_date(data_obj.getString("registration_date"));
                            login_model.setUpdate_date(data_obj.getString("updated_at"));
                            loginModel.add(login_model);
                        }

                            SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(getString(R.string.pref_user_id), login_model.getUser_id());

                        Log.e("user_id", login_model.getUser_id());

                        editor.putString(getString(R.string.pref_user_name), login_model.getName());
                        Log.e("name", login_model.getName());
                        editor.putString(getString(R.string.pref_email), login_model.getEmail());
                        Log.e("email_id", login_model.getEmail());
                        editor.putString(getString(R.string.pref_contatct), login_model.getContect_no());
                        editor.putString(getString(R.string.pref_country_id), login_model.getCountry_id());
                        editor.putString(getString(R.string.pref_user_image), login_model.getUser_image());
                        editor.putString(getString(R.string.pref_user_pwd), ed_pass.getText().toString());
                        editor.apply();

                        if (obj.optString("status").equals("true")) {

                            //  Toast.makeText(Act_Login.this, login_model.getUser_id(), Toast.LENGTH_SHORT).show();
                            //  getCarDetail(login_model.getUser_id());
                            retrieveJSON(login_model.getUser_id());
                            Toast.makeText(Act_Login.this, login_model.getMessege(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Act_Login.this, login_model.getMessege(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Catch", "" + e.getMessage());
                }
                progressDialog.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                Toast.makeText(Act_Login.this, getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                Log.e("My error", "" + error.getMessage());
                Log.e("My error", "" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("email", st_email);
                map.put("contact_no", st_email);
                map.put("password", st_pass);
                map.put("device_token", DEVICE_TOKEN);
                String language = "";
                if (sharedpreferences.getString(getString(R.string.pref_language),"").equals("ar")){
                    language = "Arabic";
                }else {
                    language = "English";
                }
                map.put("language",language);
                Log.e("params", " " + map);
                return map;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(6000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
        queue.add(request);
    }

    public static boolean isValidEmail(CharSequence target) {
        return (Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean isMobile(String data){

        if (data.matches("[0-9]+")){
            return true;
        }else {
            return false;
        }
    }

    // get car detail and store preferance
    private void getCarDetail(String user_id) {

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_car_detail", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                dialog.dismiss();
                try {
                    String car_id = "";
                    String car_model_id = "";
                    String car_brand_id = "";
                    String vin_no = "";
                    String fuel_type = "";
                    String fuel_id = "";
                    String booking_date = "";
                    String car_year = "";
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.optString("status").equals("true")) {

                        JSONArray result = mainJsonObject.getJSONArray("result");

                        for (int i = 0; i < result.length(); i++) {

                            JSONObject jsonObject = result.getJSONObject(i);
                            try {
                                car_id = jsonObject.getString("car_id");
                                vin_no = jsonObject.getString("vin_no");
                                booking_date = jsonObject.getString("registration_date");
                                fuel_type = jsonObject.getString("fule_type");
                                fuel_id = jsonObject.getString("fule_id");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                car_model_id = jsonObject.getString("model_id");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                car_year = jsonObject.getString("car_year");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                car_brand_id = jsonObject.getString("brand_id");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(getString(R.string.pref_car_id), car_id);
                        editor.putString(getString(R.string.pref_car_model_id), car_model_id);
                        editor.putString(getString(R.string.pref_car_brand_id), car_brand_id);
                        editor.putString(getString(R.string.pref_vin_no), vin_no);
                        editor.putString(getString(R.string.pref_car_year), car_year);
                        editor.putString(getString(R.string.pref_booking_date), booking_date);
                        editor.putString(getString(R.string.pref_fule_type), fuel_type);
                        editor.putString(getString(R.string.pref_fule_id), fuel_id);
                        editor.apply();
                        /*Intent i = new Intent(Act_Login.this, Navigation_Activity.class);
                        startActivity(i);*/
                    }
                    Intent i = new Intent(Act_Login.this, Navigation_Activity.class);
                    startActivity(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  displaying the error in toast if occurs

                dialog.dismiss();
                Log.e("TAG", "onErrorResponse: " + error);
                Toast.makeText(getApplicationContext(), "Network Connection ", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void active(final String id, final String type) {
        RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "active_account", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    obj.getString("message");
                    String msg = obj.getString("message").toString();
                    active_user_model.setMessage(obj.getString("message"));
                    Toast.makeText(Act_Login.this, msg, Toast.LENGTH_SHORT).show();
                    Active_user_model = new ArrayList<>();
                    Active_user_model.add(active_user_model);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id);
                map.put("type", type);
                Log.e("Tag Params", " " + map.toString());
                return map;
            }
        };
        queue.add(request);
    }

    // check loged user status and redires screen
    private void retrieveJSON(final String id) {
        RequestQueue queue = Volley.newRequestQueue(Act_Login.this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "check_user_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("check_user_response", " " + response);
                    JSONObject obj = new JSONObject(response);
                    obj.getString("request");
                    if (obj.optString("status").equals("true")) {
                        check_model = new ArrayList<>();
                        JSONArray dataArray = obj.getJSONArray("result");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data_obj = dataArray.getJSONObject(i);
                            playerModel.setAddress(data_obj.getString("user_id"));
                            playerModel.setName(data_obj.getString("name"));
                            playerModel.setEmail(data_obj.getString("email"));
                            playerModel.setContect_no(data_obj.getString("contact_no"));
                            playerModel.setContry_id(data_obj.getString("country_id"));
                            playerModel.setCity(data_obj.getString("city_id"));
                            playerModel.setAddress(data_obj.getString("address"));
                            playerModel.setLatitude(data_obj.getString("latitude"));
                            playerModel.setLongitude(data_obj.getString("longitude"));
                            playerModel.setPincode(data_obj.getString("pincode"));
                            playerModel.setStatus(data_obj.getString("status"));
                            playerModel.setRegistration_date(data_obj.getString("registration_date"));
                            playerModel.setUpdated_at(data_obj.getString("updated_at"));
                            check_model.add(playerModel);
                        }

                        SharedPreferences notification_badge = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                        if (!notification_badge.getString(getString(R.string.pref_user_id), "").toString().equals(login_model.getUser_id())) {
                            notification_badge.edit().clear().apply();
                        }

                        if (obj.getString("request").equals("profile")) {
                            Intent intent = new Intent(Act_Login.this, Act_Edit_Profile_Detail.class);
                            intent.putExtra(getString(R.string.pref_user_id), login_model.getUser_id());
                            intent.putExtra(getString(R.string.pref_user_name), login_model.getName());
                            intent.putExtra(getString(R.string.pref_email), login_model.getEmail());
                            intent.putExtra(getString(R.string.pref_country_id), login_model.getCountry_id());
                            intent.putExtra(getString(R.string.contact_no), login_model.getContect_no());
                            intent.putExtra("LoginPage", true);
                            startActivity(intent);
                        } else if (obj.getString("request").equals("car")) {
                            Intent i = new Intent(Act_Login.this, Act_car_details.class);
                            Constant.i = 1;
                            startActivity(i);
                        } else if (obj.getString("request").equals("dashboard")) {
//                            carDetailAPI(login_model.getUser_id());
                            getCarDetail(login_model.getUser_id());
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Act_Login.this, "Something went wrong", Toast.LENGTH_LONG).show();
//                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", id);

                return map;
            }
        };
        queue.add(request);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent != null && intent.getData() != null) {
            String s0 = intent.getData().toString();
            String s1 = s0.substring(0, 54);
            Log.e("first", " " + s1);
            String s2 = s0.substring(54, s0.length());
            Log.e("second", s2);

            String[] separated = s0.split("/");
//            String str1 = separated[0];
//            String str2 = separated[1];
//            String str3 = separated[2]; // = stackoverflow.com
//            String str4 = separated[3]; // = questions
//            String str5 = separated[4];
            String str6 = separated[5];
            String str7 = separated[6];

            /*byte[] decodeValue = Base64.decode(s2.trim(), Base64.DEFAULT);
            Log.e("decode", "" + new String(decodeValue));
            String decode = new String();
            decode = new String(decodeValue);*/

            active(str7, str6);
        }
    }

}
