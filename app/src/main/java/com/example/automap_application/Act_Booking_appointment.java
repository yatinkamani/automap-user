package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.City_Data_Model;
import com.example.automap_application.extra.API;
import com.example.automap_application.extra.APIResponse;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Booking_appointment extends AppCompatActivity implements View.OnClickListener {

    TextView txt_title;
    ImageView back_garage_list;
    String date = "";
    String time = "";
    String user_id = "", type = "";
    boolean is_price = false;
    public Activity instance = null;
    ProgressDialog progressDialog;
    SimpleDateFormat fmt;
    String array;

    LinearLayout lin_booking, lin_dynamic_checkbox;
    EditText Ed_problem;
    Switch switch_price;
    Button btn_booking;
    Button btn_go_garage;
    TextView txt_date_time, txt_problem;

    LinearLayout lin_img, lin_img_list, lin_error;
    Button btn_retry;
    TextView txt_error;
    HorizontalScrollView horizontalScrollView;
    TextView select_img;
    private int PICK_IMAGE_GALLERY = 100;
    ArrayList<String> encodedImageList = new ArrayList<>();
    private ArrayList<Uri> mArrayUri = new ArrayList<>();
    private ArrayList<Uri> mArrayUri3 = new ArrayList<>();
    String imageURI;
    private Bitmap bitmap;
    ArrayList<String> checkbox = new ArrayList<>();
    ArrayList<String> checked_item = new ArrayList<>();
    boolean isMultipleSelect = true;

    LinearLayout lin_add_check, lin_check, lin_tire_detail, lin_problem, lin_other, lin_oil_detail, lin_battery, lin_rims_detail;
    EditText ED_tire_brand, ED_tire_size, ED_other, ED_oil_sae, ED_oil_brand, ED_battery_brand, ED_rims_brand, ED_rims_size;
    boolean isType = false;
    boolean isBooking = false;
    boolean isImage = false;

    int year, month, dayOfMonth;
    SharedPreferences prefs;
    private String service_id = "", city_id = "", country_id = "";
    Spinner select_city;
    private ArrayList<City_Data_Model> city_data_models;

    String sub_service_id, sub_service_name;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
//        applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
//          Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng);//your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);

        String language = prefs.getString(getString(R.string.pref_language), "ar").toString().trim();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_act__booking_appointment);

        instance = this;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));

        back_garage_list = findViewById(R.id.back_garage_list);
        txt_title = findViewById(R.id.txt_title);

        txt_error = findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);
        lin_booking = (LinearLayout) findViewById(R.id.lin_booking);
        lin_error = (LinearLayout) findViewById(R.id.lin_error);
        lin_dynamic_checkbox = (LinearLayout) findViewById(R.id.lin_dynamic_checkbox);
        btn_booking = (Button) findViewById(R.id.btn_book_appointment);
        btn_go_garage = (Button) findViewById(R.id.btn_go_garage);
        switch_price = (Switch) findViewById(R.id.switch_price);
        Ed_problem = (EditText) findViewById(R.id.ED_problem);
        ED_tire_brand = (EditText) findViewById(R.id.ED_tire_brand);
        ED_tire_size = (EditText) findViewById(R.id.ED_tire_size);
        txt_date_time = (TextView) findViewById(R.id.txt_date_time);
        select_img = (TextView) findViewById(R.id.select_img);
        horizontalScrollView = findViewById(R.id.HorizontalScroll);
        lin_img = findViewById(R.id.lin_img);
        lin_img_list = findViewById(R.id.lin_img_list);
        lin_check = findViewById(R.id.lin_check);
        lin_add_check = findViewById(R.id.lin_add_cheack);
        lin_tire_detail = findViewById(R.id.lin_tire_detail);
        lin_problem = findViewById(R.id.lin_problem);
        lin_other = findViewById(R.id.lin_other);
        ED_other = findViewById(R.id.ED_other);
        txt_problem = findViewById(R.id.txt_problem);
        lin_oil_detail = findViewById(R.id.lin_oil_detail);
        lin_rims_detail = findViewById(R.id.lin_rims_detail);
        ED_oil_sae = findViewById(R.id.ED_oil_sae);
        ED_oil_brand = findViewById(R.id.ED_oil_brand);
        lin_battery = findViewById(R.id.lin_battery);
        ED_battery_brand = findViewById(R.id.ED_battery_brand);
        ED_rims_brand = findViewById(R.id.ED_rims_brand);
        ED_rims_size = findViewById(R.id.ED_rims_size);

        select_city = findViewById(R.id.select_city);

        btn_retry.setOnClickListener(this);
        back_garage_list.setOnClickListener(this);

        array = getIntent().getStringExtra("array");
        type = getIntent().getStringExtra("type");

        sub_service_id = getIntent().getStringExtra("sub_service_id");
        sub_service_name = getIntent().getStringExtra("sub_service_name");

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        date = String.valueOf(year) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(dayOfMonth);
        time = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
        Log.e("Tag time date", date + " " + time);

        txt_date_time.setOnClickListener(this);
        btn_go_garage.setOnClickListener(this);
        select_img.setOnClickListener(this);
        btn_booking.setOnClickListener(this);

        switch_price.setChecked(true);

        switch_price.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                is_price = isChecked;
            }
        });

        fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
        String s_date = fmt.format(System.currentTimeMillis());
        txt_date_time.setText(s_date);
        Log.e("Tag", s_date);

        // get register user country city spinner
        getCity();

        // selected service wise display view
        try {
            JSONArray jsonArray = new JSONArray(array);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                String service = object.getString("service_name");
                txt_title.setText(Html.fromHtml(service));
                service_id = object.getString("service_id");
                service = service.trim();
                ImageVisibility(true);
                getServiceOption();
                if (sub_service_name.toString().contains("All Auto accessories Suppliers") || service_id.equals("15")) {
                    ImageVisibility(true);
                    isImage = true;
                } else if (sub_service_name.toString().contains("All Auto body repair") || service_id.equals("")) {
                    ImageVisibility(true);
                    isImage = true;
                } else if (sub_service_name.toString().contains("Locksmiths, Keys Auto Service") || service_id.equals("13")) {
                    isType = true;
//                    isMultipleSelect = true;
                    txt_problem.setText(getString(R.string.type_your_problem));
                } else if (sub_service_name.toString().contains("Muffler Shop") || service_id.equals("")) {
                    isType = true;
//                    isMultipleSelect = true;
                    lin_problem.setVisibility(View.GONE);
                } else if (sub_service_name.toString().contains("Replace the wheels and tires") || service_id.equals("")) {
                    isType = true;
//                    isMultipleSelect = true;
                    lin_problem.setVisibility(View.GONE);
                } else if (sub_service_name.toString().contains("Car Wash and Oil Change Stations") || service_id.equals("9")) {
                    lin_problem.setVisibility(View.GONE);
                    isType = true;
//                    isMultipleSelect = true;
                } else if (sub_service_name.toString().contains("Auto glass repair") || service_id.equals("14")) {
                    isType = true;
//                    isMultipleSelect = true;
                    lin_problem.setVisibility(View.GONE);
                } else if (sub_service_name.trim().toString().contains("Upholstery Car seats & Dye leather Car seats") || service_id.equals("16")) {
                    isType = true;
//                    isMultipleSelect = true;
                } else if (service_id.toString().contains("10")) {
                    lin_problem.setVisibility(View.GONE);
//                    isMultipleSelect = true;
                    isType = true;
                } else if (sub_service_name.toString().contains("Steering Wheel Alignment") || service_id.equals("")) {
                    lin_other.setVisibility(View.VISIBLE);
                    lin_problem.setVisibility(View.GONE);
                    isType = true;
//                    isMultipleSelect = true;
                } else if (service.toString().contains("Transmission shop") || service_id.equals("")) {
                    //                    checkbox = new ArrayList<>();
                    //                    checkbox.add(getString(R.string.repair_gearbox));
                    //                    checkbox.add(getString(R.string.change_gearbox));
                    //                    checkbox.add(getString(R.string.check_gearbox));
                    isType = true;
//                    isMultipleSelect = true;
                    lin_other.setVisibility(View.VISIBLE);
                    lin_problem.setVisibility(View.GONE);
                } else if (sub_service_name.toString().contains("Mechanic shop") || service_id.equals("18")) {
//                    isMultipleSelect = true;
                    isType = true;
                    lin_problem.setVisibility(View.GONE);
                    lin_other.setVisibility(View.VISIBLE);
                } else if (sub_service_name.toString().contains("ECU and Radio repair") || service_id.equals("")) {
//                    isMultipleSelect = true;
                    isType = true;
                    lin_problem.setVisibility(View.GONE);
                    lin_other.setVisibility(View.VISIBLE);
                } else if (sub_service_name.toString().contains("Auto parts shop") || service_id.equals("4")) {
//                    isMultipleSelect = true;
                    isType = true;
                    lin_problem.setVisibility(View.VISIBLE);
                    lin_other.setVisibility(View.GONE);
                    txt_problem.setText(getString(R.string.type_your_problem));
                } else {
//                    isMultipleSelect = true;
                    isType = true;
                    lin_other.setVisibility(View.VISIBLE);
                    lin_problem.setVisibility(View.GONE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // get city data and set spinner
    private void getCity() {
        /*StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Tag Country City", response);
                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.optString("status").equals("true")) {

                                JSONObject object = obj.getJSONObject("result");

                                city_data_models = new ArrayList<>();
                                city_data_models.add(new City_Data_Model("All City", "", ""));
                                JSONArray array = object.optJSONArray("city");
                                if (array == null)
                                    array = new JSONArray();
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    City_Data_Model city_data_model = new City_Data_Model();

                                    city_data_model.setCity_id(object1.getString("city_id"));
                                    city_data_model.setCity_name(object1.getString("city_name"));
                                    city_data_model.setCountry_id(object1.getString("country_id"));
                                    *//*if (object1.getString("country_id").equals(prefs.getString(getString(R.string.pref_country_id), "11"))) {
                                        city_data_models.add(city_data_model);
                                    }*//*
                                    if (object1.getString("country_id").equals("11")) {
                                        city_data_models.add(city_data_model);
                                    }
                                }

                                ArrayList<String> names = new ArrayList<>();
                                for (int i = 0; i < city_data_models.size(); i++) {
                                    names.add(city_data_models.get(i).getCity_name().toString());
                                }

                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Booking_appointment.this, android.R.layout.simple_spinner_dropdown_item, names);
                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                select_city.setAdapter(spinnerArrayAdapter);

                                select_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        if (i == 0) {
                                            city_id = "";
                                        } else {
                                            city_id = city_data_models.get((i)).getCity_id();
                                        } 
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);*/

        API api = new API(getApplicationContext(), new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                Log.e("Tag Country City", response);
                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.optString("status").equals("true")) {

                        JSONObject object = obj.getJSONObject("result");

                        city_data_models = new ArrayList<>();
                        city_data_models.add(new City_Data_Model("All City", "", ""));
                        JSONArray array = object.optJSONArray("city");
                        if (array == null)
                            array = new JSONArray();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            City_Data_Model city_data_model = new City_Data_Model();

                            city_data_model.setCity_id(object1.getString("city_id"));
                            city_data_model.setCity_name(object1.getString("city_name"));
                            city_data_model.setCountry_id(object1.getString("country_id"));

                            Log.e("Tag Country id", prefs.getString(getString(R.string.pref_country_id), "11"));

                            if (object1.getString("country_id")
                                    .equals(prefs.getString(getString(R.string.pref_country_id), "11"))
                            ) {
                                city_data_models.add(city_data_model);
                            }
                            /*if (object1.getString("country_id").equals("11")) {
                                city_data_models.add(city_data_model);
                            }*/
                        }

                        ArrayList<String> names = new ArrayList<>();
                        for (int i = 0; i < city_data_models.size(); i++) {
                            names.add(city_data_models.get(i).getCity_name().toString());
                        }

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Booking_appointment.this, android.R.layout.simple_spinner_dropdown_item, names);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        select_city.setAdapter(spinnerArrayAdapter);

                        select_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 0) {
                                    city_id = "";
                                } else {
                                    city_id = city_data_models.get((i)).getCity_id();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
            }
        });

        api.execute(200, Constant.URL + "get_country", null, false);

    }

    // set service option check box dynamic
    public void DynamicCheckBox() {

        if (checkbox != null && checkbox.size() > 0) {

            lin_check.setVisibility(View.VISIBLE);
            lin_add_check.removeAllViews();
            for (int i = 0; i < checkbox.size(); i++) {

                CheckBox checkBox = new CheckBox(this);
                checkBox.setText(checkbox.get(i));
                checkBox.setTextColor(Color.WHITE);
                LinearLayout linearLayout = new LinearLayout(this);
                linearLayout.setVisibility(View.GONE);
                lin_add_check.addView(checkBox);
                if (checkbox.get(i).equalsIgnoreCase(getString(R.string.breaks))) {
                    AddBreakView(linearLayout, checkBox);
                    lin_add_check.addView(linearLayout);
                }

                int finalI = i;
                checkBox.setOnCheckedChangeListener((compoundButton, b) -> {

                    if (isMultipleSelect) {
                        if (b) {
                            if (!checked_item.contains(checked_item))
                                checked_item.add(compoundButton.getText().toString());
                        } else {
                            checked_item.remove(compoundButton.getText().toString());
                        }
                    } else {
                        for (int j = 0; j < lin_add_check.getChildCount(); j++) {
                            View view = lin_add_check.getChildAt(j);
                            if (view instanceof CheckBox) {
                                ((CheckBox) view).setChecked(false);
                            }
                        }
                        checked_item = new ArrayList<>();
                        if (b) {
                            if (!checked_item.contains(checked_item))
                                checked_item.add(compoundButton.getText().toString());

                            checkBox.setChecked(true);
//                            checked_item.add(compoundButton.getText().toString());
                        } else {
                            checked_item.remove(compoundButton.getText().toString());
                        }
                    }

                    Log.e("Tag Name", "" + b + "   " + compoundButton.getText().toString().toLowerCase());
                    if (b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("new tire") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("إطار جديد")) {
                        lin_tire_detail.setVisibility(View.VISIBLE);
                    } else if (b == false && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("new tire") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("إطار جديد")
                    ) {
                        lin_tire_detail.setVisibility(View.GONE);
                        ED_tire_brand.setText("");
                        ED_tire_size.setText("");
                    }

                    if (b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("rims") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("جنطات")) {
                        lin_rims_detail.setVisibility(View.VISIBLE);
                    } else if (!b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("rims") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("جنطات")) {
                        lin_rims_detail.setVisibility(View.GONE);
                        ED_rims_brand.setText("");
                        ED_rims_size.setText("");
                    }

                    if (b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("others") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("other") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("الآخرين")) {
                        lin_other.setVisibility(View.VISIBLE);
                    } else if (!b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("others") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("other") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("الآخرين")) {
                        lin_other.setVisibility(View.GONE);
                        ED_other.setText("");
                    }

                    if (b && (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("change oil")
                            || compoundButton.getText().toString().equalsIgnoreCase("engine oil")
                            || compoundButton.getText().toString().equalsIgnoreCase("زيت محرك"))) {
                        lin_oil_detail.setVisibility(View.VISIBLE);

                    } else if (!b && (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("change oil")
                            || compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("engine oil")
                            || compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("زيت محرك"))) {
                        lin_oil_detail.setVisibility(View.GONE);
                        ED_oil_sae.setText("");
                        ED_oil_brand.setText("");
                    }

                    if (b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("battery 12v") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("battery 12v") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("بطارية 12 فولت")) {
                        lin_battery.setVisibility(View.VISIBLE);
                    } else if (!b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("battery 12v") ||
                            compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("بطارية 12 فولت")) {
                        lin_battery.setVisibility(View.GONE);
                        ED_battery_brand.setText("");
                    }
                });
            }
        } else {
            lin_check.setVisibility(View.GONE);
        }
    }

    // get service option and set list of check box
    public void getServiceOption() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            ProgressDialog dialog = new ProgressDialog(Act_Booking_appointment.this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();
            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "getServiceOption", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Tag Service Option", response);

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    checkbox = new ArrayList<>();
                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equals("1")) {

                            lin_error.setVisibility(View.GONE);
                            lin_booking.setVisibility(View.VISIBLE);
                            JSONArray array = object.getJSONArray("result");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                checkbox.add(object1.getString("option_name"));
                                isMultipleSelect = object1.getString("multiple_option").equals("1") ? true : false;
                            }
                            Log.e("Tag", "" + isMultipleSelect);
                            lin_add_check.setVisibility(View.VISIBLE);
                            DynamicCheckBox();
                        } else {
                            lin_add_check.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        lin_error.setVisibility(View.VISIBLE);
                        lin_booking.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_error.setText(getString(R.string.somting_wrong_please));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    Log.e("tag Error", error.toString());
                    lin_error.setVisibility(View.VISIBLE);
                    lin_booking.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_error.setText(getString(R.string.somting_wrong_please));

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("subservice_id", sub_service_id);
                    Log.e("tag Params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        } else {
            lin_error.setVisibility(View.VISIBLE);
            lin_booking.setVisibility(View.GONE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_error.setText(getString(R.string.internet_not_connect));
        }
    }

    // data select of appointment booking
    public void date_pick() {

        Locale.setDefault(Locale.ENGLISH);
        @SuppressLint("SimpleDateFormat")
        DatePickerDialog dialog = new DatePickerDialog(Act_Booking_appointment.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Log.e("year", "" + year);
                Log.e("month", "" + month);
                Log.e("dayOfMonth", "" + dayOfMonth);
                final String dates = String.valueOf(year) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(dayOfMonth);
                date = dates;
                Log.e("date", "" + date);
                time_pick();
            }
        }, year, month, dayOfMonth);

        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dialog.show();
    }

    // time select of appointment booking
    public void time_pick() {
        @SuppressLint("SimpleDateFormat") final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        final Date[] dateObj = new Date[1];

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        Locale.setDefault(Locale.ENGLISH);
        TimePickerDialog timePickerDialog = new TimePickerDialog(Act_Booking_appointment.this, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Log.e("hourOfDay", "" + hourOfDay);
                Log.e("minute", "" + minute);
                final String times = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                try {
                    dateObj[0] = sdf.parse(times);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int hour = hourOfDay;
                int minutes = minute;
                String timeSet = "";
                if (hour > 12) {
                    hour -= 12;
                    timeSet = "PM";
                } else if (hour == 0) {
                    hour += 12;
                    timeSet = "AM";
                } else if (hour == 12) {
                    timeSet = "PM";
                } else {
                    timeSet = "AM";
                }

                String min = "";
                if (minutes < 10)
                    min = "0" + minutes;
                else
                    min = String.valueOf(minutes);

                String aTime = new StringBuilder().append(hour).append(':').append(min).append(" ").append(timeSet).toString();
                time = times;
                txt_date_time.setText(date + " " + aTime);
                Log.e("minute", "" + time);

                if (isBooking) {
                    isBooking = false;
                    Booking();
                }
//              time = new SimpleDateFormat("hh:mm aa").format(dateObj[0]);
            }
        }, hour, minute, false);

        timePickerDialog.show();
    }

    // selected data validation check and redirect
    private void Booking() {

        SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
        String sendDate = sp.getString(service_id, "");
        @SuppressLint("SimpleDateFormat")
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        //   date = numberFormat.format(date);

        Log.e("Tag Date", sendDate + "\n" + date);
        Log.e("Tag Date", service_id);

        /*if (sendDate.equalsIgnoreCase(date)) {
            Toast.makeText(getApplicationContext(), "you are use this service in day", Toast.LENGTH_LONG).show();
        } else {*/
        if (checked_item.toString().replaceAll("\\[|\\]", "").toLowerCase().toString().contains("others")) {
            checked_item.add(ED_other.getText().toString());
        } else if (!ED_other.getText().toString().trim().equalsIgnoreCase("")) {
            checked_item.add(ED_other.getText().toString());
        }
            /*if (TextUtils.isEmpty(Ed_problem.getText()) || Ed_problem.getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please Enter Problem",Toast.LENGTH_LONG).show();
                }else if (TextUtils.isEmpty(date) || date.equals("") || time.equals("") || TextUtils.isEmpty(time)){
                Toast.makeText(getApplicationContext(),"Please Select Date and Time",Toast.LENGTH_LONG).show();
            } else */
        if (isType && checked_item.size() < 1 && lin_other.getVisibility() == View.VISIBLE) {
            Toast.makeText(getApplicationContext(), getString(R.string.please_require_fill_service), Toast.LENGTH_LONG).show();
        } else if (isType && checked_item.size() < 1 && lin_other.getVisibility() == View.GONE && checkbox.size() > 0) {
            Toast.makeText(getApplicationContext(), getString(R.string.please_require_fill_service), Toast.LENGTH_LONG).show();
        } /*else if (isNewTire && ED_tire_brand.getText().toString().equals("") ){
                        Toast.makeText(getApplicationContext(),"Please enter tire brand",Toast.LENGTH_LONG).show();
                    } */ else if (lin_tire_detail.getVisibility() == View.VISIBLE && ED_tire_size.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.please_tire_size), Toast.LENGTH_LONG).show();
        } else if (lin_oil_detail.getVisibility() == View.VISIBLE && ED_oil_sae.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.please_oil_sea), Toast.LENGTH_LONG).show();
        }  /*else if (isOil && ED_oil_brand.getText().toString().equals("") ){
                        Toast.makeText(getApplicationContext(),"Please enter Oil Brand",Toast.LENGTH_LONG).show();
                    } */ else if (lin_battery.getVisibility() == View.VISIBLE && ED_battery_brand.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.please_battery_brand), Toast.LENGTH_LONG).show();
        } else if (lin_problem.getVisibility() == View.VISIBLE && Ed_problem.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.please_service_data), Toast.LENGTH_LONG).show();
        } else if (lin_rims_detail.getVisibility() == View.VISIBLE && ED_rims_size.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.please_rims_size), Toast.LENGTH_LONG).show();
        } else if (isImage && mArrayUri.size() < 1) {
            Toast.makeText(getApplicationContext(), getString(R.string.please_select_image), Toast.LENGTH_LONG).show();
        } else if (array.length() > 0) {

            Log.e("Tag Array", array.toString());
            Log.e("Tag Checked Items", "Data" + checked_item.toString());

            Intent intent = new Intent(getApplicationContext(), Act_Garage_List.class);
            intent.putExtra("service_json", array.toString());
            intent.putExtra("problem", Ed_problem.getText().toString());
            intent.putExtra("tire_brand", ED_tire_brand.getText().toString());
            intent.putExtra("tire_size_number", ED_tire_size.getText().toString());
            intent.putExtra("oil_brand", ED_oil_brand.getText().toString());
            intent.putExtra("oil_sae", ED_oil_sae.getText().toString());
            intent.putExtra("rims_brand", ED_rims_brand.getText().toString());
            intent.putExtra("rims_size", ED_rims_size.getText().toString());
            intent.putExtra("battery_brand", ED_battery_brand.getText().toString());
            intent.putExtra("time", time);
            intent.putExtra("date", date);
            intent.putExtra("type", type);
            intent.putExtra("city_id", city_id);
            Constant.mUri = mArrayUri;
            intent.putExtra("appointment_type", checked_item.toString().replaceAll("\\[|\\]", ""));
            startActivity(intent);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.please_service_data), Toast.LENGTH_LONG).show();
        }
//        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && null != data) {
                // Get the Image from data

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                encodedImageList.clear();

                mArrayUri3 = new ArrayList<>();
                if (data.getData() != null) {
                    Uri mImageUri = data.getData();
                    mArrayUri = new ArrayList<Uri>();
                    mArrayUri.add(mImageUri);
                    Log.e("Tag Data", data.getDataString());
                    Log.e("Tag Data Path", data.getData().getPath());

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri, filePathColumn, null, null, null);

                    // Move to first row
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageURI = cursor.getString(columnIndex);
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                    mArrayUri3.add(getImageUri(this, bitmap));
                    Log.e("Tag Bitmap", bitmap.toString());
                    // Log.e("Tag URI", imageURI);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                    encodedImageList.add(encodedImage);
                    cursor.close();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            // Get the cursor
                            // Move to first row
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageURI = cursor.getString(columnIndex);

                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                            mArrayUri3.add(getImageUri(this, bitmap));

                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

                            encodedImageList.add(encodedImage);
                            cursor.close();
                            if (i == 4) {
                                Toast.makeText(getApplicationContext(), getString(R.string.slect_max_image), Toast.LENGTH_LONG).show();
                                break;
                            }
                        }
//                        noImage.setText("Selected Images: " + mArrayUri.size());
                    }
                }

                lin_img_list.removeAllViews();

                for (int i = 0; i < mArrayUri3.size(); i++) {

                    View view = LinearLayout.inflate(getApplicationContext(), R.layout.appointment_image, null);

                    ImageView imageView = view.findViewById(R.id.appoint_img);
                    imageView.setImageURI(mArrayUri3.get(i));
                    lin_img_list.addView(view);
                }
                lin_img.setVisibility(View.VISIBLE);

                ImageVisibility(true);
            }
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            Log.e("Tag Error", "" + e);
        }
    }

    // image select to visible images list
    public void ImageVisibility(boolean b) {
        if (lin_img_list.getChildCount() == 0) {
            horizontalScrollView.setVisibility(View.GONE);
        } else {
            horizontalScrollView.setVisibility(View.VISIBLE);
        }

        if (b) {
            lin_img.setVisibility(View.VISIBLE);
        } else {
            lin_img.setVisibility(View.GONE);
        }
    }

    // add sub back view check box
    public void AddBreakView(LinearLayout linearLayout, CheckBox checkBox) {

//        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 0, 0, 0);
        linearLayout.setPadding(20, 0, 0, 0);

        ArrayList<String> breaks = new ArrayList<>();
        breaks.add(getString(R.string.front_break));
        breaks.add(getString(R.string.Back_break));

        for (int j = 0; j < breaks.size(); j++) {
            CheckBox checkBox1 = new CheckBox(this);
            checkBox1.setText("" + breaks.get(j));
            checkBox1.setTextColor(Color.BLUE);
            if (checked_item.contains(getString(R.string.breaks))) {
                checked_item.remove(getString(R.string.breaks));
            }

            if (linearLayout.getVisibility() == View.VISIBLE && !checked_item.contains(getString(R.string.front_break))) {
                checked_item.add(getString(R.string.front_break));
            }

            if (linearLayout.getVisibility() == View.VISIBLE && !checked_item.contains(getString(R.string.Back_break))) {
                checked_item.add(getString(R.string.Back_break));
            }

            checkBox1.setChecked(true);
            checkBox1.setOnCheckedChangeListener((compoundButton, b) -> {

                if (b && compoundButton.getText().toString().equalsIgnoreCase(getString(R.string.front_break)) && !checked_item.contains(getString(R.string.front_break))) {
                    checked_item.add(getString(R.string.front_break));
                } else if (!b && compoundButton.getText().toString().equalsIgnoreCase(getString(R.string.front_break))) {
                    checked_item.remove(getString(R.string.front_break));
                }

                if (b && compoundButton.getText().toString().equalsIgnoreCase(getString(R.string.Back_break)) && !checked_item.contains(getString(R.string.front_break))) {
                    checked_item.add(getString(R.string.Back_break));
                } else if (!b && compoundButton.getText().toString().equalsIgnoreCase(getString(R.string.Back_break))) {
                    checked_item.remove(getString(R.string.Back_break));
                }

                boolean isAllCheck = false;
                for (int c = 0; c < linearLayout.getChildCount(); c++) {
                    CheckBox box = ((CheckBox) linearLayout.getChildAt(c));

                    if (box.isChecked()) {
                        isAllCheck = true;
                        break;
                    }
                }
                if (!isAllCheck) {
                    linearLayout.setVisibility(View.GONE);
                    checkBox.setChecked(false);
                }

            });
            linearLayout.addView(checkBox1);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btn_retry) {
            getServiceOption();
        } else if (view == back_garage_list) {
            onBackPressed();
        }
        // select multiple image
        else if (view == select_img) {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Choose application"), PICK_IMAGE_GALLERY);

        } else if (view == txt_date_time) {
            isBooking = false;
            date_pick();
        } else if (view == btn_booking) {
            isBooking = true;
//            date_pick();
            Booking();
        } else if (view == btn_go_garage) {
          /*if (TextUtils.isEmpty(Ed_problem.getText()) || Ed_problem.getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please Enter Problem",Toast.LENGTH_LONG).show();
                } else*/

            SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
            String sendDate = sp.getString(service_id, "");
            @SuppressLint("SimpleDateFormat")
            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());

            /*if (sendDate.equalsIgnoreCase(date)) {
                Toast.makeText(getApplicationContext(), "You can not use the service more than once in day", Toast.LENGTH_LONG).show();
            } else {*/
            if (array.length() > 0) {

                Intent intent = new Intent(getApplicationContext(), Act_Garage_List.class);
                intent.putExtra("service_json", array.toString());
                intent.putExtra("problem", Ed_problem.getText().toString());
                intent.putExtra("type", type);
//                    Toast.makeText(getApplicationContext(), "garage is Present for your Problem", Toast.LENGTH_LONG).show();
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.please_service_data), Toast.LENGTH_LONG).show();
            }
//            }
        }
    }

}
