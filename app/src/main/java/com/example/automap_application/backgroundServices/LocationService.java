package com.example.automap_application.backgroundServices;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import android.widget.Toast;

import com.example.automap_application.Act_start_trip;
import com.example.automap_application.R;

public class LocationService extends Service implements LocationListener {

    double loc1,loc2;
    Location location1;
    boolean start = false, end = false;
    LocationManager locationManager;
    NotificationManager notificationManager;

    public LocationService() {
    }

    @Override
    public void onCreate() {
        this.locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null){

            loc1 = (Double) intent.getExtras().get("latitude");
            loc2 = (Double) intent.getExtras().get("longitude");
            location1 = new Location("location1");
            location1.setLatitude((loc1));
            location1.setLatitude((loc2));
            start = intent.getExtras().getBoolean("start");
            end = intent.getExtras().getBoolean("end");
        }

        this.locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //      ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return (int)1;
        }
        this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this);
        this.locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 10, this);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null){

//            Toast.makeText(getApplicationContext(),"on Change "+location,Toast.LENGTH_LONG).show();
            if (location1 != null){
                float distance = location.distanceTo(location);
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(), Act_start_trip.class);

                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                                .setAutoCancel(true)
                                .setTicker("Stop Your Trip")
                                .setContentTitle("Stop Your Trip")
                                .setContentText("Please end Your Trip, you can't move your current location in last 10 minute")
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentIntent(pendingIntent)
                                .setSound(RingtoneManager.getActualDefaultRingtoneUri(getApplicationContext(),RingtoneManager.TYPE_RINGTONE));

                        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            notificationManager.createNotificationChannel(new NotificationChannel("1","CHANNAL", NotificationManager.IMPORTANCE_DEFAULT));
                        }
                        notificationManager.notify(0,builder.build());
//                        Toast.makeText(getApplicationContext(),"INSIDE",Toast.LENGTH_LONG).show();
                    }
                };
//                Toast.makeText(getApplicationContext(),"Default "+distance,Toast.LENGTH_LONG).show();

                if (distance > 100){
                    Toast.makeText(getApplicationContext(),"100"+handler,Toast.LENGTH_LONG).show();
                    if (runnable != null)
                        handler.removeCallbacks(runnable);

                }else {
                    if (!end){
                        handler.postDelayed(runnable,60000 * 10);
                    }
                }
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
