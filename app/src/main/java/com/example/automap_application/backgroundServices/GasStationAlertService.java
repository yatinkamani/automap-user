package com.example.automap_application.backgroundServices;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Act_petrol_consumption;
import com.example.automap_application.R;
import com.example.automap_application.firebase.NotificationUtils;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class GasStationAlertService extends Service implements LocationListener {

    boolean message = false;
    LocationManager locationManager;

    public GasStationAlertService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            // public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return (int) 1;
        }
        this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        this.locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        Log.e("TAG onStartCommand", "" + locationManager.getAllProviders());
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        this.locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Log.e("TAG OnCreate", "" + locationManager.getLastKnownLocation("gps"));
    }

    public void check_petrol_station(Location location){

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + location.getLatitude() + "," + location.getLongitude());
        googlePlacesUrl.append("&radius=30");
        googlePlacesUrl.append("&type=" + "gas_station");
        googlePlacesUrl.append("&key=" + getString(R.string.google_api_key));

        Log.e("TAG URL",googlePlacesUrl.toString());
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest request = new StringRequest(Request.Method.GET, googlePlacesUrl.toString(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("TAG",response);
//                    progressDialog.dismiss();
                    if (!response.isEmpty()){
                        try {
                            JSONObject mainObject = new JSONObject(response);
                            Log.e("TAG",mainObject.getString("status"));
//                          if (mainObject != null){
                            if (mainObject.getString("status").equals("OK")){
                                message = true;
                                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                                Intent intent = new Intent(getApplicationContext(), Act_petrol_consumption.class);
                                intent.putExtra("gas_station","gas_station");
                                startActivity(intent);
                                notificationUtils.showNotificationMessage("Gas Station",100,"You Are Near About Gas Station", ""+System.currentTimeMillis(),intent);
                            }else {
                                if (!new Act_petrol_consumption().isFinishing()){
//                                    Toast.makeText(getApplicationContext(),getString(R.string.you_must_abide_by_the_conditions), Toast.LENGTH_LONG).show();
                                }
                                message = false;
                            }
//                        }
                        } catch (JSONException e) {
                            e.printStackTrace();
//                            progressDialog.dismiss();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("TAG",error.toString());
                    if (error instanceof com.android.volley.NoConnectionError){
                        Toast.makeText(getApplicationContext(),getString(R.string.internet_not_connect),Toast.LENGTH_LONG).show();
                    }
//                    progressDialog.dismiss();
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(request);
        }else {
            Toast.makeText(getApplicationContext(),getString(R.string.internet_not_connect),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("TAG onStartCommand","LoCaTiOn  "+location.getLatitude());

        if (location != null){
            check_petrol_station(location);
        }else {
            Log.e("TAG CAST STATION ALERT","LOCATION NOT FOUND");
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
