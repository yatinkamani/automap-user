package com.example.automap_application.backgroundServices;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;

import com.example.automap_application.Act_Edit_Profile_Detail;
import com.example.automap_application.firebase.NotificationUtils;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class NotificationService extends IntentService {

    private boolean running = true;
    private static String CHANNEL_ID = "channel_id_01";

    public NotificationService() {
        super("NotificationService");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("Tag","OnHandle Intent");

    }

    @Override
    public void onCreate() {
        Log.e("Tag", "OnCreate");
        super.onCreate();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("Tag", "OnStartCommand");
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Tag", "OnStartCommand");
        if (intent != null) {
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            Intent intent1 = new Intent(getApplicationContext(), Act_Edit_Profile_Detail.class);
            intent1.putExtra("msg","Enter VIN Number for Automap");
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            notificationUtils.showNotificationMessage("Automap VIN Number",40,"Enter VAN Number for You Register Car",""+System.currentTimeMillis(),intent1);
        }
        return START_STICKY;
    }

}
