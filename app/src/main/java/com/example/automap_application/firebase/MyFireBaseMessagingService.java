package com.example.automap_application.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.example.automap_application.Act_splash_screen;
import com.example.automap_application.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import me.leolin.shortcutbadger.ShortcutBadger;

public class MyFireBaseMessagingService extends FirebaseMessagingService {

    private static final int REQUEST_CODE = 1;
    private NotificationUtils notificationUtils;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    int get_quote = 0, mobile_request = 0, tow_track = 0, admin_msg = 0;

    int code = 0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("receive_notification", "aaa");

        preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        editor = preferences.edit();

        get_quote = preferences.getInt(getString(R.string.pref_get_quote_badge_count), 0);
        mobile_request = preferences.getInt(getString(R.string.pref_mobile_request_badge_count), 0);
        tow_track = preferences.getInt(getString(R.string.pref_towtrack_request_badge_count), 0);
        admin_msg = preferences.getInt(getString(R.string.pref_admin_msg_badge_count), 0);

       /* Intent i = new Intent(this, Act_Edit_Profile_Detail.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE, i, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                .setContentText(remoteMessage.getNotification().getBody())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build()); */

        JSONObject obj = new JSONObject(remoteMessage.getData());
        String jsonMessage = obj.toString();
        Log.e("response", "JSON Object" + obj.toString());
        try {
            String messageFor = obj.getString("message_for");

            Intent intent = new Intent("send_data");

            Log.e("Tag quotes", "" + get_quote);
            Log.e("Tag mobile", "" + mobile_request);
            Log.e("Tag towTrack", "" + tow_track);
            Log.e("Tag admin msg", "" + admin_msg);

            // set different request notification badge count and broadcast receiver

            if (messageFor.equals("garage") || messageFor.equals("user")) {
                get_quote = get_quote + 1;
                code = 100;
                Log.e("Tag quote inside", "" + get_quote);
                editor.putInt(getString(R.string.pref_get_quote_badge_count), get_quote);
                intent.putExtra(getString(R.string.pref_get_quote_badge_count), get_quote);
                intent.putExtra(getString(R.string.pref_mobile_request_badge_count), mobile_request);
                intent.putExtra(getString(R.string.pref_towtrack_request_badge_count), tow_track);
                intent.putExtra(getString(R.string.pref_admin_msg_badge_count), admin_msg);
            } else if (messageFor.equals("mobile_request")) {
                mobile_request = mobile_request + 1;
                code = 200;
                editor.putInt(getString(R.string.pref_mobile_request_badge_count), mobile_request);
                intent.putExtra(getString(R.string.pref_mobile_request_badge_count), mobile_request);
                intent.putExtra(getString(R.string.pref_get_quote_badge_count), get_quote);
                intent.putExtra(getString(R.string.pref_towtrack_request_badge_count), tow_track);
                intent.putExtra(getString(R.string.pref_admin_msg_badge_count), admin_msg);
                Log.e("Tag mobile inside", "" + mobile_request);
            } else if (messageFor.equals("towtrack")) {
                tow_track = tow_track + 1;
                code = 300;
                editor.putInt(getString(R.string.pref_towtrack_request_badge_count), tow_track);
                intent.putExtra(getString(R.string.pref_towtrack_request_badge_count), tow_track);
                intent.putExtra(getString(R.string.pref_get_quote_badge_count), get_quote);
                intent.putExtra(getString(R.string.pref_mobile_request_badge_count), mobile_request);
                intent.putExtra(getString(R.string.pref_admin_msg_badge_count), admin_msg);
                Log.e("Tag towtrack inside", "" + tow_track);
            } else if (messageFor.equals("admin_msg")) {
                admin_msg = admin_msg + 1;
                Random random = new Random();
                code = random.nextInt();
                editor.putInt(getString(R.string.pref_admin_msg_badge_count), admin_msg);
                intent.putExtra(getString(R.string.pref_towtrack_request_badge_count), tow_track);
                intent.putExtra(getString(R.string.pref_get_quote_badge_count), get_quote);
                intent.putExtra(getString(R.string.pref_mobile_request_badge_count), mobile_request);
                intent.putExtra(getString(R.string.pref_admin_msg_badge_count), admin_msg);
                Log.e("Tag towtrack inside", "" + admin_msg);
            }
            editor.apply();
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            Log.e("Tag Badge Count", "" + get_quote);
            Log.e("Tag mobile Count", "" + mobile_request);
            Log.e("Tag tow track Count", "" + tow_track);
            Log.e("Tag admin msg Count", "" + admin_msg);

            ShortcutBadger.applyCount(getApplicationContext(), (get_quote + mobile_request + tow_track + admin_msg));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //  Calling method to generate notification
        sendNotification(jsonMessage);

    }

    // send notification
    private void sendNotification(String messageBody) {

        try {
            JSONObject jsonMain = new JSONObject(messageBody);
            String messageFor = "";
            String message = "";
            String id = "";

            if (jsonMain.getString("success").equals("1")) {
                messageFor = jsonMain.getString("message_for");
                message = jsonMain.optString("msg", "");
                id = jsonMain.optString("id", "");
            }

            Intent intent = new Intent(this, Act_splash_screen.class);
            intent.putExtra("messageFor", messageFor);
            intent.putExtra("id", id);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, code, intent, PendingIntent.FLAG_ONE_SHOT);

            showNotificationMessage(getApplicationContext(), code, getString(R.string.app_name), message, "" + System.currentTimeMillis(), intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNotificationMessage(Context context, int code, String title, String message, String timeStamp, Intent intent) {

        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, code, message, timeStamp, intent);
    }

}


