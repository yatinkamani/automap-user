package com.example.automap_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Tow_Track_Request_List_Model;
import com.example.automap_application.backgroundServices.LocationUpdateService;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.view.View.GONE;

public class Act_Tow_Track_Request_List extends AppCompatActivity implements View.OnClickListener {

    ImageView back_towTruck_list, img_menu;
    RecyclerView rcy_towTruck_list;
    View lin_error;
    TextView txt_msg;
    Button btn_retry;
    String user_id = "";
    SharedPreferences preferences;
    ArrayList<Tow_Track_Request_List_Model> tow_track_request_list_models;
    AdapterTowTrackRequestList adapterTowTrackRequestList;

    public Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__tow__track__request__list);

        instance = this;

        back_towTruck_list = findViewById(R.id.back_towtrack_list);
        img_menu = findViewById(R.id.img_menu);
        rcy_towTruck_list = findViewById(R.id.rcy_towtrack_list);
        lin_error = findViewById(R.id.lin_error);
        txt_msg = findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);
        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id), "");

        btn_retry.setOnClickListener(this);
        back_towTruck_list.setOnClickListener(this);
        img_menu.setOnClickListener(this);
        startService(new Intent(getApplicationContext(), LocationUpdateService.class));
        getTowTrackRequestList();
        InitializeBroadcast();
    }

    @Override
    public void onClick(View view) {

        if (view == btn_retry) {
            getTowTrackRequestList();
        } else if (view == back_towTruck_list) {
            onBackPressed();
        }
        // 3 dots filter popup menu view
        else if (view == img_menu) {

            PopupMenu menu = new PopupMenu(Act_Tow_Track_Request_List.this, img_menu);
            menu.getMenuInflater().inflate(R.menu.sort_menu, menu.getMenu());
            menu.getMenu().findItem(R.id.price).setTitle("All Request");
            menu.getMenu().findItem(R.id.distance).setTitle("Last 7 Day Request").setVisible(true);
            menu.getMenu().findItem(R.id.time).setTitle("Last 30 Day Request");
            menu.getMenu().findItem(R.id.item1).setTitle("Last 1 Day Request").setVisible(true);

            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    if (menuItem.getItemId() == R.id.price) {
                        adapterTowTrackRequestList.setFilter(tow_track_request_list_models);
                    } else if (menuItem.getItemId() == R.id.distance) {
                        adapterTowTrackRequestList.setFilter(FilterDate(-7, tow_track_request_list_models));
                    } else if (menuItem.getItemId() == R.id.time) {
                        adapterTowTrackRequestList.setFilter(FilterDate(-30, tow_track_request_list_models));
                    } else if (menuItem.getItemId() == R.id.item1) {
                        adapterTowTrackRequestList.setFilter(FilterDate(-1, tow_track_request_list_models));
                    }
                    return true;
                }
            });
            menu.show();
        }
    }

    @Override
    protected void onResume() {
        getTowTrackRequestList();
        super.onResume();
    }

    // get towtruck request list and display list view
    private void getTowTrackRequestList() {

        ProgressDialog dialog = new ProgressDialog(Act_Tow_Track_Request_List.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "user_towtrack_request_list", new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                Log.e("TowTrack Response", " " + response);
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                try {

                    JSONObject mainJsonObject = new JSONObject(response);

                    if (mainJsonObject.getString("status").equalsIgnoreCase("true")) {

                        JSONArray result = mainJsonObject.getJSONArray("result");
                        tow_track_request_list_models = new ArrayList<>();
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject jsonObject = result.getJSONObject(i);

                            if (jsonObject.getString("user_id").equals(user_id)) {
                                tow_track_request_list_models.add(new Tow_Track_Request_List_Model(jsonObject.getString("towtrack_request_id"),
                                        jsonObject.getString("user_location"), jsonObject.optString("final_destination", ""),
                                        jsonObject.getString("created_at"), jsonObject.getString("updated_at"), ""));
                            }
//                            tow_track_request_list_models.get(i).setTowtrack_request_id(jsonObject.getString("towtrack_request_id"));
//                            tow_track_request_list_models.get(i).setCreate_at(jsonObject.getString("create_at"));
//                            tow_track_request_list_models.get(i).setCreate_at(jsonObject.getString("update_at"));
//                            tow_track_request_list_models.get(i).setFinal_destination(jsonObject.optString("final_destination",""));
//                            tow_track_request_list_models.get(i).setUser_location(jsonObject.getString("user_location"));
//                            tow_track_request_list_models.get(i).setTimeout_status(jsonObject.getString("timeout_status"));
                        }

                        Collections.reverse(tow_track_request_list_models);
                        Log.e("size==>", " " + tow_track_request_list_models.size());

                        if (tow_track_request_list_models != null) {
                            if (tow_track_request_list_models.size() > 0) {

                                adapterTowTrackRequestList = new AdapterTowTrackRequestList(Act_Tow_Track_Request_List.this, tow_track_request_list_models);
                                rcy_towTruck_list.setLayoutManager(new GridLayoutManager(Act_Tow_Track_Request_List.this, 1));
                                rcy_towTruck_list.setAdapter(adapterTowTrackRequestList);
                                lin_error.setVisibility(GONE);
                                rcy_towTruck_list.setVisibility(View.VISIBLE);

                                if (tow_track_request_list_models.size() > 1) {
                                    img_menu.setVisibility(View.VISIBLE);
                                }

                                SharedPreferences notification_badge = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                SharedPreferences.Editor editor = notification_badge.edit();
                                editor.putInt(getString(R.string.pref_towtrack_request_badge_count), 0);
                                editor.apply();
                            } else {
                                lin_error.setVisibility(View.VISIBLE);
                                btn_retry.setVisibility(GONE);
                                txt_msg.setText(getString(R.string.no_data_found));
                                rcy_towTruck_list.setVisibility(GONE);
                            }
                        }
                    } else {
                        lin_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(GONE);
                        txt_msg.setText(getString(R.string.no_data_found));
                        rcy_towTruck_list.setVisibility(GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    lin_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    rcy_towTruck_list.setVisibility(GONE);
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onErrorResponse(VolleyError error) {
                lin_error.setVisibility(View.VISIBLE);
                btn_retry.setVisibility(View.VISIBLE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                rcy_towTruck_list.setVisibility(GONE);
                Log.e("TAG", error.toString());
                dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    class AdapterTowTrackRequestList extends RecyclerView.Adapter<AdapterTowTrackRequestList.MyViewHolder> {

        private Activity activity;
        private List<Tow_Track_Request_List_Model> booking_models;

        AdapterTowTrackRequestList(Activity activity, List<Tow_Track_Request_List_Model> booking_models) {
            this.activity = activity;
            this.booking_models = booking_models;
        }

        public void setFilter(ArrayList<Tow_Track_Request_List_Model> models) {
            booking_models = new ArrayList<>();
            booking_models.addAll(models);
            notifyDataSetChanged();
        }

        @SuppressLint("SetTextI18n")
        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_tow_track_request_list, viewGroup, false);

            return new MyViewHolder(view);

        }

        @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
            Tow_Track_Request_List_Model model = booking_models.get(i);

            txt_request_date.setText(getString(R.string.request_date)+"  : " + DateFormat(model.getCreate_at()));

            if (model.getTimeout_status().equals("0")) {
                txt_timeout_status.setVisibility(GONE);
            } else if (model.getTimeout_status().equals("1")) {
                txt_timeout_status.setVisibility(View.VISIBLE);
                txt_timeout_status.setText(activity.getString(R.string.status)+"    : "+getString(R.string.you_request_has_been_timeout));
            } else {
                txt_timeout_status.setVisibility(GONE);
            }

            if (model.getFinal_destination().equals("")) {
                txt_final_destination.setVisibility(GONE);
            } else {
                txt_final_destination.setVisibility(View.VISIBLE);
                txt_final_destination.setText(Html.fromHtml("<b>"+ getString(R.string.final_destination)  +": </b>" + model.getFinal_destination()));
            }

            if (model.getUser_location().equals("")) {
                txt_user_location.setVisibility(GONE);
            } else {
                txt_user_location.setVisibility(View.VISIBLE);
                txt_user_location.setText(Html.fromHtml("<b>"+ getString(R.string.selected_address) +" : </b>" + model.getUser_location()));
            }

            llDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(activity, Act_Tow_Track_Offer_List.class);
                    in.putExtra("tow_track_offer", true);
                    in.putExtra("tow_track_request_id", model.getTowtrack_request_id());
                    activity.startActivity(in);
                }
            });

            /*myViewHolder.itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {

                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            dx = myViewHolder.itemView.getX() - motionEvent.getRawX();
                            dy = myViewHolder.itemView.getY() - motionEvent.getRawY();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            myViewHolder.itemView.animate()
                                    .x(motionEvent.getRawX() + dx)
                                    .y(motionEvent.getRawY() + dy)
                                    .setDuration(0)
                                    .start();
                            break;
                        default:
                            return false;
                    }

                    return true;
                }
            });*/

        }

        @Override
        public int getItemCount() {
            return booking_models.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private TextView txt_request_date, txt_user_location, txt_final_destination, txt_timeout_status;
        private LinearLayout llDetail;

        class MyViewHolder extends RecyclerView.ViewHolder {
            private MyViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_request_date = itemView.findViewById(R.id.txt_request_date);
                txt_user_location = itemView.findViewById(R.id.txt_select_address);
                txt_final_destination = itemView.findViewById(R.id.txt_final_address);
                txt_timeout_status = itemView.findViewById(R.id.txt_timeout_status);
                llDetail = itemView.findViewById(R.id.llDetail);

            }
        }

        private String TimeFormat(String time) {
            String aTime = "1";
            if (time.split(":").length > 1) {
                int hour = Integer.parseInt(time.split(":")[0]);
                int minute = Integer.parseInt(time.split(":")[1]);
                int minutes = minute;
                String timeSet = "";
                if (hour > 12) {
                    hour -= 12;
                    timeSet = "PM";
                } else if (hour == 0) {
                    hour += 12;
                    timeSet = "AM";
                } else if (hour == 12) {
                    timeSet = "PM";
                } else {
                    timeSet = "AM";
                }

                String min = "";
                if (minutes < 10)
                    min = "0" + minutes;
                else
                    min = String.valueOf(minutes);
                Log.e("TAG AM PM", timeSet);
                Log.e("TAG Hour ", "" + hour);
                Log.e("TAG Minute", "" + min);

                aTime = new StringBuilder().append(hour).append(':')
                        .append(min).append(" ").append(timeSet).toString();
                Log.e("TAG ATime", "" + aTime);
            }

            return aTime;
        }

        private String DateFormat(String Date) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd, MMM yyyy", Locale.ENGLISH);
            java.util.Date date = null;
            try {
                date = format.parse(Date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return newFormat.format(date);

        }
    }

    @Override
    public void onBackPressed() {
        stopService(new Intent(getApplicationContext(), LocationUpdateService.class));
        super.onBackPressed();
    }

    public ArrayList<Tow_Track_Request_List_Model> FilterDate(int day, ArrayList<Tow_Track_Request_List_Model> models) {

        ArrayList<Tow_Track_Request_List_Model> filter_models = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH) + day));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(calendar.getTime());

        Date lastDate = null;
        try {
            lastDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("Tag Date", " Last Date 0" + lastDate);

        for (int i = 0; i < models.size(); i++) {

            if (lastDate != null) {
                if (!models.get(i).getCreate_at().equals("") && !models.get(i).getCreate_at().equals("null")) {
                    try {
                        Date created_at = dateFormat.parse(AppUtils.ConvertLocalTimeSame(models.get(i).getCreate_at()));

                        if (created_at.equals(lastDate) || created_at.after(lastDate)) {
                            filter_models.add(models.get(i));
                        }
                        Log.e("Tag Date", "" + created_at + " Last Date " + lastDate);
                        Log.e("Tag Date", "" + created_at.after(lastDate));
                        Log.e("Tag Date", "" + created_at.before(lastDate));


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return filter_models;
    }

    public void InitializeBroadcast(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(Act_Tow_Track_Request_List.this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }


    // new request offer come to refres api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (tow_track_badge > 0 && !(Act_Tow_Track_Request_List.this.isFinishing())) {
                    getTowTrackRequestList();
                }
                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);

            }
        }
    };

}
