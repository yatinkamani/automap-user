package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.Garage_Model;
import com.example.automap_application.Model.VolleyMultipartRequest;
import com.example.automap_application.adapter.GarageListAdapter;
import com.example.automap_application.extra.EqualSpacingItemDecoration;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Garage_List extends AppCompatActivity implements GarageListAdapter.customButtonListener, View.OnClickListener {

    ImageView back_garage_list;
    RecyclerView rl_garage;
    GarageListAdapter garageListAdapter;
    String garage_name;
    String garage_banner;
    String garage_id;
    String facebook_link, rating = "";
    ArrayList<Garage_Model> garage_list = new ArrayList<>();
    ProgressDialog progressDialog;
    String date = "";
    String time = "";
    String type = "";
    String problem = "", appointment_type = "", tire_size_number = "", tire_brand = "", oil_sae = "", oil_brand = "",
            battery_brand = "", rims_brand = "", rims_size = "", city_id = "";
    String user_id = "";
    boolean garage_offer = false;
    RelativeLayout lin_bottom;
    View no_data_error;
    Button btn_retry;
    TextView txt_msg;
    Button btn_send;
    String model_id = "", brand_id = "", service_id = "", fule_id = "";
    public Activity instance = null;
    String request_id = "";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String facility_json = null;
    String service_json = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__garage__list);
        instance = this;

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));

        rl_garage = (RecyclerView) findViewById(R.id.rl_garage);
        back_garage_list = (ImageView) findViewById(R.id.back_garage_list);
        btn_send = (Button) findViewById(R.id.btn_send);
        no_data_error = (LinearLayout) findViewById(R.id.no_data_error);
        lin_bottom = (RelativeLayout) findViewById(R.id.lin_bottom);
        txt_msg = (TextView) findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id), "");
        model_id = preferences.getString(getString(R.string.pref_car_model_id), "");
        brand_id = preferences.getString(getString(R.string.pref_car_brand_id), "");
        fule_id = preferences.getString(getString(R.string.pref_fule_id), "");

        getIntentData();

        try {
            JSONArray array = new JSONArray(facility_json);
            Log.e("Tag Json", array.toString());
            Log.e("Tag Json Size", "" + array.length());
            JSONArray array1 = new JSONArray();
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                Log.e("Tag Json Size", "" + object.getString("service_id"));
                service_id = object.getString("service_id");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("facility_id", object.get("service_id"));
                    array1.put(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            service_json = array1.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!service_id.equals("4") && !service_id.equals("18")) {
            model_id = "";
            brand_id = "";
            fule_id = "";
        }

        if (facility_json != null) {
            if (date == null) {
                btn_send.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "select your garage to go direct", Toast.LENGTH_LONG).show();
            }
            getBookingGarageList();
        } else if (request_id != null) {
            getGarageOffer();
        } else {
            getBookingGarageList();
        }

        back_garage_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_retry.setOnClickListener(this);
        if (garage_offer) {
            btn_send.setVisibility(View.GONE);
        }

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send_dialog();
            }
        });
    }

    public void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            String messageFor = intent.getStringExtra("messageFor");
            String result_json = intent.getStringExtra("service_json");
            if (messageFor != null) {
                if (messageFor.equals("appointment_remainder")) {
                    Intent i = new Intent(this, Act_Garage.class);
                    i.putExtra("messageFor", "appointment_remainder");
                    i.putExtra("bundle", intent.getBundleExtra("bundle"));
                    startActivity(i);
                } else if (messageFor.contains("garage_offers")) {
                    garage_offer = true;
                }
            } else if (result_json != null) {
                facility_json = intent.getStringExtra("service_json");
                problem = intent.getStringExtra("problem");
                time = intent.getStringExtra("time");
                date = intent.getStringExtra("date");
                appointment_type = intent.getStringExtra("appointment_type");
                tire_size_number = intent.getStringExtra("tire_size_number");
                tire_brand = intent.getStringExtra("tire_brand");
                oil_sae = intent.getStringExtra("oil_sae");
                oil_brand = intent.getStringExtra("oil_brand");
                rims_brand = intent.getStringExtra("rims_brand");
                rims_size = intent.getStringExtra("rims_size");
                type = intent.getStringExtra("type");
                city_id = intent.getStringExtra("city_id");
            } else {
                request_id = intent.getStringExtra("request_id");
                garage_offer = intent.getBooleanExtra("garage_offer", false);
            }
        }
    }

    // send new request dialog
    public void send_dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Garage_List.this);
        builder.setMessage(getString(R.string.send_request_for_workshop));
        builder.setPositiveButton(getString(R.string.confirm_and_send), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendRequest();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // get workshop list base on service, city and brand wise
    private void getBookingGarageList() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_garage_request", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("garage booking response", response);
                    try {
                        List<Garage_Model> garage_list_in = new ArrayList<>();
                        JSONObject mainJsonObject = new JSONObject(response);
                        if (mainJsonObject.getString("status").equals("true")) {
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject jsonObject = result.getJSONObject(i);
                                    garage_name = jsonObject.getString("garage_name");
                                    garage_banner = jsonObject.getString("garage_banner");
                                    garage_id = jsonObject.getString("garage_id");
                                    facebook_link = jsonObject.getString("facebook_link");

                                    if (jsonObject.has("ratting")) {
                                        rating = jsonObject.getString("ratting");
                                    }

                                    garage_list_in.add(new Garage_Model(garage_name, garage_banner, garage_id, facebook_link,rating));
                                }

                                if (garage_list_in.size() > 0) {

                                    for (Garage_Model garage_model : garage_list_in) {
                                        boolean isFound = false;
                                        for (Garage_Model garage_model1 : garage_list) {
                                            if (garage_model1.getGarage_id().equals(garage_model.getGarage_id())) {
                                                isFound = true;
                                                break;
                                            }
                                        }
                                        if (!isFound) {
                                            garage_list.add(garage_model);
                                        }
                                    }
                                    no_data_error.setVisibility(View.GONE);
                                    lin_bottom.setVisibility(View.VISIBLE);
                                    garageListAdapter = new GarageListAdapter(Act_Garage_List.this, garage_list);
                                    rl_garage.setAdapter(garageListAdapter);
                                    garageListAdapter.setCustomButtonListener(Act_Garage_List.this);
                                    rl_garage.setLayoutManager(new GridLayoutManager(Act_Garage_List.this, 2));
                                    rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16)); // 16px. In practice, you'll want to use getDimensionPixelSize
                                    rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
                                } else {
                                    lin_bottom.setVisibility(View.GONE);
                                    no_data_error.setVisibility(View.VISIBLE);
                                    txt_msg.setText(getString(R.string.no_service_provider_found));
                                    btn_retry.setVisibility(View.GONE);
                                }
                            } else {
                                lin_bottom.setVisibility(View.GONE);
                                no_data_error.setVisibility(View.VISIBLE);
                                txt_msg.setText(getString(R.string.no_service_provider_found));
                                btn_retry.setVisibility(View.GONE);
                            }
                        } else {
                            lin_bottom.setVisibility(View.GONE);
                            no_data_error.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.somting_wrong_please));
                            btn_retry.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        lin_bottom.setVisibility(View.GONE);
                        no_data_error.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                        btn_retry.setVisibility(View.VISIBLE);
                    }
                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    lin_bottom.setVisibility(View.GONE);
                    no_data_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                    Log.e("response", " " + error);
                    progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("user_id", user_id);
                    map.put("fule_id", fule_id);
                    map.put("model_id", model_id);
                    map.put("brand_id", brand_id);
                    map.put("service_id", service_id);
                    map.put("city_id", city_id);
                    Log.e("Tag Params", map.toString());
                    return map;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    3000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(request);
        } else {
            lin_bottom.setVisibility(View.GONE);
            no_data_error.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.internet_not_connect));
            btn_retry.setVisibility(View.GONE);
        }
    }

    private void getMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Garage_List.this);
//        builder.setMessage(getString(R.string.your_request_will_be_active_for_2_hours));
        builder.setMessage(msg);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showThankDialog("");
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onButtonClickListener(int position) {
        /*Constant.model = null;
        Intent in = new Intent(getApplicationContext(), Act_Workshop.class);
        in.putExtra("workshop_id", garage_list.get(position).getGarage_id());
        in.putExtra("workshop_name", garage_list.get(position).getGarage_name());
        in.putExtra("request_id",request_id);
        in.putExtra("garage_offer",garage_offer);
        if(garage_list.get(position).getWebsite() != null){
            in.putExtra("website", garage_list.get(position).getWebsite());
        }
        startActivity(in);*/
    }

    // send appointment request to listed workshop
    private void sendRequest() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();

            VolleyMultipartRequest request = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "send_garage_appointment",
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            Log.e("send response", new String(response.data));
                            dialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(new String(response.data));
                                if (object.getString("status").equals("1")) {
//                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                    btn_send.setVisibility(View.GONE);
                                    SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sp.edit();
                                    Log.e("Tag service_id", service_id);
                                    editor.putString(service_id, new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date()));
                                    editor.apply();
                                    getMessage(object.getString("message"));
                                } else {
                                    showThankDialog(object.getString("message"));
//                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response Error", "" + error);
                    if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                    } else if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                    }
//                    Toast.makeText(getApplicationContext(),""+error,Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
//                    map.put("appointment_time",time);
//                    map.put("appointment_date",date);
                    map.put("user_id", user_id);
                    map.put("car_problem", problem);
                    map.put("facility_id_json", "" + facility_json);
                    map.put("appointment_type", appointment_type);
                    map.put("tire_size_number", tire_size_number);
                    map.put("tire_brand", tire_brand);
                    map.put("oil_sae", oil_sae);
                    map.put("oil_brand", oil_brand);
                    map.put("battery_brand", battery_brand);
                    map.put("rims_brand", rims_brand);
                    map.put("rims_size", rims_size);
                    JSONArray json = new JSONArray();
                    for (int i = 0; i < garage_list.size(); i++) {
                        JSONObject object = new JSONObject();
                        try {
                            object.put("garage_id", garage_list.get(i).getGarage_id());
                            json.put(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("garage_id_json", "" + json);

                    Log.e("TAG user_id", user_id);
                    Log.e("TAG appointment_date", date);
                    Log.e("TAG appointment_time", time);
                    Log.e("TAG problem", problem);
                    Log.e("TAG model_id", model_id);
                    Log.e("TAG brand_id", brand_id);
                    Log.e("TAG facility_id_json", facility_json);
                    Log.e("TAG garage_id_json", "" + json);
                    Log.e("Tag appointment", appointment_type);
                    Log.e("Tag tire_brand", tire_brand);
                    Log.e("Tag tire_size_number", tire_size_number);
                    Log.e("Tag Params", map.toString());
                    return map;
                }

                @Override
                protected Map<String, DataPart> getByteData() throws AuthFailureError {
                    Map<String, DataPart> params = new HashMap<>();

                    long image_name = System.currentTimeMillis();
                    int i = 0;
                    for (Uri object : Constant.mUri) {
                        InputStream iStream = null;
                        try {
                            iStream = getContentResolver().openInputStream(object);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        byte[] inputData = new byte[0];
                        try {
                            assert iStream != null;
                            inputData = getBytes(iStream);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        params.put("appointment_images[" + (i++) + "]", new DataPart(image_name + ".png", inputData, "image/*"));
                        Log.e("Tag Appointment Images", " " + params);
                        Log.e("Tag Appointment Images", " " + image_name + ".png " + Arrays.toString(inputData));

                    }
                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);
            request.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 100000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 100000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    @SuppressLint("SetTextI18n")
    public void showThankDialog(String msg) {
        try {
            final Dialog dialog = new Dialog(this);
            Objects.requireNonNull(dialog.getWindow()).clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND); // for dialog shadow
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
            dialog.setContentView(R.layout.popup_thank);
            dialog.setCancelable(false);
            // set values for custom dialog components - text, image and button
            TextView tvMessageTwo = (TextView) dialog.findViewById(R.id.tvMessageTwo);
            if (msg.equals("")) {
                tvMessageTwo.setText(getString(R.string.thayu_you_for_request));
            } else {
                tvMessageTwo.setText(msg);
            }
            //  final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
            TextView tvOK = (TextView) dialog.findViewById(R.id.tvOk);

            dialog.show();

            tvOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    if (new Act_Booking_appointment().instance != null) {
                        new Act_Booking_appointment().instance.finish();
                    }
                    if (new Act_Booking_service_select().instance != null) {
                        new Act_Booking_service_select().instance.finish();
                    }
                    Intent i = new Intent(getApplicationContext(), Navigation_Activity.class);
                    startActivity(i);
                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // get appointment request workshop list base on service, city and brand wise
    private void getGarageOffer() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "user_garage_appointment_list", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("notification_list", " " + response);
                    try {

                        JSONObject mainJsonObject = new JSONObject(response);
                        garage_list = new ArrayList<>();
                        if (mainJsonObject.getString("status").equals("1")) {
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject jsonObject = result.getJSONObject(i);
                                    if (request_id.equals(jsonObject.getString("mobile_request_id"))) {

                                        garage_name = jsonObject.getString("garage_name");
                                        garage_banner = jsonObject.getString("garage_banner");
                                        garage_id = jsonObject.getString("garage_id");
                                        garage_list.add(new Garage_Model(garage_name, garage_banner, garage_id, "",""));
                                    }
                                }

                                if (garage_list.size() > 0) {

                                    garageListAdapter = new GarageListAdapter(Act_Garage_List.this, garage_list);
                                    rl_garage.setAdapter(garageListAdapter);
                                    garageListAdapter.setCustomButtonListener(Act_Garage_List.this);
                                    rl_garage.setLayoutManager(new GridLayoutManager(Act_Garage_List.this, 2));
                                    rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16)); // 16px. In practice, you'll want to use getDimensionPixelSize
                                    rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
                                    lin_bottom.setVisibility(View.VISIBLE);
                                    no_data_error.setVisibility(View.GONE);
                                } else {
                                    lin_bottom.setVisibility(View.VISIBLE);
                                    no_data_error.setVisibility(View.GONE);
                                    txt_msg.setText(getString(R.string.no_data_found));
                                    btn_retry.setVisibility(View.GONE);
                                }
                            } else {
                                lin_bottom.setVisibility(View.VISIBLE);
                                no_data_error.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_data_found));
                                btn_retry.setVisibility(View.GONE);
                            }
                        } else {
                            lin_bottom.setVisibility(View.VISIBLE);
                            no_data_error.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.somting_wrong_please));
                            btn_retry.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        lin_bottom.setVisibility(View.VISIBLE);
                        no_data_error.setVisibility(View.GONE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                        btn_retry.setVisibility(View.VISIBLE);
                    }

                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    lin_bottom.setVisibility(View.VISIBLE);
                    no_data_error.setVisibility(View.GONE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                    Log.e("Tag ", "" + error);
                    dialog.dismiss();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            lin_bottom.setVisibility(View.VISIBLE);
            no_data_error.setVisibility(View.GONE);
            txt_msg.setText(getString(R.string.internet_not_connect));
            btn_retry.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry) {
            if (facility_json != null) {
                if (date == null) {
                    btn_send.setVisibility(View.GONE);
                }
                getBookingGarageList();
            } else if (request_id != null) {
                getGarageOffer();
            } else {
                btn_send.setVisibility(View.GONE);
                getBookingGarageList();
            }
        }
    }

}
