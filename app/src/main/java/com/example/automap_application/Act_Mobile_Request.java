package com.example.automap_application;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Examples.PlaceMarks;
import com.example.automap_application.Model.Sub_Service_Model;
import com.example.automap_application.utils.AppUtils;
import com.example.automap_application.utils.CustomMapFragment;
import com.example.automap_application.utils.LatLngInterpolator;
import com.example.automap_application.utils.MarkerAnimation;
import com.example.automap_application.view.WorkaroundMapFragment;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Act_Mobile_Request extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    ImageView back;
    LinearLayout lin_services;
    List<Sub_Service_Model> sub_services_models = new ArrayList<>();
    View no_date_error;
    Button btn_retry;
    TextView txt_msg;
    RecyclerView rl_service;
    AdapterService adapterService;

    CustomMapFragment map;
    FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;
    LocationCallback mLocationCallback;
    Location mLastLocation;
    String location_address;
    LocationManager locationManager;
    private final Handler handler = new Handler();

    private GoogleMap mMap;
    List<PlaceMarks> placeMarks1 = new ArrayList<>();
    List<PlaceMarks> placeMarksCurrent = null;
    List<PlaceMarks> placeMarksLast = new ArrayList<>();
    HashMap<String, String> place_image = new HashMap<>();
    HashMap<String, String> place_name = new HashMap<>();
    List<PlaceMarks> placeMarks;
    JSONArray array = new JSONArray();
    List<String> id_list = new ArrayList<>();

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @SuppressLint("StaticFieldLeak")
    public Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__request_mobile);

        instance = this;

        back = findViewById(R.id.back_request_mobile);
        lin_services = findViewById(R.id.lin_services);
        no_date_error = findViewById(R.id.no_date_error);
        btn_retry = findViewById(R.id.btn_retry);
        txt_msg = findViewById(R.id.txt_msg);
        rl_service = findViewById(R.id.rl_service);

        back.setOnClickListener(this);
        btn_retry.setOnClickListener(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //  Log.e("Tag Provider", ""+locationManager.getAllProviders());
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlertToUser();
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        map = (CustomMapFragment) getFragmentManager().findFragmentById(R.id.map);
        map.getMapAsync(Act_Mobile_Request.this);

        handler.postDelayed(sendUpdatesToUI, 500);

        get_garage_facility();

    }

    // get mobile request service list and set list view
    private void get_garage_facility() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "getServiceData", new Response.Listener<String>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(String response) {
                    Log.e("garage_facility", " " + response);
                    try {

                        JSONObject obj = new JSONObject(response);
                        if (obj.getString("status").equals("true")) {
                            JSONArray jsonArray = obj.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String sub_service_id = jsonObject.optString("subservice_id", " ");
                                String sub_service_name = jsonObject.optString("subservice_name", " ");
                                String service_image = jsonObject.optString("service_image", " ");
                                String service_id = jsonObject.optString("service_id", " ");
                                String service_name = jsonObject.optString("service_name", " ");
                                String service_provider = jsonObject.optString("service_provider", " ");

                                sub_services_models.add(new Sub_Service_Model(service_id,service_name,sub_service_id,sub_service_name,service_image,service_provider));
                            }

                            if (sub_services_models != null && sub_services_models.size() > 0) {
                                rl_service.setVisibility(View.VISIBLE);
                                no_date_error.setVisibility(View.GONE);
                                adapterService = new AdapterService(sub_services_models, getApplicationContext());
                                rl_service.setLayoutManager(new GridLayoutManager(Act_Mobile_Request.this, 3));
                                rl_service.setAdapter(adapterService);
                            } else {
                                rl_service.setVisibility(View.GONE);
                                no_date_error.setVisibility(View.VISIBLE);
                                btn_retry.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_data_found));
                            }
                        } else {
                            rl_service.setVisibility(View.GONE);
                            no_date_error.setVisibility(View.VISIBLE);
                            btn_retry.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.no_data_found));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        rl_service.setVisibility(View.GONE);
                        no_date_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                    }
                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Act_Garage_List.this, "my error :" + error, Toast.LENGTH_LONG).show();
                    Log.e("My error", "" + error);
                    rl_service.setVisibility(View.GONE);
                    no_date_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("service_provider", getString(R.string.parametr_request_auto_mobile_service));
                    Log.e("Tag params", map.toString());
                    return map;

                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    6000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(request);
        } else {

            no_date_error.setVisibility(View.VISIBLE);
            rl_service.setVisibility(View.GONE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.internet_not_connect));
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            onBackPressed();
        } else if (view == btn_retry) {
            get_garage_facility();
        }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                callGPSSettingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        /*alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });*/
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(2000); // two minute interval
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                List<Location> locationList = locationResult.getLocations();
                if (locationList.size() > 0) {
                    //  The last location in the list is the newest
                    Location location = locationList.get(locationList.size() - 1);
                    Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());

                    if (mLastLocation == null) {

                        mLastLocation = location;
                        location_address = AppUtils.getCompleteAddressString(Act_Mobile_Request.this, location.getLatitude(),location.getLongitude());
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                                location.getLongitude()), 15));
                    }
                }
            }
        };

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mMap.setMyLocationEnabled(true);
            } else {
                //  Request Location Permission
                checkLocationPermission();
            }
        } else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mMap.setMyLocationEnabled(true);
        }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                    mLastLocation = location;
                    location_address = AppUtils.getCompleteAddressString(Act_Mobile_Request.this, location.getLatitude(),location.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                            location.getLongitude()), 15));

                    /* Marker marker = mMap.addMarker(new MarkerOptions()
                               .position(carLoc)
                               .title("Test 1")
                               .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                       carLoc = new LatLng(placeMarks.get(1).getCoordinates().get(0),
                               placeMarks.get(1).getCoordinates().get(1)); */
                }
            }
        });

//        InfoWindowAdapter infoWindowAdapter = new InfoWindowAdapter(Act_Tow_track.this);
//        mMap.setInfoWindowAdapter(infoWindowAdapter);

    }

    // update map marker
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            Log.e("Tag", "Test");
            getGarageList();
            handler.postDelayed(this, 10000); // broadcast in every 10 seconds
        }
    };

    // change position marker of workshop
    private void broadcastUpdateInfo() {
//        Log.d("Tag", "entered DisplayLoggingInfo");

        //increment position(lat,lng) by 0.000005
        //changePositionBy = changePositionBy + 0.00005;

        placeMarks1 = new ArrayList<>();

        //Here I have incremented lat and long by 0.00005 in every 10 seconds. In real scenario you
        //should fetch current location data from your web service.

        for (int i = 0; i < placeMarks.size(); i++) {
            List<Double> doubles = new ArrayList<>();
            String name = "", image = "";
            for (int ii = 0; ii < placeMarks.get(i).getCoordinates().size(); ii++) {
                doubles.add(placeMarks.get(i).getCoordinates().get(ii));
                name = placeMarks.get(i).getName();
                image = placeMarks.get(i).getImage();
            }
            placeMarks1.add(new PlaceMarks(doubles, name, image));
        }

        addMarker();
        placeMarksCurrent = placeMarks1;

        //        if (placeMarks1 != null && placeMarks1.size()>0){
//        }
    }

    List<Marker> marker = new ArrayList<>();

    // add marker on map workshop wise
    private void addMarker() {

//        mMap.clear();

        String lat = Double.toString(placeMarks.get(0).getCoordinates().get(0));
        Log.d("TAG", lat);

        LatLng carLoc = null, newCarLoc = null;

        //Initially add markers for all cars to the map
        place_name = new HashMap<>();
        place_image = new HashMap<>();
        if (placeMarksCurrent == null) {
            placeMarksLast = placeMarks1;
            Log.e("TAG if", "" + placeMarks1.size());
            for (PlaceMarks car : placeMarks1) {
                carLoc = new LatLng(car.getCoordinates().get(0), car.getCoordinates().get(1));

                marker.add(mMap.addMarker(new MarkerOptions()
                        .position(carLoc)
                        .title(car.getName())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.car))));


                //                        .icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_pin)));
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
            }
        } else {
            // then update each car's position by moving the marker smoothly from previous
            // location to the current location
//            Log.e("TAG else", "" + placeMarksCurrent.size());
            for (int i = 0; i < placeMarksCurrent.size(); i++) {
                carLoc = new LatLng(placeMarksLast.get(i).getCoordinates().get(0), placeMarksLast.get(i).getCoordinates().get(1));
                newCarLoc = new LatLng(placeMarksCurrent.get(i).getCoordinates().get(0), placeMarksCurrent.get(i).getCoordinates().get(1));

                if (marker.get(i) == null) {
                    marker.add(mMap.addMarker(new MarkerOptions()
                            .position(carLoc)
                            .title(placeMarksCurrent.get(i).getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car))));


                } else {
                    MarkerAnimation.animateMarkerToGB(marker.get(i), newCarLoc, new LatLngInterpolator.Spherical());
                }
//                animateMarker(i, carLoc, newCarLoc, false);
            }

            if (marker != null && marker.size() > 0) {
                for (int m = 0; m < marker.size(); m++) {
                    place_name.put(marker.get(m).getId(), placeMarksCurrent.get(m).getName());
                    place_image.put(marker.get(m).getId(), placeMarksCurrent.get(m).getImage());
                }
            }

            //set the the last known location of each car to the current location of each car
            //so we will get the updates of car's location then we will move the marker from
            //last known location to the current location again.
            placeMarksLast = placeMarksCurrent;
        }
    }

    // get workshop base on mobile request service
    private void getGarageList() {

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_all_garage", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("garage response", response);
                try {
                    String name = "", id = "", profile_img = "", latitude, longitude;
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.getString("status").equals("true")) {
                        JSONObject obj = mainJsonObject.getJSONObject("result");
                        JSONArray result = obj.getJSONArray("garage");
                        if (result.length() > 0) {

                            placeMarks = new ArrayList<>();
                            id_list = new ArrayList<>();
                            for (int i = 0; i < result.length(); i++) {
                                List<Double> doubles = new ArrayList<>();
                                JSONObject jsonObject = result.getJSONObject(i);
                                id = jsonObject.getString("garage_id");
                                name = jsonObject.getString("garage_name");
                                profile_img = jsonObject.getString("owner_profile_img");
                                latitude = jsonObject.getString("live_latitude" );
                                longitude = jsonObject.getString("live_longitude");
                                if (latitude.equals("") || longitude.equals("")){
                                    continue;
                                }
                                doubles.add(Double.parseDouble(latitude));
                                doubles.add(Double.parseDouble(longitude));

                                if (jsonObject.has("provider_type_id") && jsonObject.getString("provider_type_id").equals("34")
                                        || jsonObject.has("service_id") && jsonObject.getString("service_id").equals("18")){

                                    placeMarks.add(new PlaceMarks(doubles, name, profile_img));
                                    id_list.add(id);
                                } else if (!jsonObject.has("provider_type_id")){
                                    placeMarks.add(new PlaceMarks(doubles, name, profile_img));
                                    id_list.add(id);
                                }

                            }
                            if (placeMarks != null && placeMarks.size() > 0) {
                                broadcastUpdateInfo();
                            }

                            if (array != null && array.length() < 1) {

                                Log.e("Tag Array", "" + id_list.size());
                                for (int i = 0; i < id_list.size(); i++) {
                                    JSONObject object = new JSONObject();
                                    object.put("id", id_list.get(i));
                                    array.put(object);
                                }
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("response", " " + error);
            }
        }) /*{
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", "210");
                map.put("service_id", "10");
                Log.e("Tag Params", map.toString());
                return map;
            }
        }*/;

        request.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }

    // service adapter
    public class AdapterService extends RecyclerView.Adapter<AdapterService.MyViewHolder> {

        List<Sub_Service_Model> services_models;
        Context context;

        AdapterService(List<Sub_Service_Model> services_models, Context context) {
            this.services_models = services_models;
            this.context = context;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_mobile_service, viewGroup, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int i) {

            holder.service_name.setText(Html.fromHtml(services_models.get(i).getSub_service_name()));
            if (services_models.get(i).getService_img() != null) {
                Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.mipmap.ic_launcher).into(holder.service_image);
            } else {
                holder.service_image.setImageResource(R.mipmap.ic_launcher);
            }

            holder.lin_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
                    String sendDate = sp.getString(getString(R.string.pref_isSend), "");
                    @SuppressLint("SimpleDateFormat")
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
                    Log.e("Tag Date", sendDate + "\n" + date);

                    assert sendDate != null;
                    if (sendDate.equalsIgnoreCase(date)) {
                        Toast.makeText(getApplicationContext(), getString(R.string.you_can_not_use_the_sertvice_moe_than), Toast.LENGTH_LONG).show();
                    } else {
                        JSONArray array = new JSONArray();
                        for (int j = 0; j < 1; j++) {
                            JSONObject obj = new JSONObject();
                            try {
                                obj.put("service_id", services_models.get(i).getService_id());
                                obj.put("service_name", services_models.get(i).getSub_service_name());
                                array.put(obj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        Intent intent = new Intent(getApplicationContext(), Act_Mobile_Request_Booking.class);
                        intent.putExtra("service_json", array.toString());
                        intent.putExtra("sub_service_id", services_models.get(i).getSub_service_id());
                        intent.putExtra("sub_service_name", services_models.get(i).getSub_service_name());
                        intent.putExtra("type", "Special Service");
                        intent.putExtra("cur_lat", ""+mLastLocation.getLatitude());
                        intent.putExtra("cur_lng", ""+mLastLocation.getLongitude());
                        intent.putExtra("loc_address", location_address);
//                        if (services_models.get(i).getService_type().contains("Special Service")) {
//                        } else {
//                            intent.putExtra("type", "General Service");
//                        }
                        startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return services_models.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView service_name;
            private ImageView service_image;
            LinearLayout lin_main;

            MyViewHolder(@NonNull View itemView) {
                super(itemView);

                service_image = itemView.findViewById(R.id.service_image);
                lin_main = itemView.findViewById(R.id.lin_main);
                service_name = itemView.findViewById(R.id.service_name);
            }
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(Act_Mobile_Request.this,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        }).create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onDestroy() {
        if (handler != null && sendUpdatesToUI != null){
            handler.removeCallbacks(sendUpdatesToUI);
        }
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        if (handler != null && sendUpdatesToUI != null){
            handler.removeCallbacks(sendUpdatesToUI);
        }
        super.onStop();
    }
}
