package com.example.automap_application;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Examples.PlaceMarks;
import com.example.automap_application.utils.AppUtils;
import com.example.automap_application.utils.CustomMapFragment;
import com.example.automap_application.utils.LatLngInterpolator;
import com.example.automap_application.utils.MarkerAnimation;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.example.automap_application.Constant.Constant.i;

public class Act_Tow_track extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMapClickListener {

    View lin_error;
    TextView txt_msg;
    Button btn_retry;
    private GoogleMap mMap;
    CustomMapFragment customMapFragment;
    List<PlaceMarks> placeMarks;
    private final Handler handler = new Handler();
    List<PlaceMarks> placeMarks1 = new ArrayList<>();
    List<PlaceMarks> placeMarksCurrent = null;
    List<PlaceMarks> placeMarksLast = new ArrayList<>();
    HashMap<String, String> place_image = new HashMap<>();
    HashMap<String, String> place_name = new HashMap<>();
    Toolbar toolbar;
    private GoogleApiClient mGoogleApiClient;
    // TextView txt_destination;
    TextView txt_send_request;
    FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;
    LocationCallback mLocationCallback;
    Location mLastLocation;
    LocationManager locationManager;
    AutocompleteSupportFragment place_autocomplete_fragment;

    LinearLayout lin_fragment;
    TextView txt_select_location;
    double final_destination_lat, final_destination_lng, cur_lat, cur_log;
    private int PLACE_PICKER_REQUEST = 1;

    JSONArray array = new JSONArray();
    List<String> towtrack_id_list = new ArrayList<>();

    SharedPreferences preferences;
    String user_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__tow_track);

        lin_error = findViewById(R.id.lin_error);
        btn_retry = findViewById(R.id.btn_retry);
        txt_msg = findViewById(R.id.txt_msg);
        toolbar = findViewById(R.id.toolbar);
        //  txt_destination = findViewById(R.id.txt_destination);
        txt_send_request = findViewById(R.id.txt_send_request);
        lin_fragment = findViewById(R.id.lin_fragment);
        txt_select_location = findViewById(R.id.txt_select_location);

        btn_retry.setOnClickListener(this);
        txt_select_location.setOnClickListener(this);
        txt_send_request.setOnClickListener(this);
        //  getPlaceMarker();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //  Log.e("Tag Provider", ""+locationManager.getAllProviders());
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlertToUser();
        }

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id), "");

        SearchPlace();
        ActionBar();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        customMapFragment = (CustomMapFragment) getFragmentManager().findFragmentById(R.id.map);
        customMapFragment.getMapAsync(Act_Tow_track.this);

        handler.postDelayed(sendUpdatesToUI, 500);

    }

    public void ActionBar() {

//        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.request_tow_truck));
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    // auto complete search place dialog
    public void SearchPlace() {
        if (!Places.isInitialized())
            Places.initialize(getApplicationContext(), getString(R.string.google_api_key));

        place_autocomplete_fragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        place_autocomplete_fragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS));
        if (place_autocomplete_fragment != null) {
            if (place_autocomplete_fragment.getView() != null)
                place_autocomplete_fragment.getView().setBackgroundColor(Color.WHITE);
//                ((EditText)place_autocomplete_fragment.getView().findViewById(R.id.place_autocomplete_search_input)).setBackgroundColor(Color.WHITE);
//                ((EditText)place_autocomplete_fragment.getView().findViewById(R.id.place_autocomplete_search_input)).setSingleLine(false);
//                ((EditText)place_autocomplete_fragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(14);
            place_autocomplete_fragment.setText(getString(R.string.where_too));
            place_autocomplete_fragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(@NonNull Place place) {
                    if (markers == null) {
                        markers = mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title("" + place.getAddress()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                    } else {
                        if (markers.isInfoWindowShown())
                            markers.hideInfoWindow();
                        markers.setPosition(place.getLatLng());
                        markers.setTitle("" + place.getAddress());
                    }
                }

                @Override
                public void onError(@NonNull Status status) {

                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        int top = lin_fragment.getMeasuredHeight();
        int top1 = lin_fragment.getTop();
        int top2 = lin_fragment.getPaddingTop();
        int top3 = lin_fragment.getHeight();
        mMap.setPadding(0, top, 0, 0);
        Log.e("tag Height", "" + top + "\n" + top1 + "\n" + top2 + "\n" + top3);
//        mMap.setPadding();
//        addMarker();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(2000); // two minute interval
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                List<Location> locationList = locationResult.getLocations();
                if (locationList.size() > 0) {
                    //  The last location in the list is the newest
                    Location location = locationList.get(locationList.size() - 1);
                    Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());

                    if (mLastLocation == null) {

                        mLastLocation = location;
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                                location.getLongitude()), 15));
                    }
                }
            }
        };

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mMap.setMyLocationEnabled(true);
            } else {
                //  Request Location Permission
                checkLocationPermission();
            }
        } else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mMap.setMyLocationEnabled(true);
        }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                            location.getLongitude()), 15));

                    /* Marker marker = mMap.addMarker(new MarkerOptions()
                               .position(carLoc)
                               .title("Test 1")
                               .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                       carLoc = new LatLng(placeMarks.get(1).getCoordinates().get(0),
                               placeMarks.get(1).getCoordinates().get(1)); */
                }
            }
        });

        mMap.setOnMapClickListener(this);
//        InfoWindowAdapter infoWindowAdapter = new InfoWindowAdapter(Act_Tow_track.this);
//        mMap.setInfoWindowAdapter(infoWindowAdapter);

    }

    public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        Context context;

        public InfoWindowAdapter(Context context) {
            this.context = context;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.adapter_info_windows, null);

            RoundedImageView image = (RoundedImageView) v.findViewById(R.id.info_img);
            TextView title = (TextView) v.findViewById(R.id.title);

            Log.e("Tag name Na", "" + place_name.get(marker.getId()));
            Log.e("Tag id", "" + marker.getId());

            if (place_image.get(marker.getId()) == null) {
                image.setVisibility(View.GONE);
            } else {
                image.setVisibility(View.VISIBLE);
            }

            Glide.with(context).asBitmap().load(place_image.get(marker.getId())).thumbnail(0.01f).placeholder(R.drawable.tow_track)
                    .into(new BitmapImageViewTarget(image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            image.setImageBitmap(resource);
                            getInfoContents(marker);
                        }
                    });
            title.setText("" + marker.getTitle());

            return v;
        }
    }

    // update map marker
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            Log.e("Tag", "Test");
            getTowTrackList();
            handler.postDelayed(this, 10000); // broadcast in every 10 seconds
        }
    };

    // towtruck marker add
    private void broadcastUpdateInfo() {
//        Log.d("Tag", "entered DisplayLoggingInfo");

        //increment position(lat,lng) by 0.000005
        //changePositionBy = changePositionBy + 0.00005;

        placeMarks1 = new ArrayList<>();

        //Here I have incremented lat and long by 0.00005 in every 10 seconds. In real scenario you
        //should fetch current location data from your web service.

        for (int i = 0; i < placeMarks.size(); i++) {
            List<Double> doubles = new ArrayList<>();
            String name = "", image = "";
            for (int ii = 0; ii < placeMarks.get(i).getCoordinates().size(); ii++) {
                doubles.add(placeMarks.get(i).getCoordinates().get(ii));
                name = placeMarks.get(i).getName();
                image = placeMarks.get(i).getImage();
            }
            placeMarks1.add(new PlaceMarks(doubles, name, image));
        }

        addMarker();
        placeMarksCurrent = placeMarks1;

        //        if (placeMarks1 != null && placeMarks1.size()>0){
//        }
    }

    // get towtruck list on set each marker
    private void getTowTrackList() {

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_towtrack_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("garage booking response", response);
                try {
                    String towtrack_name = "", towtrack_id = "", towtrack_profile_img = "", latitude, longitude;
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.getString("status").equals("true")) {
                        JSONArray result = mainJsonObject.getJSONArray("result");
                        if (result.length() > 0) {
                            lin_error.setVisibility(View.GONE);
                            placeMarks = new ArrayList<>();
                            towtrack_id_list = new ArrayList<>();
                            for (int i = 0; i < result.length(); i++) {
                                List<Double> doubles = new ArrayList<>();
                                JSONObject jsonObject = result.getJSONObject(i);
                                towtrack_id = jsonObject.getString("towtrack_id");
                                towtrack_name = jsonObject.getString("towtrack_name");
                                towtrack_profile_img = jsonObject.getString("towtrack_profile_img");
                                latitude = jsonObject.getString("latitude" );
                                longitude = jsonObject.getString("longitude");
                                if (latitude.equals("") || longitude.equals("")){
                                    continue;
                                }
                                doubles.add(Double.parseDouble(latitude));
                                doubles.add(Double.parseDouble(longitude));
                                placeMarks.add(new PlaceMarks(doubles, towtrack_name, towtrack_profile_img));
                                towtrack_id_list.add(towtrack_id);
                            }
                            if (placeMarks != null && placeMarks.size() > 0) {
                                broadcastUpdateInfo();
                            }

                            if (array != null && array.length() < 1) {

                                Log.e("Tag Array", "" + towtrack_id_list.size());
                                for (int i = 0; i < towtrack_id_list.size(); i++) {
                                    JSONObject object = new JSONObject();
                                    object.put("towtrack_id", towtrack_id_list.get(i));
                                    array.put(object);
                                }
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("response", " " + error);
            }
        }) /*{
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", "210");
                map.put("service_id", "10");
                Log.e("Tag Params", map.toString());
                return map;

            }
        }*/;

        request.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }

    // add marker on map
    private void addMarker() {

//        mMap.clear();
        String lat = Double.toString(placeMarks.get(0).getCoordinates().get(0));
        Log.d("TAG", lat);

        LatLng carLoc = null, newCarLoc = null;

        //Initially add markers for all cars to the map
        place_name = new HashMap<>();
        place_image = new HashMap<>();
        if (placeMarksCurrent == null) {
            placeMarksLast = placeMarks1;
            Log.e("TAG if", "" + placeMarks1.size());
            for (PlaceMarks car : placeMarks1) {
                carLoc = new LatLng(car.getCoordinates().get(0), car.getCoordinates().get(1));

                marker.add(mMap.addMarker(new MarkerOptions()
                        .position(carLoc)
                        .title(car.getName())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.transport_32))));


//                        .icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_pin)));
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
            }
        } else {
            // then update each car's position by moving the marker smoothly from previous
            // location to the current location
            Log.e("TAG else", "" + placeMarksCurrent.size());
            for (int i = 0; i < placeMarksCurrent.size(); i++) {
                carLoc = new LatLng(placeMarksLast.get(i).getCoordinates().get(0), placeMarksLast.get(i).getCoordinates().get(1));
                newCarLoc = new LatLng(placeMarksCurrent.get(i).getCoordinates().get(0), placeMarksCurrent.get(i).getCoordinates().get(1));

                if (marker.get(i) == null) {
                    marker.add(mMap.addMarker(new MarkerOptions()
                            .position(carLoc)
                            .title(placeMarksCurrent.get(i).getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.transport_32))));


                } else {
                    MarkerAnimation.animateMarkerToGB(marker.get(i), newCarLoc, new LatLngInterpolator.Spherical());
                }
//                animateMarker(i, carLoc, newCarLoc, false);
            }

            if (marker != null && marker.size() > 0) {
                for (int m = 0; m < marker.size(); m++) {
                    place_name.put(marker.get(m).getId(), placeMarksCurrent.get(m).getName());
                    place_image.put(marker.get(m).getId(), placeMarksCurrent.get(m).getImage());
                }
            }

            //set the the last known location of each car to the current location of each car
            //so we will get the updates of car's location then we will move the marker from
            //last known location to the current location again.
            placeMarksLast = placeMarksCurrent;
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_pin);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, 40, 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                callGPSSettingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    // This methods is used to move the marker of each car smoothly when there are any updates of their position
    List<Marker> marker = new ArrayList<>();

    Handler handlerAnim = null;
    Runnable runnableAnim = null;

    public void animateMarker(final int position, final LatLng startPosition, final LatLng toPosition, final boolean hideMarker) {

        //                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
        //                    .icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_pin)));
        if (marker.get(position) == null) {
            marker.set(i, mMap.addMarker(new MarkerOptions()
                    .position(startPosition)
                    .title(placeMarksCurrent.get(i).getName())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.transport_32))));
        }

        handlerAnim = new Handler();
        final long start = SystemClock.uptimeMillis();

        final long duration = 1000;
        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                runnableAnim = this;
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * toPosition.longitude + (1 - t) * startPosition.longitude;
                double lat = t * toPosition.latitude + (1 - t) * startPosition.latitude;

                marker.get(position).setPosition(new LatLng(lat, lng));

//                Log.e("Tag Timing", "" + t);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.get(position).setVisible(false);
                    } else {
                        marker.get(position).setVisible(true);
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry) {
            //  getPlaceMarker();
        } else if (v == txt_send_request) {

            if (markers != null) {
                final_destination_lat = markers.getPosition().latitude;
                final_destination_lng = markers.getPosition().longitude;
            }

            if (markers == null) {
                Toast.makeText(getApplicationContext(), getString(R.string.where_too), Toast.LENGTH_LONG).show();
            } else if (cur_markers == null) {
                Toast.makeText(getApplicationContext(), getString(R.string.select_your_location), Toast.LENGTH_LONG).show();
            } else if (array != null && array.length() < 1) {
                Toast.makeText(getApplicationContext(), getString(R.string.towtruck_no_foun_this_location), Toast.LENGTH_LONG).show();
            } else {
//                Toast.makeText(getApplicationContext(), "Coming Soon", Toast.LENGTH_LONG).show();
//                ShowMessage();
                SendRequest();
            }

        } else if (v == txt_select_location) {
            Intent intent = new Intent(getApplicationContext(), LocationPickerActivity.class);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);
        }
    }

    // send tow trcuk request
    public void SendRequest() {

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "send_towtrack_request", response -> {
                Log.e("Tag Response", response);

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("true")) {
                        ShowMessage(object.getString("message"));
                    } else {
                        showThankDialog(object.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();

            }, error -> {
                Log.e("Tag Error", error.toString());
                dialog.dismiss();
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", user_id);
                    map.put("towtrack_id_json", "" + array);
                    map.put("user_location", txt_select_location.getText().toString());
//                    map.put("final_destination", markers.getTitle().toString());
                    map.put("user_latitude", "" + cur_lat);
                    map.put("user_longitude", "" + cur_log);
                    map.put("towtrack_latitude", "" + final_destination_lat);
                    map.put("towtrack_longitude", "" + final_destination_lng);
//                    map.put("final_lng", "" + final_destination_lng);
//                    map.put("final_lat", "" + final_destination_lat);
                    Log.e("Tag Params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    public void ShowMessage(String msg) {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(Act_Tow_track.this);
//        builder.setMessage(getString(R.string.your_request_will_be_active_for_15_minute));
        builder.setMessage(msg);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showThankDialog("");
                dialog.dismiss();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showThankDialog(String msg) {
        try {
            final Dialog dialog = new Dialog(this);
            Objects.requireNonNull(dialog.getWindow()).clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND); // for dialog shadow
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
            dialog.setContentView(R.layout.popup_thank);
            dialog.setCancelable(false);
            // set values for custom dialog components - text, image and button
            TextView tvMessageTwo = (TextView) dialog.findViewById(R.id.tvMessageTwo);
            if (msg.equals("")) {
                tvMessageTwo.setText(msg);
            } else {
                tvMessageTwo.setText(R.string.thank_you_for_request_tow_truck);
            }
            //  final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
            TextView tvOK = (TextView) dialog.findViewById(R.id.tvOk);

            dialog.show();

            tvOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    Intent i = new Intent(getApplicationContext(), Navigation_Activity.class);
                    startActivity(i);
                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double selectedLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                    double selectedLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);

                    txt_select_location.setSelected(true);
                    txt_select_location.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                    txt_select_location.setMarqueeRepeatLimit(-1);
                    cur_lat = selectedLatitude;
                    cur_log = selectedLongitude;

                    if (cur_markers == null) {
                        cur_markers = mMap.addMarker(new MarkerOptions().position(new LatLng(selectedLatitude, selectedLongitude)).title("" + address).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                    } else {
                        if (cur_markers.isInfoWindowShown())
                            cur_markers.hideInfoWindow();
                        cur_markers.setTitle("" + address);
                        cur_markers.setPosition(new LatLng(selectedLatitude, selectedLongitude));
                    }

                    txt_select_location.setText(address);
                    Log.e("Tag Address", address);
                    Log.e("TagLat Log", " " + selectedLatitude + "  " + selectedLongitude);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        handler.removeCallbacks(sendUpdatesToUI);
        if (handlerAnim != null && runnableAnim != null) {
            handlerAnim.removeCallbacks(runnableAnim);
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(sendUpdatesToUI, 10000);
    }

    Marker markers = null;
    Marker cur_markers = null;

    @Override
    public void onMapClick(LatLng latLng) {

        Geocoder geocoder = new Geocoder(Act_Tow_track.this, Locale.getDefault());
        List<Address> addresses = new ArrayList<>();
        String Area = "";
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null && addresses.size() > 0) {

            Area = addresses.get(0).getAddressLine(0);
        }

        if (markers == null) {
            markers = mMap.addMarker(new MarkerOptions().position(latLng).title("" + Area).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        } else {
            if (markers.isInfoWindowShown())
                markers.hideInfoWindow();
            markers.setTitle("" + Area);
            markers.setPosition(latLng);
        }

        place_autocomplete_fragment.setText(Area);
        markers.setDraggable(true);
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                Log.e("Tag address drag", marker.getTitle());
            }

            @Override
            public void onMarkerDrag(Marker marker) {

                if (marker.getId().equals(markers.getId())) {
                    Log.e("Tag Title", marker.getPosition().toString());

                    LatLng lng = marker.getPosition();
                    Geocoder geocoder = new Geocoder(Act_Tow_track.this, Locale.getDefault());
                    List<Address> addresses = new ArrayList<>();
                    String Area = "";
                    Log.e("Tag Title", "" + marker.getRotation());
                    try {
                        addresses = geocoder.getFromLocation(lng.latitude, lng.longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("Tag Title", e.toString());
                    }
                    if (addresses != null && addresses.size() > 0) {

                        Area = addresses.get(0).getAddressLine(0);
                    }
                    Log.e("Tag address drag", Area);
                    markers.setTitle(Area);
                    place_autocomplete_fragment.setText(Area);
                }

                Log.e("Tag Tile", markers.getTitle());
                Log.e("Tag id 1", marker.getId());
                Log.e("Tag id 2", markers.getId());
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Log.e("Tag address drag", marker.getTitle());
            }
        });
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(Act_Tow_track.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        }).create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
