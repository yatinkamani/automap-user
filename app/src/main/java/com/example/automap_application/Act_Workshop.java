package com.example.automap_application;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.VolleyMultipartRequest;
import com.example.automap_application.adapter.SlidingImage_Adapter;
import com.example.automap_application.extra.WorkaroundMapFragment;
import com.example.automap_application.utils.AppUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Workshop extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES = {R.drawable.car1, R.drawable.car2, R.drawable.car3, R.drawable.car4, R.drawable.car5};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 20000; /* 20 sec */

    private LocationManager locationManager;
    private LatLng latLng;
    private boolean isPermission;
    private GoogleMap mMap;
    ScrollView mScrollView;
    EditText editText;
    SharedPreferences prefs;
    ImageView back;
    TextView text_garage_name, txt_address, txt_phone_no;
    ImageView img_banner;
    String workshop_id = "";
    String facebook_link = "";

    String workshop_banner = "";
    String workshop_name = "";
    String contact_no = "";
    String address = "";
    LinearLayout ll_call;
    LinearLayout ll_web;
    LinearLayout go_to_garage, need_now_service;
    TextView txt_distance;
    View no_data_error;
    Button btn_retry;
    TextView txt_msg;
    Toolbar toolbar;

    String  appointment_type = "" , come_now = "", amount = "", oil_sae = "", oil_brand = "", leather_seat = "",
            lat = "",log = "",location = "", change_filter = "", garage_id_json = "", facility_id_json = "", request_date = "",
            request_time = "", model_id = "", brand_id = "", user_id = "";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
//        applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            //Use you logic to update overrideConfiguration locale
            SharedPreferences  preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language),"ar");
            Log.e("Tag LANG+++\n",lng);
            Locale locale = new Locale(lng);//your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Locale locale = new Locale("ar");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_act_workshop);
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        user_id = prefs.getString(getString(R.string.pref_user_id),"");
        back = findViewById(R.id.back);
        img_banner = findViewById(R.id.img_banner);
        text_garage_name = findViewById(R.id.text_garage_name);
        txt_address = findViewById(R.id.txt_address);
        txt_phone_no = findViewById(R.id.phone_no);
        ll_call = findViewById(R.id.ll_call);
        editText = findViewById(R.id.ed_garage);
        ll_web = findViewById(R.id.ll_web);
        txt_distance = findViewById(R.id.txt_distance);
        go_to_garage = findViewById(R.id.go_to_garage);
        need_now_service = findViewById(R.id.need_now_service);
        no_data_error = findViewById(R.id.no_data_error);
        btn_retry = findViewById(R.id.btn_retry);
        txt_msg = findViewById(R.id.txt_msg);
        toolbar = findViewById(R.id.toolbar);

        btn_retry.setOnClickListener(this);
        init();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        workshop_id = getIntent().getStringExtra("workshop_id");
        workshop_name = getIntent().getStringExtra("workshop_name");
        mScrollView = findViewById(R.id.scroll_view);

        ActionBar();

        facebook_link = getIntent().getStringExtra("website");
        getWorkShopInfo();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        checkLocation();

        ll_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_no));
                startActivity(intent);
            }
        });

        ll_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!facebook_link.startsWith("http://") && !facebook_link.startsWith("https://"))
                    facebook_link = "http://" + facebook_link;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebook_link));
                startActivity(browserIntent);
            }
        });

        go_to_garage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Act_Map.class);
                intent.putExtra("notificationLatitude",latitude);
                intent.putExtra("notificationLongitude",longitude);
                intent.putExtra("address",address);
                intent.putExtra(getString(R.string.contact_no),contact_no);
                startActivity(intent);
            }
        });

        need_now_service.setOnClickListener(this);

        // mobile service to visible view
        if (getIntent().getBooleanExtra("isMobileRequest",false)){
            need_now_service.setVisibility(View.VISIBLE);
            go_to_garage.setVisibility(View.GONE);
        }else {
            go_to_garage.setVisibility(View.VISIBLE);
            need_now_service.setVisibility(View.GONE);
        }

        if (getIntent().getStringExtra("come_now") != null && getIntent().getStringExtra("come_now").equals("2")){
            need_now_service.setVisibility(View.GONE);
        }else if (getIntent().getStringExtra("come_now") != null && getIntent().getStringExtra("come_now").equals("1")){
            need_now_service.setVisibility(View.VISIBLE);
            Intent intent = getIntent();
            come_now = intent.getStringExtra("come_now");
            need_now_service.setVisibility(View.VISIBLE);
            facility_id_json = intent.getStringExtra("facility_id_json");
            request_date = intent.getStringExtra("request_date");
            request_time = intent.getStringExtra("request_time");
            appointment_type = intent.getStringExtra("request_type");
            oil_sae = intent.getStringExtra("oil_sae");
            oil_brand = intent.getStringExtra("oil_brand");
            amount = intent.getStringExtra("amount");
            lat = intent.getStringExtra("user_latitude");
            log = intent.getStringExtra("user_longitude");
            location = intent.getStringExtra("location_address");
            change_filter = intent.getStringExtra("change_filter");
            leather_seat = intent.getStringExtra("leather_seat");
            garage_id_json = intent.getStringExtra("garage_id_json");
            model_id = intent.getStringExtra("model_id");
            brand_id = intent.getStringExtra("brand_id");

        }
    }

    String latitude, longitude;

    public void ActionBar(){
//        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(workshop_name);
            actionBar.setDisplayShowHomeEnabled(true);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // get workshop detail set detail on view
    private void getWorkShopInfo() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL+"check_garage_status", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("status_response", " " + response);

                    try {
                        JSONObject mainJsonObject = new JSONObject(response);
                        if (mainJsonObject.getString("status").equals("true")){
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject jsonObject = result.getJSONObject(i);
                                contact_no = jsonObject.getString("contact_no");
                                workshop_name = jsonObject.getString("garage_name");
                                workshop_banner = jsonObject.getString("garage_banner");
                                facebook_link = jsonObject.getString("facebook_link");
                                address = jsonObject.getString("address");
                                latitude = jsonObject.getString("latitude");
                                longitude = jsonObject.getString("longitude");
                            }

                            if (facebook_link.equals("null") || facebook_link.equals("")){
                                ll_web.setVisibility(View.GONE);
                            }

                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("LATITUDE", latitude);
                            editor.putString("LONGITUDE", longitude);
                            editor.apply();

                            text_garage_name.setText(workshop_name);
                            if (!address.equals("") && address.split(",").length >0){
                                String s = address.split(",")[0];
                                try {
                                    txt_address.setText(s + ", \n"+ address.replaceAll(s+",",""));
                                }catch (Exception e){
                                    txt_address.setText(address);
                                }
                            }else {
                                txt_address.setText(address);
                            }
                            txt_address.setText(address);
                            txt_phone_no.setText(contact_no);
                            Glide.with(getApplicationContext()).load(workshop_banner).into(img_banner);

                            if (result.length()>0){
                                mScrollView.setVisibility(View.VISIBLE);
                                no_data_error.setVisibility(View.GONE);
                                WorkaroundMapFragment mapFragment = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
                                mapFragment.getMapAsync(Act_Workshop.this);
                            }else {
                                no_data_error.setVisibility(View.VISIBLE);
                                btn_retry.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_data_found));
                                mScrollView.setVisibility(View.GONE);
                            }
                        }else {
                            no_data_error.setVisibility(View.VISIBLE);
                            btn_retry.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.no_data_found));
                            mScrollView.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        no_data_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                        mScrollView.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //displaying the error in toast if occurs
                    no_data_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    mScrollView.setVisibility(View.GONE);
                    Log.e("Tag",""+error);

                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("garage_id", workshop_id);
                    Log.e("params", " " + params);

                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }else {
            no_data_error.setVisibility(View.VISIBLE);
            mScrollView.setVisibility(View.GONE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.no_data_found));
        }
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location").setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean requestSinglePermission() {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        //  Single Permission is granted
                        isPermission = true;
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        //  check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            isPermission = false;
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {

                    }
                }).check();
        return isPermission;
    }

    // workshop banner mutiple show indicatore
    private void init() {

        for (int i = 0; i < IMAGES.length; i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImage_Adapter(Act_Workshop.this, ImagesArray));
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//      Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 1000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });
    }

    Marker marker;
    String latitude1;
    String longitude1;

    // set  workshop location on map
    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
//        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
//        mMap.getUiSettings().setMyLocationButtonEnabled(true);
//        mMap.getUiSettings().setMapToolbarEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        if (mMap != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            latitude1 = prefs.getString("LATITUDE", "");
            longitude1 = prefs.getString("LONGITUDE", "");

            Location locations = mMap.getMyLocation();

            if (locations != null){
                Location locationB = new Location("");
                locationB.setLatitude(ParseDouble(latitude1));
                locationB.setLongitude(ParseDouble(longitude1));
                double distance = locations.distanceTo(locationB) / 1000;

                // txt_distance.setText(String.valueOf(distance)+"km");
                txt_distance.setText(String.format("%.2f", distance) + "km");
            }else {
                txt_distance.setText("Loading Distance...");
            }

            if (latitude1 != null && longitude1 != null) {
                latLng = new LatLng(ParseDouble(latitude1), ParseDouble(longitude1));
                if (marker != null){
                    marker.remove();
                }

                if (mMap != null) {
                    marker = mMap.addMarker(new MarkerOptions().position(latLng).title(workshop_name));
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                    mMap.animateCamera(location);
                }
            }

            //  parent scrollview in xml, give your scrollview id value
            ((WorkaroundMapFragment) Objects.requireNonNull(getSupportFragmentManager().findFragmentById(R.id.map))).setListener(new WorkaroundMapFragment.OnTouchListener() {
                        @Override
                        public void onTouch() {
                            mScrollView.requestDisallowInterceptTouchEvent(true);
                        }
                    });
        }
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //   ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, getString(R.string.location_not_detected), Toast.LENGTH_SHORT).show();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        Log.d("request", "--- >>>");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        Log.e("Tag ","ConnectionSuspended"+i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Tag ","ConnectionResult"+connectionResult.getErrorMessage());
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onLocationChanged(Location location) {

        ArrayList<Location> locations =new ArrayList<>();
        locations.add(location);

        Log.e("Tag location"," "+locations.size());

        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        Location locationB = new Location("");
        locationB.setLatitude(ParseDouble(latitude1));
        locationB.setLongitude(ParseDouble(longitude1));

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(locations.get(0).getLatitude(), locations.get(0).getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        double distance = (locations.get(0).distanceTo(locations.get(locations.size()-1))) / 1000;
        double distance = location.distanceTo(locationB) / 1000;

//        txt_distance.setText(String.valueOf(distance)+"km");
        txt_distance.setText(String.format("%.2f", distance) + "km");

        SupportMapFragment mapFragment = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch(Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        }
        else return 0;
    }

    public static float distance(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry){
            getWorkShopInfo();
        }else if (v == need_now_service){
//            Toast.makeText(getApplicationContext(),"This service under contraction",Toast.LENGTH_LONG).show();
            getMessage();
        }
    }

    private void getMessage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Workshop.this);
        builder.setMessage(R.string.send_request_for_this_workshop_only);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendRequest();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    public void showThankDialog(String msg) {
        try {
            final Dialog dialog = new Dialog(this);
            Objects.requireNonNull(dialog.getWindow()).clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
            dialog.setContentView(R.layout.popup_thank);
            dialog.setCancelable(false);
            // for dialog shadow
            // set values for custom dialog components - text, image and button
            //  final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
            TextView tvMessageTwo = (TextView) dialog.findViewById(R.id.tvMessageTwo);
            if (msg.equals("")){
                tvMessageTwo.setText(msg);
            }else {
                tvMessageTwo.setText(getString(R.string.thank_you_for_a_request));
            }
            TextView tvOK = (TextView) dialog.findViewById(R.id.tvOk);

            dialog.show();

            tvOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    if (new Act_Mobile_Request_Booking().instance != null){
                        new Act_Mobile_Request_Booking().instance.finish();
                    }
                    if (new Act_Mobile_Request().instance != null){
                        new Act_Mobile_Request().instance.finish();
                    }
                    if (new Act_Mobile_Request_Workshop_List().instance != null){
                        new Act_Mobile_Request_Workshop_List().instance.finish();
                    }
                    Intent i = new Intent(getApplicationContext(), Navigation_Activity.class);
                    startActivity(i);
                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // send mobile request to perticuler workshop not use
    private void sendRequest() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())){

            ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();

            VolleyMultipartRequest request = new VolleyMultipartRequest(Request.Method.POST, Constant.URL+"send_mobile_request",
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            Log.e("send response", new String(response.data));
                            dialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(new String(response.data));
                                if (object.getString("status").equals("1")){
//                                    Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_LONG).show();
//                                    btn_send.setVisibility(View.GONE);
                                    JSONArray jsonArray = new JSONArray(facility_id_json);
                                    JSONArray array = new JSONArray();
                                    String servie_id = "";
                                    for (int i=0; i<jsonArray.length(); i++){
                                        JSONObject objects = jsonArray.getJSONObject(i);
                                        JSONObject object1 = new JSONObject();
                                        servie_id = objects.getString("service_id");
                                        object1.put("service_id",servie_id);
                                        array.put(object1);
                                    }
                                    SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service),MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(servie_id, new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date()));
                                    editor.apply();
                                    showThankDialog("");
                                } else {
                                    showThankDialog(object.getString("message"));
//                                    Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),getString(R.string.somting_wrong_please),Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response Error",""+error);
                    if (error instanceof TimeoutError){
                        Toast.makeText(getApplicationContext(),getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                    }else if (error instanceof NetworkError){
                        Toast.makeText(getApplicationContext(),getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getApplicationContext(),getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                    }
//                    Toast.makeText(getApplicationContext(),""+error,Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<>();
                    map.put("user_id",user_id);
                    map.put("request_date",request_date);
                    map.put("request_time",request_time);
                    map.put("model_id",model_id);
                    map.put("brand_id",brand_id);
                    map.put("facility_id_json",""+facility_id_json);
                    map.put("request_type",appointment_type);
                    map.put("oil_sae",oil_sae);
                    map.put("oil_brand",oil_brand);
                    map.put("amount",amount);
                    map.put("leather_seat",leather_seat);
                    map.put("change_filter",change_filter);
                    map.put("location_address",location);
                    map.put("user_latitude",lat);
                    map.put("user_longitude",log);
                    map.put("garage_id_json",""+garage_id_json);

                    Log.e("Tag Parameter",map.toString());
                    return map;
                }
            };

            Volley.newRequestQueue(this).add(request);
            request.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 100000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 100000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        }else {
            Toast.makeText(getApplicationContext(),getString(R.string.internet_not_connect),Toast.LENGTH_LONG).show();
        }
    }

 /*   private double getDistanceInfo(String destinationAddress) {
        StringBuilder stringBuilder = new StringBuilder();
        Double dist = 0.0;
        try {

            destinationAddress = destinationAddress.replaceAll(" ","%20");
            String url = "http://maps.googleapis.com/maps/api/directions/json?origin=" + latFrom + "," + lngFrom + "&destination=" + latTo + "," + lngTo + "&mode=driving&sensor=false";

            HttpPost httppost = new HttpPost(url);

            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();
            response = client.execute(httppost);
            HttpEntity entity = (response.getEntity());
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {

        }
            catch (IOException e) {
            }

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject = new JSONObject(stringBuilder.toString());

            JSONArray array = jsonObject.getJSONArray("routes");

            JSONObject routes = array.getJSONObject(0);

            JSONArray legs = routes.getJSONArray("legs");

            JSONObject steps = legs.getJSONObject(0);

            JSONObject distance = steps.getJSONObject("distance");

            Log.i("Distance", distance.toString());
            dist = Double.parseDouble(distance.getString("text").replaceAll("[^\\.0123456789]","") );

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return dist;
    }*/

}


