package com.example.automap_application;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Contry_spinner_model;
import com.example.automap_application.Model.Login_Model;
import com.example.automap_application.Model.Register_model;
import com.example.automap_application.extra.API;
import com.example.automap_application.extra.APIResponse;
import com.example.automap_application.newPackage.Helper;
import com.example.automap_application.utils.AppUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mukesh.OtpView;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Act_Registration extends AppCompatActivity {

    ImageView login_back;
    EditText ed_name, ed_password, ed_mobile, ed_confirm_PASSWORD;
    Spinner spinner_country;
    TextView btn_reg;
    TextView tv_login, tv_ph_code;
    private static ProgressDialog mProgressDialog;

    String c_id = "1";
    String concat;
    String c_currency;
    private ArrayList<Contry_spinner_model> goodModelArrayList;
    private ArrayList<String> names = new ArrayList<String>();
    ProgressDialog progress;
    Register_model regModel = new Register_model();
    private ArrayList<Register_model> Reg_Model;
    String st_nm, st_pass, st_mobile, st_confirm_pass;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    boolean isVerified = false;
    FirebaseAuth auth;

    PhoneAuthProvider.ForceResendingToken token;
    String verificationId;
    AlertDialog dialog;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String DEVICE_TOKEN;

    PhoneAuthCredential authCredential;
    boolean isInstant = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__registration);

        bindView();
        retrieveJSON();

        sharedPreferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        editor = sharedPreferences.edit();

        auth = FirebaseAuth.getInstance();
        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Log.e("Tag trim email", ed_email.getText().toString());
                Log.e("Tag trim contact", ed_mobile.getText().toString());

                st_nm = ed_name.getText().toString();
//                st_email = ed_email.getText().toString().trim();
                st_pass = ed_password.getText().toString();
                st_confirm_pass = ed_confirm_PASSWORD.getText().toString();
                st_mobile = ed_mobile.getText().toString().trim();

//                Log.e("Tag trim email", st_email);
                Log.e("Tag trim contact", st_mobile);
                if (TextUtils.isEmpty(st_nm)) {
                    ed_name.setError(getString(R.string.name));
                } /*else if (TextUtils.isEmpty(st_email)) {
                    ed_email.setError("Enter email");
                } else if (!Act_Login.isValidEmail(st_email.trim())) {
                    ed_email.setError("Email is not valid");
                } */else if (TextUtils.isEmpty(st_pass)) {
                    ed_password.setError(getString(R.string.enter_password));
                } else if (st_pass.length() < 6) {
                    ed_password.setError(getString(R.string.please_enter6digit_password));
                } else if (TextUtils.isEmpty(st_confirm_pass)) {
                    ed_confirm_PASSWORD.setError(getString(R.string.confirm_password));
                } else if (!st_pass.matches(st_confirm_pass)) {
                    ed_confirm_PASSWORD.setError(getString(R.string.does_not_match_password));
                } else if (TextUtils.isEmpty(st_mobile)) {
                    ed_mobile.setError(getString(R.string.enter_contact));
                } else if (st_mobile.length() < 6 || st_mobile.length() > 13) {
                    // if(phone.length() != 10) {
                    ed_mobile.setError(getString(R.string.contact_invalide));
                } else if (!isValidNumber()) {
                    return;
                }  else if (!Helper.isNetworkAvailable(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), R.string.internet_not_connect, Toast.LENGTH_LONG).show();
                } else {
                    checkNumberEmail();
                }
            }
        });

        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                st_mobile = ed_mobile.getText().toString();
                goodModelArrayList.get(i).getCon_currency();
                String p_code = goodModelArrayList.get(i).getCon_ph_code();
                c_id = goodModelArrayList.get(i).getCon_id();
                c_currency = goodModelArrayList.get(i).getCon_currency();
                tv_ph_code.setText("+" + p_code);
                concat = st_mobile;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Act_Registration.this, Act_Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });

        login_back.setOnClickListener(view -> {
            onBackPressed();
        });

        String d_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);;
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    DEVICE_TOKEN = d_id;
                    return;
                }
                String token = task.getResult().getToken();
                DEVICE_TOKEN = token;
                Log.d("DEVICE_TOKEN", DEVICE_TOKEN);
            }
        });

    }

    public void bindView() {
        ed_name = findViewById(R.id.Ed_name);
//        ed_email = findViewById(R.id.Ed_Email);
        ed_password = findViewById(R.id.Ed_PASSWORD);
        ed_mobile = findViewById(R.id.Ed_CONTACT);
        ed_confirm_PASSWORD = findViewById(R.id.Ed_confirm_PASSWORD);
        spinner_country = findViewById(R.id.Sp_select_country);
        tv_login = findViewById(R.id.tv_login);
        tv_ph_code = findViewById(R.id.tv_ph_code);
        login_back = findViewById(R.id.login_back);
        login_back = findViewById(R.id.login_back);
        btn_reg = findViewById(R.id.Btn_registration);

    }

    // check contact and email is regiter or not
    ProgressDialog sendOtpDialog;
    private void checkNumberEmail() {

        PhoneCallBack(true);
        API api = new API(Act_Registration.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

                Log.e("Tag Response", response);

                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    if (object.getString("status").equals("true")) {
                        sendOtpDialog = new ProgressDialog(Act_Registration.this);
                        sendOtpDialog.setMessage(getString(R.string.loading));
                        sendOtpDialog.show();
                        PhoneAuthProvider.getInstance().verifyPhoneNumber(tv_ph_code.getText().toString() + st_mobile
                                , 2
                                , TimeUnit.MINUTES
                                , Act_Registration.this
                                , callbacks);
                    } else {
//                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), getString(R.string.contact_number_already_registered), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
            }
        });
        Map<String, String> map = new HashMap<>();
        map.put("contact_no", st_mobile);
//        map.put("email", st_email);

        api.execute(100, Constant.URL + "checkUserEmailAndContact", map, true);
    }

    // firebase otp callback listener
    private void PhoneCallBack(boolean isOptSend) {

        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
//                Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();

                // verify otp
                isInstant = true;
                authCredential = phoneAuthCredential;
                OtpDialog();
                String code = phoneAuthCredential.getSmsCode();
                if (sendOtpDialog != null){
                    sendOtpDialog.dismiss();
                }
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Log.e("tag Error", e.getMessage());
                e.printStackTrace();
                if (isOptSend) {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_to_send_otp), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Verification Failed", Toast.LENGTH_LONG).show();
                }

                if (e instanceof FirebaseTooManyRequestsException){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

                if (sendOtpDialog != null){
                    sendOtpDialog.dismiss();
                }
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                if (sendOtpDialog != null){
                    sendOtpDialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), getString(R.string.otp_send_successfully), Toast.LENGTH_LONG).show();
                token = forceResendingToken;
                verificationId = s;
                isVerified = false;
                // otp send successfully
                OtpDialog();
            }
        };
    }

    //  otp dialog view
    public void OtpDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Registration.this, R.style.AppCompatAlertDialogStyle);
        ViewGroup viewGroup = findViewById(android.R.id.content);

        View view = LayoutInflater.from(Act_Registration.this).inflate(R.layout.opt_verification_dialog, viewGroup, false);
        builder.setView(view);
        builder.setCancelable(false);

        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        TextView txt_message = view.findViewById(R.id.txt_message);
        OtpView otpView = view.findViewById(R.id.otpView);
        TextView txt_timer = view.findViewById(R.id.txt_timer);
        AVLoadingIndicatorView av_loading = view.findViewById(R.id.av_loading);
        LinearLayout linBtn = view.findViewById(R.id.linBtn);
        otpView.setCursorVisible(true);
        otpView.setOtpCompletionListener(otp -> {
            AppUtils.hideKeyboard(Act_Registration.this);
        });
        startCountDown(txt_timer);
        otpView.setItemBackgroundColor(Color.BLACK);
        Button btnVerify = view.findViewById(R.id.btn_verify);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        TextView btn_contact_us = view.findViewById(R.id.btn_contact_us);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            btn_contact_us.setText(Html.fromHtml(getString(R.string.contact_us_to_problem_with_verification_code), Html.FROM_HTML_MODE_LEGACY));
        }else {
            btn_contact_us.setText(Html.fromHtml(getString(R.string.contact_us_to_problem_with_verification_code)));
        }

        btn_contact_us.setOnClickListener(view1 -> {startActivity(new Intent(getApplicationContext(), Act_Contact_Us.class));});
        btn_cancel.setOnClickListener(view12 -> dialog.dismiss());

        txt_message.setText(getString(R.string.please_type_verification_code)+" \n " + tv_ph_code.getText().toString() + " " + ed_mobile.getText().toString());

        // instance verify without otp get
        if (isInstant && authCredential != null){
            auth.signInWithCredential(authCredential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                isInstant = false;

                                RegisterApi();
//                                        Toast.makeText(Act_Registration.this, getString(R.string.varification_successfully), Toast.LENGTH_SHORT).show();
                            } else {
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    Toast.makeText(Act_Registration.this, getString(R.string.verification_fail_invalid_credentials), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(Act_Registration.this, getString(R.string.verification_failed), Toast.LENGTH_SHORT).show();
                                }
                            }
                            linBtn.setVisibility(View.VISIBLE);
                            av_loading.setVisibility(View.GONE);
                        }
                    });
        }

        btnVerify.setOnClickListener(view1 -> {
            linBtn.setVisibility(View.GONE);
            av_loading.setVisibility(View.VISIBLE);
            if (isVerified) {
                RegisterApi();
            } else {
                if (otpView.getText().toString().length() < 6) {
                    Toast.makeText(Act_Registration.this, getString(R.string.resend_otp), Toast.LENGTH_SHORT).show();
                    linBtn.setVisibility(View.VISIBLE);
                    av_loading.setVisibility(View.GONE);
                } else{

                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, otpView.getText().toString());
                    auth.signInWithCredential(credential)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        isVerified = true;
                                        RegisterApi();
//                                        Toast.makeText(Act_Registration.this, getString(R.string.verification_successfully), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Log.e("Tag Exception", task.getException().getMessage());
                                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                            Toast.makeText(Act_Registration.this, getString(R.string.verification_fail_invalid_credentials), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(Act_Registration.this, getString(R.string.verification_failed), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    linBtn.setVisibility(View.VISIBLE);
                                    av_loading.setVisibility(View.GONE);
                                }
                            });
                }
            }
        });

        dialog.show();
    }

    // count down timer
    public void startCountDown(TextView textView) {

        //60_000 = 60 sec or 1 min and another is interval of count down is 1 sec
        new CountDownTimer(120_000, 1000) {
            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                textView.setText(getString(R.string.enter_code_within)+" " + (remainedSecs / 60) + ":" + (remainedSecs % 60));
                textView.setTextColor(Color.BLACK);
                textView.setBackground(null);
            }
            public void onFinish() {
                textView.setText(getString(R.string.resend_otp));
                textView.setTextColor(Color.parseColor("#002AC1"));
                textView.setBackground(getResources().getDrawable(R.drawable.btm_line_shape));
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PhoneAuthProvider.getInstance().verifyPhoneNumber(tv_ph_code.getText().toString() + st_mobile
                                , 2
                                , TimeUnit.MINUTES
                                , Act_Registration.this
                                , new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        Log.e("tag Error", e.getMessage());
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Failed to send Opt", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        token = forceResendingToken;
                                        verificationId = s;
                                        startCountDown(textView);
                                    }
                                },
                                token);
                    }
                });
            }
        }.start();
    }

    // after otp verity register new account
    public void RegisterApi() {
        progress = new ProgressDialog(Act_Registration.this);
        progress.setMessage(getString(R.string.wait_a_moment_until_verication_link_not_send));
        progress.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "user_register", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("register_process", " " + response);
                progress.dismiss();
                try {

                    JSONObject obj = new JSONObject(response);
                    Reg_Model = new ArrayList<>();
                    if (obj.optString("status").equals("true")) {

//                        Toast.makeText(Act_Registration.this, obj.optString("message"), Toast.LENGTH_SHORT).show();
                        Toast.makeText(Act_Registration.this, getString(R.string.registration_successfully), Toast.LENGTH_SHORT).show();

                        /*regModel.setMessage(obj.getString("message"));
                        JSONObject data_obj = obj.getJSONObject("result");

                        regModel.setName(data_obj.getString("name"));
                        regModel.setEmail(data_obj.getString("email"));
                        regModel.setCountry_id(data_obj.getString("country_id"));
                        regModel.setContact_no(data_obj.getString("contact_no"));
                        regModel.setUser_id(data_obj.getString("user_id"));
                        Reg_Model.add(regModel);*/

                        if (dialog != null) {
                            dialog.dismiss();
                            Login();
                        }

                    } else if (obj.optString("status").equals("false")) {
                        Toast.makeText(Act_Registration.this, obj.optString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Act_Registration.this, getString(R.string.somting_wrong_please), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                Toast.makeText(Act_Registration.this, getString(R.string.somting_wrong_please), Toast.LENGTH_SHORT).show();
                Log.e("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("name", ed_name.getText().toString());
//                map.put("email", ed_mobile.getText().toString());
                map.put("password", ed_password.getText().toString());
                map.put("country_id", c_id);
                map.put("contact_no", ed_mobile.getText().toString());
                map.put("language", sharedPreferences.getString(getString(R.string.pref_language), "ar"));
                Log.e("Tag Params", map.toString());
                return map;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    // get country list and add spinner
    private void retrieveJSON() {
        if (AppUtils.isNetworkAvailable(this)) {

            API api = new API(getApplicationContext(), new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Log.e("Country Response", response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        if (obj.optString("status").equals("true")) {
                            goodModelArrayList = new ArrayList<>();
                            JSONObject object = obj.getJSONObject("result");
                            JSONArray dataArray = object.getJSONArray("country");

                            for (int i = 0; i < dataArray.length(); i++) {

                                Contry_spinner_model playerModel = new Contry_spinner_model();
                                JSONObject data_obj = dataArray.getJSONObject(i);
                                playerModel.setCon_id(data_obj.getString("country_id"));
                                playerModel.setCon_code(data_obj.getString("country_code"));
                                playerModel.setCon_name(data_obj.getString("country_name"));
                                playerModel.setCon_currency(data_obj.getString("currency"));
                                playerModel.setCon_language(data_obj.getString("language"));
                                playerModel.setCon_ph_code(data_obj.getString("phone_code"));
                                goodModelArrayList.add(playerModel);
                            }
                            for (int i = 0; i < goodModelArrayList.size(); i++) {
                                names.add(goodModelArrayList.get(i).getCon_name().toString());
                            }
                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Registration.this, android.R.layout.simple_spinner_dropdown_item, names);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinner_country.setAdapter(spinnerArrayAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });

            api.execute(200,Constant.URL + "get_country",null,false);

            /*StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Country Response", response);
                            try {
                                JSONObject obj = new JSONObject(response);
                                if (obj.optString("status").equals("true")) {
                                    goodModelArrayList = new ArrayList<>();
                                    JSONObject object = obj.getJSONObject("result");
                                    JSONArray dataArray = object.getJSONArray("country");

                                    for (int i = 0; i < dataArray.length(); i++) {

                                        Contry_spinner_model playerModel = new Contry_spinner_model();
                                        JSONObject data_obj = dataArray.getJSONObject(i);
                                        playerModel.setCon_id(data_obj.getString("country_id"));
                                        playerModel.setCon_code(data_obj.getString("country_code"));
                                        playerModel.setCon_name(data_obj.getString("country_name"));
                                        playerModel.setCon_currency(data_obj.getString("currency"));
                                        playerModel.setCon_language(data_obj.getString("language"));
                                        playerModel.setCon_ph_code(data_obj.getString("phone_code"));
                                        goodModelArrayList.add(playerModel);
                                    }
                                    for (int i = 0; i < goodModelArrayList.size(); i++) {
                                        names.add(goodModelArrayList.get(i).getCon_name().toString());
                                    }
                                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_Registration.this, android.R.layout.simple_spinner_dropdown_item, names);
                                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                    spinner_country.setAdapter(spinnerArrayAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //  displaying the error in toast if occurs
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            // request queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            requestQueue.add(stringRequest);*/
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    private boolean isValidNumber() {
        if (tv_ph_code.getText().toString().equals("+91") && ed_mobile.getText().toString().length() < 10) {
            ed_mobile.setError(getString(R.string.contact_invalide));
            return false;
        } else if (tv_ph_code.getText().equals("+962") && ed_mobile.getText().toString().length() < 9) {
            ed_mobile.setError(getString(R.string.contact_invalide));
            return false;
        } else {
            return true;
        }
    }

    // after regite automatically call login
    public void Login() {
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"user_login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    List<Login_Model> loginModel = new ArrayList<>();
                    Login_Model login_model = new Login_Model();
                    login_model.setMessege(obj.getString("message"));
                    if (obj.optString("status").equals("true")) {

                        login_model.setStatus_up(obj.getString("status"));
                        login_model.setMessege(obj.getString("message"));

                        JSONArray dataArray = obj.getJSONArray("result");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data_obj = dataArray.getJSONObject(i);
                            login_model.setUser_id(data_obj.getString("user_id"));
                            login_model.setName(data_obj.getString("name"));
                            login_model.setEmail(data_obj.getString("email"));
                            login_model.setContect_no(data_obj.getString("contact_no"));
                            login_model.setCountry_id(data_obj.getString("country_id"));
//                            login_model.setState(data_obj.getString("state"));
//                            login_model.setCity(data_obj.getString("city_id"));
                            login_model.setAddress(data_obj.getString("address"));
                            login_model.setLati(data_obj.getString("latitude"));
                            login_model.setLongi(data_obj.getString("longitude"));
//                            login_model.setPin(data_obj.getString("pinCode"));
                            login_model.setUser_image(data_obj.getString("user_image"));
                            login_model.setStatus(data_obj.getString("status"));
                            login_model.setReg_date(data_obj.getString("registration_date"));
                            login_model.setUpdate_date(data_obj.getString("updated_at"));
                            loginModel.add(login_model);
                        }

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(getString(R.string.pref_user_id), login_model.getUser_id());

                        Log.e("user_id", login_model.getUser_id());

                        editor.putString(getString(R.string.pref_contatct), login_model.getContect_no());
                        editor.putString(getString(R.string.pref_user_name), login_model.getName());
                        editor.putString(getString(R.string.pref_country_id), login_model.getCountry_id());
                        editor.putString(getString(R.string.pref_user_pwd), ed_password.getText().toString());
                        editor.apply();

                        Intent i = new Intent(Act_Registration.this, Act_car_details.class);
                        Constant.i = 1;
                        startActivity(i);
                        finish();

                        Toast.makeText(Act_Registration.this, login_model.getMessege(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(Act_Registration.this, login_model.getMessege(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Catch", "" + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Act_Registration.this, getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                Log.e("My error", "" + error.getMessage());
                Log.e("My error", "" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("email", ed_mobile.getText().toString());
                map.put("contact_no", ed_mobile.getText().toString());
                map.put("password", ed_password.getText().toString());
                map.put("device_token", DEVICE_TOKEN);
                String language = "";
//                if (sharedPreferences.getString(getString(R.string.pref_language),"").equals("ar")){
                    language = "Arabic";
//                }else {
//                    language = "English";
//                }
                map.put("language",language);
                Log.e("params", " " + map);
                return map;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(6000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(Act_Registration.this);
        queue.add(request);
    }

}
