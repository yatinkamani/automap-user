package com.example.automap_application;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.utils.AppUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_splash_screen extends AppCompatActivity implements View.OnClickListener {

    Handler handler;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String storedUserId = "", car_id = "", messageFor = "", n_id = "";
    Bundle bundle ;
    View lin_error;
    TextView txt_error;
    Button btn_retry;
    AVLoadingIndicatorView avi;
    private FirebaseAnalytics mFirebaseAnalytics;

    String AMR_WORKSHOP = "com.kcs.automapgarage";
    String AMR_TOW_TRUCK = "com.kcs.automaptwotrack";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        //  applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            // Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng); //your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);

        String language = prefs.getString(getString(R.string.pref_language), "ar").trim();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_act_splesh_screen);

        lin_error = findViewById(R.id.lin_error);
        txt_error = findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);
        avi = findViewById(R.id.avi);
        editor = prefs.edit();
//        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
//        storedUserId = prefs.getString(getString(R.string.pref_user_id), "");
        setDefault();
        car_id = prefs.getString("CAR_ID", "");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(Act_splash_screen.this, "Splash Screen", null);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Splash Screen");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, Settings.Global.getString(getContentResolver(), "device_name"));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        Intent intent = getIntent();
        if (intent != null) {
            messageFor = intent.getStringExtra("messageFor");
            n_id = intent.getStringExtra("id");
            bundle = intent.getBundleExtra("bundle");
            if (messageFor != null)
                Log.e("Tag messageFor",messageFor);

            Bundle bundle1 = new Bundle();
            bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "Intent Not Null");
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
        }

        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("Tag Android","Android ID : "+android_id);

        // check other automap install this device or not

        if (android_id.equals("86a863cfbf6f9951") || android_id.equals("b66fb69a0426e31d")){
            startThread();
        } else if (isInstalled(AMR_TOW_TRUCK)) {
//            Toast.makeText(getApplicationContext(), "not use this app because AMR Tow Truck installed", Toast.LENGTH_LONG).show();
            avi.setVisibility(View.GONE);
            lin_error.setVisibility(View.VISIBLE);
            txt_error.setText("Not Uses same device tow AMR App \nUninstall AMR Tow Truck");
            btn_retry.setVisibility(View.GONE);
        } else if (isInstalled(AMR_WORKSHOP)) {
//            Toast.makeText(getApplicationContext(), "not use this app because tow AMR-Automap.repair installed", Toast.LENGTH_LONG).show();
            avi.setVisibility(View.GONE);
            lin_error.setVisibility(View.VISIBLE);
            txt_error.setText("Not Uses same device tow AMR App \nUninstall AMR Workshop");
            btn_retry.setVisibility(View.GONE);
        } else if (isStoragePermissionGranted()){
            startThread();
        }

        // en check
        btn_retry.setOnClickListener(this);
    }

    // set default laguage
    public void setDefault() {
        Locale locale = Locale.getDefault();

        locale.getLanguage();
        Log.e("Tag Language", getResources().getConfiguration().locale.getLanguage());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(getString(R.string.pref_language), getResources().getConfiguration().locale.getLanguage()).apply();
    }

    public boolean isStoragePermissionGranted() {

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Bundle bundle1 = new Bundle();
                bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "Permission is revoked");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(Act_splash_screen.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk < 23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    private void startThread() {

        Bundle bundle1 = new Bundle();
        bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "Out Of Tread");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
        storedUserId = prefs.getString(getString(R.string.pref_user_id), "");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Bundle bundle1 = new Bundle();
                bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "inside Tread "+storedUserId+" User Id");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
                if (storedUserId == null || storedUserId.equals("")){
                    Intent intent = new Intent(Act_splash_screen.this, Act_Login_first.class);
                    startActivity(intent);
                    finish();
                }else{
                    retrieveJSON(storedUserId);
                            /*if (car_id.equals("")){
                                Intent intent = new Intent(Act_splash_screen.this, Act_car_details.class);
                                startActivity(intent);
                            }else {
                                if (messageFor != null){
                                    Intent i = new Intent(Act_splash_screen.this, Navigation_Activity.class);
                                    i.putExtra("messageFor", messageFor);
                                    if (bundle != null)
                                        i.putExtra("bundle",bundle);
                                    startActivity(i);
                                }else {
                                    Intent intent = new Intent(Act_splash_screen.this, Navigation_Activity.class);
                                    startActivity(intent);
                                }
                            }*/
                }
            }
        },2000);

        /*Thread thread = new Thread() {
            public void run() {

                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (storedUserId == null || storedUserId.equals("")){
                            Intent intent = new Intent(Act_splash_screen.this, Act_Login_first.class);
                            startActivity(intent);
                            finish();
                        }else{
                            retrieveJSON(storedUserId);
                            */
        /*if (car_id.equals("")){
                                Intent intent = new Intent(Act_splash_screen.this, Act_car_details.class);
                                startActivity(intent);
                            }else {
                                if (messageFor != null){
                                    Intent i = new Intent(Act_splash_screen.this, Navigation_Activity.class);
                                    i.putExtra("messageFor", messageFor);
                                    if (bundle != null)
                                        i.putExtra("bundle",bundle);
                                    startActivity(i);
                                }else {
                                    Intent intent = new Intent(Act_splash_screen.this, Navigation_Activity.class);
                                    startActivity(intent);
                                }
                            }
                        }
                    }
                });
            }
        };
        thread.start();*/

    }

    private void retrieveJSON(final String id) {

        /*ProgressDialog dialog = new ProgressDialog(Act_splash_screen.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();*/

        lin_error.setVisibility(View.GONE);
        btn_retry.setVisibility(View.GONE);
        avi.setVisibility(View.VISIBLE);
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            RequestQueue queue = Volley.newRequestQueue(Act_splash_screen.this);
            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"check_user_status", new Response.Listener<String>() {
                @SuppressLint("CommitPrefEdits")
                @Override
                public void onResponse(String response) {
//                dialog.dismiss();
                    try {
                        Log.e("check_user_response"," " +response);
                        Bundle bundle1 = new Bundle();
                        bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "inside Tread "+response+" User Id");
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
                        JSONObject obj = new JSONObject(response);
                        if (!response.isEmpty()){

                            if (obj.optString("status").equals("true")) {
                                String user_id = "";
                                String name = "";
                                String Email = "";
                                String country_id = "";
                                String contact = "";
                                JSONArray dataArray = obj.getJSONArray("result");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject data_obj = dataArray.getJSONObject(i);

                                    user_id = data_obj.getString("user_id");
                                    name = data_obj.getString("name");
                                    Email = data_obj.getString("email");
                                    country_id = data_obj.getString("country_id");
                                    contact = data_obj.getString("contact_no");

                                    editor = prefs.edit();
                                    editor.putString(getString(R.string.pref_user_id), user_id);
                                    editor.putString(getString(R.string.pref_user_name), name);
                                    editor.putString(getString(R.string.pref_email), Email);
                                    editor.putString(getString(R.string.pref_country_id), country_id);
                                    editor.putString(getString(R.string.pref_contatct), contact);
                                    editor.apply();

                                }
                                if (obj.getString("request").equals("profile")) {
                                    Intent intent = new Intent(Act_splash_screen.this, Act_Edit_Profile_Detail.class);
                                    intent.putExtra(getString(R.string.pref_user_id), user_id);
                                    intent.putExtra(getString(R.string.pref_user_name), name);
                                    intent.putExtra(getString(R.string.pref_email), Email);
                                    intent.putExtra(getString(R.string.pref_country_id), country_id);
                                    intent.putExtra(getString(R.string.contact_no), contact);
                                    intent.putExtra("LoginPage",true);
                                    startActivity(intent);
                                    finish();
                                } else if (obj.getString("request").equals("car")) {
                                    Intent i = new Intent(Act_splash_screen.this, Act_car_details.class);
                                    Constant.i = 1;
                                    startActivity(i);
                                    finish();
                                } else  {
                                    if (messageFor != null){
                                        Intent i = new Intent(Act_splash_screen.this, Navigation_Activity.class);
                                        i.putExtra("messageFor", messageFor);
                                        i.putExtra("id", n_id);
                                        if (bundle != null)
                                            i.putExtra("bundle",bundle);
                                        startActivity(i);
                                        finish();
                                    }else {
                                        Intent intent = new Intent(Act_splash_screen.this, Navigation_Activity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            }else {
                                prefs.edit().clear().apply();
                                getSharedPreferences(getString(R.string.pref_Badge),MODE_PRIVATE).edit().clear().apply();
                                lin_error.setVisibility(View.VISIBLE);
                                txt_error.setText(getString(R.string.account_deleted));
                                btn_retry.setVisibility(View.VISIBLE);
                                avi.setVisibility(View.GONE);
                            }
                        }else {
                            prefs.edit().clear().apply();
                            getSharedPreferences(getString(R.string.pref_Badge),MODE_PRIVATE).edit().clear().apply();
                            lin_error.setVisibility(View.VISIBLE);
                            txt_error.setText(getString(R.string.no_data_available));
                            btn_retry.setVisibility(View.VISIBLE);
                            avi.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        prefs.edit().clear().apply();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().apply();
                        e.printStackTrace();
                        Bundle bundle1 = new Bundle();
                        bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "Exception Volley "+e);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_error.setText(getString(R.string.no_data_found));
                        btn_retry.setVisibility(View.VISIBLE);
                        avi.setVisibility(View.GONE);
                    }
//                dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Bundle bundle1 = new Bundle();
                    bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "Error Volley "+error);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
                    if (error instanceof TimeoutError){
                        txt_error.setText(getString(R.string.internet_not_connect));
                    }else {
                        txt_error.setText(getString(R.string.somting_wrong_please));
                    }
                    lin_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    avi.setVisibility(View.GONE);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", id);
                    Bundle bundle1 = new Bundle();
                    bundle1.putString(FirebaseAnalytics.Param.ITEM_ID, "Params "+map.toString());
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
                    return map;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        }else {
            lin_error.setVisibility(View.VISIBLE);
            txt_error.setText(getString(R.string.internet_not_connect));
            btn_retry.setVisibility(View.VISIBLE);
            avi.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Bundle bundle1 = new Bundle();
        bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "Granted Permission Size "+grantResults.length);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
        if (grantResults.length > 0) {
            Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
            startThread();
        }else {
            Toast.makeText(getApplicationContext(), "Permission Not Granted", Toast.LENGTH_SHORT).show();
            startThread();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry){
            lin_error.setVisibility(View.GONE);
            btn_retry.setVisibility(View.GONE);
            avi.setVisibility(View.VISIBLE);
            startThread();
        }
    }

    private boolean isInstalled(String PackageName) {

        Bundle bundle1 = new Bundle();
        bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, "isInstalled");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);
        boolean isInstall = false;
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> appInfo = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        for (ApplicationInfo info : appInfo) {

            if (info.packageName.equals(PackageName)) {
                isInstall = true;
                break;
            }
        }
        Bundle bundle2 = new Bundle();
        bundle2.putString(FirebaseAnalytics.Param.ITEM_NAME, ""+isInstall);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle2);
        return isInstall;
    }
}
