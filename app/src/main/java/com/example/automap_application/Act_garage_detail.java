    package com.example.automap_application;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.automap_application.Constant.Constant;
import com.jackandphantom.circularimageview.RoundedImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Act_garage_detail extends AppCompatActivity {
    String garage_id="" , garage_banner="", garage_name="", contact_no="", state="", city="", address="", pincode="", country_name="";
    RoundedImage img_garage_banner;
    TextView txt_garage_name,phone_no, txt_garage_address, txt_garage_pincode, txt_garage_state, txt_garage_city, txt_garage_country;
    ImageView back_garage_detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_garage_detail);

        back_garage_detail = findViewById(R.id.back_garage_detail);
        back_garage_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        garage_id = getIntent().getStringExtra("garage_id");
        img_garage_banner = findViewById(R.id.garage_banner);
        txt_garage_name = findViewById(R.id.garage_name);
        phone_no = findViewById(R.id.phone_no);
        txt_garage_address = findViewById(R.id.txt_garage_address);
        txt_garage_pincode = findViewById(R.id.txt_garage_pincode);
        txt_garage_state = findViewById(R.id.txt_garage_state);
        txt_garage_city = findViewById(R.id.txt_garage_city);
        txt_garage_country = findViewById(R.id.txt_garage_country);

        getGarageInfo();
    }

    // get workshop detail and set view
    private void getGarageInfo() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL+"check_garage_status", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response"," " +response);

                        try {
                            JSONObject mainJsonObject = new JSONObject(response);

                            JSONArray result = mainJsonObject.getJSONArray("result");
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject jsonObject=result.getJSONObject(i);
                                contact_no = jsonObject.getString("contact_no");
                                garage_name = jsonObject.getString("garage_name");
                                garage_banner = jsonObject.getString("garage_banner");
                                state = jsonObject.getString("state");
                                city = jsonObject.getString("city");
                                address = jsonObject.getString("address");
                                pincode = jsonObject.getString("pincode");

                                JSONObject country_data = jsonObject.getJSONObject("country_data");
                                country_name = country_data.getString("country_name");
                            }

                            txt_garage_name.setText(garage_name);
                            txt_garage_address.setText(address);
                            txt_garage_city.setText(city);
                            txt_garage_pincode.setText(pincode);
                            txt_garage_state.setText(state);
                            txt_garage_country.setText(country_name);

                            phone_no.setText(contact_no);
                            Glide.with(getApplicationContext()).load(garage_banner).into(img_garage_banner);

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("response", " "+response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params=new HashMap<String, String>();
                params.put("garage_id",garage_id);
                Log.e("params"," " +params);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
