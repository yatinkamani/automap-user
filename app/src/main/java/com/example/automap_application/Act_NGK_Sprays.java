package com.example.automap_application;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class Act_NGK_Sprays extends AppCompatActivity {

    LinearLayout c_cleaning_spray, c_change_ngk, c_change_spray;

    ImageView back1;
    ArrayList<String> object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__ngk__sprays);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<String>) args.getSerializable("ARRAYLIST");

        back1 = findViewById(R.id.back_ngk_and_sprays);

        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        c_cleaning_spray = findViewById(R.id.card_cleaning_spray);
        c_change_ngk = findViewById(R.id.card_change_ngk);
        c_change_spray = findViewById(R.id.card_change_spray);

        c_cleaning_spray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(1);
                Intent intent = new Intent(Act_NGK_Sprays.this, Act_cleaning_spray.class);
                intent.putExtra("string", string);
                startActivity(intent);
            }
        });

        c_change_ngk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(0);
                Intent intent = new Intent(Act_NGK_Sprays.this, Act_change_NGK.class);
                intent.putExtra("string", string);
                startActivity(intent);
            }
        });

        c_change_spray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(2);
                Intent intent = new Intent(Act_NGK_Sprays.this, Act_change_spray.class);
                intent.putExtra("string", string);
                startActivity(intent);
            }
        });
    }
}
