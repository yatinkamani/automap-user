package com.example.automap_application.interfaces;

public interface RecyclerViewItemClickedCallback {
    void onItemClicked(int position);
}
