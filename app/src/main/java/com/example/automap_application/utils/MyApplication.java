package com.example.automap_application.utils;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.automap_application.R;

import java.util.Locale;

public class MyApplication extends Application {
    private Locale locale = null;
    private static MyApplication mInstance;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate(){

        super.onCreate();
        mInstance = this;
        Context context = getBaseContext();

        /*SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();
        String lang = settings.getString(getString(R.string.pref_language), "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang))
        {
            locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }*/

        //  changeLocale(context);

        /*SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String lang = settings.getString(getString(R.string.pref_language), "ar");
        Resources res = getApplicationContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        if (lang != null) {
            conf.setLocale(new Locale(lang.toLowerCase())); // API 17+ only.
        }
        res.updateConfiguration(conf, dm);*/
    }

    public void changeLocale(Context context) {
        Configuration config = context.getResources().getConfiguration();
        @SuppressLint({"NewApi", "LocalSuppress"})
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String lang = sharedPreferences.getString(context.getString(R.string.pref_language),"ar");
        Log.e("Tag MyApp", "language in MyApp = " + lang);
        if (!(config.locale.getLanguage().equals(lang))) {
            locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            Log.e("MyApp", "Inside if = " + lang);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }
}
