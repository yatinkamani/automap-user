package com.example.automap_application.utils;

import java.util.Map;

/*
 * Created by Kaprat on 26-10-2018.
 */

public interface PlaceDetailsResponse {
    void onPlaceDetailSuccess(boolean isSuccess, Map map);
}