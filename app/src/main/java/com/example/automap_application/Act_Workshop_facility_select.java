package com.example.automap_application;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Facility_Model;
import com.example.automap_application.Model.Services_Model;
import com.example.automap_application.Model.Sub_Service_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Act_Workshop_facility_select extends AppCompatActivity implements View.OnClickListener {

    ImageView back_garage_list;
    LinearLayout lin_dynamic_checkbox;
    List<Services_Model> services_models = new ArrayList<>();
    ArrayList<Facility_Model> facility_result = new ArrayList<>();
    Button btn_submit;
    RecyclerView rl_service;
    JSONArray array = null;
    AdapterService adapterService;
    View lin_error;
    TextView txt_msg;
    Button btn_retry;
    RelativeLayout lin_list;
    String sub_service_id, sub_service_name;
    List<Sub_Service_Model> sub_service_models = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__workshop_facilty_select);

        back_garage_list = findViewById(R.id.back_garage_list);
        btn_submit = findViewById(R.id.btn_submit);
        lin_dynamic_checkbox = findViewById(R.id.lin_dynamic_checkbox);
        back_garage_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rl_service = findViewById(R.id.rl_service);
        lin_error = findViewById(R.id.lin_error);
        txt_msg = findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);
        lin_list = findViewById(R.id.lin_list);

        get_garage_facility();

        btn_submit.setOnClickListener(this);

        btn_retry.setOnClickListener(this);
    }

    int i = 0;

    // get all workshop service list
    private void get_garage_facility() {

        lin_dynamic_checkbox.removeAllViews();
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "getServiceData", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("garage_facility", " " + response);
                try {

                    lin_dynamic_checkbox.removeAllViews();
                    JSONObject obj = new JSONObject(response);
                    sub_service_models = new ArrayList<>();
                    if (obj.getString("status").equals("true")) {

                        JSONArray jsonArray = obj.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String sub_service_id = jsonObject.optString("subservice_id", " ");
                            String sub_service_name = jsonObject.optString("subservice_name", " ");
                            String service_image = jsonObject.optString("service_image", " ");
                            String service_id = jsonObject.optString("service_id", " ");
                            String service_name = jsonObject.optString("service_name", " ");
                            String service_provider = jsonObject.optString("service_provider", " ");

                            if (service_provider.contains(getString(R.string.parametr_workshop_list))) {
//                                sub_service_models.get(i).setSub_service_id(sub_service_id);
//                                sub_service_models.get(i).setSub_service_name(sub_service_name);
//                                sub_service_models.get(i).setService_id(service_id);
//                                sub_service_models.get(i).setService_img(service_image);
                                sub_service_models.add(new Sub_Service_Model(service_id,service_name,sub_service_id,sub_service_name,service_image,service_provider));
                            }
                        }
                        if (sub_service_models != null && sub_service_models.size() > 0) {

                            adapterService = new AdapterService(sub_service_models, getApplicationContext());
                            rl_service.setLayoutManager(new GridLayoutManager(Act_Workshop_facility_select.this, 2));
                            rl_service.setAdapter(adapterService);
                            lin_error.setVisibility(View.GONE);
                            lin_list.setVisibility(View.VISIBLE);

                        } else {
                            lin_error.setVisibility(View.VISIBLE);
                            lin_list.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.no_data_found));
                            btn_retry.setVisibility(View.GONE);
                        }

                        /*for (int i=0;i<services_models.size();i++){

                            AppCompatCheckBox checkBox1 = new AppCompatCheckBox(Act_Workshop_facility_select.this);
                            checkBox1.setText(Html.fromHtml(services_models.get(i).getFacility_name()));
                            int states[][] = {{android.R.attr.state_checked}, {}};
                            int colors[] = {Color.RED, Color.WHITE};
                            CompoundButtonCompat.setButtonTintList(checkBox1, new ColorStateList(states, colors));
                            LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                            checkBox1.setLayoutParams(par);
                            checkBox1.setTextColor(Color.WHITE);

                            int finalI = i;
                            checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked){
                                        facility_result.add(new Facility_Model(facility_models.get(finalI).getFacility_id(),"","","","",""));
                                    }else {

                                        List<Facility_Model> newsList = facility_result;
                                        Facility_Model facility_model = null;
                                        for (Facility_Model facility_model1 : newsList){
                                            if (facility_model1.getFacility_id() == facility_models.get(finalI).getFacility_id()){
                                                facility_model = facility_model1;
                                            }
                                        }
                                        if (facility_model != null){
                                            newsList.remove(facility_model);
                                        }
                                    }
                                }
                            });
                            lin_dynamic_checkbox.addView(checkBox1);

                        }*/
                    } else {
                        lin_list.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                        btn_retry.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    lin_list.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.GONE);
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Act_Garage_List.this, "my error :" + error, Toast.LENGTH_LONG).show();
                lin_list.setVisibility(View.GONE);
                lin_error.setVisibility(View.VISIBLE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                btn_retry.setVisibility(View.GONE);
                Log.e("My error", "" + error);
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("service_provider", getString(R.string.parametr_workshop_list));
                Log.e("Tag params", map.toString());
                return map;

            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }

    int[] Image = new int[]{R.drawable.change_abs, R.drawable.dry_clean, R.drawable.b_break, R.drawable.b_wash, R.drawable.r_wheel};
    Random random = new Random();

    @Override
    public void onClick(View v) {
        if (v == btn_retry) {
            get_garage_facility();
        } else if (v == btn_submit) {
            if (facility_result.size() > 0) {
                Toast.makeText(getApplicationContext(), getString(R.string.done), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), Act_Workshop_List.class);
                intent.putExtra("search", true);
                intent.putExtra(getString(R.string.title), getString(R.string.workshop_list));
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.please_select_workshop_speciality), Toast.LENGTH_LONG).show();
            }
        }
    }

    class AdapterService extends RecyclerView.Adapter<AdapterService.MyViewHolder> {

        //   List<Facility_Model> facility_model;
        List<Sub_Service_Model> services_models;
        Context context;

        AdapterService(List<Sub_Service_Model> services_models, Context context) {
            this.services_models = services_models;
            this.context = context;
        }

        @NonNull
        @Override
        public AdapterService.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_service_list, viewGroup, false);

            return new AdapterService.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AdapterService.MyViewHolder holder, int i) {

            /*if (services_models.get(i).getService_name().contains("All")) {
                holder.service_name.setText(Html.fromHtml(services_models.get(i).getService_name()));
            } else if (services_models.get(i).getService_name().equals("Mechanic shop") || services_models.get(i).getService_id().equals("18")) {
                holder.service_name.setText(Html.fromHtml("All Workshop List"));
            } else if (services_models.get(i).getService_name().equals("Auto parts shop") || services_models.get(i).getService_id().equals("4")) {
                holder.service_name.setText(Html.fromHtml("All Auto parts Companies"));
            } else if (services_models.get(i).getService_name().equals("Car Wash & oil change") || services_models.get(i).getService_id().equals("9")) {
                holder.service_name.setText(Html.fromHtml("All Car Wash & change oil Station"));
            } else if (services_models.get(i).getService_name().equals("Auto glass") || services_models.get(i).getService_id().equals("14")) {
                holder.service_name.setText(Html.fromHtml("All Auto Glass Companies"));
            } else if (services_models.get(i).getService_name().equals("Auto accessories") || services_models.get(i).getService_id().equals("15")) {
                holder.service_name.setText(Html.fromHtml("All Auto Accessories Suppliers"));
            } else {
                holder.service_name.setText(Html.fromHtml("All " + services_models.get(i).getService_name()));
            }*/
            holder.service_name.setText(Html.fromHtml(services_models.get(i).getSub_service_name()));

            if (services_models.get(i).getService_img() != null) {
                if (i == 0)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.c_ngk).into(holder.service_image);
                else if (i == 1)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.dry_clean).into(holder.service_image);
                else if (i == 2)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.b_break).into(holder.service_image);
                else if (i == 3)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.b_wash).into(holder.service_image);
                else if (i == 4)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.change_abs).into(holder.service_image);
                else if (i == 5)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.cleaning_spray).into(holder.service_image);
                else if (i == 6)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.engine_oil_service).into(holder.service_image);
                else if (i == 7)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.gear_oil_service).into(holder.service_image);
                else if (i == 8)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.f_break).into(holder.service_image);
                else if (i == 9)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.polish).into(holder.service_image);
                else if (i == 10)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.power_oil_service).into(holder.service_image);
                else if (i == 11)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.r_wheel).into(holder.service_image);
                else
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(Image[(int) (Math.random() * Image.length)]).into(holder.service_image);
            } else {
                holder.service_image.setImageResource(Image[(int) (Math.random() * Image.length)]);
            }

            holder.lin_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /* facility_result = new ArrayList<>();
                       Log.e("Log",""+services_models.get(i).getFacility_id());
                       facility_result = services_models;*/
                    array = new JSONArray();
                    for (int j = 0; j < 1; j++) {
//                        Log.e("Tag", ""+facility_result.size());
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("service_id", services_models.get(i).getService_id());
                            array.put(obj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Intent intent = new Intent(getApplicationContext(), Act_Workshop_List.class);
                    intent.putExtra("facility_json", array.toString());
                    intent.putExtra("service_id", services_models.get(i).getService_id());
                    intent.putExtra("service_name", services_models.get(i).getService_name());
                    intent.putExtra("search", true);
                    intent.putExtra(getString(R.string.title), holder.service_name.getText().toString());
                    intent.putExtra("type", "Special Service");
                    //                    if (services_models.get(i).getService_type().contains("Special Service")) {
//                    } else {
//                        intent.putExtra("type", "General Service");
//                    }
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return services_models.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView service_name;
            private ImageView service_image;
            LinearLayout lin_main;

            MyViewHolder(@NonNull View itemView) {
                super(itemView);

                service_image = itemView.findViewById(R.id.service_image);
                lin_main = itemView.findViewById(R.id.lin_main);
                service_name = itemView.findViewById(R.id.service_name);

            }
        }
    }
}
