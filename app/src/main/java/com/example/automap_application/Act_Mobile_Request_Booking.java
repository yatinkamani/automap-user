package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;

import androidx.core.view.GravityCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.cardview.widget.CardView;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.backgroundServices.RemainderBroadcast;
import com.example.automap_application.utils.AppUtils;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Act_Mobile_Request_Booking extends AppCompatActivity implements View.OnClickListener {

    TextView txt_title;
    LinearLayout lin_oil, lin_radio, lin_other, lin_add_checkbox, lin_patrol_amount, lin_location;
    EditText ED_oil_brand, ED_oil_sae, ED_other, ED_amount;
    TextView txt_radio_group, txt_location, txt_date_time;
    RadioGroup rg_seats;
    CardView btn_booking_appointment, btn_come_now;
    ImageView back_mobile_request;
    String service_json, service_name, service_id;
    ArrayList<String> request = new ArrayList<>();
    ArrayList<String> selected = new ArrayList<>();
    String radioCheck = "", date = "", time = "", type = "";
    boolean isMultiple = false;
    private int month, year, dayOfMonth, hourOfDay, minute;
    String lat, log;
    boolean isSelect = false, isTime = false;

    String sub_service_id = "", sub_service_name = "", loc_address = "";
    private int PLACE_PICKER_REQUEST = 1;

    ScrollView scroll_view;
    LinearLayout lin_error;
    Button btn_retry;
    TextView txt_error;

    @SuppressLint("StaticFieldLeak")
    public Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__booking__mobile__request);

        instance = this;

        txt_title = findViewById(R.id.txt_title);
        lin_oil = findViewById(R.id.lin_oil);
        lin_radio = findViewById(R.id.lin_radio);
        lin_other = findViewById(R.id.lin_other);
        lin_add_checkbox = findViewById(R.id.lin_add_checkbox);
        lin_patrol_amount = findViewById(R.id.lin_patrol_amount);
        lin_location = findViewById(R.id.lin_location);
        ED_oil_brand = findViewById(R.id.ED_oil_brand);
        ED_oil_sae = findViewById(R.id.ED_oil_sae);
        ED_other = findViewById(R.id.ED_other);
        ED_amount = findViewById(R.id.ED_amount);
        txt_radio_group = findViewById(R.id.txt_radio_group);
        rg_seats = findViewById(R.id.rg_seats);
        btn_booking_appointment = findViewById(R.id.btn_book_appointment);
        btn_come_now = findViewById(R.id.btn_come_now);
        back_mobile_request = findViewById(R.id.back_mobile_request);
        txt_location = findViewById(R.id.txt_location);
        txt_date_time = findViewById(R.id.txt_date_time);

        scroll_view = findViewById(R.id.scroll_view);
        txt_error = findViewById(R.id.txt_msg);
        lin_error = findViewById(R.id.lin_error);
        btn_retry = findViewById(R.id.btn_retry);

        back_mobile_request.setOnClickListener(this);
        btn_booking_appointment.setOnClickListener(this);
        btn_come_now.setOnClickListener(this);
        txt_location.setOnClickListener(this);
        txt_date_time.setOnClickListener(this);
        btn_retry.setOnClickListener(this);

        getIntentData();
        init();

        txt_location.setText(loc_address != null ? loc_address : getString(R.string.select_location));
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        date = String.valueOf(year) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(dayOfMonth);
        time = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);

        @SuppressLint("SimpleDateFormat")
        String s_date = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH).format(System.currentTimeMillis());
        txt_date_time.setText(s_date);
        Log.e("Tag", s_date);

        Log.e("Tag time date", date + " " + time);

        MapUtility.apiKey = getString(R.string.google_maps_key);

    }

    // select service id wise visible view
    public void init() {

        CheckForAlarm(service_id);
        getServiceOption();
        txt_title.setText(Html.fromHtml(service_name));
        if (sub_service_name.equalsIgnoreCase("Request Mobile Maintenance") ||
            sub_service_id.equals("39")) {
            /*request = new ArrayList<>();
            request.add(getString(R.string.need_engin_oil));
            request.add(getString(R.string.need_break));
            request.add(getString(R.string.need_spark_plugs));
            request.add(getString(R.string.need_battery_12v));
            request.add(getString(R.string.need_charge_electric_car));
            request.add(getString(R.string.vehicle_is_not_start));*/
            lin_other.setVisibility(View.VISIBLE);
            isMultiple = true;
        } else if (sub_service_name.equalsIgnoreCase("Request Mobile Dry-clean")
        || sub_service_id.equals("41")) {
            /*request = new ArrayList<>();
            request.add(getString(R.string.need_detailing));
            request.add(getString(R.string.need_polish));
            request.add(getString(R.string.need_nono_ceramic));*/
            lin_radio.setVisibility(View.VISIBLE);
            txt_radio_group.setText(getString(R.string.leather_seats));
            lin_other.setVisibility(View.VISIBLE);
            isMultiple = true;
        } else if (sub_service_name.equalsIgnoreCase("Request Mobile Gas supply") ||
        sub_service_id.equals("43")) {
            /*request = new ArrayList<>();
            request.add(getString(R.string.need_gas));
            request.add(getString(R.string.need_diesel));*/
            lin_other.setVisibility(View.VISIBLE);
            isMultiple = false;
        } else if (sub_service_name.equalsIgnoreCase("Request Key Specialist") ||
        sub_service_id.equals("44")) {
            /*request = new ArrayList<>();
            request.add(getString(R.string.key_locked_in_vehicle));
            request.add(getString(R.string.key_locked_in_vehicle));
            request.add(getString(R.string.duplicate_smart_key));*/
            lin_other.setVisibility(View.VISIBLE);
            isMultiple = true;
        } else if (sub_service_name.equalsIgnoreCase("Request mobile oil service")
        || sub_service_id.equals("40")) {
            /*request = new ArrayList<>();
            request.add(getString(R.string.need_spark_plugs));
            request.add(getString(R.string.need_engine_oil));*/
            lin_other.setVisibility(View.VISIBLE);
            lin_radio.setVisibility(View.VISIBLE);
            txt_radio_group.setText(getString(R.string.do_you_want_to_the_filter));
            isMultiple = true;
        } else if (sub_service_name.equalsIgnoreCase("Request Mobile wheels service")
        || sub_service_id.equals("42")) {
            /*request.add(getString(R.string.need_spare_wheel_change));
            request.add(getString(R.string.i_have_not_spare_wheel));*/
            lin_other.setVisibility(View.VISIBLE);
            isMultiple = true;
        } else {
            lin_other.setVisibility(View.VISIBLE);
        }

        if (rg_seats.getCheckedRadioButtonId() == R.id.radioYes) {
            isSelect = true;
            if (service_name.equalsIgnoreCase("Dry clean,detailing polish,nano -ceramic") ||
            service_id.equals("16")) {
                radioCheck = getString(R.string.leather_seats);
            } else {
                radioCheck = getString(R.string.filters);
            }
        } else {
            isSelect = false;
            if (service_name.equalsIgnoreCase("Dry clean,detailing polish,nano -ceramic")
            || service_id.equals("16")) {
                radioCheck = getString(R.string.leather_seats);
            } else {
                radioCheck = getString(R.string.filters);
            }
        }
    }

    // listing dynamic service check box
    public void DynamicCheckbox() {

        if (request != null && request.size() > 0) {

            lin_add_checkbox.removeAllViews();
            for (int i = 0; i < request.size(); i++) {

                CheckBox checkBox = new CheckBox(this);
                checkBox.setText(request.get(i));
                ColorFilter colorFilter = new PorterDuffColorFilter(Color.CYAN, PorterDuff.Mode.SRC_ATOP);
                Drawable drawable = CompoundButtonCompat.getButtonDrawable(checkBox);
                if (drawable != null) {
                    drawable.setColorFilter(colorFilter);
                }
                checkBox.setTextColor(getResources().getColor(R.color.white));
                LinearLayout linearLayout = new LinearLayout(this);
                linearLayout.setVisibility(View.GONE);
                lin_add_checkbox.addView(checkBox);
                if (request.get(i).equalsIgnoreCase("need break")) {
                    AddBreakView(linearLayout, checkBox);
                    lin_add_checkbox.addView(linearLayout);
                }
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                        if (isMultiple) {
                            if (b) {
                                if (!selected.contains(selected))
                                    selected.add(compoundButton.getText().toString());
                            } else {
                                selected.remove(compoundButton.getText().toString());
                            }
                        } else {
                            selected = new ArrayList<>();
                            for (int j = 0; j < lin_add_checkbox.getChildCount(); j++) {
                                if (lin_add_checkbox.getChildAt(j) instanceof CheckBox)
                                    ((CheckBox) lin_add_checkbox.getChildAt(j)).setChecked(false);
                            }

                            if (b) {
                                if (!selected.contains(selected))
                                    selected.add(compoundButton.getText().toString());
                                checkBox.setChecked(true);
                            } else {
                                selected.remove(compoundButton.getText().toString());
                            }
                        }

                        if (b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("need engine oil") ||
                                compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("زيت محرك")) {
                            lin_oil.setVisibility(View.VISIBLE);
                        } else if (!b && compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("need engine oil") ||
                                compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("زيت محرك")) {
                            lin_oil.setVisibility(View.GONE);
                            ED_oil_brand.setText("");
                            ED_oil_sae.setText("");
                        }

                        if (b && (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("other")) ||
                                (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("others")) ||
                                (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("الآخرين"))) {
                            lin_other.setVisibility(View.VISIBLE);
                        } else if (!b && (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("others")) ||
                                (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("other")) ||
                                (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("الآخرين"))) {
                            lin_other.setVisibility(View.GONE);
                            ED_other.setText("");
                        }

                        if (b && (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("need gas") ||
                                compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("need diesel") ||
                                compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("تحتاج ديزل"))) {
                            lin_patrol_amount.setVisibility(View.VISIBLE);
                        } else if (!b && (compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("need gas") ||
                                compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("need diesel") ||
                                compoundButton.getText().toString().toLowerCase().equalsIgnoreCase("تحتاج ديزل"))) {
                            lin_patrol_amount.setVisibility(View.GONE);
                            ED_amount.setText("");
                        }
                    }
                });
            }
        }
    }

    // get service option and set check box view
    public void getServiceOption() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            ProgressDialog dialog = new ProgressDialog(Act_Mobile_Request_Booking.this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "getServiceOption", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Tag Service Option", response);

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    selected = new ArrayList<>();
                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equals("1")) {

                            lin_error.setVisibility(View.GONE);
                            scroll_view.setVisibility(View.VISIBLE);
                            JSONArray array = object.getJSONArray("result");
                            if (array.length()>0){

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    request.add(object1.getString("option_name"));
                                    isMultiple = object1.getString("multiple_option").equals("1") ? true : false;
                                }
                                Log.e("Tag", "" + isMultiple);
                                DynamicCheckbox();
                                lin_add_checkbox.setVisibility(View.VISIBLE);
                            }else {
                                lin_add_checkbox.setVisibility(View.GONE);
                            }
                        } else {
                            lin_add_checkbox.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        lin_error.setVisibility(View.VISIBLE);
                        scroll_view.setVisibility(View.GONE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_error.setText(getString(R.string.somting_wrong_please));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    Log.e("tag Error", error.toString());
                    lin_error.setVisibility(View.VISIBLE);
                    scroll_view.setVisibility(View.GONE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_error.setText(getString(R.string.somting_wrong_please));
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("subservice_id", sub_service_id);
                    Log.e("tag Params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(stringRequest);

        } else {
            lin_error.setVisibility(View.VISIBLE);
            scroll_view.setVisibility(View.GONE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_error.setText(getString(R.string.internet_not_connect));
        }
    }

    // add sub check box view
    public void AddBreakView(@NotNull LinearLayout linearLayout, CheckBox checkBox) {

        //  LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 0, 0, 0);
        linearLayout.setPadding(20, 0, 0, 0);

        ArrayList<String> breaks = new ArrayList<>();
        breaks.add(getString(R.string.front_break));
        breaks.add(getString(R.string.Back_break));

        for (int j = 0; j < breaks.size(); j++) {
            CheckBox checkBox1 = new CheckBox(this);
            checkBox1.setText("" + breaks.get(j));
            checkBox1.setTextColor(Color.BLUE);
            if (selected.contains(getString(R.string.need_break))) {
                selected.remove(getString(R.string.need_break));
            }

            if (linearLayout.getVisibility() == View.VISIBLE && !selected.contains(getString(R.string.front_break))) {
                selected.add(getString(R.string.front_break));
            }

            if (linearLayout.getVisibility() == View.VISIBLE && !selected.contains(getString(R.string.Back_break))) {
                selected.add(getString(R.string.Back_break));
            }

            checkBox1.setChecked(true);
            checkBox1.setOnCheckedChangeListener((compoundButton, b) -> {

                if (b && compoundButton.getText().toString().equalsIgnoreCase(getString(R.string.front_break)) && !selected.contains(getString(R.string.front_break))) {
                    selected.add(getString(R.string.front_break));
                } else if (!b && compoundButton.getText().toString().equalsIgnoreCase(getString(R.string.front_break))) {
                    selected.remove(getString(R.string.front_break));
                }

                if (b && compoundButton.getText().toString().equalsIgnoreCase(getString(R.string.Back_break)) && !selected.contains(getString(R.string.front_break))) {
                    selected.add(getString(R.string.Back_break));
                } else if (!b && compoundButton.getText().toString().equalsIgnoreCase(getString(R.string.Back_break))) {
                    selected.remove(getString(R.string.Back_break));
                }

                boolean isAllCheck = false;
                for (int c = 0; c < linearLayout.getChildCount(); c++) {
                    CheckBox box = ((CheckBox) linearLayout.getChildAt(c));

                    if (box.isChecked()) {
                        isAllCheck = true;
                        break;
                    }
                }
                if (!isAllCheck) {
                    linearLayout.setVisibility(View.GONE);
                    checkBox.setChecked(false);
                }

            });
            linearLayout.addView(checkBox1);
        }
    }

    // get selected data
    public void getIntentData() {

        service_json = getIntent().getStringExtra("service_json");
        type = getIntent().getStringExtra("type");
        sub_service_id = getIntent().getStringExtra("sub_service_id");
        sub_service_name = getIntent().getStringExtra("sub_service_name");
        lat = getIntent().getStringExtra("cur_lat");
        log = getIntent().getStringExtra("cur_lng");
        loc_address = getIntent().getStringExtra("loc_address");
        try {
            JSONArray jsonArray = new JSONArray(service_json);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                service_name = object.getString("service_name");
                service_id = object.getString("service_id");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void date_pick() {

        Locale.setDefault(Locale.ENGLISH);
        @SuppressLint("SimpleDateFormat")
        DatePickerDialog dialog = new DatePickerDialog(Act_Mobile_Request_Booking.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Log.e("year", "" + year);
                Log.e("month", "" + month);
                Log.e("dayOfMonth", "" + dayOfMonth);
                final String dates = String.valueOf(year) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(dayOfMonth);
                date = dates;
                Log.e("date", "" + date);
                time_pick();
            }
        }, year, month, dayOfMonth);
        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dialog.show();
    }

    public void time_pick() {
        @SuppressLint("SimpleDateFormat") final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        final Date[] dateObj = new Date[1];

        Locale.setDefault(Locale.ENGLISH);
        TimePickerDialog timePickerDialog = new TimePickerDialog(Act_Mobile_Request_Booking.this, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Log.e("hourOfDay", "" + hourOfDay);
                Log.e("minute", "" + minute);
                final String times = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                try {
                    dateObj[0] = sdf.parse(times);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int hour = hourOfDay;
                int minutes = minute;
                String timeSet = "";
                if (hour > 12) {
                    hour -= 12;
                    timeSet = "PM";
                } else if (hour == 0) {
                    hour += 12;
                    timeSet = "AM";
                } else if (hour == 12) {
                    timeSet = "PM";
                } else {
                    timeSet = "AM";
                }

                String min = "";
                if (minutes < 10)
                    min = "0" + minutes;
                else
                    min = String.valueOf(minutes);

                String aTime = String.valueOf(hour) + ':' + min + " " + timeSet;
                time = times;
                txt_date_time.setText(date + " " + aTime);
                Log.e("minute", "" + time);

                if (isTime) {
                    sendIntent();
                    isTime = false;
                }

                //  time = new SimpleDateFormat("hh:mm aa").format(dateObj[0]);
            }
        }, hourOfDay, minute, false);

        timePickerDialog.show();
    }

    @Override
    public void onClick(View view) {
        if (view == back_mobile_request) {
            onBackPressed();
        } else if (view == btn_come_now) {

            SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
            String sendDate = sp.getString(service_id, "");
            @SuppressLint("SimpleDateFormat")
            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            //  date = numberFormat.format(date);

            Log.e("Tag Date", sendDate + "\n" + date);
            Log.e("Tag Date", service_id);

                /*if (date.equals(sendDate)) {

                Toast toast = Toast.makeText(this, "you can use mobile request for one time in day", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                } else {*/
                /*if (!ED_other.getText().toString().equals("")) {
                    if (selected.contains(ED_other.getText().toString()))
                        selected.add(ED_other.getText().toString());
                }*/

            if (selected.toString().replaceAll("\\[|\\]", "").toString().contains(getString(R.string.others)) ||
                    selected.toString().replaceAll("\\[|\\]", "").toString().contains("other")) {
                selected.add(ED_other.getText().toString());
            } else if (!ED_other.getText().toString().trim().equalsIgnoreCase("")) {
                selected.add(ED_other.getText().toString());
            }

            String appointment_type = selected.toString().replaceAll("\\[|\\]", "");

            if (selected != null && selected.size() < 1 && lin_other.getVisibility() == View.VISIBLE) {
                Toast.makeText(this, getString(R.string.require_servicec_type), Toast.LENGTH_SHORT).show();
            } else if (txt_location.getText().toString().equalsIgnoreCase("Select Location")) {
                Toast.makeText(this, getString(R.string.selcte_final_location), Toast.LENGTH_LONG).show();
            } else if (lin_oil.getVisibility() == View.VISIBLE && ED_oil_brand.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(this, getString(R.string.enter_oil_brand), Toast.LENGTH_LONG).show();
            } else if (lin_oil.getVisibility() == View.VISIBLE && ED_oil_sae.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(this, getString(R.string.enter_oil_sea), Toast.LENGTH_LONG).show();
            } else if (lin_patrol_amount.getVisibility() == View.VISIBLE && ED_amount.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(this, getString(R.string.enter_patrol_amount), Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent(getApplicationContext(), Act_Mobile_Request_Workshop_List.class);
                intent.putExtra("come_now", "1");
                intent.putExtra("service_json", service_json);
                intent.putExtra("time", time);
                intent.putExtra("date", date);
                intent.putExtra("oil_brand", ED_oil_brand.getText().toString());
                intent.putExtra("oil_sae", ED_oil_sae.getText().toString());
                intent.putExtra("appointment_type", appointment_type);
                intent.putExtra("amount", ED_amount.getText().toString());
                intent.putExtra("lat", lat);
                intent.putExtra("log", log);
                intent.putExtra("type", type);
                intent.putExtra("location", txt_location.getText().toString());
                if (radioCheck.equalsIgnoreCase(getString(R.string.leather_seats))) {
                    if (isSelect) {
                        intent.putExtra("leather_seat", "True");
                        intent.putExtra("change_filter", "True");
                    } else {
                        intent.putExtra("change_filter", "False");
                        intent.putExtra("leather_seat", "False");
                    }
                } else if (radioCheck.equalsIgnoreCase(getString(R.string.filters))) {
                    if (isSelect) {
                        intent.putExtra("leather_seat", "False");
                        intent.putExtra("change_filter", "True");
                    } else {
                        intent.putExtra("leather_seat", "False");
                        intent.putExtra("change_filter", "False");
                    }
                }
                Log.e("Tag Services", service_json + "\n" + appointment_type + "\n" + time + "\n" + date + "\n" + ED_oil_brand.getText().toString() + "\n"
                        + ED_oil_sae.getText().toString() + "\n" + ED_amount.getText().toString() + "\n" + ED_amount.getText().toString()
                        + "\n" + lat + "\n" + log + "\n" + txt_location.getText().toString() + "\n" + radioCheck);
                startActivity(intent);
            }
//            }

        } else if (view == btn_booking_appointment) {

            BookRequest();
            isTime = true;

        } else if (view == txt_location) {
            Intent intent = new Intent(Act_Mobile_Request_Booking.this, LocationPickerActivity.class);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);
        } else if (view == txt_date_time) {
            date_pick();
            isTime = false;
        } else if (view == btn_retry) {
            getServiceOption();
        }
    }

    // book mobile request validation
    String appointment_type = "";
    public void BookRequest() {

        SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
        String sendDate = sp.getString(service_id, "");
        @SuppressLint("SimpleDateFormat")
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        //     date = numberFormat.format(date);

        Log.e("Tag Date", sendDate + "\n" + date);
        Log.e("Tag Date", service_id);

        /*if (date.equals(sendDate)) {

            Toast toast = Toast.makeText(Act_Mobile_Request_Booking.this, "You use this mobile request for next Day", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

        } else {*/
        /*if (!ED_other.getText().toString().equals("")) {
                if (selected.contains(ED_other.getText().toString()))
                    selected.add(ED_other.getText().toString());
        }*/

        if (selected.toString().replaceAll("\\[|\\]", "").toString().contains(getString(R.string.others)) ||
                selected.toString().replaceAll("\\[|\\]", "").toString().contains("other")) {
            selected.add(ED_other.getText().toString());
        } else if (!ED_other.getText().toString().trim().equalsIgnoreCase("")) {
            selected.add(ED_other.getText().toString());
        }

        appointment_type = selected.toString().replaceAll("\\[|\\]", "").trim();

        if (selected != null && selected.size() < 1) {
            Toast.makeText(this, getString(R.string.select_service_type), Toast.LENGTH_SHORT).show();
        } else if (txt_location.getText().toString().equalsIgnoreCase(getString(R.string.select_location))) {
            Toast.makeText(this, getString(R.string.selcte_final_location), Toast.LENGTH_LONG).show();
        } else if (lin_oil.getVisibility() == View.VISIBLE && ED_oil_brand.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, getString(R.string.enter_oil_brand), Toast.LENGTH_LONG).show();
        } else if (lin_oil.getVisibility() == View.VISIBLE && ED_oil_sae.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, getString(R.string.enter_oil_sea), Toast.LENGTH_LONG).show();
        } else if (lin_patrol_amount.getVisibility() == View.VISIBLE && ED_amount.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, getString(R.string.enter_patrol_amount), Toast.LENGTH_LONG).show();
        } else {
            date_pick();
        }
    }

    // selected  data send next screen
    public void sendIntent() {
        Intent intent = new Intent(getApplicationContext(), Act_Mobile_Request_Workshop_List.class);
        intent.putExtra("come_now", "2");
        intent.putExtra("service_json", service_json);
        intent.putExtra("time", time);
        intent.putExtra("date", date);
        intent.putExtra("oil_brand", ED_oil_brand.getText().toString());
        intent.putExtra("oil_sae", ED_oil_sae.getText().toString());
        intent.putExtra("amount", ED_amount.getText().toString());
        intent.putExtra("appointment_type", appointment_type);
        intent.putExtra("lat", lat);
        intent.putExtra("log", log);
        intent.putExtra("type", type);
        intent.putExtra("location", txt_location.getText().toString());
        if (radioCheck.equalsIgnoreCase(getString(R.string.leather_seats))) {
            if (isSelect) {
                intent.putExtra("leather_seat", "True");
                intent.putExtra("change_filter", "");
            } else {
                intent.putExtra("leather_seat", "False");
                intent.putExtra("change_filter", "");
            }
        } else if (radioCheck.equalsIgnoreCase(getString(R.string.filters))) {
            if (isSelect) {
                intent.putExtra("change_filter", "True");
                intent.putExtra("leather_seat", "");
            } else {
                intent.putExtra("change_filter", "False");
                intent.putExtra("leather_seat", "");
            }
        }
        Log.e("Tag Services", service_json + "\n" + appointment_type + "\n" + time + "\n" + date + "\n" + ED_oil_brand.getText().toString() + "\n"
                + ED_oil_sae.getText().toString() + "\n" + ED_amount.getText().toString() + "\n" + ED_amount.getText().toString()
                + "\n" + lat + "\n" + log + "\n" + txt_location.getText().toString() + "\n" + radioCheck);
        startActivity(intent);
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.e("My Current location", strReturnedAddress.toString());
            } else {
                Log.e("My Current location", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("My Current location", "Canont get Address!");
        }
        return strAdd;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double selectedLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                    double selectedLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);

                    getCompleteAddressString(selectedLatitude, selectedLongitude);
                    if (address.equals("" + selectedLatitude + "," + selectedLongitude)) {
                        txt_location.setText(getCompleteAddressString(selectedLatitude, selectedLongitude));
                    } else {
                        txt_location.setText(address);
                    }
                    txt_location.setSelected(true);
                    txt_location.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                    txt_location.setMarqueeRepeatLimit(-1);
                    lat = "" + selectedLatitude;
                    log = "" + selectedLongitude;
//                    txtLatLong.setText("Lat:"+selectedLatitude+"  Long:"+selectedLongitude);
                    Log.e("Tag Address", address);
                    Log.e("TagLat Log", " " + selectedLatitude + "  " + selectedLongitude);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    // request is send set remainder pending
    private void CheckForAlarm(String id) {
        // checking if alarm is working with pendingIntent #3
        Intent intent = new Intent(getApplicationContext(), RemainderBroadcast.class);//the same as up
        boolean isWorking = (PendingIntent.getBroadcast(getApplicationContext(), Integer.parseInt(id), intent, PendingIntent.FLAG_NO_CREATE) != null);//just changed the flag
        Log.e("TAG: TEST APP:  ", "alarm is " + (isWorking ? "" : "not") + " working...");

    }

}
