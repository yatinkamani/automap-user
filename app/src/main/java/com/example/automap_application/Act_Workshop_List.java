package com.example.automap_application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.Garage_Model;
import com.example.automap_application.Model.Workshop.Workshop;
import com.example.automap_application.adapter.WorkshopListAdapter;
import com.example.automap_application.extra.EqualSpacingItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Workshop_List extends AppCompatActivity implements WorkshopListAdapter.customButtonListener, View.OnClickListener {

    ImageView back_workshop_list, img_sort;
    TextView toolbar_title;
    RecyclerView rl_garage;
    String garage_name;
    String garage_id;
    String garage_banner;
    String facebook_link;
    ArrayList<Workshop> workshop_list = new ArrayList<>();
    ProgressDialog progressDialog;
    Intent mainIntent;

    LinearLayout lin_search_workshop, lin_error, lin_list;
    EditText edt_search_workshop;
    TextView txt_msg;
    Button btn_retry;
    workshopAdapter garageListAdapter;
    ArrayList<Garage_Model> garage_list = new ArrayList<>();

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String user_id = "", model_id = "", brand_id = "", facility_json = "", service_id = "", service_name = "", type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshop_list);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));

        preferences = getSharedPreferences(LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id), "");
        model_id = preferences.getString(getString(R.string.pref_car_model_id), "");
        brand_id = preferences.getString(getString(R.string.pref_car_brand_id), "");

        mainIntent = getIntent();
        rl_garage = findViewById(R.id.rl_garage);
        back_workshop_list = findViewById(R.id.back_workshop_list);
        img_sort = findViewById(R.id.img_sort);
        toolbar_title = findViewById(R.id.toolbar_title);
        lin_search_workshop = findViewById(R.id.lin_search_workshop);
        edt_search_workshop = findViewById(R.id.edt_search_workshop);
        lin_error = findViewById(R.id.lin_error);
        txt_msg = findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);
        lin_list = findViewById(R.id.lin_list);
        facility_json = mainIntent.getStringExtra("facility_json");
        service_id = mainIntent.getStringExtra("service_id");
        service_name = mainIntent.getStringExtra("service_name");
        type = mainIntent.getStringExtra("type");

        toolbar_title.setText(mainIntent.getStringExtra(getString(R.string.title)));
        getIntentData();
        if (mainIntent.getBooleanExtra("search", false)) {
            lin_search_workshop.setVisibility(View.VISIBLE);
        } else {
            lin_search_workshop.setVisibility(View.GONE);
        }

        edt_search_workshop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                final ArrayList<Garage_Model> filteredModelList = filter(garage_list, s.toString());
                if (filteredModelList != null && filteredModelList.size() > 0) {
                    garageListAdapter.setFilter(filteredModelList);
                    rl_garage.scrollToPosition(0);
                    rl_garage.setVisibility(View.VISIBLE);
                    lin_error.setVisibility(View.GONE);
                } else {
                    lin_error.setVisibility(View.VISIBLE);
                    rl_garage.setVisibility(View.GONE);
                    txt_msg.setText(getString(R.string.no_search_result));
                    btn_retry.setVisibility(View.GONE);
                }
            }
        });

        btn_retry.setOnClickListener(this);
        back_workshop_list.setOnClickListener(this);
        img_sort.setOnClickListener(this);

        Log.e("tag service name", service_name.toLowerCase());
        Log.e("tag service name", ""+service_name.toLowerCase().matches(".* tow *."));
        Log.e("tag service name", ""+service_name.toLowerCase().matches(".*tow*."));
        Log.e("tag service name", ""+service_name.toLowerCase().matches(".*tow trucks*."));

        // service name towtruck
        if (service_name.toLowerCase().matches(".*tow trucks*.") || service_name.toLowerCase().equalsIgnoreCase("tow trucks") || service_name.toLowerCase().equalsIgnoreCase("tow trucks")) {
            getTowTrackList();
        } else {
            getGarageList();
        }
    }

    private void getIntentData() {
        service_name = getIntent().getStringExtra("service_name");
        service_id = getIntent().getStringExtra("service_id");
    }

    /*private void getWorkShopList() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL+"get_workshop_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Tag response",response);
                try {
                    JSONObject mainJsonObject = new JSONObject(response);
                    JSONArray result = mainJsonObject.getJSONArray("result");
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject jsonObject=result.getJSONObject(i);
                        garage_name = jsonObject.getString("workshop_name");
                        garage_banner = jsonObject.getString("workshop_banner");
                        workshop_id = jsonObject.getString("workshop_id");
                        //  facebook_link = jsonObject.getString("facebook_link");
                        workshop_list.add(new Workshop(garage_name,garage_banner,workshop_id,facebook_link));
                    }
                    workshopListAdapter = new WorkshopListAdapter(Act_Workshop_List.this, workshop_list);
                    rl_garage.setAdapter(workshopListAdapter);
                    workshopListAdapter.setCustomButtonListener(Act_Workshop_List.this);
                    rl_garage.setLayoutManager(new GridLayoutManager(Act_Workshop_List.this, 2));
                    rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16));
                    rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
                    // 16px. In practice, you'll want to use getDimensionPixelSize
                    if (workshop_list.size()>0){
                        rl_garage.setVisibility(View.VISIBLE);
                        lin_error.setVisibility(View.GONE);
                    }else {
                        rl_garage.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    rl_garage.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_msg.setText("Something went wrong please try again");
                }
                progressDialog.dismiss();
            }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // displaying the error in toast if occurs
//                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    error_dialog();
                }
            }){
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        progressDialog.show();
    }*/

    @Override
    public void onButtonClickListener(String workshop_id, String workshop_name) {
        Intent in = new Intent(getApplicationContext(), Act_Workshop.class);
        in.putExtra("workshop_id", workshop_id);
        in.putExtra("workshop_name", workshop_name);
        startActivity(in);
    }

    private ArrayList<Garage_Model> filter(@NonNull ArrayList<Garage_Model> models, String query) {
        query = query.toLowerCase();
//        final List<Workshop> filteredModelList = new ArrayList<Workshop>();
        final ArrayList<Garage_Model> filteredModelList = new ArrayList<Garage_Model>();

        for (Garage_Model model : models) {

            final String workshop_name = model.getGarage_name().toString().toLowerCase().trim();
            if (!query.equals("")) {
                if (workshop_name.contains(query)) {
                    filteredModelList.add(model);
                }
            } else {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    // get workshop list and display on listView
    private void getGarageList() {

        img_sort.setVisibility(View.GONE);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_workshop_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("garage booking response", response);
                try {
                    workshop_list = new ArrayList<>();
                    String rating = "";
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.getString("status").equals("1")) {
                        JSONArray result = mainJsonObject.getJSONArray("result");
                        if (result.length() > 0) {
                            lin_error.setVisibility(View.GONE);
                            lin_list.setVisibility(View.VISIBLE);
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject jsonObject = result.getJSONObject(i);
                                garage_name = jsonObject.getString("garage_name");
                                garage_banner = jsonObject.getString("garage_banner");
                                garage_id = jsonObject.getString("garage_id");
                                facebook_link = jsonObject.getString("facebook_link");

                                if (jsonObject.has("ratting")){
                                    rating = jsonObject.getString("ratting");
                                }

                                garage_list.add(new Garage_Model(garage_name, garage_banner, garage_id, facebook_link,rating));
                            }
                            garageListAdapter = new workshopAdapter(Act_Workshop_List.this, garage_list);
                            rl_garage.setAdapter(garageListAdapter);
//                            garageListAdapter.setCustomButtonListener(Act_Workshop_List.this);
                            rl_garage.setLayoutManager(new GridLayoutManager(Act_Workshop_List.this, 2));
                            rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16)); // 16px. In practice, you'll want to use getDimensionPixelSize
                            rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
                        } else {
                            lin_list.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.no_data_found));
                            btn_retry.setVisibility(View.GONE);
                        }
                        if (garage_list.size()>1){
                            img_sort.setVisibility(View.VISIBLE);
                        }
                    } else {
                        lin_list.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                        btn_retry.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    lin_list.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//              displaying the error in toast if occurs
//              Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("response", " " + error);
                progressDialog.dismiss();
                lin_list.setVisibility(View.GONE);
                lin_error.setVisibility(View.VISIBLE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
//                error_dialog_send();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("service_id", service_id);
                map.put("user_id", user_id);
                Log.e("Tag Params", map.toString());
                return map;

            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }

    // get towtruck list and display on listView
    private void getTowTrackList() {

        img_sort.setVisibility(View.GONE);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_towtrack_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("garage booking response", response);
                try {
                    workshop_list = new ArrayList<>();
                    String towtrack_name, towtrack_id, towtrack_image, rate = "";
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.getString("status").equals("true")) {
                        JSONArray result = mainJsonObject.getJSONArray("result");
                        if (result.length() > 0) {
                            lin_error.setVisibility(View.GONE);
                            lin_list.setVisibility(View.VISIBLE);
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject jsonObject = result.getJSONObject(i);
                                towtrack_name = jsonObject.getString("towtrack_name");
                                towtrack_image = jsonObject.getString("towtrack_profile_img");
                                towtrack_id = jsonObject.getString("towtrack_id");
//                                facebook_link = jsonObject.getString("facebook_link");

                                if (jsonObject.has("ratting")){
                                    rate = jsonObject.getString("ratting");
                                }

                                garage_list.add(new Garage_Model(towtrack_name, towtrack_image, towtrack_id, "",rate));
                            }
                            garageListAdapter = new workshopAdapter(Act_Workshop_List.this, garage_list);
                            rl_garage.setAdapter(garageListAdapter);
//                            garageListAdapter.setCustomButtonListener(Act_Workshop_List.this);
                            rl_garage.setLayoutManager(new GridLayoutManager(Act_Workshop_List.this, 2));
                            rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16)); // 16px. In practice, you'll want to use getDimensionPixelSize
                            rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
                        } else {
                            lin_list.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.no_data_found));
                            btn_retry.setVisibility(View.GONE);
                        }

                        if (garage_list.size()>1){
                            img_sort.setVisibility(View.VISIBLE);
                        }
                    } else {
                        lin_list.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                        btn_retry.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    lin_list.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//              displaying the error in toast if occurs
//              Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("response", " " + error);
                progressDialog.dismiss();
                lin_list.setVisibility(View.GONE);
                lin_error.setVisibility(View.VISIBLE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
//                error_dialog_send();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", user_id);
                Log.e("Tag Params", map.toString());
                return map;

            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry) {
            if (service_name.toLowerCase().matches(".*tow trucks*.") || service_name.toLowerCase().equalsIgnoreCase("tow trucks") || service_name.toLowerCase().equalsIgnoreCase("tow trucks")) {
                getTowTrackList();
            } else {
                getGarageList();
            }
        } else if (v == back_workshop_list) {
            onBackPressed();
        } else if (v == img_sort){
            SortByRate();
            Toast.makeText(getApplicationContext(),getString(R.string.sort_by_rating),Toast.LENGTH_LONG).show();
        }
    }

    public class workshopAdapter extends RecyclerView.Adapter<workshopAdapter.MyViewHolder> {

        private Activity activity;
        private ArrayList<Garage_Model> garageList;

        workshopAdapter(Activity activity, ArrayList<Garage_Model> garageList) {
            this.activity = activity;
            this.garageList = garageList;
        }

        public void setData(ArrayList<Garage_Model> workshop){
            garageList = new ArrayList<>();
            garageList = workshop;
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return garageList.size();
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_garage_list, parent, false);
            return new workshopAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
            Garage_Model garage_model = garageList.get(position);
            holder.garage_name.setText(garage_model.getGarage_name());
            if (garage_model.getGarage_banner().startsWith("http://") || garage_model.getGarage_banner().startsWith("https://")) {

                Log.e("Tag image", garage_model.getGarage_banner());
                Glide.with(activity).asBitmap().load(garage_model.getGarage_banner()).thumbnail(0.01f).placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(holder.garage_banner) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.garage_banner.setImageBitmap(resource);
                    }
                });
            } else {
                String url = Constant.IMAGE_URL+"Garage_Banner_Img/";
                Glide.with(activity).asBitmap().load(url + garage_model.getGarage_banner()).thumbnail(0.01f).placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(holder.garage_banner) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.garage_banner.setImageBitmap(resource);
                    }
                });
                Log.e("Tag image", url + garage_model.getGarage_banner());
            }

            holder.garage_banner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (service_name.toLowerCase().matches(".*tow trucks*.") || service_name.toLowerCase().equalsIgnoreCase("tow trucks") || service_name.toLowerCase().equalsIgnoreCase("tow trucks")) {
                        Intent in = new Intent(getApplicationContext(), Act_TowTrack_Detail.class);
                        in.putExtra(getString(R.string.pref_towtrack_id), garage_model.getGarage_id());
                        in.putExtra(getString(R.string.pref_name), garage_model.getGarage_name());
                        startActivity(in);
                    } else {
                        Intent in = new Intent(getApplicationContext(), Act_Workshop.class);
                        in.putExtra("workshop_id", garage_model.getGarage_id());
                        in.putExtra("workshop_name", garage_model.getGarage_name());
                        startActivity(in);
                    }
                }
            });
        }

        public void setFilter(List<Garage_Model> studentListModel) {
            garageList = new ArrayList<>();
            garageList.addAll(studentListModel);
            notifyDataSetChanged();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView garage_name;
            ImageView garage_banner;

            MyViewHolder(View itemView) {
                super(itemView);
                garage_name = itemView.findViewById(R.id.garage_name);
                garage_banner = itemView.findViewById(R.id.garage_banner);
            }
        }
    }

    public void SortByRate() {

        Collections.sort(garage_list, new Comparator<Garage_Model>() {
            @Override
            public int compare(Garage_Model t0, Garage_Model t1) {

                String tt1 = t0.getRate().toString().replaceAll("\\D+", "");
                String tt2 = t1.getRate().toString().replaceAll("\\D+", "");
                if (tt1.equals("")) {
                    tt1 = "-1";
                } else {
                    tt1 = tt1;
                }

                if (tt2.equals("")) {
                    tt2 = "-1";
                } else {
                    tt2 = tt2;
                }

                if (Integer.parseInt(tt1) > Integer.parseInt(tt2)) {
                    return 1;
                } else if (Integer.parseInt(tt1) < Integer.parseInt(tt2)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        if (garageListAdapter != null) {
            garageListAdapter.setData(garage_list);
        }
    }

}
