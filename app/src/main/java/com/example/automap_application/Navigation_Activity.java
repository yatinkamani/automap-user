package com.example.automap_application;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.MenuModel;
import com.example.automap_application.adapter.ExpandableListAdapter;
import com.example.automap_application.extra.API;
import com.example.automap_application.extra.APIResponse;
import com.example.automap_application.firebase.Config;
import com.example.automap_application.utils.AppUtils;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import de.hdodenhof.circleimageview.CircleImageView;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Navigation_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();

    LinearLayout L_service_book, L_petrol_expense, C_Workshop, l_car_expense, L_goto_garage, LRequest_maintenance, L_car_parts, C_Tow_Track, C_Request_M_shop;

    DrawerLayout drawer;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String user_email, user_name, user_id, user_image, vin_no, booking_date, contact_no;
    View header;
    TextView txt_user_email, txt_user_name, badge_textView;
    Spinner spinner;
//    CircleImageView imageView;
    BroadcastReceiver mBroadcastReceiver;
    Bundle bundle;
    static FloatingActionMenu circleMenu;

    ImageView img_menu, img_item_menu;
    RelativeLayout rel_notification;
    int check = 0, from = 0;
    int getQuotes_badge = 0, mobile_request_badge = 0, tow_track_badge = 0, admin_count = 0;
    TextView txt_car_expense;

    View circleView, gideView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
//        applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            //Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng);//your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // currentLanguage = getIntent().getStringExtra(currentLang);
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        editor = prefs.edit();
        editor.putString("CURRENCY", "JD").apply();
        String language = prefs.getString(getString(R.string.pref_language), "ar").trim();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        if (Locale.getDefault().getLanguage().equals("ar"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        setContentView(R.layout.activity_navigation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        getIntentData();
        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        user_email = prefs.getString(getString(R.string.pref_email), "");
        user_id = prefs.getString(getString(R.string.pref_user_id), "");
        user_name = prefs.getString(getString(R.string.pref_user_name), "");
        contact_no = prefs.getString(getString(R.string.contact_no), "");
        user_image = prefs.getString(getString(R.string.pref_user_image), "");
        vin_no = prefs.getString(getString(R.string.pref_vin_no), "");
        booking_date = prefs.getString(getString(R.string.pref_booking_date), "");

        viewGride();
        circleView = findViewById(R.id.circleView);
        gideView = findViewById(R.id.grideView);
        ImageView img_change_view = findViewById(R.id.img_change_view);

        img_change_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (circleMenu.isOpen()) {
                    circleMenu.close(false);
                }
                if (circleView.getVisibility() == View.VISIBLE){
                    circleView.setVisibility(View.GONE);
                    gideView.setVisibility(View.VISIBLE);
                }else {
                    circleView.setVisibility(View.VISIBLE);
                    gideView.setVisibility(View.GONE);
                }
            }
        });

        img_menu = findViewById(R.id.img_menu);
        img_item_menu = findViewById(R.id.img_item_menu);
        badge_textView = findViewById(R.id.badge_textView);
        txt_car_expense = findViewById(R.id.txt_car_expense);

        rel_notification = findViewById(R.id.rel_notification);
        L_petrol_expense = findViewById(R.id.C_petrol_expenses);
        L_service_book = findViewById(R.id.C_sevicebook);
        L_car_parts = findViewById(R.id.C_car_parts);
        L_goto_garage = findViewById(R.id.C_goto_garage);
        LRequest_maintenance = findViewById(R.id.C_Request_M_shop);
        l_car_expense = findViewById(R.id.C_car_expense);
        C_Workshop = findViewById(R.id.C_Workshop);
        C_Tow_Track = findViewById(R.id.C_Tow_Track);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
            }
        };

        ShortcutBadger.removeCount(getApplicationContext());

        L_car_parts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = new Toast(Navigation_Activity.this);
                toast.setGravity(Gravity.CENTER, 0, 0);
            }
        });

        L_petrol_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Navigation_Activity.this, Act_daily_payment.class);
                startActivity(intent);
            }
        });

        L_service_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation_Activity.this, Act_Service_book.class);
                startActivity(intent);
            }
        });

        L_goto_garage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Navigation_Activity.this, Act_Booking_service_select.class);
                startActivity(intent);
            }
        });

        C_Workshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Navigation_Activity.this, Act_Workshop_facility_select.class);
                intent.putExtra(getString(R.string.title), getString(R.string.workshops_list));
                startActivity(intent);
            }
        });

        C_Tow_Track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation_Activity.this, Act_Tow_track.class);
                intent.putExtra(getString(R.string.title), getString(R.string.request_tow_truck));
                startActivity(intent);
            }
        });

        LRequest_maintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Navigation_Activity.this, Act_Mobile_Request.class);
                startActivity(intent);
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        /*ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);*/

//        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);

        UpdateUI(user_email, user_image, user_name);
        /*spinner = (Spinner) findViewById(R.id.spinner_select_country);
        spinner.setOnItemSelectedListener(this);
        List categories = new ArrayList<>();

        categories.add(getString(R.string.lang_english));
        categories.add(getString(R.string.lang_arbic));

        String country = prefs.getString("SPINNER", "");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        for (int i = 0; i < categories.size(); i++) {
            if (country.equals(spinner.getItemAtPosition(i).toString())) {
                spinner.setSelection(i);
                break;
            }
        }*/

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        //        getSupportActionBar().setTitle(getString(R.string.AutoMap));
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

                if (circleMenu.isOpen()) {
                    circleMenu.close(false);
                }

            }
        });

        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        // end menu 3 dots click  view
        img_item_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu menu = new PopupMenu(Navigation_Activity.this, img_item_menu);
                menu.getMenuInflater().inflate(R.menu.navigation_, menu.getMenu());
                menu.getMenu().findItem(R.id.action_my_profile);
                menu.getMenu().findItem(R.id.action_edit_car_detail);
                menu.getMenu().findItem(R.id.action_change_language).setVisible(false);

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        int id = menuItem.getItemId();

                        if (id == R.id.action_my_profile) {
                            Constant.i = 1;
                            Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Profile_Detail.class);
                            startActivity(intent);
                            return true;
                        } else if (id == R.id.action_edit_car_detail) {
                            Constant.i = 2;
                            Intent intent = new Intent(Navigation_Activity.this, Act_car_details.class);
                            startActivity(intent);
                            return true;
                        } else if (id == R.id.action_change_language) {
                            SelectLanguageDialog();
                            return true;
                        }else if (id == R.id.change_pwd) {
                            Intent intent1 = new Intent(Navigation_Activity.this, Act_change_password.class);
                            startActivity(intent1);
                            return true;
                        }

                        return true;
                    }
                });
                menu.show();
            }
        });

        // request badge count preference
        SharedPreferences notification_badge = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        SharedPreferences.Editor editor1 = notification_badge.edit();
        editor1.putString(getString(R.string.pref_user_id), user_id);

        getQuotes_badge = notification_badge.getInt(getString(R.string.pref_get_quote_badge_count), 0);
        mobile_request_badge = notification_badge.getInt(getString(R.string.pref_mobile_request_badge_count), 0);
        tow_track_badge = notification_badge.getInt(getString(R.string.pref_towtrack_request_badge_count), 0);
        admin_count = notification_badge.getInt(getString(R.string.pref_admin_msg_badge_count), 0);
        Log.e("Tag getQuotes", "" + getQuotes_badge);
        Log.e("Tag Mobile Request", "" + mobile_request_badge);
        Log.e("Tag Tow Track Request", "" + tow_track_badge);
        Log.e("Tag Admin  Message", "" + admin_count);

        if ((getQuotes_badge + mobile_request_badge + tow_track_badge + admin_count) == 0) {
            badge_textView.setVisibility(View.GONE);
        } else {
            badge_textView.setVisibility(View.VISIBLE);
            badge_textView.setText("" + (getQuotes_badge + mobile_request_badge + tow_track_badge + admin_count));
        }

        // request menu view
        rel_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupWindow popup_window_obj = popupDisplay();
                popup_window_obj.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                popup_window_obj.showAsDropDown(rel_notification, -40, 18); // where u want show on view click event popupwindow.showAsDropDown(view, x, y);
            }
        });

        InitializeBroadcast();

//        VinNumberRequire();

        // round menu fragment
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new CustomButtonFragment()).commit();
        }

        /*if (prefs.getString(getString(R.string.pref_fule_type), "").toLowerCase().equalsIgnoreCase("electric") ||
                prefs.getString(getString(R.string.pref_fule_type), "").toLowerCase().matches(".*electric*.") ||
                prefs.getString(getString(R.string.pref_fule_id), "").equals("8")) {
            txt_car_expense.setText(getString(R.string.electric_car_charging_point));
            img_car_expense.setImageDrawable(getResources().getDrawable(R.drawable.electric_car));
            l_car_expense.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Navigation_Activity.this, Act_Charge_Point.class);
                    startActivity(intent);
                }
            });
        } else {*/
        txt_car_expense.setText(getString(R.string.Fuel_Expenses));
//        img_car_expense.setImageDrawable(getResources().getDrawable(R.drawable.car_maintain));
        l_car_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation_Activity.this, Act_Car_Expense.class);
                startActivity(intent);
            }
        });
//        }
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(Navigation_Activity.this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }

    // update edit name image etc data
    public void UpdateUI(String user_email, String user_image, String user_name) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);

        txt_user_email = (TextView) header.findViewById(R.id.txt_user_email);
        txt_user_email.setText(user_email);

        txt_user_name = (TextView) header.findViewById(R.id.txt_user_name);
        txt_user_name.setText(user_name);

//        imageView = (CircleImageView) header.findViewById(R.id.imageView);
        Log.e("ppp OnCreate", "" + user_image);
        /*Glide.with(getApplicationContext()).asBitmap().load(user_image).thumbnail(0.01f)
                .apply(new RequestOptions().placeholder(R.drawable.amr_icon))
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        imageView.setImageBitmap(resource);
                    }
                });*/
    }

    // notification badge count receiver
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                badge_textView = findViewById(R.id.badge_textView);
                if ((getQuotes_badge + mobile_request_badge + tow_track_badge + admin_count) > 0) {
                    badge_textView.setVisibility(View.VISIBLE);
                    badge_textView.setText("" + (getQuotes_badge + mobile_request_badge + tow_track_badge + admin_count));
                }
                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);

            }
        }
    };

    private void VinNumberRequire() {

        Log.e("tag Vin no", vin_no);
        Log.e("tag Vin no", booking_date);
        Date b_date = null;
        Calendar calendar = Calendar.getInstance();
        try {
            b_date = new SimpleDateFormat("yyyy-MM-dd").parse(booking_date);
            calendar.setTime(b_date);
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 3);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.e("tag Vin no", "" + new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()));

        if (vin_no.equals("")) {

            editor = prefs.edit();
            String BookDateCancel = prefs.getString("BookDateCancel", "");
            String cur_date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            String reg_date = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).format(new Date());

            if (!BookDateCancel.toString().equals(cur_date)) {
                Log.e("Tag Date", cur_date + "  " + BookDateCancel);
                AlertAddVinNoDialog();
            }

            Log.e("Tag Date", cur_date + "  " + BookDateCancel);
        }
    }

    // notification menu popup view
    public PopupWindow popupDisplay() {

        final PopupWindow popupWindow = new PopupWindow(this);

        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.menu_notification, null);

        RelativeLayout rel_getQuotes = view.findViewById(R.id.rel_get_quotes);
        RelativeLayout rel_mobileRequest = view.findViewById(R.id.rel_mobile_request);
        RelativeLayout rel_towTrackRequest = view.findViewById(R.id.rel_towtrack_request);
        RelativeLayout rel_admin_msg = view.findViewById(R.id.rel_admin_msg);
        CardView badge_get_quote = (CardView) view.findViewById(R.id.badge_get_quote);
        CardView badge_mobile_request = (CardView) view.findViewById(R.id.badge_mobile_request);
        CardView badge_towTrack_request = (CardView) view.findViewById(R.id.badge_towtrack_request);
        CardView badge_admin_msg = (CardView) view.findViewById(R.id.badge_admin_msg);

        if (getQuotes_badge == 0) {
            badge_get_quote.setVisibility(View.GONE);
        } else {
            badge_get_quote.setVisibility(View.VISIBLE);
        }

        if (mobile_request_badge == 0) {
            badge_mobile_request.setVisibility(View.GONE);
        } else {
            badge_mobile_request.setVisibility(View.VISIBLE);
        }

        if (tow_track_badge == 0) {
            badge_towTrack_request.setVisibility(View.GONE);
        } else {
            badge_towTrack_request.setVisibility(View.VISIBLE);
        }

        if (admin_count == 0) {
            badge_admin_msg.setVisibility(View.GONE);
        } else {
            badge_admin_msg.setVisibility(View.VISIBLE);
        }

        rel_getQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                Intent intent = new Intent(Navigation_Activity.this, Act_Booking_List.class);
                intent.putExtra("messageFore", "garage_offer");
                startActivity(intent);
            }
        });

        rel_mobileRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                Intent intent = new Intent(Navigation_Activity.this, Act_Mobile_Request_List.class);
                intent.putExtra("messageFore", "mobile_request");
                startActivity(intent);
            }
        });

        rel_towTrackRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                Intent intent = new Intent(getApplicationContext(), Act_Tow_Track_Request_List.class);
                intent.putExtra("messageFore", "towtrack");
                startActivity(intent);
            }
        });

        rel_admin_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                Intent intent = new Intent(getApplicationContext(), Act_Admin_Message.class);
                intent.putExtra("messageFore", "admin_msg");
                startActivity(intent);
            }
        });

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(view);
        return popupWindow;
    }

    // circle effect menu center view
    public static class CustomButtonFragment extends Fragment {

        //        FloatingActionMenu circleMenu;
        public CustomButtonFragment() {
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_default, container, false);

            // Our action button is this time just a regular view!
            final CircleImageView centerActionButton = (CircleImageView) rootView.findViewById(R.id.centerActionButton);

            // Add some items to the menu. They are regular views as well!

            View get_quote = inflater.inflate(R.layout.menu_name_image, null, false);
            ImageView imageView = get_quote.findViewById(R.id.img_menu);
            imageView.setImageResource(R.drawable.get_quets_icon);
//            imageView.setColorFilter(ContextCompat.getColor(getActivity(), R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
            TextView txt_name = get_quote.findViewById(R.id.txt_name);
            txt_name.setVisibility(View.GONE);
            txt_name.setText(getString(R.string.get_quote_and_booking));

            View workshop_list = inflater.inflate(R.layout.menu_name_image, null, false);
            ImageView w_imageView = workshop_list.findViewById(R.id.img_menu);
            w_imageView.setImageResource(R.drawable.all_workshop);
//            w_imageView.setColorFilter(ContextCompat.getColor(getActivity(), R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
            TextView w_txt = workshop_list.findViewById(R.id.txt_name);
            w_txt.setText(getString(R.string.all_workshops));
            w_txt.setVisibility(View.GONE);

            View tow_track = inflater.inflate(R.layout.menu_name_image, null, false);
            ImageView t_imageView = tow_track.findViewById(R.id.img_menu);
            t_imageView.setImageResource(R.drawable.tow);
            TextView t_txt = tow_track.findViewById(R.id.txt_name);
            t_txt.setVisibility(View.GONE);
            t_txt.setText(getString(R.string.request_tow_truck));

            View mobile_request = inflater.inflate(R.layout.menu_name_image, null, false);
            ImageView m_imageView = mobile_request.findViewById(R.id.img_menu);
            m_imageView.setImageResource(R.drawable.mobile_req);
            TextView m_txt = mobile_request.findViewById(R.id.txt_name);
            m_txt.setVisibility(View.GONE);
            m_txt.setText(getString(R.string.request_mobile_auto_servic));

            /*View part = inflater.inflate(R.layout.menu_name_image, null, false);
            CircleImageView p_imageView = part.findViewById(R.id.img_menu);
            p_imageView.setImageResource(R.drawable.car_parts);
            TextView p_txt = part.findViewById(R.id.txt_name);
            p_txt.setText(getString(R.string.car_parts));*/

//            FrameLayout.LayoutParams tvParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            FrameLayout.LayoutParams tvParams = new FrameLayout.LayoutParams(200, 200);
            get_quote.setLayoutParams(tvParams);
            workshop_list.setLayoutParams(tvParams);
            tow_track.setLayoutParams(tvParams);
            mobile_request.setLayoutParams(tvParams);
//            part.setLayoutParams(tvParams);

            get_quote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("click chat", "click chat");
                    Intent intent = new Intent(getActivity(), Act_Booking_service_select.class);
                    startActivity(intent);
                }
            });

            workshop_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("click chat", "click chat");
                    Intent intent = new Intent(getActivity(), Act_Workshop_facility_select.class);
                    intent.putExtra(getString(R.string.title), getString(R.string.workshops_list));
                    startActivity(intent);
                }
            });

            tow_track.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("click chat", "click chat");
                    Intent intent = new Intent(getActivity(), Act_Tow_track.class);
                    intent.putExtra(getString(R.string.title), getString(R.string.request_tow_truck));
                    startActivity(intent);
                }
            });

            mobile_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("click chat", "click chat");
                    Intent intent = new Intent(getActivity(), Act_Mobile_Request.class);
                    startActivity(intent);
                }
            });

            /*part.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity(), "Coming Soon", Toast.LENGTH_LONG).show();

                    */
            /*Date Date = new Date();
                    Date CurDate = new Date();

                    try {
//                        Date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2020-05-22 17:30");
                        Date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(Date.getTime());
                        calendar.add(Calendar.MINUTE, 10);

                        Calendar calendar1 = Calendar.getInstance();
                        calendar1.setTimeInMillis(Date.getTime());

                        Log.e("Tag Date 1", new SimpleDateFormat("hh:mm aa").format(calendar1.getTime()));
                        Log.e("Tag Date 2", new SimpleDateFormat("hh:mm aa").format(calendar.getTime()));
                        Intent intent = new Intent(getActivity(), RemainderBroadcast.class);

                        String msg = "Your Appointment Time 2 hour Remaining \n Time : " + new SimpleDateFormat("hh:mm aa").format(calendar1.getTime());
                        intent.putExtra("title", "Appointment Request");
                        intent.putExtra("messageFor", "mobile_request");
                        intent.putExtra("msg", msg);
                        intent.putExtra("id", new Random().nextInt());
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(),  new Random().nextInt(), intent, Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        AlarmManager manager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                        Log.e("Tag Date Time", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(calendar.getTime()));
                        manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);                    } catch (ParseException e) {
                        e.printStackTrace();
                    }*/
            /*
                }
            });*/

            circleMenu = new FloatingActionMenu.Builder(getActivity())
                    .setStartAngle(0) // A whole circle!
                    .setEndAngle(360)
                    .setRadius(getResources().getDimensionPixelSize(R.dimen.radius_large))
                    .addSubActionView(get_quote)
                    .addSubActionView(tow_track)
                    .addSubActionView(workshop_list)
                    .addSubActionView(mobile_request)
//                    .addSubActionView(part)
                    .attachTo(centerActionButton)
                    .build();

            circleMenu.setStateChangeListener(new FloatingActionMenu.MenuStateChangeListener() {
                @Override
                public void onMenuOpened(FloatingActionMenu menu) {
                    // Rotate the icon of rightLowerButton 45 degrees clockwise
                    centerActionButton.setRotation(0);
                    PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 45);
                    ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(centerActionButton, pvhR);
                    animation.start();
                    Log.e("Tak Center", "" + menu.getActionViewCenter());
                }

                @Override
                public void onMenuClosed(FloatingActionMenu menu) {
                    // Rotate the icon of rightLowerButton 45 degrees counter-clockwise
                    centerActionButton.setRotation(45);
                    PropertyValuesHolder pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 0);
                    ObjectAnimator animation = ObjectAnimator.ofPropertyValuesHolder(centerActionButton, pvhR);
                    animation.start();
                    Log.e("Tak Center", "" + menu.getActionViewCenter());
                }
            });

            return rootView;
        }
    }

    // drawer sidebar menu data listing
    private void prepareMenuData() {

        MenuModel menuModel;

        /*= new MenuModel(R.drawable.langage, getString(R.string.change_language), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);*/

        /*menuModel = new MenuModel(R.drawable.icon_service_book, getString(R.string.Service_Book), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /*if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }*/

        /*if (prefs.getString(getString(R.string.pref_fule_type), "").toLowerCase().equalsIgnoreCase("electric") ||
                prefs.getString(getString(R.string.pref_fule_type), "").toLowerCase().matches(".*electric*.") ||
                prefs.getString(getString(R.string.pref_fule_id), "").equals("8")) {
            menuModel = new MenuModel(R.drawable.icon_car_expenses, getString(R.string.electric_car_charging_point), true, true, 0); //Menu of Java Tutorials
            headerList.add(menuModel);
        } else {
        menuModel = new MenuModel(R.drawable.icon_car_expenses, getString(R.string.Car_Expenses), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);*/
//        }

        menuModel = new MenuModel(R.drawable.icon_garage, getString(R.string.workshop_offer), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        menuModel = new MenuModel(R.drawable.icon_garage, getString(R.string.mobile_request_offer), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        menuModel = new MenuModel(R.drawable.icon_tow, getString(R.string.tow_track_offer), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        menuModel = new MenuModel(android.R.drawable.ic_dialog_email, getString(R.string.admin_message), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        menuModel = new MenuModel(R.drawable.contact_us, getString(R.string.contact_us), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        menuModel = new MenuModel(android.R.drawable.ic_menu_share, getString(R.string.share), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        menuModel = new MenuModel(android.R.drawable.ic_dialog_info, getString(R.string.about_us), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        /*menuModel = new MenuModel(R.drawable.domain, getString(R.string.website_link), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);*/

        menuModel = new MenuModel(R.drawable.domain, getString(R.string.amr_group), true, true, 0); //Menu of Java Tutorials
        headerList.add(menuModel);

        menuModel = new MenuModel(R.drawable.icon_logout, getString(R.string.Sign_Out), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        // List<MenuModel> childModelsList = new ArrayList<>();
//        MenuModel childModel = new MenuModel(R.drawable.ic_arrow_back_24dp, "Break pads Honda city", false, false, R.drawable.ic_arrow_back_24dp);
        //childModelsList.add(childModel);

//        childModel = new MenuModel(R.drawable.ic_arrow_back_24dp, "Break pads Maruti 800", false, false, R.drawable.ic_arrow_back_24dp);
        // childModelsList.add(childModel);

//        childModel = new MenuModel(R.drawable.ic_arrow_back_24dp, "Break pads Scoda", false, false, R.drawable.ic_arrow_back_24dp);
        // childModelsList.add(childModel);

        /*if (menuModel.hasChildren) {
            Log.d("API 123", "here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();*/

        /*menuModel = new MenuModel(R.drawable.icon_garage, getString(R.string.get_quote_and_booking), true, true, 0); //Menu of Python Tutorials
        headerList.add(menuModel);*/

        //  childModel = new MenuModel(R.drawable.ic_arrow_back_24dp, "Garage item 1", false, false, R.drawable.ic_arrow_back_24dp);
        //  childModelsList.add(childModel);

        //  childModel = new MenuModel(R.drawable.ic_arrow_back_24dp, "Garage item 2", false, false, R.drawable.ic_arrow_back_24dp);

       /* menuModel = new MenuModel(R.drawable.icon_petrol_expenses, getString(R.string.Petrol_Expenses), true, false, 0); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        //  childModel = new MenuModel(R.drawable.ic_arrow_back_24dp, "Petrol Expenses item 1", false, false, R.drawable.ic_arrow_back_24dp);

        /*menuModel = new MenuModel(R.drawable.icon_request_maintenance, getString(R.string.request_mobile_auto_servic), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /*if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }*/

        /*menuModel = new MenuModel(R.drawable.icon_car_parts, getString(R.string.Car_Parts), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /*if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }*/

        /*menuModel = new MenuModel(R.drawable.icon_garage, getString(R.string.workshops_list), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /*if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }*/

        /*menuModel = new MenuModel(R.drawable.icon_tow, getString(R.string.request_tow_truck), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /*if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }*/

        /*menuModel = new MenuModel(R.drawable.icon_account, getString(R.string.Account_Details), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /*if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }*/

        /* menuModel = new MenuModel(R.drawable.icon_setting, getString(R.string.Settings), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel); */

        /*if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }*/

        /*menuModel = new MenuModel(R.drawable.ic_notifications_none_black_24dp, getString(R.string.notifications_workshop), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);*/

        /* menuModel = new MenuModel(R.drawable.ic_notifications_none_black_24dp, getString(R.string.notifications_workshop), true, false, R.drawable.trans); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel); */

        /*if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }*/
    }

    // drawer side bar menu click event
    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (headerList.get(groupPosition).menuName == getString(R.string.Sign_Out)) {
                        deleteFirebase();
                        SharedPreferences settings = getApplicationContext().getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
                        String LocalName = settings.getString(getString(R.string.pref_language), "ar");
                        settings.edit().clear().apply();
                        editor.putString(getString(R.string.pref_language), LocalName).apply();
                        SharedPreferences notification = getApplicationContext().getSharedPreferences(getString(R.string.pref_Badge), Context.MODE_PRIVATE);
                        notification.edit().clear().apply();
                        SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
                        sp.edit().clear().apply();
                        Intent intent = new Intent(Navigation_Activity.this, Act_Login.class);
                        startActivity(intent);
                        finishAffinity();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.change_language)) {
                        SelectLanguageDialog();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.Service_Book)) {
                        /* SharedPreferences settings = getApplicationContext().getSharedPreferences("Login_prefrence", Context.MODE_PRIVATE);
                        settings.edit().clear().commit(); */
                        Intent intent = new Intent(Navigation_Activity.this, Act_Service_book.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.get_quote_and_booking)) {
                        /* SharedPreferences settings = getApplicationContext().getSharedPreferences("Login_prefrence", Context.MODE_PRIVATE);
                           settings.edit().clear().commit(); */
                        // Intent intent = new Intent(Navigation_Activity.this, Act_Booking_appointment.class);
                        Intent intent = new Intent(Navigation_Activity.this, Act_Booking_service_select.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.Car_Expenses)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Car_Expense.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.electric_car_charging_point)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Charge_Point.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.admin_message)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Admin_Message.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.workshops_list)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Workshop_facility_select.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.Petrol_Expenses)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_daily_payment.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.about_us)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_About_Us.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.contact_us)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Contact_Us.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.share)) {
                        shareApp();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.Request_Maintenance)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Mobile_Request.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.request_mobile_auto_servic)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Workshop_List.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.request_tow_truck)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Workshop_List.class);
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.Account_Details)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Profile_Detail.class);
                        Constant.i = 1;
                        startActivity(intent);
                    }  else if (headerList.get(groupPosition).menuName == getString(R.string.workshop_offer)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Booking_List.class);
                        intent.putExtra("messageFore", "garage_offer");
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.mobile_request_offer)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Mobile_Request_List.class);
                        intent.putExtra("messageFore", "mobile_request");
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.tow_track_offer)) {
                        Intent intent = new Intent(Navigation_Activity.this, Act_Tow_Track_Request_List.class);
                        intent.putExtra("messageFore", "tow_track_request");
                        startActivity(intent);
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.website_link)) {
                        OpenWebsite();
                    } else if (headerList.get(groupPosition).menuName == getString(R.string.amr_group)) {
                        OpenAMRGroupLink();
                    }

                    if (!headerList.get(groupPosition).hasChildren) {
                        onBackPressed();
                    }
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
//                        if (model.url.length() > 0) {
//                        WebView webView = findViewById(R.id.webView);
//                        webView.loadUrl(model.url);
//                        onBackPressed();
//                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notification_menu, menu);

        menu.findItem(R.id.action_change_language).setVisible(false);
        return true;
    }

    // Not Used
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //  noinspection SimplifiableIfStatement

        if (id == R.id.action_my_profile) {
            Constant.i = 1;
            Intent intent = new Intent(Navigation_Activity.this, Act_Edit_Profile_Detail.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_edit_car_detail) {
            Constant.i = 2;
            Intent intent = new Intent(Navigation_Activity.this, Act_car_details.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_change_language) {
            SelectLanguageDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*@Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//         int lang_pos = spinner.getSelectedItemPosition();
        String item = parent.getItemAtPosition(position).toString();
        String string = spinner.getSelectedItem().toString();
//        spinner.setSelection(0);
//        Toast.makeText(getApplicationContext(),"Device not support",Toast.LENGTH_LONG).show();

        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putString("SPINNER", spinner.getSelectedItem().toString());
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        prefEditor.apply();
        if (++check > 1) {
            switch (item) {
                case "English": //  English
                    editor.putString(getString(R.string.pref_language), "ar").apply();
                    setLangRecreate("ar");
                    break;
                case "Arabic": //   Arabic
                    editor.putString(getString(R.string.pref_language), "ar").apply();
                    setLangRecreate("ar");
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }*/

    // select language dialog
    public void SelectLanguageDialog() {

        String[] language = {getString(R.string.lang_english), getString(R.string.lang_arbic)};
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = preferences.edit();
        String selected_lang = prefs.getString(getString(R.string.pref_language), "ar");
        int checked = -1;
        if (selected_lang.toString().equals("en")) {
            checked = 0;
        } else if (selected_lang.toString().equals("ar")) {
            checked = 1;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(Navigation_Activity.this);
        builder.setTitle("Select Language");
        builder.setSingleChoiceItems(language, checked, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (language[i].equals(getString(R.string.lang_english))) {
//                    edit.putString(getString(R.string.pref_language), "ar").apply();
//                    setLangRecreate("en");
                    updateLangApi("ar");
                } else {
//                    edit.putString(getString(R.string.pref_language), "ar").apply();
//                    setLangRecreate("ar");
                    updateLangApi("ar");
                }
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // langae change effect
    public void setLangRecreate(String localeName) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(getString(R.string.pref_language), localeName);
        edit.apply();
//          edit.putString("SPINNER", string);
        //        onRestart();
        //        Intent intent = new Intent(this,Navigation_Activity.class);
        //        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //        startActivity(intent);
        startActivity(new Intent(getApplicationContext(), Navigation_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
//        if (!localeName.equals(currentLanguage)) {
//            //  PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(getString(R.string.pref_language), localeName).commit();
//            //  this.recreate();
//            Intent refresh = new Intent(this, Navigation_Activity.class);
//            refresh.putExtra(currentLang, localeName);
//            startActivity(refresh);
//            finish();
//            //  new Handler().post(new Runnable() { @Override public void run() { recreate(); } });
//        } else {
//            Toast.makeText(Navigation_Activity.this, "Language already selected!", Toast.LENGTH_SHORT).show();
//        }
    }

    // update language api call
    private void updateLangApi(String localeName){
        API api = new API(Navigation_Activity.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                if (isSuccess){
                    Log.e("Tag response", response);
                    setLangRecreate(localeName);
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(),getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
            }
        });

        Map<String, String> map = new HashMap<>();
        String language = "";
        if (localeName.equals("ar")){
            language = "Arabic";
        }else {
            language = "English";
        }
        map.put("language",language);
        map.put("user_id", user_id);
        api.execute(100, Constant.URL+"changeLanguage", map, true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_email = prefs.getString(getString(R.string.pref_email), "");
        user_id = prefs.getString(getString(R.string.pref_user_id), "");
        user_name = prefs.getString(getString(R.string.pref_user_name), "");
        contact_no = prefs.getString(getString(R.string.contact_no), "");
        user_image = prefs.getString(getString(R.string.pref_user_image), "");
        vin_no = prefs.getString(getString(R.string.pref_vin_no), "");
        booking_date = prefs.getString(getString(R.string.pref_booking_date), "");

        UpdateUI(user_email, user_image, user_name);

        SharedPreferences notification_badge = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
        SharedPreferences.Editor editor1 = notification_badge.edit();
        editor1.putString(getString(R.string.pref_user_id), user_id);

        getQuotes_badge = notification_badge.getInt(getString(R.string.pref_get_quote_badge_count), 0);
        mobile_request_badge = notification_badge.getInt(getString(R.string.pref_mobile_request_badge_count), 0);
        tow_track_badge = notification_badge.getInt(getString(R.string.pref_towtrack_request_badge_count), 0);
        admin_count = notification_badge.getInt(getString(R.string.pref_admin_msg_badge_count), 0);
        Log.e("Tag getQuotes", "" + getQuotes_badge);
        Log.e("Tag mobile request", "" + mobile_request_badge);
        Log.e("Tag Tow Truck", "" + tow_track_badge);
        Log.e("Tag Admin Count", "" + admin_count);

        if ((getQuotes_badge + mobile_request_badge + tow_track_badge + admin_count) == 0) {
            badge_textView.setVisibility(View.GONE);
        } else {
            badge_textView.setVisibility(View.VISIBLE);
            badge_textView.setText("" + (getQuotes_badge + mobile_request_badge + tow_track_badge + admin_count));
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        // NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        super.onPause();
    }

    // notification click and redirect screen
    public void getIntentData() {
        Log.e("response", "Intent" + getIntent());
        Intent intent = getIntent();
        if (intent != null) {
            Log.e("response", intent.toString());
            String messageFor = intent.getStringExtra("messageFor");
            String id = intent.getStringExtra("id");
            if (messageFor != null) {
                if (messageFor.contains("garage") || messageFor.equals("user")) {
                    Intent i = new Intent(this, Act_Booking_List.class);
                    i.putExtra("id", id);
                    startActivity(i);
                } else if (messageFor.contains("appointment_remainder")) {
                    Intent i = new Intent(this, Act_Garage_List.class);
                    i.putExtra("messageFor", "appointment_remainder");
                    i.putExtra("bundle", intent.getBundleExtra("bundle"));
                    startActivity(i);
                } else if (messageFor.contains("garage_offer")) {
                    Intent i = new Intent(this, Act_Garage_List.class);
                    intent.putExtra("messageFor", "garage_offer");
                    startActivity(i);
                } else if (messageFor.equalsIgnoreCase("mobile_request")) {
                    Intent intent1 = new Intent(getApplicationContext(), Act_Mobile_Request_List.class);
                    intent1.putExtra("id", id);
                    startActivity(intent1);
                } else if (messageFor.equalsIgnoreCase("towtrack")) {
                    Intent intent1 = new Intent(getApplicationContext(), Act_Tow_Track_Request_List.class);
                    intent1.putExtra("id", id);
                    startActivity(intent1);
                } else if (messageFor.equalsIgnoreCase("admin_msg")) {
                    Intent intent1 = new Intent(getApplicationContext(), Act_Admin_Message.class);
                    intent.putExtra("messageFore", "admin_msg");
                    startActivity(intent1);
                }
            }
        } else {
            Log.e("response", "No Data");
        }
    }

    // not  use
    public void AlertAddVinNoDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Navigation_Activity.this);
        builder.setMessage(getString(R.string.enter_vin_number_for));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), Act_car_details.class);
                intent.putExtra("vin_no_require", true);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor prefEditor = prefs.edit();
                Calendar calendar = Calendar.getInstance();
//                String date = "" + calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
                prefEditor.putString("BookDateCancel", date);
                prefEditor.apply();

            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this app at: https://play.google.com/store/apps/details?id=" + Navigation_Activity.this.getPackageName());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void OpenWebsite() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://automap.repair/")));
    }

    private void OpenAMRGroupLink() {
//        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://automap.repair/")));
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/1750431415165198")));
    }

    // chage screen view to grid
    public void viewGride(){

        CardView card_mobile_request = findViewById(R.id.card_mobile_request);
        CardView card_get_quest_request = findViewById(R.id.card_get_quest_request);
        CardView card_tow_truck = findViewById(R.id.card_tow_truck);
        CardView card_all_workshop = findViewById(R.id.card_all_workshop);
        CardView card_fuel = findViewById(R.id.card_fuel);
        CardView card_total_expence = findViewById(R.id.card_total_expence);
        CardView car_service_book = findViewById(R.id.car_service_book);

        card_mobile_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Act_Mobile_Request.class);
                startActivity(intent);
            }
        });
        card_get_quest_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Act_Booking_service_select.class);
                startActivity(intent);
            }
        });
        card_tow_truck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Act_Tow_track.class);
                intent.putExtra(getString(R.string.title), getString(R.string.request_tow_truck));
                startActivity(intent);
            }
        });
        card_all_workshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Act_Workshop_facility_select.class);
                intent.putExtra(getString(R.string.title), getString(R.string.workshops_list));
                startActivity(intent);
            }
        });
        card_fuel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Navigation_Activity.this, Act_daily_payment.class);
                startActivity(intent);
            }
        });
        card_total_expence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Navigation_Activity.this, Act_Car_Expense.class);
                startActivity(intent);
            }
        });
        car_service_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Navigation_Activity.this, Act_Service_book.class);
                startActivity(intent);
            }
        });

    }

    public static void deleteFirebase() {
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try
                {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    FirebaseInstanceId.getInstance().getInstanceId();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                // Call your Activity where you want to land after log out
            }
        }.execute();
    }

}
