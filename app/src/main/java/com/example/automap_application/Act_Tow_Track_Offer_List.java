package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.GarageNotificationModel;
import com.example.automap_application.Model.Tow_Track_Offer_Model;
import com.example.automap_application.backgroundServices.LocationUpdateService;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Act_Tow_Track_Offer_List extends AppCompatActivity implements View.OnClickListener {

    ImageView back_towtrack, img_menu;
    RecyclerView rcy_view;
    View lin_error;
    TextView txt_msg;
    Button btn_retry;
    List<Tow_Track_Offer_Model> towTrackOfferModels = new ArrayList<>();
    Adapter_Tow_Track_Offers adapter_tow_track_offers;
    SharedPreferences preferences;

    String towtrack_request_id = "", user_id = "";
    Location location;
    private boolean SortPrice = false, SortTime = false, SortDistance = false;
    public Activity instance = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__tow__track__offer__list);

        instance = this;
        back_towtrack = findViewById(R.id.back_towtrack);
        img_menu = findViewById(R.id.img_menu);
        rcy_view = findViewById(R.id.rcy_view);
        lin_error = findViewById(R.id.lin_error);
        txt_msg = findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id), "");

        btn_retry.setOnClickListener(this);
        img_menu.setOnClickListener(this);
        rcy_view.setOnClickListener(this);
        back_towtrack.setOnClickListener(this);
        location = LocationUpdateService.locations;

        getIntentData();
        getTowTrackOffer();
        InitializeBroadcast();

    }

    public void getIntentData() {

        towtrack_request_id = getIntent().getStringExtra("tow_track_request_id");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    // get towtruck offer list and set list view ,set distance
    private void getTowTrackOffer() {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            ProgressDialog dialog = new ProgressDialog(Act_Tow_Track_Offer_List.this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_response_list", new Response.Listener<String>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(String response) {
                    Log.e("notification_list", " " + response);
                    try {

                        JSONObject mainJsonObject = new JSONObject(response);
                        towTrackOfferModels = new ArrayList<>();
                        if (mainJsonObject.getString("status").equals("true")) {
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            if (result.length() > 0) {
                                rcy_view.setVisibility(View.VISIBLE);
                                lin_error.setVisibility(View.GONE);
                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject jsonObject = result.getJSONObject(i);

                                    Tow_Track_Offer_Model track_offer_model = new Tow_Track_Offer_Model();
                                    track_offer_model.setTow_track_request_id(jsonObject.getString("towtrack_request_id"));
                                    track_offer_model.setTow_track_name(jsonObject.getString("towtrack_name"));
                                    track_offer_model.setTow_track_id(jsonObject.getString("towtrack_id"));
                                    track_offer_model.setStatus(jsonObject.getString("status"));
                                    track_offer_model.setCity(jsonObject.optString("city_name"));
                                    track_offer_model.setAddress(jsonObject.getString("address"));
                                    track_offer_model.setTow_track_contact(jsonObject.getString("contact_no"));
                                    track_offer_model.setPrice(jsonObject.optString("price", ""));
                                    track_offer_model.setArrival_time(jsonObject.optString("time", ""));
                                    track_offer_model.setTow_tack_image(jsonObject.getString("towtrack_profile_img"));

                                    if (jsonObject.getString("latitude") != null && !(jsonObject.getString("latitude").equalsIgnoreCase("null"))) {
                                        if (0 < jsonObject.getString("latitude").length())
                                            track_offer_model.setLatitude(jsonObject.getString("latitude"));
                                        else
                                            track_offer_model.setLatitude("0.0");
                                    } else {
                                        track_offer_model.setLatitude("0.0");
                                    }

                                    if (jsonObject.getString("longitude") != null && !(jsonObject.getString("longitude").equalsIgnoreCase("null"))) {
                                        if (0 < jsonObject.getString("longitude").length())
                                            track_offer_model.setLongitude(jsonObject.getString("longitude"));
                                        else
                                            track_offer_model.setLongitude("0.0");
                                    } else {
                                        track_offer_model.setLongitude("0.0");
                                    }

                                    if (location != null) {
                                        if (track_offer_model.getLatitude() != null && track_offer_model.getLongitude() != null) {
                                            Location locationA = new Location("");
                                            locationA.setLatitude(Double.parseDouble("" + track_offer_model.getLatitude()));
                                            locationA.setLongitude(Double.parseDouble("" + track_offer_model.getLongitude()));

                                            Location locationB = new Location("");
                                            locationB.setLatitude(Double.parseDouble("" + location.getLatitude()));
                                            locationB.setLongitude(Double.parseDouble("" + location.getLongitude()));

                                            double mDistance = locationA.distanceTo(locationB) / 1000;
                                            track_offer_model.setDistance(String.format("%.2f", mDistance));
                                        } else {
                                            track_offer_model.setDistance("");
                                        }
                                    } else {
                                        track_offer_model.setDistance("");
                                    }

                                    Log.e("TAG", jsonObject.getString("status"));
                                    if (jsonObject.getString("status").equals("2") || jsonObject.getString("status").equals("0")) {

                                    } else {
                                        towTrackOfferModels.add(track_offer_model);
                                        Log.e("TAG IND", jsonObject.getString("status"));
                                    }
                                }
                                if (towTrackOfferModels.size() > 0) {
                                    if (adapter_tow_track_offers == null) {

                                        adapter_tow_track_offers = new Adapter_Tow_Track_Offers(Act_Tow_Track_Offer_List.this, towTrackOfferModels);
                                        rcy_view.setAdapter(adapter_tow_track_offers);
                                        rcy_view.setLayoutManager(new LinearLayoutManager(Act_Tow_Track_Offer_List.this));

                                    } else {

                                        if (SortPrice) {
                                            SortPrice();
                                        } else if (SortTime) {
                                            SortTime();
                                        } else if (SortDistance && location != null) {
                                            SortTime();
                                        } else {
                                            adapter_tow_track_offers.setData(towTrackOfferModels);
                                        }
                                    }
                                    rcy_view.setVisibility(View.VISIBLE);
                                    lin_error.setVisibility(View.GONE);
                                    img_menu.setVisibility(View.VISIBLE);
                                } else {
                                    rcy_view.setVisibility(View.GONE);
                                    lin_error.setVisibility(View.VISIBLE);
                                    btn_retry.setVisibility(View.GONE);
                                    txt_msg.setText(getString(R.string.no_data_found));
                                }
                            } else {
                                rcy_view.setVisibility(View.GONE);
                                lin_error.setVisibility(View.VISIBLE);
                                btn_retry.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_data_found));
                            }
                        } else {
                            rcy_view.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            btn_retry.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.no_data_found));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        rcy_view.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                    }

                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    rcy_view.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("towtrack_request_id", towtrack_request_id);
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {

            rcy_view.setVisibility(View.GONE);
            lin_error.setVisibility(View.VISIBLE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.internet_not_connect));
        }
    }

    public void InitializeBroadcast(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(Act_Tow_Track_Offer_List.this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }

    // new offer com to refress api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (tow_track_badge > 0 && !(Act_Tow_Track_Offer_List.this.isFinishing())) {
                    getTowTrackOffer();
                }
                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);

            }
        }
    };

    @Override
    public void onClick(View view) {

        if (view == btn_retry) {
            getTowTrackOffer();
        }
        // 3dot menu click to shop popup filter
        else if (view == img_menu) {
            PopupMenu menu = new PopupMenu(this, img_menu);
            menu.getMenuInflater().inflate(R.menu.sort_menu, menu.getMenu());
            menu.getMenu().findItem(R.id.price).setTitle("Sort by Price");
            menu.getMenu().findItem(R.id.time).setTitle("Sort by Time");
            if (location != null) {
                menu.getMenu().findItem(R.id.distance).setTitle("Sort by Distance");
                menu.getMenu().findItem(R.id.distance).setVisible(true);
            } else {
                menu.getMenu().findItem(R.id.distance).setVisible(false);
            }

            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if (menuItem.getItemId() == R.id.price) {
                        SortPrice();
                        SortPrice = true;
                        SortTime = false;
                        SortDistance = false;

                    } else if (menuItem.getItemId() == R.id.time) {
                        SortTime();
                        SortPrice = false;
                        SortDistance = false;
                        SortTime = true;

                    } else if (menuItem.getItemId() == R.id.distance) {
                        SortDistance();
                        SortDistance = true;
                        SortPrice = false;
                        SortTime = false;
                    }
                    return true;
                }
            });
            menu.show();
        } else if (view == back_towtrack) {
            onBackPressed();
        }
    }

    class Adapter_Tow_Track_Offers extends RecyclerView.Adapter<Adapter_Tow_Track_Offers.MyViewHolder> {

        private Activity activity;
        private List<Tow_Track_Offer_Model> track_offer_models;
        Location location;

        public Adapter_Tow_Track_Offers(Activity activity, List<Tow_Track_Offer_Model> trackOfferModels) {
            this.activity = activity;
            this.track_offer_models = trackOfferModels;
            location = LocationUpdateService.locations;
        }

        public void setData(List<Tow_Track_Offer_Model> data) {
            Log.e("Tag data 1", "" + data.size());
            track_offer_models = new ArrayList<>();
            track_offer_models.addAll(data);
            notifyDataSetChanged();
        }

        @SuppressLint("SetTextI18n")
        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_tow_track_offers, viewGroup, false);

            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
            Tow_Track_Offer_Model model = track_offer_models.get(i);

            holder.txt_name.setText("  " + model.getTow_track_name());
            holder.txt_city.setText(Html.fromHtml("<b>"+getString(R.string.city2)+"</b> " + model.getCity()));
            if (!model.getPrice().equals("")) {
                holder.txt_price.setVisibility(View.VISIBLE);
                holder.txt_price.setText(Html.fromHtml("<b>"+getString(R.string.price)  +": </b>" + model.getPrice()));
            } else {
                holder.txt_price.setVisibility(View.GONE);
            }

            if (model.getDistance().equals("")){
                holder.txt_distance.setVisibility(View.GONE);
            } else {
                holder.txt_distance.setVisibility(View.VISIBLE);
                holder.txt_distance.setText(Html.fromHtml("<b>"+ getString(R.string.disatance) +": </b>" + model.getDistance()));
            }

            Glide.with(getApplicationContext()).asBitmap().load(model.getTow_tack_image())
                    .placeholder(getResources().getDrawable(R.drawable.user))
                    .thumbnail(0.01f)
                    .into(new BitmapImageViewTarget(holder.user_image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            holder.user_image.setImageBitmap(resource);
                        }
                    });

            if (!model.getArrival_time().equals("")) {
                holder.txt_arrival_time.setVisibility(View.VISIBLE);
                holder.txt_arrival_time.setText(Html.fromHtml("<b>"+getString(R.string.arrival_time)+"    : </b>" + (model.getArrival_time() + " Hour")));
            } else {
                holder.txt_arrival_time.setVisibility(View.GONE);
            }

            if (model.getStatus().equals("2")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"    :  "+activity.getString(R.string.ignore_your_request));
            } else if (model.getStatus().equals("3") || model.getStatus().equals("4")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"    :  "+getString(R.string.you_have_confirm_thiese_tow_truck_offers));
            } else if (model.getStatus().equals("5")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"    :  "+getString(R.string.you_have_canceled_the_order));
            } else if (model.getStatus().equals("6")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"    :  "+getString(R.string.you_have_selected_another_offers));
            } else if (model.getStatus().equals("7")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"    :  "+getString(R.string.complete_request));
            } else if (model.getStatus().equals("9")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"    :  "+getString(R.string.you_have_complained_this_tow_truck));
            } else if (model.getStatus().equals("10")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"    :  "+getString(R.string.tow_truck_owner_complained_you));
            } else {
                holder.txt_status.setVisibility(View.GONE);
            }

            holder.llDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (model.getStatus().equals("2")) {
                        Toast.makeText(activity, getString(R.string.ignore_your_request), Toast.LENGTH_LONG).show();
                    } else if (model.getStatus().equals("5")) {
                        Toast.makeText(activity, getString(R.string.you_have_canceled_request), Toast.LENGTH_LONG).show();
                    } else if (model.getStatus().equals("6")) {
                        Toast.makeText(activity, getString(R.string.you_have_not_selected), Toast.LENGTH_LONG).show();
                    } else if (model.getStatus().equals("7")) {
                        Toast.makeText(activity, getString(R.string.complete_request), Toast.LENGTH_LONG).show();
                    } else if (model.getStatus().equals("9") && model.getStatus().equals("10")) {
                        Toast.makeText(activity, getString(R.string.complained_request), Toast.LENGTH_LONG).show();
                    } else {
                        Intent in = new Intent(activity, Act_Tow_Track_Offers_Detail.class);
                        in.putExtra(getString(R.string.pref_towtrack_id), model.getTow_track_id());
                        in.putExtra(getString(R.string.name), model.getTow_track_name());
                        in.putExtra(getString(R.string.pref_request_id), model.getTow_track_request_id());
                        activity.startActivity(in);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return track_offer_models.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView txt_name, txt_city, txt_arrival_time,txt_distance, txt_price, txt_status;
            private ImageView user_image;
            private LinearLayout llDetail;

            MyViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_name = itemView.findViewById(R.id.txt_name);
                user_image = itemView.findViewById(R.id.user_image);
                txt_city = itemView.findViewById(R.id.txt_city);
                txt_distance = itemView.findViewById(R.id.txt_distance);
                txt_price = itemView.findViewById(R.id.txt_price);
                txt_status = itemView.findViewById(R.id.txt_status);
                txt_arrival_time = itemView.findViewById(R.id.txt_arraival_time);
                txt_price = itemView.findViewById(R.id.txt_price);
                llDetail = itemView.findViewById(R.id.llDetail);

            }
        }

    }

    public void SortPrice() {

        Collections.sort(towTrackOfferModels, new Comparator<Tow_Track_Offer_Model>() {
            @Override
            public int compare(Tow_Track_Offer_Model t0, Tow_Track_Offer_Model t1) {

                String tt1 = t0.getPrice().toString().replaceAll("\\D+", "");
                String tt2 = t1.getPrice().toString().replaceAll("\\D+", "");
                if (tt1.equals("")) {
                    tt1 = "-1";
                } else {
                    tt1 = tt1;
                }

                if (tt2.equals("")) {
                    tt2 = "-1";
                } else {
                    tt2 = tt2;
                }

                if (Integer.parseInt(tt1) > Integer.parseInt(tt2)) {
                    return 1;
                } else if (Integer.parseInt(tt1) < Integer.parseInt(tt2)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        if (adapter_tow_track_offers != null) {
            adapter_tow_track_offers.setData(towTrackOfferModels);
        }
    }

    public void SortTime() {

        Collections.sort(towTrackOfferModels, new Comparator<Tow_Track_Offer_Model>() {
            @Override
            public int compare(Tow_Track_Offer_Model t0, Tow_Track_Offer_Model t1) {

                String tt1 = t0.getArrival_time().toString().replaceAll("\\D+", "");
                String tt2 = t1.getArrival_time().toString().replaceAll("\\D+", "");
                Log.e("Tag Sort 1", tt1);
                Log.e("Tag Sort 2", tt2);
                if (tt1.equals("")) {
                    tt1 = "-1";
                } else {
                    tt1 = tt1;
                }

                if (tt2.equals("")) {
                    tt2 = "-1";
                } else {
                    tt2 = tt2;
                }

                if (Integer.parseInt(tt1) > Integer.parseInt(tt2)) {
                    return 1;
                } else if (Integer.parseInt(tt1) < Integer.parseInt(tt2)) {
                    return -1;
                } else {
                    return 0;
                }

            }
        });
        if (adapter_tow_track_offers != null) {
            adapter_tow_track_offers.setData(towTrackOfferModels);
        }
    }

    public void SortDistance() {

        Collections.sort(towTrackOfferModels, new Comparator<Tow_Track_Offer_Model>() {
            @Override
            public int compare(Tow_Track_Offer_Model t0, Tow_Track_Offer_Model t1) {

                String tt1 = t0.getDistance().toString().replaceAll("\\D+", "")/*.substring(0,t0.getDistance().indexOf("."))*/;
                String tt2 = t1.getDistance().toString().replaceAll("\\D+", "")/*.substring(0,t1.getDistance().indexOf("."))*/;
                Log.e("Tag Sort 1", tt1);
                Log.e("Tag Sort 2", tt2);
                if (tt1.equals("")) {
                    tt1 = "-1";
                } else {
                    tt1 = tt1;
                }

                if (tt2.equals("")) {
                    tt2 = "-1";
                } else {
                    tt2 = tt2;
                }

                if (Float.parseFloat(tt1) > Float.parseFloat(tt2)) {
                    return 1;
                } else if (Float.parseFloat(tt1) < Float.parseFloat(tt2)) {
                    return -1;
                } else {
                    return 0;
                }

            }
        });
        if (adapter_tow_track_offers != null) {
            adapter_tow_track_offers.setData(towTrackOfferModels);
        }
    }

}