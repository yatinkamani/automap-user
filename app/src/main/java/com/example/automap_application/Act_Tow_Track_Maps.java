package com.example.automap_application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.utils.CustomMapFragment;
import com.example.automap_application.utils.DirectionsJSONParser;
import com.example.automap_application.utils.LatLngInterpolator;
import com.example.automap_application.utils.MapWrapperLayout;
import com.example.automap_application.utils.MarkerAnimation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Act_Tow_Track_Maps extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleMap.OnMarkerDragListener, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, MapWrapperLayout.OnDragListener {

    private GoogleMap mMap;
    private static final String TAG = "Act_Map";
    private CustomMapFragment mCustomMapFragment;
    private Location myLocation;
    String dest_lat = "", dest_lng = "";
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    double latitude, longitude;

    ImageView img_back;
    TextView txt_call;

    String towtrack_name, contact_no, towtrack_id, towtrack_request_id, user_id, final_lat, final_lng;
    ProgressBar progress_circular;
    LinearLayout lin_progress;
    private Polyline polyline;
    List<LatLng> latLng = new ArrayList<>();
    LocationSettingsRequest.Builder builder;

    Runnable runnable, mRunnable;
    Handler handler = new Handler();
    Handler mHandler = new Handler();
    boolean isEnable = true, start = false;
    Marker srcMarker = null, destMarker = null, finalMarker = null;

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onResume() {
        int permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            getMyLocation();
        }
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__tow__track__maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        txt_call = findViewById(R.id.txt_call);
        img_back = findViewById(R.id.img_back);
        lin_progress = findViewById(R.id.lin_progress);
        progress_circular = findViewById(R.id.progress_circular);

        img_back.setOnClickListener(this);
        txt_call.setOnClickListener(this);

        changeValue();
        getIntentData();
        initializeMap();
        buildGoogleApiClient();

        final int[] c = {0};
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isEnable) {
                    checkPermissions();
                }

                Log.e("Tag", "" + (c[0]++));
                Log.e("Tag", "" + isEnable);
                runnable = this;
                handler.postDelayed(runnable, 10000);
            }
        }, 1000);

        getRequestStatus();
    }

    // get towtruck data
    public void getIntentData() {

        String dest_lat = getIntent().getStringExtra("latitude");
        String dest_lng = getIntent().getStringExtra("longitude");
        final_lat = getIntent().getStringExtra("final_lat");
        final_lng = getIntent().getStringExtra("final_lng");
        towtrack_name = getIntent().getStringExtra("towtrack_name");
        contact_no = getIntent().getStringExtra(getString(R.string.contact_no));

        if (dest_lat != null) {
            this.dest_lat = dest_lat;
        }
        if (dest_lng != null) {
            this.dest_lng = dest_lng;
        }
        /*this.dest_lat = "22.87964468";
        this.dest_lng = "70.87560976";*/
        towtrack_request_id = getIntent().getStringExtra(getString(R.string.pref_request_id));
        user_id = getIntent().getStringExtra(getString(R.string.pref_user_id));
        towtrack_id = getIntent().getStringExtra(getString(R.string.pref_garage_id));

    }

    private void initializeMap() {
        if (mMap == null) {
            mCustomMapFragment = ((CustomMapFragment) getFragmentManager().findFragmentById(R.id.map));
            mCustomMapFragment.setOnDragListener(Act_Tow_Track_Maps.this);
            mCustomMapFragment.getMapAsync(this);
            mCustomMapFragment.onResume();
        }
    }

    Handler mhandler = new Handler();
    Runnable mrunnable = null;
    boolean run = false;

    // on map update value
    public void changeValue() {

        mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mrunnable = this;
                run = true;
//                handler.postDelayed(runnable,10000);
            }
        }, 1000);
    }

    // draw path between twotruck location
    public void betweenPathDraw(String userFromLat, String userFromLong, String userToLat, String userToLong) {

        if (run) {
            run = false;
            mhandler.postDelayed(mrunnable, 10000);
            MakeCameraFocus(userFromLat, userFromLong, userToLat, userToLong);
            String url = getDirectionsUrl(userFromLat, userFromLong, userToLat, userToLong);
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
        }
//        mMap.clear();

        Log.e("Tag Size", "" + latLng.size());
        LatLng srcLatLng = null;
        if (latLng != null && latLng.size() > 0) {
            srcLatLng = latLng.get(0);
        } else {
            srcLatLng = new LatLng(Double.parseDouble(userFromLat), Double.parseDouble(userFromLong));
        }

        if (srcMarker == null) {
            srcMarker = mMap.addMarker(new MarkerOptions().position(srcLatLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                    .title("Your Position"));
        } else {
//            MarkerAnimation.animateMarkerToGB(srcMarker, srcLatLng, new LatLngInterpolator.Spherical());
            srcMarker.setPosition(srcLatLng);
        }
        /*  mMap.addMarker(new MarkerOptions().position(srcLatLng).icon(BitmapDescriptorFactory.
                fromResource(R.drawable.pickup_new)));

        //       fromBitmap(createCustomPinMarker(getActivity(), R.drawable.pickupPin))));*/

        LatLng desLatLng = null;
        if (latLng != null && latLng.size() > 0) {
            desLatLng = latLng.get(latLng.size() - 1);
        } else {
            desLatLng = new LatLng(Double.parseDouble(userToLat), Double.parseDouble(userToLong));
        }
//        destMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(userToLat), Double.parseDouble(userToLong))).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(address));
        if (destMarker == null) {
            destMarker = mMap.addMarker(new MarkerOptions().position(desLatLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            destMarker.setTitle(towtrack_name);

        } else {
//            MarkerAnimation.animateMarkerToGB(destMarker, desLatLng, new LatLngInterpolator.Spherical());
            destMarker.setPosition(desLatLng);
        }

        // Final Location Marker
        LatLng finalLatLng = new LatLng(Double.parseDouble(final_lat),Double.parseDouble(final_lng));

        if (finalMarker == null){
            finalMarker = mMap.addMarker(new MarkerOptions().position(finalLatLng)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
            finalMarker.setTitle("final destination");
        }

        /* mMap.addMarker(new MarkerOptions().position(desLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.destinationpin_new))); */
    }

    // focus on point of location
    private void MakeCameraFocus(String userFromLat, String userFromLong, String userToLat, String userToLong) {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        final LatLng srcLatLng = new LatLng(Double.parseDouble(userFromLat), Double.parseDouble(userFromLong));
        final LatLng destLatLng = new LatLng(Double.parseDouble(userToLat), Double.parseDouble(userToLong));
        final LatLng finalLatLng = new LatLng(Double.parseDouble(final_lat), Double.parseDouble(final_lng));
        /*final Location sl = new Location("gps");
        sl.setLatitude(Double.parseDouble(userFromLat));
        sl.setLongitude(Double.parseDouble(userFromLong));*/
        builder.include(srcLatLng);
        builder.include(destLatLng);
        builder.include(finalLatLng);
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20);
        //  CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        int pinShowTime = getResources().getInteger(R.integer.map_pin_time);
        mMap.animateCamera(cameraUpdate, pinShowTime, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                //  mTotalDistance.setText("Distance" + mDistance + " Meters");
            }

            @Override
            public void onCancel() {
            }
        });

    }

    private float getBearing(LatLng begin, LatLng end) {
        double dLon = (end.longitude - begin.longitude);
        double x = Math.sin(Math.toRadians(dLon)) * Math.cos(Math.toRadians(end.latitude));
        double y = Math.cos(Math.toRadians(begin.latitude)) * Math.sin(Math.toRadians(end.latitude))
                - Math.sin(Math.toRadians(begin.latitude)) * Math.cos(Math.toRadians(end.latitude)) * Math.cos(Math.toRadians(dLon));
        double bearing = Math.toDegrees((Math.atan2(x, y)));
        return (float) bearing;
    }

    // get towtrcuk request status
    private void getRequestStatus() {

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_request_detail",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("Request_status_response", " " + response);
                                try {
                                    JSONObject mainJsonObject = new JSONObject(response);
                                    if (mainJsonObject.getString("status").equals("1")) {
                                        JSONArray result = mainJsonObject.getJSONArray("result");
                                        for (int i = 0; i < result.length(); i++) {
                                            JSONObject jsonObject = result.getJSONObject(i);
                                            if (!jsonObject.getString("latitude").equals("")) {
                                                dest_lat = jsonObject.optString("latitude", "0.00");
                                                dest_lng = jsonObject.optString("longitude", "0.00");
                                            }
                                        }
                                        betweenPathDraw(String.valueOf(latitude), String.valueOf(longitude), String.valueOf(dest_lat), String.valueOf(dest_lng));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Tag Error", "" + error);
                    }
                }) {

                    @Override
                    public Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("towtrack_request_id", towtrack_request_id);
                        params.put("towtrack_id", towtrack_id);
                        Log.e("tag param,", params.toString());
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);

                mRunnable = this;
                mHandler.postDelayed(mRunnable, 10000);

            }
        }, 1000);

    }

    private String getDirectionsUrl(String fromLat, String fromLong, String toLet, String toLong) {

        // Origin of route
        String str_origin = "origin=" + fromLat + "," + fromLong;

        // Destination of route
        String str_dest = "destination=" + toLet + "," + toLong;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = Constant.DIRECTION_URL + output + "?" + parameters + "&key=" + Constant.API_KEY;
//        String url = Constant.DIRECTION_URL + output + "?" + parameters + "&key=AIzaSyDMa4KIH9DEWgih6sQiPeXK3NNb6L4eoYQ";
        // String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=AIzaSyAsGI0P0ORyhuRvUnMo4ecIRSnpvb48roc";
        Log.e(TAG, "getDirectionsUrl => " + url);

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {

        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setPadding(0, img_back.getHeight() + 60, 0, 0);
        LatLng india = new LatLng(latitude, longitude);
        if (india != null)
            if (dest_lng != null && !dest_lng.equalsIgnoreCase(""))
                betweenPathDraw(String.valueOf(latitude), String.valueOf(longitude), dest_lat, dest_lng);

    }

    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
    }

    private void checkPermissions() {

        int permissionLocation = ContextCompat.checkSelfPermission(Act_Tow_Track_Maps.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                Log.e(TAG, "checkPermissions: ");
                isEnable = false;
            }
        } else {
            getMyLocation();
        }
    }

    private void getMyLocation() {

        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {

                int permissionLocation = ContextCompat.checkSelfPermission(Act_Tow_Track_Maps.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    myLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(2000);
                    locationRequest.setFastestInterval(2000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                    builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(@NonNull LocationSettingsResult result) {
                            if (result == null){
                                getMyLocation();
                                return;
                            }
                            final Status status = result.getStatus();
                            Log.e("Tag Status", "" + status.getStatusCode());
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat.checkSelfPermission(Act_Tow_Track_Maps.this, Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        myLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                                        if (myLocation != null) {
                                            if (dest_lng != null && !dest_lng.equalsIgnoreCase(""))
                                                betweenPathDraw(String.valueOf(myLocation.getLatitude()), String.valueOf(myLocation.getLongitude()), dest_lat, dest_lng);
                                        }
                                        isEnable = true;
                                        Log.e(TAG, "onResult: " + myLocation);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        isEnable = false;
                                        status.startResolutionForResult(Act_Tow_Track_Maps.this, REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    isEnable = true;
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("Tag On", "" + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Tag Connection Failed", connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            lin_progress.setVisibility(View.GONE);
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Log.e(TAG, "onLocationChanged: " + latitude);
            Log.e(TAG, "onLocationChanged: " + longitude);
            if (dest_lng != null && !dest_lng.equalsIgnoreCase(""))
                betweenPathDraw(String.valueOf(latitude), String.valueOf(longitude), dest_lat, dest_lng);

        } else {
            lin_progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDrag(MotionEvent motionEvent) {

    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    PolylineOptions lineOptions = null;

    @SuppressLint("StaticFieldLeak")
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            Log.e(TAG, "jsonData[0]=>" + jsonData[0]);

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList points = null;
//            PolylineOptions lineOptions = null;
            latLng = new ArrayList<>();

            if (result != null) {
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList();
                    List<HashMap<String, String>> path = result.get(i);
                    Log.e("Tag Path", "\n" + path.get(0).toString());

                    for (int j = 0; j < path.size(); j++) {
                        Log.e("Tag Path Points", "\n" + path.get(j).toString());
                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                        latLng.add(position);
                    }

                    if (lineOptions == null) {
                        lineOptions = new PolylineOptions();
                        lineOptions.addAll(points);
                        lineOptions.width(12);
                        lineOptions.color(Color.parseColor("#FF2E8FED"));
                        lineOptions.geodesic(true);
                    } else {
                        if (polyline != null) {
                            polyline.setPoints(points);
                        }
                    }
                }
            }

            if (lineOptions != null) {

                if (polyline == null) {
                    polyline = mMap.addPolyline(lineOptions);
                }

            } else {
                //  Toast.makeText(getApplicationContext(), "Direction not Found..!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        if (handler != null && runnable != null)
            handler.removeCallbacks(runnable);
        if (mHandler != null && mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }
        if (mhandler != null && mrunnable != null) {
            mhandler.removeCallbacks(mrunnable);
        }
        super.onBackPressed();
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;
    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            onBackPressed();
        } else if (v == txt_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_no));
            startActivity(intent);
        }
    }
}
