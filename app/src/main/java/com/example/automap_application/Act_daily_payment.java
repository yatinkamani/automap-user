package com.example.automap_application;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_daily_payment extends AppCompatActivity {

    LinearLayout tv_daily_Payments,tv_trip_cost,tv_petrol_cunsuption;
    ImageView back;
    SharedPreferences prefs ;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_daily_payment);

        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        editor = prefs.edit();

        back=findViewById(R.id.petrol_back);
        tv_daily_Payments = findViewById(R.id.Tv_daily_payment);
        tv_petrol_cunsuption = findViewById(R.id.Tv_petrol_cunsuption);
        tv_trip_cost = findViewById(R.id.Tv_Trip_cost);

        tv_daily_Payments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Act_daily_payment.this, Act_daily_petrol_payment.class);
                startActivity(intent);
            }
        });

        tv_trip_cost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!prefs.getString("pcr","-1").equals("-1")){

                    Intent intent = new Intent(Act_daily_payment.this, Act_My_trip_cost.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(), getString(R.string.attention_this_service_will_be_available_after_patrol),Toast.LENGTH_LONG).show();
                }
            }
        });

        tv_petrol_cunsuption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Act_daily_payment.this, Act_petrol_consumption.class);
                startActivity(intent);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
