package com.example.automap_application;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.GarageNotificationModel;
import com.example.automap_application.Model.RejectReason;
import com.example.automap_application.RetrofitApi.ApiConfig;
import com.example.automap_application.RetrofitApi.AppConfig;
import com.example.automap_application.RetrofitApi.UpdateProfile.ServerResponse;
import com.example.automap_application.adapter.Adapter_Garage_offer_List;
import com.example.automap_application.adapter.SlidingImage_Adapter;
import com.example.automap_application.backgroundServices.RemainderBroadcast;
import com.example.automap_application.utils.AppUtils;
import com.example.automap_application.view.WorkaroundMapFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Garage extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {

    @SuppressLint("StaticFieldLeak")
    static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES = {R.drawable.car1, R.drawable.car2, R.drawable.car3, R.drawable.car4, R.drawable.car5};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
//    private ScheduleClient scheduleClient;
    private GoogleApiClient mGoogleApiClient;
    Location mLocation;
    LocationManager mLocationManager;
    LocationRequest mLocationRequest;
    private LocationListener listener;
    long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    long FASTEST_INTERVAL = 20000;    /* 20 sec */
    LocationManager locationManager;
    private LatLng latLng;
    private boolean isPermission;
    GoogleMap mMap;
    ScrollView mScrollView;
    EditText editText;
    SharedPreferences prefs;
    ImageView back;
    TextView text_garage_name, txt_address, txt_phone_no, txt_multiple;
    ImageView img_banner;
    String garage_id = "", appointment_id = "", facebook_link = "", garage_banner = "", garage_name = "", rating = "", contact_no = "", address = "";
    LinearLayout ll_call, ll_web, go_to_garage, ll_go_garage, lin_send, ll_book_appointment, ll_rate;
    TextView txt_distance, txt_rate;
    RatingBar ratingBar;
    String date = "", time = "";
    String appointment_date = "", appointment_time = "";

    List<RejectReason> rejectReasons = new ArrayList<>();

    boolean call = false, garage_offer = false;

    ProgressDialog progressDialog;

    LinearLayout lin_order, lin_agree, lin_work_done;
    Button btn_order_yes, btn_order_no, btn_agree_yes, btn_agree_no, btn_work_yes;

    View no_data;
    Button btn_retry;
    TextView txt_msg;
    Toolbar toolbar;

    Button btn_complain;
    int year, month, dayOfMonth, hourOfDay, minute;

    String name, user_id, email;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
//        applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            //Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng);//your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_act__garage);
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);

        String language = prefs.getString(getString(R.string.pref_language), "ar").toString().trim();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));

//        scheduleClient = new ScheduleClient(getApplicationContext());
//        scheduleClient.doBindService();

        back = findViewById(R.id.back);
        img_banner = findViewById(R.id.img_banner);
        text_garage_name = findViewById(R.id.text_garage_name);
        txt_address = findViewById(R.id.txt_address);
        txt_phone_no = findViewById(R.id.phone_no);
        ll_call = findViewById(R.id.ll_call);
        editText = findViewById(R.id.ed_garage);
        ll_web = findViewById(R.id.ll_web);
        txt_distance = findViewById(R.id.txt_distance);
        go_to_garage = findViewById(R.id.go_to_garage);
        ll_go_garage = findViewById(R.id.ll_go_garage);
        lin_send = findViewById(R.id.lin_send);
        ll_book_appointment = findViewById(R.id.ll_book_appointment);
        txt_multiple = findViewById(R.id.txt_multiples);
        toolbar = findViewById(R.id.toolbar);
        btn_complain = findViewById(R.id.btn_complain);

        ll_rate = findViewById(R.id.ll_rate);
        txt_rate = findViewById(R.id.txt_rate);
        ratingBar = findViewById(R.id.ratingBar);

        lin_order = findViewById(R.id.lin_order);
        lin_agree = findViewById(R.id.lin_agree);
        lin_work_done = findViewById(R.id.lin_work);
        btn_order_yes = findViewById(R.id.btn_order_yes);
        btn_order_no = findViewById(R.id.btn_order_no);
        btn_agree_yes = findViewById(R.id.btn_agree_yes);
        btn_agree_no = findViewById(R.id.btn_agree_no);
        btn_work_yes = findViewById(R.id.btn_work_yes);

        mScrollView = findViewById(R.id.scroll_view);
        no_data = findViewById(R.id.no_data_error);
        btn_retry = findViewById(R.id.btn_retry);
        txt_msg = findViewById(R.id.txt_msg);

        init();
        getIntentData();
        getRejectionReasonList();
        HandlerCall();

        user_id = prefs.getString(getString(R.string.pref_user_id), "");
        name = prefs.getString(getString(R.string.pref_user_name), "");
        email = prefs.getString(getString(R.string.pref_email), "");
        contact_no = prefs.getString(getString(R.string.pref_contatct), "");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getGarageInfo();
        btn_retry.setOnClickListener(this);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(Act_Garage.this)
                .addConnectionCallbacks(Act_Garage.this)
                .addOnConnectionFailedListener(Act_Garage.this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        checkLocation();
        // check whether location service is enable or not in your phone

        ll_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call = true;
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_no));
                startActivity(intent);
            }
        });

        ll_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!facebook_link.startsWith("http://") && !facebook_link.startsWith("https://"))
                    facebook_link = "http://" + facebook_link;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebook_link));
                startActivity(browserIntent);
            }
        });

//        ll_go_garage.setVisibility(View.GONE);

        setStatusView();

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        appointment_date = String.valueOf(year) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(dayOfMonth);
        appointment_time = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
        Log.e("Tag time date", date + " " + time);

        ll_go_garage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txt_multiple.getText().equals(getString(R.string.confirm))) {
                    /*if (!isMyServiceRunning(NotifyService.class)) {
                        onDateSelectedButtonClick(Constant.model.getAppointment_date(), time, 4);
                    }*/
                    txt_multiple.setText(getString(R.string.go_now_to_workshop));
                } else {
                    /*if (!isMyServiceRunning(NotifyService.class)) {
                        scheduleClient.doUnbindService();
                    }*/
                    lin_order.setVisibility(View.VISIBLE);
                    lin_order.requestFocus();
                    message();
                }
            }
        });

        ll_book_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_pick();
            }
        });

        btn_order_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CancelOrder();
            }
        });

        btn_order_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lin_order.setVisibility(View.GONE);
                lin_agree.setVisibility(View.VISIBLE);
                lin_agree.requestFocus();
            }
        });

        btn_agree_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constant.model.getStatus().equals("5")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.cancel_order), Toast.LENGTH_LONG).show();
                } else if (Constant.model.getStatus().equals("3")) {
                    agree_dialog();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.wait_some_time_to_workshop_respose), Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            }
        });

        btn_agree_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constant.model.getStatus().equals("5")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.cancel_order), Toast.LENGTH_LONG).show();
                } else if (Constant.model.getStatus().equals("3")) {
                    agree_dialog();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.wait_some_time_to_workshop_respose), Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            }
        });

        btn_work_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constant.model.getStatus().equals("8"))
                    CancelOrderApi("10", null);
                else if (Constant.model.getStatus().equals("6")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.wait_some_time_to_workshop_respose), Toast.LENGTH_LONG).show();
                    onBackPressed();
                } else if (Constant.model.getStatus().equals("7")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.cancel_agreement), Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            }
        });

        btn_complain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ComplainDialog();
            }
        });

        ActionBar();

        InitializeBroadcast();
    }

    public void setStatusView(){
        if (Constant.model != null) {

            /*@SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
            Date appointmentDate = null;
            Date currentDate = null;
            try {
                appointmentDate = format.parse(Constant.model.getAppointment_date() + " " + Constant.model.getAppointment_time());
                currentDate = format.parse(format.format(new Date()));
                Log.e("TAG TIME", "" + format.format(currentDate));
                Log.e("TAG TIME", "" + format.format(appointmentDate));

            } catch (ParseException e) {
                e.printStackTrace();
//                Log.e("TAG TIME", e.toString());
            }
//            Log.e("TAG TIME", ""+Constant.model.getAppointment_date() +" "+Constant.model.getAppointment_time());
            if (currentDate.before(appointmentDate)) {
                long diff = appointmentDate.getTime() - currentDate.getTime();
                Log.e("days", "" + diff);
                if (diff > 0) {

                    long second = diff / 1000;
                    long minute = second / 60;
                    long hour = minute / 60;
                    long days = (hour / 24) + 1;
                    Log.e("hour", "" + hour);
                    if (hour > 20) {
                        ll_go_garage.setVisibility(View.VISIBLE);
                        if (Constant.model.getStatus().equals("1")) {
                            txt_multiple.setText(getString(R.string.confirm));
                        } else if (Constant.model.getStatus().equals("3")) {
                            txt_multiple.setText(getString(R.string.go_now_to_workshop));
                        }
                    } else {
                        txt_multiple.setText(getString(R.string.go_now_to_workshop));
                        if (Constant.model.getStatus().equals("3")) {
                            ll_go_garage.setVisibility(View.GONE);
                            lin_order.setVisibility(View.VISIBLE);
                        } else if (Constant.model.getStatus().equals("1")) {
                            ll_go_garage.setVisibility(View.VISIBLE);
                            txt_multiple.setText(getString(R.string.go_now_to_workshop));
                        }
                    }
                } else {
                    ll_go_garage.setVisibility(View.GONE);
                }
            } else {
                ll_go_garage.setVisibility(View.GONE);
            }*/

            appointment_id = Constant.model.getGarage_appointment_id();
            date = Constant.model.getAppointment_date();
            time = Constant.model.getAppointment_time();
            // status 3 workshop ignore appointmentt request
            if (Constant.model.getStatus().equals("3")) {
                lin_order.setVisibility(View.VISIBLE);
                btn_complain.setVisibility(View.VISIBLE);
                lin_order.requestFocus();
            }
            // status 6 customer declain appointment
            else if (Constant.model.getStatus().equals("6")) {
                lin_order.setVisibility(View.GONE);
                lin_agree.setVisibility(View.GONE);
                btn_complain.setVisibility(View.VISIBLE);
                ll_go_garage.setVisibility(View.GONE);
            }
            // status 8 customer workshop agree appointment request
            else if (Constant.model.getStatus().equals("8")) {
                lin_order.setVisibility(View.GONE);
                lin_agree.setVisibility(View.GONE);
                lin_work_done.setVisibility(View.VISIBLE);
                btn_complain.setVisibility(View.VISIBLE);
                lin_send.setVisibility(View.GONE);
            }
            // status 1 workshop participate appointment request
            else if (Constant.model.getStatus().equals("1")) {
                lin_send.setVisibility(View.VISIBLE);
                txt_multiple.setText(getString(R.string.go_now_to_workshop));
            }
            // status 8 work done this appointment request
            else if (Constant.model.getStatus().equals("10")) {
                lin_send.setVisibility(View.GONE);
                RattingDialog();
            }
        }
    }

    // new offers come to refress api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (getQuotes_badge != 0 && !(Act_Garage.this.isFinishing())){
                    getGarageOffer();
                }

                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);
            }
        }
    };

    public void InitializeBroadcast() {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }

    // get offer for appointment sended and clear notification and get distace
    private void getGarageOffer() {

            ProgressDialog dialog = new ProgressDialog(Act_Garage.this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL+"user_garage_list", new Response.Listener<String>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(String response) {
                    Log.e("notification_list"," " +response);
                    try {

                        JSONObject mainJsonObject = new JSONObject(response);
                        if (mainJsonObject.getString("status").equals("1")){
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            if (result.length()>0){

                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject jsonObject = result.getJSONObject(i);

                                    if (jsonObject.getString("garage_appointment_id").equals(Constant.model.getGarage_appointment_id()) &&
                                            jsonObject.getString("garage_id").equals(Constant.model.getGarage_id())){

                                        GarageNotificationModel notification_model = new GarageNotificationModel();
                                        notification_model.setGarage_appointment_id(jsonObject.getString("garage_appointment_id"));
                                        notification_model.setName(jsonObject.getString("garage_name"));
                                        notification_model.setGarage_id(jsonObject.getString("garage_id"));
                                        notification_model.setStatus(jsonObject.getString("status"));
                                        notification_model.setEmail(jsonObject.getString("email"));
                                        notification_model.setCity(jsonObject.getString("city_name"));
                                        notification_model.setAddress(jsonObject.getString("address"));
                                        notification_model.setContact_no(jsonObject.getString("contact_no"));
                                        notification_model.setPrice(jsonObject.optString("price",""));
                                        notification_model.setTime(jsonObject.optString("time",""));
                                        notification_model.setUser_image(jsonObject.getString("owner_profile_img"));
                                        notification_model.setAppointment_date(jsonObject.optString("appointment_date",""));
                                        notification_model.setAppointment_time(jsonObject.optString("appointment_time",""));

                                        if (jsonObject.has("user_complain")){
                                            notification_model.setUser_complain(jsonObject.getString("user_complain"));
                                        }

                                        if (jsonObject.has("garage_complain")){
                                            notification_model.setUser_complain(jsonObject.getString("garage_complain"));
                                        }
                                        if (jsonObject.getString("latitude") != null && !(jsonObject.getString("latitude").equalsIgnoreCase("null"))){
                                            if (0 < jsonObject.getString("latitude").length())
                                                notification_model.setLatitude(jsonObject.getString("latitude"));
                                            else
                                                notification_model.setLatitude("0.0");
                                        } else {
                                            notification_model.setLatitude("0.0");
                                        }

                                        if (jsonObject.getString("longitude") != null && !(jsonObject.getString("longitude").equalsIgnoreCase("null"))){
                                            if (0 < jsonObject.getString("longitude").length())
                                                notification_model.setLongitude(jsonObject.getString("longitude"));
                                            else
                                                notification_model.setLongitude("0.0");
                                        } else {
                                            notification_model.setLongitude("0.0");
                                        }

                                        notification_model.setDistance("");

                                        Log.e("TAG",jsonObject.getString("garage_name"));
                                        Constant.model = notification_model;
                                        setStatusView();
                                        break;
                                    }
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dialog.dismiss();
                }
            },  new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    dialog.dismiss();
                }
            }){
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params=new HashMap<String, String>();
                    params.put("user_id",user_id);
                    params.put("garage_appointment_id",Constant.model.getGarage_appointment_id());
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

    }

    // notification click open this screen
    public void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            String messageFor = intent.getStringExtra("messageFor");
            if (messageFor != null) {
                if (intent.getStringExtra("messageFor").contains("appointment_remainder")) {
                    garage_id = intent.getBundleExtra("bundle").getString("garage_id");
                    appointment_id = intent.getBundleExtra("bundle").getString("appointment_id");
                    garage_name = intent.getBundleExtra("bundle").getString("garage_name");
                } else {
                    garage_id = intent.getStringExtra("garage_id");
                    garage_name = intent.getStringExtra("garage_name");
                    facebook_link = intent.getStringExtra("website");
                }
            } else {
                garage_id = intent.getStringExtra("garage_id");
                garage_name = intent.getStringExtra("garage_name");
                facebook_link = intent.getStringExtra("website");
                garage_offer = intent.getBooleanExtra("garage_offer", false);
            }
            garage_id = intent.getStringExtra("garage_id");
            garage_name = intent.getStringExtra("garage_name");
            facebook_link = intent.getStringExtra("website");
            garage_offer = intent.getBooleanExtra("garage_offer", false);
        }
    }

    public void ActionBar() {
//        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(garage_name);
            actionBar.setDisplayShowHomeEnabled(true);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    String latitude, longitude;

    public void onDateSelectedButtonClick(String date, String time, int time_diff) {
        // Get the date from our date_picker
        // Create a new calendar set to the date chosen
        // we set the time to midnight (i.e. the first minute of that day)
        String year = date.split("-")[0];
        String month = date.split("-")[1];
        String day = date.split("-")[2];
        String hour = time.split(":")[0];
        String mini = time.split(":")[1];
        Calendar c = Calendar.getInstance();

        if (Integer.parseInt(hour) > 4) {
            int time_date = (Integer.parseInt(hour)) - time_diff;
            Log.e("TAG", "" + hour + " " + time_date);
            c.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day), time_date, Integer.parseInt(mini));
        } else if (Integer.parseInt(hour) == 4) {
            int time_date = (Integer.parseInt(hour)) - time_diff;
            c.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day), time_date, Integer.parseInt(mini));
        } else if (Integer.parseInt(hour) == 3) {
            c.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day) - 1, 23, Integer.parseInt(mini));
        } else if (Integer.parseInt(hour) == 2) {
            c.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day) - 1, 22, Integer.parseInt(mini));
        } else if (Integer.parseInt(hour) == 1) {
            c.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day) - 1, 21, Integer.parseInt(mini));
        } else if (Integer.parseInt(hour) == 0) {
            c.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day) - 1, 20, Integer.parseInt(mini));
        }

        // Ask our service to set an alarm for that date, this activity talks to the client that talks to the service

        Bundle bundle = new Bundle();
        bundle.putString("garage_id", garage_id);
        bundle.putString("appointment_id", appointment_id);
        bundle.putString("garage_name", garage_name);
        bundle.putString("title", "Go Now To The Garage");
        bundle.putString("msg", "Thank you");
        /*Log.e("Tag s",""+c.get(Calendar.SECOND));
          Log.e("Tag m",""+c.get(Calendar.MINUTE));
          Log.e("Tag d" ,""+c.get(Calendar.HOUR_OF_DAY));
          Log.e("Tag D",""+c.get(Calendar.DATE));
          Log.e("Tag M",""+c.get(Calendar.MONTH)+1);
          Log.e("Tag y",""+c.get(Calendar.YEAR)); */
//        cabled.set(cabled.get(Calendar.YEAR), cabled.get(Calendar.MONTH)+1, cabled.get(Calendar.DATE),cabled.get(Calendar.HOUR),cabled.get(Calendar.MINUTE)+1);
        Calendar cabled = Calendar.getInstance();

        cabled.set(Calendar.YEAR, 2020);
        cabled.set(Calendar.MONTH, Calendar.FEBRUARY);
        cabled.set(Calendar.DAY_OF_MONTH, 10);
        cabled.set(Calendar.HOUR_OF_DAY, 16);
        cabled.set(Calendar.MINUTE, 0);
        cabled.set(Calendar.SECOND, 0);
        cabled.set(Calendar.MILLISECOND, 0);
//        scheduleClient.setAlarmForNotification(cabled, bundle, 12);

        Log.e("TAG", cabled.get(Calendar.MINUTE) + " " + cabled.get(Calendar.HOUR) + " " + cabled.get(Calendar.DAY_OF_MONTH));
    }

    // get workshop detail
    private void getGarageInfo() {

        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "check_garage_status",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("status_response", " " + response);
                        try {
                            JSONObject mainJsonObject = new JSONObject(response);
                            if (mainJsonObject.getString("status").equals("true")) {
                                JSONArray result = mainJsonObject.getJSONArray("result");
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject jsonObject = result.getJSONObject(i);
                                    contact_no = jsonObject.getString("contact_no");
                                    garage_name = jsonObject.getString("garage_name");
                                    garage_banner = jsonObject.getString("garage_banner");
                                    address = jsonObject.getString("address");
                                    latitude = jsonObject.getString("latitude");
                                    longitude = jsonObject.getString("longitude");
                                    facebook_link = jsonObject.getString("facebook_link");
                                    if (jsonObject.has("ratting")) {
                                        rating = jsonObject.getString("ratting");
                                    }
                                }

                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("LATITUDE", latitude);
                                editor.putString("LONGITUDE", longitude);
                                editor.apply();

                                if (facebook_link.equals("null") || facebook_link.equals("")) {
                                    ll_web.setVisibility(View.GONE);
                                } else {
                                    ll_web.setVisibility(View.VISIBLE);
                                }

                                if (!rating.equals("") || !rating.isEmpty()) {
                                    ll_rate.setVisibility(View.VISIBLE);
                                    txt_rate.setText(Html.fromHtml(rating));
                                    ratingBar.setRating(Float.parseFloat(rating));
                                } else {
                                    ll_rate.setVisibility(View.GONE);
                                }

                                latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                                if (mMap != null) {
                                    marker = mMap.addMarker(new MarkerOptions().position(latLng).title(garage_name));
                                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                                    mMap.animateCamera(location);
                                }

                                final WorkaroundMapFragment mapFragment = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
                                mapFragment.getMapAsync(Act_Garage.this);

                                text_garage_name.setText(garage_name);
                                if (!address.equals("") && address.split(",").length > 0) {
                                    String s = address.split(",")[0];
                                    try {
                                        txt_address.setText(s + ", \n"+ address.replaceAll(s+",",""));
                                    }catch (Exception e){
                                        txt_address.setText(address);
                                    }
                                } else {
                                    txt_address.setText(address);
                                }
                                txt_phone_no.setText(contact_no);
                                Glide.with(getApplicationContext()).asBitmap().load(garage_banner)
                                        .placeholder(getResources().getDrawable(R.drawable.amr_icon))
                                        .thumbnail(0.01f)
                                        .into(new BitmapImageViewTarget(img_banner) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                img_banner.setImageBitmap(resource);
                                            }
                                        });

                                if (result.length() > 0) {
                                    no_data.setVisibility(View.GONE);
                                    mScrollView.setVisibility(View.VISIBLE);

                                } else {
                                    no_data.setVisibility(View.VISIBLE);
                                    mScrollView.setVisibility(View.GONE);
                                    txt_msg.setText(getString(R.string.no_detail_found));
                                    btn_retry.setVisibility(View.GONE);
                                }

                            } else {
                                no_data.setVisibility(View.VISIBLE);
                                mScrollView.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_detail_found));
                                btn_retry.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            no_data.setVisibility(View.VISIBLE);
                            mScrollView.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.somting_wrong_please));
                            btn_retry.setVisibility(View.VISIBLE);
                        }
                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                no_data.setVisibility(View.VISIBLE);
                mScrollView.setVisibility(View.GONE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
            }
        }) {

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("garage_id", garage_id);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\n Please Enable Location to " + "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Log.e("Log Provide", "" + locationManager.getProviders(true));
        Log.e("Log Provide 1", "" + locationManager.getAllProviders());
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return true;
        } else {
            return false;
        }
//        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean requestSinglePermission() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        //Single Permission is granted
                        isPermission = true;
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            isPermission = false;
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {
                    }
                }).check();

        return isPermission;
    }

    // garage banner image agger idicatore
    private void init() {
        for (int i = 0; i < IMAGES.length; i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);

        mPager.setAdapter(new SlidingImage_Adapter(Act_Garage.this, ImagesArray));

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//      Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 1000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    Marker marker;
    String latitude1;
    String longitude1;

    // on map view display location
    @SuppressLint({"DefaultLocale", "SetTextI18n", "MissingPermission"})
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.setMyLocationEnabled(true);
        mScrollView.fullScroll(View.FOCUS_DOWN);
        if (mMap != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                //      here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                //  to handle the case where the user grants the permission. See the documentation
                //  for ActivityCompat#requestPermissions for more details.
                return;
            }

            latitude1 = prefs.getString("LATITUDE", "");
            longitude1 = prefs.getString("LONGITUDE", "");

            Location locations = mMap.getMyLocation();

            if (locations != null) {
                Location locationB = new Location("");
                locationB.setLatitude(ParseDouble(latitude1));
                locationB.setLongitude(ParseDouble(longitude1));
                double distance = locations.distanceTo(locationB) / 1000;

                // txt_distance.setText(String.valueOf(distance)+"km");
                txt_distance.setText(String.format("%.2f", distance) + "km");
            } else {
                txt_distance.setText(getString(R.string.loading_distance));
            }

            if (latitude1 != null && longitude1 != null) {
                latLng = new LatLng(ParseDouble(latitude1), ParseDouble(longitude1));
                if (marker != null) {
                    marker.remove();
                }
                if (mMap != null) {
                    marker = mMap.addMarker(new MarkerOptions().position(latLng).title(garage_name));
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                    mMap.animateCamera(location);
                }
            }
            Log.d("pp_pp_pp_pp", "onMapReady: " + latLng);
            //  parent scrollview in xml, give your scrollview id value
            ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
                @Override
                public void onTouch() {
                    mScrollView.requestDisallowInterceptTouchEvent(true);
                }
            });
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //   int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {

            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            // mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));

        } else {
            Toast.makeText(this, getString(R.string.location_not_detected), Toast.LENGTH_SHORT).show();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY).setInterval(UPDATE_INTERVAL).setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //       ActivityCompat#requestPermissions
            //       here to request the missing permissions, and then overriding
            //       public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                           int[] grantResults)
            //        to handle the case where the user grants the permission. See the documentation
            //         for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            Log.d("request", "Connected --->>>>");
        }
        Log.d("request", "--->>>>");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mHandler != null && mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Tag", "Connection Result " + connectionResult.getErrorMessage());
    }

    Runnable mRunnable;
    Handler mHandler = new Handler();
    boolean update = false;

    // location update api
    private void UpdateLocation(String latitude, String longitude) {

        if (update) {
            update = false;
            ApiConfig config = AppConfig.getRetrofit().create(ApiConfig.class);
            config.UpdateLocation(user_id, name, email, contact_no, "", latitude, longitude).enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(@NonNull Call<ServerResponse> call, @NonNull retrofit2.Response<ServerResponse> response) {
                    Log.e("tag response", response.body().toString());
                    Log.e("Tag Response", response.body().getMessage());
                }

                @Override
                public void onFailure(@NonNull Call<ServerResponse> call, @NonNull Throwable t) {
                    Log.e("tag Error", t.toString());

                }
            });
        }
    }

    // update 5 second
    private void HandlerCall() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                update = true;
                mRunnable = this;
                mHandler.postDelayed(mRunnable, 10000);
            }
        }, 5000);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onLocationChanged(Location location) {

        ArrayList<Location> locations = new ArrayList<>();
        locations.add(location);

        Log.e("location", " " + locations.size());
        UpdateLocation("" + location.getLatitude(), "" + location.getLongitude());

        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        Location locationB = new Location("");
        locationB.setLatitude(ParseDouble(latitude1));
        locationB.setLongitude(ParseDouble(longitude1));

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(locations.get(0).getLatitude(), locations.get(0).getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*  double distance = (locations.get(0).distanceTo(locations.get(locations.size() - 1))) / 1000;  */
        double distance = (location.distanceTo(locationB)) / 1000;
        // txt_distance.setText(String.valueOf(distance)+"km");
        txt_distance.setText(String.format("%.2f", distance) + "km");

        float[] dist = new float[1];
        Location.distanceBetween(location.getLatitude(), location.getLongitude(), locationB.getLatitude(), locationB.getLongitude(), dist);

//        Log.e("TAG", ""+dist[0]/1000);
        /*if (dist[0] / 1000 < 1) {
            agree_dialog();
        }*/
        /* WorkaroundMapFragment mapFragment = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this); */
        startLocationUpdates();
    }

    double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;
                // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (call) {
//            message();
            call = false;
        }
    }

    // message dialog
    public void message() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.do_you_want_to_go_to_this_garage_now));
        builder.setPositiveButton(getString(R.string.yess), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                timer_dialog();
            }
        });
        builder.setNegativeButton(getString(R.string.nos), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    // booking any one offer dialog
    public void messageBooking() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.you_sure_want_to_booking));
        builder.setPositiveButton(getString(R.string.yess), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (Constant.model.getStatus().equals("1")) {
                    ConformedAppointment();
                }
            }
        });
        builder.setNegativeButton(getString(R.string.nos), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    int count = 10;
    Handler handler;
    Runnable runnable;

    // after book show 10 count dialog after automatically booked and show map screen
    @SuppressLint("SetTextI18n")
    public void timer_dialog() {
        count = 10;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);
        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        TextView txt_message = view.findViewById(R.id.txt_message);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_cancel.setText(getString(R.string.cancel));
        txt_dialog_title.setText(getString(R.string.show_the_map_on_real_time_location));
        txt_message.setVisibility(View.VISIBLE);
        txt_message.setGravity(Gravity.CENTER);
        txt_message.setText("10");
        txt_message.setTextSize(20);
        txt_message.setTextColor(Color.RED);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                count--;
                txt_message.setText(String.valueOf(count));
                btn_ok.setText(getString(R.string.confirm) + "(" + count + ")");
                if (count == 0) {
                    dialog.dismiss();
                    if (Constant.model.getStatus().equals("1")){
                        ConformedAppointment();
                    }
                    Intent intent = new Intent(getApplicationContext(), Act_Map.class);
                    intent.putExtra("notificationLatitude", latitude);
                    intent.putExtra("notificationLongitude", longitude);
                    intent.putExtra(getString(R.string.contact_no), contact_no);
                    startActivity(intent);
                } else {
                    handler.postDelayed(runnable, 1000);
                }
            }
        };

//        btn_ok.setText(getString(R.string.confirm));
        handler.postDelayed(runnable, 1000);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                dialog.dismiss();
                if (Constant.model.getStatus().equals("1")){
                    ConformedAppointment();
                }
                Intent intent = new Intent(getApplicationContext(), Act_Map.class);
                intent.putExtra("notificationLatitude", latitude);
                intent.putExtra("notificationLongitude", longitude);
                startActivity(intent);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                dialog.dismiss();
            }
        });
    }

    // booked workshop agree with workshop dialog
    public void agree_dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.message));
        builder.setMessage(getString(R.string.do_you_agree_with_the_owner_of_the_garage));

        builder.setPositiveButton(getString(R.string.yess), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogs, int which) {
//                dialogs.dismiss();
                // status = 6 ;
                lin_agree.setVisibility(View.GONE);
                lin_work_done.setVisibility(View.VISIBLE);
                lin_work_done.requestFocus();
                CancelOrderApi("6", dialogs);
            }
        });
        builder.setNegativeButton(getString(R.string.nos), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // status = 7 ;
                CancelOrderApi("7", dialog);
//                CancelReasonDialog();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

    }

    boolean price = false, times = false, trust = false;
    ArrayList<String> checkReason = new ArrayList<>();

    ArrayList<String> strings = new ArrayList<>();

    @SuppressLint("SetTextI18n")
    public void CancelReasonDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);

        CheckBox chk_price = view.findViewById(R.id.chk_price);
        CheckBox chk_time = view.findViewById(R.id.chk_time);
        CheckBox chk_trust = view.findViewById(R.id.chk_trust);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        txt_dialog_title.setText("Reason For Cancel Agreement");
        LinearLayout linearLayout = view.findViewById(R.id.lin_dynamic_checkbox);
        btn_cancel.setVisibility(View.GONE);
        chk_price.setVisibility(View.GONE);
        chk_time.setVisibility(View.GONE);
        chk_trust.setVisibility(View.GONE);

        strings = new ArrayList<>();
        int states[][] = {{android.R.attr.state_checked}, {}};
        int colors[] = {Color.BLACK, Color.RED};
        CompoundButtonCompat.setButtonTintList(chk_price, new ColorStateList(states, colors));
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        linearLayout.removeAllViews();
        for (int i = 0; i < rejectReasons.size(); i++) {
            if (rejectReasons.get(i).getStatus().equals("0") || rejectReasons.get(i).getStatus().equals("3")) {

                CheckBox checkBox = new CheckBox(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                checkBox.setText(rejectReasons.get(i).getReason());
                checkBox.setTextColor(Color.BLACK);
                checkBox.setLayoutParams(params);
                final int finalI = i;
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            rejectReasons.get(finalI).setIs_checked("1");
                            strings.add(rejectReasons.get(finalI).getId());
                        } else {
                            rejectReasons.get(finalI).setIs_checked("0");
                            strings.remove(rejectReasons.get(finalI).getId());
                        }
                    }
                });
                linearLayout.addView(checkBox);
            }
        }

        /*chk_price.setOnCheckedChangeListener((buttonView, isChecked) -> {
            price = isChecked;
            if (isChecked){
                checkReason.add(buttonView.getText().toString());
            }else {
                checkReason.remove(buttonView.getText().toString());
            }
        });
        chk_time.setOnCheckedChangeListener((buttonView, isChecked) -> {
            times = isChecked;
            if (isChecked){
                checkReason.add(buttonView.getText().toString());
            }else {
                checkReason.remove(buttonView.getText().toString());
            }
        });
        chk_trust.setOnCheckedChangeListener((buttonView, isChecked) -> {
            trust = isChecked;
            if (isChecked){
                checkReason.add(buttonView.getText().toString());
            }else {
                checkReason.remove(buttonView.getText().toString());
            }
        });*/

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onClick(View v) {
                // status =  7

                // on back the activity
                Toast.makeText(getApplicationContext(), checkReason.toString(), Toast.LENGTH_LONG);
                Log.e("Tag", checkReason.toString());
                CancelOrderApi("7", dialog);
//                dialog.dismiss();
            }
        });
    }

    // after work done redirect service book screen dialog
    public void Service_Book() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        TextView txt_message = view.findViewById(R.id.txt_message);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText(getString(R.string.open_service_book));
        txt_message.setText(getString(R.string.please_record_the_service_in_the_service_book));
        btn_ok.setText(getString(R.string.yess));
        btn_cancel.setText(getString(R.string.nos));
        txt_message.setVisibility(View.VISIBLE);
        btn_cancel.setVisibility(View.GONE);

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(),checkReason.toString(), Toast.LENGTH_LONG).show();
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), Act_Service_book.class);
                startActivity(intent);
                finish();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "will be asked after 60 min", Toast.LENGTH_LONG).show();
                /* Intent intent = new Intent(getApplicationContext(), AlertDialogWorkDoneService.class);
                   intent.putExtra("time",30000);
                   startService(intent); */
                dialog.dismiss();
            }
        });
    }

    // booked order request cancel dialog
    public void CancelOrder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        TextView txt_message = view.findViewById(R.id.txt_message);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText(getString(R.string.cancel_order));
        txt_message.setText(getString(R.string.if_you_sure_want_cancel_order));
        btn_ok.setText(getString(R.string.yess));
        btn_cancel.setText(getString(R.string.nos));
        txt_message.setVisibility(View.VISIBLE);

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(Act_Garage.this);
                builder1.setMessage(getString(R.string.you_have_canceled_the_order_now_and_if_this_process_repeats));

                builder1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (Act_Booking_List.instance != null) {
                            Act_Booking_List.instance.finish();
                        }
                        if (Act_Garage_Offer_List.instance != null) {
                            Act_Garage_Offer_List.instance.finish();
                        }
                        CancelOrderApi("5", dialog);

                    }
                });
                AlertDialog dialog1 = builder1.create();
                dialog1.show();

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                lin_order.setVisibility(View.GONE);
                lin_agree.setVisibility(View.VISIBLE);
            }
        });
    }

    float ratings = 0;

    // work don after rate this workshop dialog
    public void RattingDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        TextView txt_message = view.findViewById(R.id.txt_message);
        RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText(getString(R.string.please_rate_the_service));
        btn_ok.setText(getString(R.string.yess));
        btn_cancel.setText(getString(R.string.nos));
        btn_cancel.setVisibility(View.GONE);
        ratingBar.setVisibility(View.VISIBLE);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP); // for filled stars
        stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP); // for half filled stars
        stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_ATOP); // for empty stars
        txt_message.setVisibility(View.VISIBLE);
        txt_message.setText(R.string.ratting_service_time);

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (v == 1) {
                    Toast.makeText(getApplicationContext(), "Bad", Toast.LENGTH_LONG).show();
                } else if (v == 2) {
                    Toast.makeText(getApplicationContext(), "Bad Good", Toast.LENGTH_LONG).show();
                } else if (v == 3) {
                    Toast.makeText(getApplicationContext(), "Good", Toast.LENGTH_LONG).show();
                } else if (v == 4) {
                    Toast.makeText(getApplicationContext(), "Excellent Good", Toast.LENGTH_LONG).show();
                } else if (v == 5) {
                    Toast.makeText(getApplicationContext(), "Excellent", Toast.LENGTH_LONG).show();
                }
                ratings = v;
            }
        });
        btn_ok.setOnClickListener(v -> {
            if (ratings != 0) {
                dialog.dismiss();
//                Intent intent = new Intent(getApplicationContext(),Act_Service_book.class);
//                startActivity(intent);
                if (new Act_Garage_List().instance != null) {
                    new Act_Garage_List().instance.finish();
                }
                if (new Act_Garage_Offer_List().instance != null) {
                    new Act_Garage_Offer_List().instance.finish();
                }
                if (new Act_Booking_List().instance != null) {
                    new Act_Booking_List().instance.finish();
                }
                ratting("11", "" + ratings, dialog);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.please_rate_service), Toast.LENGTH_LONG).show();
            }
            /* Intent intents = new Intent(getApplicationContext(), AlertDialogWorkDoneService.class);
            stopService(intents); */
//            finish();
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "will be notified after 24 hours", Toast.LENGTH_LONG).show();
                /* Intent intent = new Intent(getApplicationContext(), AlertDialogWorkDoneService.class);
                intent.putExtra("time",30000);
                startService(intent); */
            }
        });
    }

    public void error_dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Network Connection");
        builder.setMessage("Click \"Ok\" retry Connection or Internet");
        builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getGarageInfo();
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass)  {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
        }
        return false;
    }

    // book appointment dialog confirm
    public void ConformedAppointment() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "change_garage_appointment_status",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Conformed response", response);
                        dialog.dismiss();
                        if (response != null) {
                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("1")) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.confirm_workshop_request), Toast.LENGTH_LONG).show();
                                    Constant.model.setStatus("3");
                                    lin_order.setVisibility(View.VISIBLE);
                                    ll_book_appointment.setVisibility(View.GONE);
                                    NotificationRemainder(appointment_date + " " + appointment_time, Integer.parseInt(Constant.model.getGarage_appointment_id()));

                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.not_confirm_workshop_request), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> map = new HashMap<>();
                map.put("status", "3");
                map.put("garage_appointment_id", Constant.model.getGarage_appointment_id());
                map.put("garage_id", garage_id);
                map.put("appointment_date", appointment_date);
                map.put("appointment_time", appointment_time);
                Log.e("Tag params", map.toString());
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    // remide before 5 hour not use
    public void NotificationRemainder(String datetime, int id) {
        Date Date = new Date();
        Date CurDate = new Date();
        try {
            Date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(datetime);
            CurDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));

            if (Date.after(CurDate) && AppUtils.printDifference(CurDate, Date, "h") > 5) {

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Date.getTime());
                calendar.add(Calendar.HOUR, -2);

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTimeInMillis(Date.getTime());

                Log.e("Tag Date 1", new SimpleDateFormat("hh:mm aa").format(calendar1.getTime()));
                Log.e("Tag Date 2", new SimpleDateFormat("hh:mm aa").format(calendar.getTime()));
                Intent intent = new Intent(getApplicationContext(), RemainderBroadcast.class);

                String msg = "Your Appointment Time 2 hour Remaining \n Time : " + new SimpleDateFormat("hh:mm aa").format(calendar1.getTime());
                intent.putExtra("title", "Appointment Request");
                intent.putExtra("messageFor", "garage");
                intent.putExtra("msg", msg);
                intent.putExtra("id", id);
                @SuppressLint("WrongConstant")
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), new Random().nextInt(), intent, Intent.FLAG_ACTIVITY_CLEAR_TASK);
                AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                Log.e("Tag Date Time", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(calendar.getTime()));
                manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void date_pick() {

        Locale.setDefault(Locale.ENGLISH);
        @SuppressLint("SimpleDateFormat")
        DatePickerDialog dialog = new DatePickerDialog(Act_Garage.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Log.e("year", "" + year);
                Log.e("month", "" + month);
                Log.e("dayOfMonth", "" + dayOfMonth);
                final String dates = String.valueOf(year) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(dayOfMonth);
                appointment_date = dates;
                Log.e("date", "" + date);
                time_pick();
            }
        }, year, month, dayOfMonth);

        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dialog.show();
    }

    public void time_pick() {
        @SuppressLint("SimpleDateFormat") final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        final Date[] dateObj = new Date[1];

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        Locale.setDefault(Locale.ENGLISH);
        TimePickerDialog timePickerDialog = new TimePickerDialog(Act_Garage.this, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Log.e("hourOfDay", "" + hourOfDay);
                Log.e("minute", "" + minute);
                final String times = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                try {
                    dateObj[0] = sdf.parse(times);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                int hour = hourOfDay;
                int minutes = minute;
                String timeSet = "";
                if (hour > 12) {
                    hour -= 12;
                    timeSet = "PM";
                } else if (hour == 0) {
                    hour += 12;
                    timeSet = "AM";
                } else if (hour == 12) {
                    timeSet = "PM";
                } else {
                    timeSet = "AM";
                }

                String min = "";
                if (minutes < 10)
                    min = "0" + minutes;
                else
                    min = String.valueOf(minutes);

                String aTime = String.valueOf(hour) + ':' + min + " " + timeSet;
                appointment_time = times;
                Log.e("minute", "" + time);
                messageBooking();
//              time = new SimpleDateFormat("hh:mm aa").format(dateObj[0]);
            }
        }, hour, minute, false);

        timePickerDialog.show();

    }

    // cancel appointment api
    public void CancelOrderApi(String status, DialogInterface alertDialog) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "cancel_garage_order",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        response = Html.fromHtml(response).toString();
                        response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                        Log.e("Appointment List", " " + response);
                        Log.e("Cancel Order response", response);
                        dialog.dismiss();
                        if (response != null) {
                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("1")) {
                                    if (alertDialog != null) {
                                        alertDialog.dismiss();
                                    }
                                    if (status.equals("5")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.cancel_workshop_request), Toast.LENGTH_LONG).show();
                                        finish();
                                    } else if (status.equals("6")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.agree_with_workshop), Toast.LENGTH_LONG).show();
                                    } else if (status.equals("7")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.not_agree_with_workshop), Toast.LENGTH_LONG).show();
                                        finish();
                                    } else if (status.equals("10")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.work_done), Toast.LENGTH_LONG).show();
                                        RattingDialog();
                                    } else if (status.equals("11")) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.thank_for_rate_this_workshop), Toast.LENGTH_LONG).show();
                                        Service_Book();
                                    }
                                    Constant.model.setStatus(status);
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.not_confirm_workshop), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag", error.toString());
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("status", status);
                map.put("garage_appointment_id", Constant.model.getGarage_appointment_id());
                map.put("garage_id", garage_id);
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    // not use
    private void getRejectionReasonList() {
        final ProgressDialog dialog = new ProgressDialog(getApplicationContext());
        dialog.setMessage(getString(R.string.loading));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_rejection_reason",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("notification_list", " " + response);
                        dialog.dismiss();
                        rejectReasons = new ArrayList<>();
                        JSONObject mainJsonObject = null;
                        try {
                            mainJsonObject = new JSONObject(response);
                            JSONArray result = mainJsonObject.getJSONArray("result");

                            for (int i = 0; i < result.length(); i++) {
                                JSONObject object = result.getJSONObject(i);
                                String id = object.getString("rejection_reson_id");
                                String status = object.getString("status");
                                String reason = object.getString("reason");
                                rejectReasons.add(new RejectReason(id, status, reason, "0"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(stringRequest);

    }

    // ratting workshop api
    private void ratting(String _status, String ratting,AlertDialog dialog) {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "user_ratting",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            response = Html.fromHtml(response).toString();
                            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                            Log.e("Appointment List", " " + response);
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    if (new Act_Mobile_Request_Offers_List().instance != null) {
                                        new Act_Mobile_Request_Offers_List().instance.finish();
                                    }

                                    if (new Act_Mobile_Request_List().instance != null) {
                                        new Act_Mobile_Request_List().instance.finish();
                                    }
                                    if (dialog != null){
                                        dialog.dismiss();
                                    }
                                    Service_Book();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("garage_appointment_id", appointment_id);
                    params.put("status", _status);
                    params.put("garage_id", garage_id);
                    params.put("user_id", user_id);
                    params.put("ratting", ratting);
                    Log.e("tag params", params.toString());
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry) {
            getGarageInfo();
        }
    }

    // complain dialog
    private void ComplainDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Garage.this);
        View view = LayoutInflater.from(Act_Garage.this).inflate(R.layout.dialog_complain, null, false);
        EditText ed_complain = view.findViewById(R.id.ed_complain);
        Button btn_complain = view.findViewById(R.id.btn_complain);
        builder.setView(view);

        AlertDialog dialog = builder.create();
        dialog.show();

        btn_complain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed_complain.getText().toString().equals("")) {
                    ed_complain.requestFocus();
                    ed_complain.setError(getString(R.string.enter_detial));
                } else {
                    StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "user_garage_complain", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Tag Response", response);

                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("1")) {
                                    dialog.dismiss();
//                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), getString(R.string.complain), Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Tag error", error.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("garage_id", garage_id);
                            map.put("user_id", user_id);
                            map.put("status", "13");
                            map.put("garage_appointment_id", appointment_id);
                            map.put("complain", ed_complain.getText().toString());
                            Log.e("Tag Params", map.toString());
                            return map;
                        }
                    };

                    RequestQueue queue = Volley.newRequestQueue(Act_Garage.this);
                    queue.add(request);
                }
            }
        });
    }
}