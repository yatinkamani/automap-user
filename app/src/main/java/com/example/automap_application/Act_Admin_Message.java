package com.example.automap_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Admin_Msg_Model;
import com.example.automap_application.extra.Constants;
import com.example.automap_application.extra.EqualSpacingItemDecoration;
import com.example.automap_application.utils.AppUtils;
import com.makeramen.roundedimageview.RoundedImageView;
import com.potyvideo.library.AndExoPlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Act_Admin_Message extends AppCompatActivity implements View.OnClickListener {

    ImageView back, img_refresh;
    TextView txt_title;
    RecyclerView rcv_view;
    View lin_error;
    Button btnRetry;
    TextView txtMsg;
    List<Admin_Msg_Model> admin_msg_models = new ArrayList<>();
    AdapterAdminMsg adapterAdminMsg;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    String model_id, brand_id, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__admin__message);

        back = findViewById(R.id.back);
        img_refresh = findViewById(R.id.img_refresh);
        txt_title = findViewById(R.id.txt_title);
        lin_error = findViewById(R.id.lin_error);
        rcv_view = findViewById(R.id.rcy_view);
        txtMsg = findViewById(R.id.txt_msg);
        btnRetry = findViewById(R.id.btn_retry);
        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);

        model_id = preferences.getString(getString(R.string.pref_car_model_id), "");
        brand_id = preferences.getString(getString(R.string.pref_car_brand_id), "");
        user_id = preferences.getString(getString(R.string.pref_user_id), "");
        back.setOnClickListener(this);
        btnRetry.setOnClickListener(this);
        img_refresh.setOnClickListener(this);

        AdminMsg();

    }

    // get all message and list out all
    private void AdminMsg() {

        ProgressDialog dialog = new ProgressDialog(Act_Admin_Message.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_admin_messages", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("Tag Error", response);
                    List<Admin_Msg_Model> admin_msg_models_in = new ArrayList<>();

                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getString("status").equals("true")) {

                            JSONArray array = object.getJSONArray("result");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                String id = object1.getString("message_id");
                                String message = object1.getString("message");
                                String img = object1.getString("message_image");
                                String created_at = object1.getString("created_at");
                                String video = "";
                                if (object1.has("message_video")) {
                                    video = object1.getString("message_video");
                                }

                                admin_msg_models_in.add(new Admin_Msg_Model(id, message, img, video, created_at));
                            }

                            admin_msg_models = new ArrayList<>();
                            for (Admin_Msg_Model msgModel : admin_msg_models_in) {
                                boolean isFound = false;
                                for (Admin_Msg_Model model : admin_msg_models) {
                                    if (model.getId().equals(msgModel.getId())) {
                                        isFound = true;
                                        break;
                                    }
                                }
                                if (!isFound) {
                                    admin_msg_models.add(msgModel);
                                }
                            }

                            if (admin_msg_models != null && admin_msg_models.size() > 0) {

                                rcv_view.setVisibility(View.VISIBLE);
                                lin_error.setVisibility(View.GONE);

                                adapterAdminMsg = new AdapterAdminMsg(admin_msg_models, getApplicationContext());
                                rcv_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
                                rcv_view.setAdapter(adapterAdminMsg);

                                SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt(getString(R.string.pref_admin_msg_badge_count), 0);
                                editor.apply();

                            } else {
                                rcv_view.setVisibility(View.GONE);
                                lin_error.setVisibility(View.VISIBLE);
                                txtMsg.setText(getString(R.string.no_data_found));
                                btnRetry.setVisibility(View.GONE);
                            }

                        } else {
                            rcv_view.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            txtMsg.setText(getString(R.string.no_data_available));
                            btnRetry.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        rcv_view.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txtMsg.setText(getString(R.string.somting_wrong_please));
                        btnRetry.setVisibility(View.VISIBLE);
                    }
                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Tag Error", error.toString());
                    rcv_view.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txtMsg.setText(getString(R.string.somting_wrong_please));
                    btnRetry.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", user_id);
                    Log.e("Tag Params", map.toString());
                    return map;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);

        } else {
            rcv_view.setVisibility(View.GONE);
            lin_error.setVisibility(View.VISIBLE);
            txtMsg.setText(getString(R.string.internet_not_connect));
            btnRetry.setVisibility(View.VISIBLE);
            dialog.dismiss();

        }
    }

    public void InitializeBroadcast(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(Act_Admin_Message.this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (admin_count > 0 && !(Act_Admin_Message.this.isFinishing())) {
                    AdminMsg();
                }
                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);

            }
        }
    };

    @Override
    public void onClick(View view) {

        if (view == back) {
            onBackPressed();
        } else if (view == btnRetry) {
            AdminMsg();
        } else if (view == img_refresh) {
            img_refresh.setRotation(0);
            img_refresh.animate().rotation(360).setDuration(1000).start();
            AdminMsg();
        }
    }

    static class AdapterAdminMsg extends RecyclerView.Adapter<AdapterAdminMsg.ViewHolder> {

        List<Admin_Msg_Model> msg_models;
        Context context;

        public AdapterAdminMsg(List<Admin_Msg_Model> msg_models, Context context) {
            this.msg_models = msg_models;
            this.context = context;
        }

        @NonNull
        @Override
        public AdapterAdminMsg.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(context).inflate(R.layout.adapter_admin_msg, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AdapterAdminMsg.ViewHolder holder, int position) {

            if (msg_models.get(position).getMsg().equals("")) {
                holder.txt_msg.setVisibility(View.GONE);
            } else {
                holder.txt_msg.setVisibility(View.VISIBLE);
                holder.txt_msg.setText(Html.fromHtml(msg_models.get(position).getMsg()));
            }

            if (msg_models.get(position).getImg().equals("")) {
                holder.img_msg.setVisibility(View.GONE);
            } else {
                holder.img_msg.setVisibility(View.VISIBLE);
                Glide.with(context).asBitmap().load(msg_models.get(position).getImg()).thumbnail(0.0f)
                        .placeholder(R.drawable.garrage).into(new BitmapImageViewTarget(holder.img_msg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.img_msg.setImageBitmap(resource);
                    }
                });
            }

            if (!msg_models.get(position).getVideo().equals("")){
                holder.video_play.setVisibility(View.VISIBLE);
                holder.video_play.setSource(msg_models.get(position).getVideo());
            }else {
                holder.video_play.setVisibility(View.GONE);
            }

            holder.txt_time.setText(AppUtils.ConvertLocalTime(msg_models.get(position).getCreated_at()));
        }

        @Override
        public int getItemCount() {
            return msg_models.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {

            TextView txt_time, txt_msg;
            RoundedImageView img_msg;
            AndExoPlayerView video_play;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                txt_msg = itemView.findViewById(R.id.txt_msg);
                img_msg = itemView.findViewById(R.id.img_msg);
                txt_time = itemView.findViewById(R.id.txt_time);
                video_play = itemView.findViewById(R.id.video_play);
            }
        }
    }

}
