package com.example.automap_application.Model;

public class Tow_Track_Offer_Model {

    String tow_track_request_id, tow_track_id, tow_track_name, tow_track_contact, tow_tack_image, price, arrival_time, distance,
           city, address, latitude, longitude, status ;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTow_track_request_id() {
        return tow_track_request_id;
    }

    public void setTow_track_request_id(String tow_track_request_id) {
        this.tow_track_request_id = tow_track_request_id;
    }

    public String getTow_track_id() {
        return tow_track_id;
    }

    public void setTow_track_id(String tow_track_id) {
        this.tow_track_id = tow_track_id;
    }

    public String getTow_track_name() {
        return tow_track_name;
    }

    public void setTow_track_name(String tow_track_name) {
        this.tow_track_name = tow_track_name;
    }

    public String getTow_track_contact() {
        return tow_track_contact;
    }

    public void setTow_track_contact(String tow_track_contact) {
        this.tow_track_contact = tow_track_contact;
    }

    public String getTow_tack_image() {
        return tow_tack_image;
    }

    public void setTow_tack_image(String tow_tack_image) {
        this.tow_tack_image = tow_tack_image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
