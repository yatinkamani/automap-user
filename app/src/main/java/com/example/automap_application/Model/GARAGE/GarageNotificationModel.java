package com.example.automap_application.Model.GARAGE;

import androidx.annotation.NonNull;

public class GarageNotificationModel {

    private String garage_appointment_id;
    private String user_id;
    private String garage_id;
    private String name;
    private String email;
    private String contact_no;
    private String state;
    private String city;
    private String address;
    private String pincode;
    private String user_image;
    private String status;
    private String latitude;
    private String longitude;
    private String user_agree;
    private String price;
    private String time;
    private String appointment_time;
    private String appointment_date;
    private String ratting;
    private String distance;
    private String model_name;
    private String brand;
    private String user_complain;
    private String garage_complain;
    private String fule_name;
    private String garage_reason_id;
    private String user_reason_id;
    private String problem;
    private String garage_agree;
    private String timeout_status;
    private String created_at;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTimeout_status() {
        return timeout_status;
    }

    public void setTimeout_status(String timeout_status) {
        this.timeout_status = timeout_status;
    }

    public String getGarage_id() {
        return garage_id;
    }

    public void setGarage_id(String garage_id) {
        this.garage_id = garage_id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getGarage_reason_id() {
        return garage_reason_id;
    }

    public void setGarage_reason_id(String garage_reason_id) {
        this.garage_reason_id = garage_reason_id;
    }

    public String getUser_reason_id() {
        return user_reason_id;
    }

    public void setUser_reason_id(String user_reason_id) {
        this.user_reason_id = user_reason_id;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUser_complain() {
        return user_complain;
    }

    public void setUser_complain(String user_complain) {
        this.user_complain = user_complain;
    }

    public String getFule_name() {
        return fule_name;
    }

    public void setFule_name(String fule_name) {
        this.fule_name = fule_name;
    }

    public String getGarage_appointment_id() {
        return garage_appointment_id;
    }

    public void setGarage_appointment_id(String garage_appointment_id) {
        this.garage_appointment_id = garage_appointment_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pin_code) {
        this.pincode = pin_code;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUser_agree() {
        return user_agree;
    }

    public void setUser_agree(String user_agree) {
        this.user_agree = user_agree;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGarage_complain() {
        return garage_complain;
    }

    public void setGarage_complain(String garage_complain) {
        this.garage_complain = garage_complain;
    }

    public String getRatting() {
        return ratting;
    }

    public void setRatting(String ratting) {
        this.ratting = ratting;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getGarage_agree() {
        return garage_agree;
    }

    public void setGarage_agree(String garage_agree) {
        this.garage_agree = garage_agree;
    }

    /*public Notification_Model(String garage_appointment_id, String user_id, String name, String email,
                              String status, String city, String state, String address,
                              String contact_no, String pin_code, String user_image) {

        this.garage_appointment_id = garage_appointment_id;
        this.user_id = user_id;
        this.name = name;
        this.email = email;
        this.status = status;
        this.city = city;
        this.state = state;
        this.address = address;
        this.contact_no = contact_no;
        this.pincode = pin_code;
        this.user_image = user_image;
    }*/

    @NonNull
    @Override
    public String toString() {
        return "GarageNotificationModel{" +
                "garage_appointment_id='" + garage_appointment_id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", contact_no='" + contact_no + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", pin code='" + pincode + '\'' +
                ", user_image='" + user_image + '\'' +
                ", status='" + status + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", user_agree='" + user_agree + '\'' +
                ", price='" + price + '\'' +
                ", time='" + time + '\'' +
                ", Garage_id='" + garage_id + '\'' +
                ", Garage_complain='" + garage_complain + '\'' +
                ", ratting='" + ratting + '\'' +
                ", model_name='" + model_name + '\'' +
                ", brand='" + brand + '\'' +
                ", User_complain='" + user_complain + '\'' +
                ", fuel_name='" + fule_name + '\'' +
                '}';
    }
}
