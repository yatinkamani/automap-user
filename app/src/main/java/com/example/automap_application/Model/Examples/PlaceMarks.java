package com.example.automap_application.Model.Examples;

import java.util.List;

public class PlaceMarks {

    List<Double> coordinates;
    String name, image;
    public PlaceMarks(List<Double> coordinates, String name, String image) {
        this.coordinates = coordinates;
        this.name = name;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }
}
