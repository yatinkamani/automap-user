package com.example.automap_application.Model;

public class Booking_Model  {

    private String booking_id;
    private String user_id;
    private String user_name;
    private String booking_date;
    private String car_problem;
    private String is_booked;
    private String is_price;

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getCar_problem() {
        return car_problem;
    }

    public void setCar_problem(String car_problem) {
        this.car_problem = car_problem;
    }

    public String getIs_booked() {
        return is_booked;
    }

    public void setIs_booked(String is_booked) {
        this.is_booked = is_booked;
    }

    public String getIs_price() {
        return is_price;
    }

    public void setIs_price(String is_price) {
        this.is_price = is_price;
    }

    public Booking_Model(String booking_id, String user_id, String user_name, String booking_date, String car_problem, String is_booked, String is_price) {
        this.booking_id = booking_id;
        this.user_id = user_id;
        this.user_name = user_name;
        this.booking_date = booking_date;
        this.car_problem = car_problem;
        this.is_booked = is_booked;
        this.is_price = is_price;
    }
}
