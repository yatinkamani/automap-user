package com.example.automap_application.Model;

public class Check_user_Model {

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContect_no() {
        return contect_no;
    }

    public void setContect_no(String contect_no) {
        this.contect_no = contect_no;
    }

    public String getContry_id() {
        return contry_id;
    }

    public void setContry_id(String contry_id) {
        this.contry_id = contry_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(String registration_date) {
        this.registration_date = registration_date;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    private String request;
    private String message;
    private String userid;
    private String name;
    private String email;
    private String contect_no;
    private String contry_id;
    private String state;
    private String city;
    private String address;
    private String latitude;
    private String longitude;
    private String pincode;
    private String user_image;
    private String status;
    private String registration_date;
    private String updated_at;

    public Check_user_Model(String request, String message, String userid, String name, String email, String contect_no, String contry_id, String state, String city, String address, String latitude, String longitude, String pincode, String user_image, String status, String registration_date, String updated_at) {
        this.request = request;
        this.message = message;
        this.userid = userid;
        this.name = name;
        this.email = email;
        this.contect_no = contect_no;
        this.contry_id = contry_id;
        this.state = state;
        this.city = city;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.pincode = pincode;
        this.user_image = user_image;
        this.status = status;
        this.registration_date = registration_date;
        this.updated_at = updated_at;
    }

    public Check_user_Model() {
    }
}
