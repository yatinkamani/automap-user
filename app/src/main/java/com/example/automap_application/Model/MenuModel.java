package com.example.automap_application.Model;

/**
 * Created by anupamchugh on 22/12/17.
 */

public class MenuModel {

    public String menuName, url;
    public int icon,icon2;
    public boolean hasChildren, isGroup;

    public MenuModel(int icon,String menuName, boolean isGroup, boolean hasChildren, int icon2) {

        this.menuName = menuName;
        this.icon=icon;
        this.icon2=icon2;
        this.isGroup = isGroup;
        this.hasChildren = hasChildren;
    }
}
