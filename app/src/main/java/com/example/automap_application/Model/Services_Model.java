package com.example.automap_application.Model;

public class Services_Model {

    String service_id,service_provider,service_type,service_name,service_image,created_at,updated_at;

    public Services_Model(String service_id, String service_provider, String service_type, String service_name, String service_image, String created_at, String updated_at) {
        this.service_id = service_id;
        this.service_provider = service_provider;
        this.service_type = service_type;
        this.service_name = service_name;
        this.service_image = service_image;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_provider() {
        return service_provider;
    }

    public void setService_provider(String service_provider) {
        this.service_provider = service_provider;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_image() {
        return service_image;
    }

    public void setService_image(String service_image) {
        this.service_image = service_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
