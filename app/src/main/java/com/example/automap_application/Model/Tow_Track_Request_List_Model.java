package com.example.automap_application.Model;

public class Tow_Track_Request_List_Model {

    private String towtrack_request_id, user_location, final_destination, create_at, update_at, timeout_status;

    public Tow_Track_Request_List_Model(String towtrack_request_id, String user_location, String final_destination, String create_at, String update_at, String timeout_status) {
        this.towtrack_request_id = towtrack_request_id;
        this.user_location = user_location;
        this.final_destination = final_destination;
        this.create_at = create_at;
        this.update_at = update_at;
        this.timeout_status = timeout_status;
    }

    public String getTowtrack_request_id() {
        return towtrack_request_id;
    }

    public void setTowtrack_request_id(String towtrack_request_id) {
        this.towtrack_request_id = towtrack_request_id;
    }

    public String getUser_location() {
        return user_location;
    }

    public void setUser_location(String user_location) {
        this.user_location = user_location;
    }

    public String getFinal_destination() {
        return final_destination;
    }

    public void setFinal_destination(String final_destination) {
        this.final_destination = final_destination;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getTimeout_status() {
        return timeout_status;
    }

    public void setTimeout_status(String timeout_status) {
        this.timeout_status = timeout_status;
    }
}
