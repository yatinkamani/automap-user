package com.example.automap_application.Model;

public class Notification_Model {

    private String garage_appointment_id;
    private String user_id;
    private String name;
    private String email;
    private String contact_no;
    private String state;
    private String city;
    private String address;
    private String pincode;
    private String user_image;
    private String status;
    private String latitude;
    private String longitude;
    private String user_agree;
    private String price;
    private String time;
    private String work_done;

    public String getGarage_appointment_id() {
        return garage_appointment_id;
    }

    public void setGarage_appointment_id(String garage_appointment_id) {
        this.garage_appointment_id = garage_appointment_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pin_code) {
        this.pincode = pin_code;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUser_agree() {
        return user_agree;
    }

    public void setUser_agree(String user_agree) {
        this.user_agree = user_agree;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWork_done() {
        return work_done;
    }

    public void setWork_done(String work_done) {
        this.work_done = work_done;
    }


    /*public Notification_Model(String garage_appointment_id, String user_id, String name, String email,
                              String status, String city, String state, String address,
                              String contact_no, String pin_code, String user_image) {

        this.garage_appointment_id = garage_appointment_id;
        this.user_id = user_id;
        this.name = name;
        this.email = email;
        this.status = status;
        this.city = city;
        this.state = state;
        this.address = address;
        this.contact_no = contact_no;
        this.pincode = pin_code;
        this.user_image = user_image;
    }*/
}
