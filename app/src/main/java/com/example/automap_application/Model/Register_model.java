package com.example.automap_application.Model;

public class Register_model {

    public Register_model(String message) {
        this.message = message;
    }

    public Register_model(String message, String name, String email, String country_id, String contact_no, String user_id) {
        this.message = message;
        this.name = name;
        this.email = email;
        this.country_id = country_id;
        this.contact_no = contact_no;
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    private String message;
    private String name;
    private String email;
    private String country_id;
    private String contact_no;
    private String user_id;

    public Register_model() {
    }
}
