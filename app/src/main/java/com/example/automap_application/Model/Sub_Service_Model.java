package com.example.automap_application.Model;

public class Sub_Service_Model {

    private String service_id, service_name, sub_service_id, sub_service_name, service_img,  provider;

    public Sub_Service_Model(String service_id,String service_name, String sub_service_id, String sub_service_name, String service_img, String provider) {
        this.service_id = service_id;
        this.service_name = service_name;
        this.sub_service_id = sub_service_id;
        this.sub_service_name = sub_service_name;
        this.service_img = service_img;
        this.provider = provider;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_img() {
        return service_img;
    }

    public void setService_img(String service_img) {
        this.service_img = service_img;
    }

    public String getSub_service_id() {
        return sub_service_id;
    }

    public void setSub_service_id(String sub_service_id) {
        this.sub_service_id = sub_service_id;
    }

    public String getSub_service_name() {
        return sub_service_name;
    }

    public void setSub_service_name(String sub_service_name) {
        this.sub_service_name = sub_service_name;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
