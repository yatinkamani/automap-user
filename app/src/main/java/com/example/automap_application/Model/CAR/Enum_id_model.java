package com.example.automap_application.Model.CAR;

public class Enum_id_model {


    private String enum_id;
    private String type;

    public Enum_id_model(String enum_id, String type) {
        this.enum_id=enum_id;
        this.type=type;
    }

    public String getEnum_id() {
        return enum_id;
    }

    public void setEnum_id(String enum_id) {
        this.enum_id = enum_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
