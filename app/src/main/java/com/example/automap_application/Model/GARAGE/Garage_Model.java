package com.example.automap_application.Model.GARAGE;

public class Garage_Model {

    private String garage_name;
    private String garage_id;
    private String rate;

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    private String website;

    public String getGarage_id() {
        return garage_id;
    }

    public void setGarage_id(String garage_id) {
        this.garage_id = garage_id;
    }

    public Garage_Model(String garage_name, String garage_banner, String garage_id, String website, String rate) {
        this.garage_name = garage_name;
        this.garage_banner = garage_banner;
        this.garage_id = garage_id;
        this.website = website;
        this.rate = rate;
    }


    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getGarage_name() {
        return garage_name;
    }

    public void setGarage_name(String garage_name) {
        this.garage_name = garage_name;
    }

    public String getGarage_banner() {
        return garage_banner;
    }

    public void setGarage_banner(String garage_banner) {
        this.garage_banner = garage_banner;
    }

    private String garage_banner;
}
