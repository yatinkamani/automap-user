package com.example.automap_application.Model;

public class RejectReason {

    private String id;
    private String status;
    private String reason;
    private String is_checked;

    public RejectReason(String id, String status, String reason, String is_checked) {
        this.id = id;
        this.status = status;
        this.reason = reason;
        this.is_checked = is_checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getIs_checked() {
        return is_checked;
    }

    public void setIs_checked(String is_checked) {
        this.is_checked = is_checked;
    }
}
