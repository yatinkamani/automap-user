package com.example.automap_application.Model;

public class Login_Model {
    public String getStatus_up() {
        return status_up;
    }

    public void setStatus_up(String status_up) {
        this.status_up = status_up;
    }

    private String status_up;
    public Login_Model(String messege, String result, String user_id, String name, String email, String contect_no, String country_id, String state, String city, String address, String lati, String longi, String pin, String user_image, String status) {
        this.messege = messege;
        this.result = result;
        this.user_id = user_id;
        this.name = name;
        this.email = email;
        this.contect_no = contect_no;
        this.country_id = country_id;
        this.state = state;
        this.city = city;
        this.address = address;
        this.lati = lati;
        this.longi = longi;
        this.pin = pin;
        this.user_image = user_image;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessege() {
        return messege;
    }

    public void setMessege(String messege) {
        this.messege = messege;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    private String messege;
    private String result;
    private String user_id;
    private String name;
    private String email;
    private String contect_no;
    private String country_id;
    private String state;
    private String city;
    private String address;
    private String lati;
    private String longi;
    private String pin;
    private String user_image;
    private String status;

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    private String reg_date;
    private String update_date;

    public Login_Model() {
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContect_no() {
        return contect_no;
    }

    public void setContect_no(String contect_no) {
        this.contect_no = contect_no;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLati() {
        return lati;
    }

    public void setLati(String lati) {
        this.lati = lati;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }
}
