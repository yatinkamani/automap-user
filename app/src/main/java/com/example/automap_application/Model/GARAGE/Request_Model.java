package com.example.automap_application.Model.GARAGE;

public class Request_Model {

    private String garage_appointment_id, user_id, car_problem, appointment_date, appointment_time, tire_brand, tire_size_number, oil_sae, oil_brand,
            rims_brand, rims_size, battery_brand, appointment_type, created_at;

    public String getAppointment_type() {
        return appointment_type;
    }

    public void setAppointment_type(String appointment_type) {
        this.appointment_type = appointment_type;
    }

    public String getGarage_appointment_id() {
        return garage_appointment_id;
    }

    public void setGarage_appointment_id(String garage_appointment_id) {
        this.garage_appointment_id = garage_appointment_id;
    }

    public String getCar_problem() {
        return car_problem;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setCar_problem(String car_problem) {
        this.car_problem = car_problem;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getTire_brand() {
        return tire_brand;
    }

    public void setTire_brand(String tire_brand) {
        this.tire_brand = tire_brand;
    }

    public String getTire_size_number() {
        return tire_size_number;
    }

    public void setTire_size_number(String tire_size_number) {
        this.tire_size_number = tire_size_number;
    }

    public String getOil_sae() {
        return oil_sae;
    }

    public void setOil_sae(String oil_sae) {
        this.oil_sae = oil_sae;
    }

    public String getOil_brand() {
        return oil_brand;
    }

    public void setOil_brand(String oil_brand) {
        this.oil_brand = oil_brand;
    }

    public String getRims_brand() {
        return rims_brand;
    }

    public void setRims_brand(String rims_brand) {
        this.rims_brand = rims_brand;
    }

    public String getRims_size() {
        return rims_size;
    }

    public void setRims_size(String rims_size) {
        this.rims_size = rims_size;
    }

    public String getBattery_brand() {
        return battery_brand;
    }

    public void setBattery_brand(String battery_brand) {
        this.battery_brand = battery_brand;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
