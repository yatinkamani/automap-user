package com.example.automap_application.Model;

public class Checkbox_Model {
    private String garage_facility_name;

    public Checkbox_Model(String garage_facility_id, String garage_facility_name) {
        this.garage_facility_id=garage_facility_id;
        this.garage_facility_name=garage_facility_name;
    }

    public String getGarage_facility_name() {
        return garage_facility_name;
    }

    public void setGarage_facility_name(String garage_facility_name) {
        this.garage_facility_name = garage_facility_name;
    }

    public String getGarage_facility_id() {
        return garage_facility_id;
    }

    public void setGarage_facility_id(String garage_facility_id) {
        this.garage_facility_id = garage_facility_id;
    }

    private String garage_facility_id;

}
