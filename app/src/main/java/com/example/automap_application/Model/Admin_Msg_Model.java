package com.example.automap_application.Model;

public class Admin_Msg_Model {

    private String id, msg, img, video, created_at;

    public Admin_Msg_Model(String id, String msg, String img, String video, String created_at) {
        this.id = id;
        this.msg = msg;
        this.img = img;
        this.video = video;
        this.created_at = created_at;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
