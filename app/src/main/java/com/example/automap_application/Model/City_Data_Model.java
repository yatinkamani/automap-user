package com.example.automap_application.Model;

public class City_Data_Model {

    private String city_name, city_id, country_id ;

    public City_Data_Model() {
    }

    public City_Data_Model(String city_name, String city_id, String country_id) {
        this.city_name = city_name;
        this.city_id = city_id;
        this.country_id = country_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }
}
