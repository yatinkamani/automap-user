package com.example.automap_application.Model.CAR;

public class Manufacture_Country_Model {

    private String mc_Id, mc_name, isChecked;

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public String getMc_Id() {
        return mc_Id;
    }

    public void setMc_Id(String mc_Id) {
        this.mc_Id = mc_Id;
    }

    public String getMc_name() {
        return mc_name;
    }

    public void setMc_name(String mc_name) {
        this.mc_name = mc_name;
    }

    public Manufacture_Country_Model() {}

    public Manufacture_Country_Model(String mc_Id, String mc_name) {
        this.mc_Id = mc_Id;
        this.mc_name = mc_name;
    }

    @Override
    public String toString() {
        return mc_name;
    }
}
