package com.example.automap_application.Model;

public class Active_user_model {

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    private String status;
    private String Message;

    public Active_user_model(String status, String message) {
        this.status = status;
        Message = message;
    }

    public Active_user_model() {
    }
}
