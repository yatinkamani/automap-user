package com.example.automap_application.Model.CAR;

public class car_fule_model {

    public String getFule_id() {
        return fule_id;
    }

    public void setFule_id(String fule_id) {
        this.fule_id = fule_id;
    }

    public String getFule_type() {
        return fule_type;
    }

    public void setFule_type(String fule_type) {
        this.fule_type = fule_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    private String fule_id;
    private String fule_type;
    private String created_at;
    private String updated_at;

    public car_fule_model() {
    }
}
