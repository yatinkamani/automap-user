package com.example.automap_application.Model;

public class Edit_User_Model {

    public Edit_User_Model(String status, String message, String name, String email, String country_id, String contact_no, String state, String city, String address, String pincode, String latitude, String longitude, String status_d) {
        this.status = status;
        this.message = message;
        this.name = name;
        this.email = email;
        this.country_id = country_id;
        this.contact_no = contact_no;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pincode = pincode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status_d = status_d;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatus_d() {
        return status_d;
    }

    public void setStatus_d(String status_d) {
        this.status_d = status_d;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    private String status;
    private String message;
    private String name;
    private String email;
    private String country_id;
    private String contact_no;
    private String state;
    private String city;
    private String address;
    private String pincode;
    private String latitude;
    private String longitude;
    private String status_d;
    private String user_image;

    public Edit_User_Model() {
    }
}
