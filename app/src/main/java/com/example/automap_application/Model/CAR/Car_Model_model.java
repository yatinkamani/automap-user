package com.example.automap_application.Model.CAR;

public class Car_Model_model {

    private String model_id;
    private String brand_id;
    private String model_nm;
    private String manufacture_country_id;
    private String model_year;
    private String created_at;
    private String updated_at;

    public String getManufacture_country_id() {
        return manufacture_country_id;
    }

    public void setManufacture_country_id(String manufacture_country_id) {
        this.manufacture_country_id = manufacture_country_id;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getModel_nm() {
        return model_nm;
    }

    public void setModel_nm(String model_nm) {
        this.model_nm = model_nm;
    }

    public String getModel_year() {
        return model_year;
    }

    public void setModel_year(String model_year) {
        this.model_year = model_year;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Car_Model_model() {
    }
}
