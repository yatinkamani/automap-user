package com.example.automap_application.Model.Workshop;

/**
 * Created  Email by abc on 10/14/2019.
 */

public class Workshop {
    private final String workshop_banner;
    private final String website;
    private String workshop_id;
    private String workshop_name;
    private String rate;

    public Workshop(String workshop_name, String workshop_banner, String workshop_id, String website, String rate) {
        this.workshop_name = workshop_name;
        this.workshop_banner = workshop_banner;
        this.workshop_id = workshop_id;
        this.website = website;
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getWorkshop_id() {
        return workshop_id;
    }

    public void setWorkshop_id(String workshop_id) {
        this.workshop_id = workshop_id;
    }

    public String getWorkshop_name() {
        return workshop_name;
    }

    public void setWorkshop_name(String workshop_name) {
        this.workshop_name = workshop_name;
    }

    public String getWebsite() {
        return website;
    }

    public String getWorkshop_banner() {
        return workshop_banner;
    }
}
