package com.example.automap_application;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Charge_Point_Model;
import com.example.automap_application.backgroundServices.LocationService;
import com.example.automap_application.utils.CustomMapFragment;
import com.example.automap_application.utils.DirectionsJSONParser;
import com.example.automap_application.utils.LatLngInterpolator;
import com.example.automap_application.utils.MapWrapperLayout;
import com.example.automap_application.utils.MarkerAnimation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.jackandphantom.circularimageview.RoundedImage;
import com.makeramen.roundedimageview.RoundedImageView;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.ReferenceQueue;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//not use
public class Act_Charge_Point extends AppCompatActivity implements View.OnClickListener,
        MapWrapperLayout.OnDragListener, OnMapReadyCallback,
        LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener {

    ImageView img_back, img_add;
    GoogleApiClient apiClient;
    CustomMapFragment mapFragment;
    Location location;
    GoogleMap map;
    private LocationSettingsRequest.Builder builder;
    boolean isEnable = true, start = false;
    Handler handler = new Handler();
    Runnable runnable;
    String user_id = "";
    SharedPreferences preferences;
    Charge_Point_Model point_model = null;

    BottomSheetDialog ReviewDialog;
    List<Charge_Point_Model> charge_point_models;
    LinearLayout lin_bottom_direction;
    Button btn_stop;
    TextView txt_away_time;
    boolean isDirection = false, isFirst = false;

    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;

    @Override
    protected void onStart() {
        apiClient.connect();
        super.onStart();
    }

    @Override
    protected void onResume() {
        int permissionLocation = ContextCompat.checkSelfPermission(Act_Charge_Point.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            location = LocationServices.FusedLocationApi.getLastLocation(apiClient);
            getMyLocation();
        }
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__charge__point);

        img_back = findViewById(R.id.img_back);
        img_add = findViewById(R.id.img_add);

        lin_bottom_direction = findViewById(R.id.lin_bottom_direction);
        txt_away_time = findViewById(R.id.txt_away_time);
        btn_stop = findViewById(R.id.btn_stop);

        img_back.setOnClickListener(this);
        img_add.setOnClickListener(this);
        btn_stop.setOnClickListener(this);

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id), "");

        checkPermissions();
        final int[] c = {0};
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isEnable) {
                    checkPermissions();
                }

                Log.e("Tag", "" + (c[0]++));
                Log.e("Tag", "" + isEnable);
                runnable = this;
                handler.postDelayed(runnable, 10000);
            }
        }, 1000);

        initializeMap();
        buildGoogleApiClient();
//        getChargePoint();

        charge_point_models = new ArrayList<>();

        charge_point_models.add(new Charge_Point_Model("1", "First", "", "First audience", "22.240201", "70.351815", "0", "0"));
        charge_point_models.add(new Charge_Point_Model("2", "First 1", "", "First sdks sdks", "22.2808036", "70.3578299", "0", "2"));
        charge_point_models.add(new Charge_Point_Model("3", "First 2", "", "First were ref", "22.240100", "70.351810", "0", "1"));
        charge_point_models.add(new Charge_Point_Model("4", "First 4", "", "First sdf faff", "22.2808037", "70.3578300", "0", "0"));
        charge_point_models.add(new Charge_Point_Model("5", "First 5", "", "First sdks oks", "22.2756377", "70.3593178", "0", "1"));
        charge_point_models.add(new Charge_Point_Model("6", "First 6", "", "First sacked salk's", "22.2756301", "70.3593102", "0", "0"));
        charge_point_models.add(new Charge_Point_Model("7", "First 7", "", "First sd kfm ld fe d ew ps", "22.2756378", "70.3593179", "0", "1"));
        charge_point_models.add(new Charge_Point_Model("8", "First 8", "", "First au dio sn ced.;dfp[d/ ", "22.240195", "70.351817", "0", "1"));
        charge_point_models.add(new Charge_Point_Model("9", "First 9", "", "First aud io sn c said vfe", "22.2756300", "70.3593100", "0", "2"));
        charge_point_models.add(new Charge_Point_Model("10", "First 10", "", "First ramie nader polish", "22.275638", "70.359318", "0", "0"));

    }

    private void getChargePoint() {

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_charge_point", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Tag Response ", response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("true")) {
                        charge_point_models = new ArrayList<>();
                        JSONArray array = object.getJSONArray("result");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            String id = jsonObject.getString("charge_point_id");
                            String name = jsonObject.getString("charge_point_name");
                            String latitude = jsonObject.getString("latitude");
                            String address = jsonObject.getString("address");
                            String longitude = jsonObject.getString("longitude");
                            String image = jsonObject.getString("charge_point_image");
                            String isReview = jsonObject.getString("isReview");
                            String status = jsonObject.getString("status");
                            charge_point_models.add(new Charge_Point_Model(id, name, address, latitude, longitude, image, isReview, status));
                        }

                        addMarker(charge_point_models);
                    } else {
                        Toast.makeText(getApplicationContext(), "not found any charge point", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Tag error", e.toString());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Tag Error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", user_id);
                Log.e("Tag Params", map.toString());
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);

    }

    @Override
    public void onClick(View view) {

        if (view == img_back) {
            onBackPressed();
        } else if (view == img_add) {
            Intent intent = new Intent(getApplicationContext(), Act_Add_Charge_Point.class);
            startActivity(intent);
        } else if (view == btn_stop) {
            if (map != null && location != null) {
                lin_bottom_direction.setVisibility(View.GONE);
                polyline.remove();
                lineOptions = null;
                polyline = null;
                CameraUpdate factory = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15f);
                map.animateCamera(factory);
                isDirection = false;
                point_model = null;
            }
        }
    }

    private void initializeMap() {
        if (map == null) {
            mapFragment = ((CustomMapFragment) getFragmentManager().findFragmentById(R.id.map));
            mapFragment.setOnDragListener(this);
            mapFragment.getMapAsync(this);
            mapFragment.onResume();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        map.setBuildingsEnabled(true);
        map.setTrafficEnabled(true);

        map.setOnMarkerClickListener(this);

        if (charge_point_models != null && charge_point_models.size() > 0) {
            addMarker(charge_point_models);
        }
    }

    private void buildGoogleApiClient() {
        apiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onDrag(MotionEvent motionEvent) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(Act_Charge_Point.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            // getCurrentLocation();
            // getMyLocation();.
        }
        isEnable = true;
    }

    private void getMyLocation() {

        if (apiClient != null) {
            if (apiClient.isConnected()) {

                int permissionLocation = ContextCompat.checkSelfPermission(Act_Charge_Point.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    location = LocationServices.FusedLocationApi.getLastLocation(apiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(2000);
                    locationRequest.setFastestInterval(2000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

                    builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(apiClient, builder.build());

                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(@NonNull LocationSettingsResult result) {
                            if (result == null) {
                                getMyLocation();
                                return;
                            }
                            final Status status = result.getStatus();
                            Log.e("Tag Status", "" + status.getStatusCode());
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat.checkSelfPermission(Act_Charge_Point.this, Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        location = LocationServices.FusedLocationApi.getLastLocation(apiClient);
                                        isEnable = true;
                                        Log.e("TAG", "onResult: " + location);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        isEnable = false;
                                        status.startResolutionForResult(Act_Charge_Point.this, REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    isEnable = true;
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    private void checkPermissions() {

        int permissionLocation = ContextCompat.checkSelfPermission(Act_Charge_Point.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                Log.e("TAG", "checkPermissions: ");
                isEnable = false;
            }
        } else {
            getMyLocation();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
//         Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        isEnable = true;
                        break;
                    case Activity.RESULT_CANCELED:
                        isEnable = false;
                        break;
                }
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            this.location = location;

            Log.e("Tag Location", "" + location);
            if (!isFirst) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15F));
                isFirst = true;
            }

            Location loc2 = new Location("gps");
            loc2.set(location);
            /*if (charge_point_models != null && charge_point_models.size() > 0) {
                for (int i = 0; i < charge_point_models.size(); i++) {
                    if (charge_point_models.get(i).getIsReview().equals("0")) {
                        Location loc1 = new Location("gps");
                        loc1.setLatitude(Double.parseDouble(charge_point_models.get(i).getLatitude()));
                        loc1.setLongitude(Double.parseDouble(charge_point_models.get(i).getLongitude()));
                        float distance = loc1.distanceTo(loc2);
                        Log.e("Tag Distance", "" + distance);
                        if (distance < 30) {
                            if (ReviewDialog != null && ReviewDialog.isShowing()) {
                            } else {
                                ReviewDialog(charge_point_models.get(i).getId(), charge_point_models.get(i).getName());
                            }
                            break;
                        }
                    }
                }
            }*/

            if (isDirection && point_model != null) {
                drawPath(new LatLng(location.getLatitude(), location.getLongitude()), new LatLng(Double.parseDouble(point_model.getLatitude()), Double.parseDouble(point_model.getLongitude())), point_model.getName());
                Location loc1 = new Location("gps");
                loc1.setLatitude(Double.parseDouble(point_model.getLatitude()));
                loc1.setLongitude(Double.parseDouble(point_model.getLongitude()));
                float distance = loc1.distanceTo(loc2);
                Log.e("Tag Distance", "" + distance);
                if (distance < 30 && point_model.getIsReview().equals("0")) {
                    if (ReviewDialog != null && ReviewDialog.isShowing()) {
                    } else {
                        ReviewDialog(point_model.getId(), point_model.getName());
                    }
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("Tag Connection Failed", "" + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Tag Connection Failed", connectionResult.getErrorMessage());
    }

    @Override
    public void onBackPressed() {
        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable);
        }
        LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, this);
        super.onBackPressed();
    }

    private void ReviewDialog(String id, String name) {

        ArrayList<String> list = new ArrayList<>();
        list.add("Charge Station Working Good");
        list.add("Under Maintenance");
        list.add("Not Exit");

        ReviewDialog = new BottomSheetDialog(Act_Charge_Point.this, R.style.SheetDialog);
        ReviewDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (ReviewDialog.getWindow() != null) {
            ReviewDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_charge_point_review, null, false);

        TextView textView = view.findViewById(R.id.txt_review_title);
        RadioGroup lin_dynamic_radiobutton = view.findViewById(R.id.lin_dynamic_radiobutton);
        Button btn_submit = view.findViewById(R.id.btn_submit);
        AVLoadingIndicatorView av_loading = view.findViewById(R.id.av_loading);

        textView.setText(name);
        lin_dynamic_radiobutton.removeAllViews();

        for (int i = 0; i < list.size(); i++) {

            RadioButton button = new RadioButton(this);
            button.setText(list.get(i));
            lin_dynamic_radiobutton.addView(button);

        }
        final String[] status = {""};
        if (lin_dynamic_radiobutton != null) {

            lin_dynamic_radiobutton.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {

                    for (int j = 0; j < radioGroup.getChildCount(); j++) {
                        RadioButton button = (RadioButton) radioGroup.getChildAt(j);
                        if (button.getId() == i) {
                            status[0] = "" + i;
//                            Toast.makeText(getApplicationContext(), "Checked" + i, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                btn_submit.setVisibility(View.GONE);
//                av_loading.setVisibility(View.VISIBLE);
                btn_submit.setAlpha(1f);
                btn_submit.animate().alpha(0f).setDuration(400).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        btn_submit.setVisibility(View.GONE);
                        av_loading.setVisibility(View.VISIBLE);
                        av_loading.setAlpha(0f);
                        av_loading.animate().alpha(1f).setDuration(400).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {

                                StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "charge_point_review", new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e("Tag Response", response);
                                        try {
                                            JSONObject object = new JSONObject(response);
                                            if (object.getString("status").equals("true")) {
                                                Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        ReviewDialog.dismiss();
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("Tag Error", error.toString());
                                        btn_submit.setVisibility(View.VISIBLE);
                                        av_loading.setVisibility(View.GONE);
//                        ReviewDialog.dismiss();
                                    }
                                }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> map = new HashMap<>();
                                        map.put("charge_point_id", id);
                                        map.put("status", status[0]);
                                        Log.e("Tag Parameter", map.toString());
                                        return map;
                                    }
                                };
                                RequestQueue queue = Volley.newRequestQueue(Act_Charge_Point.this);
                                queue.add(request);
                            }
                        });
                    }
                });
                for (int i = 0; i < charge_point_models.size(); i++) {
                    if (charge_point_models.get(i).getId().equals(id)) {
                        charge_point_models.get(i).setIsReview("1");
                        point_model.setIsReview("1");
                    }
                }

            }
        });

        ReviewDialog.setContentView(view);
        ReviewDialog.setCanceledOnTouchOutside(false);
        ReviewDialog.setDismissWithAnimation(true);
        ReviewDialog.show();
    }

    private void DetailDialog(Charge_Point_Model chargePointModel) {

        BottomSheetDialog sheetDialog = new BottomSheetDialog(Act_Charge_Point.this, R.style.SheetDialog);

        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_charge_point_detail, null, false);

        AppCompatImageButton img_direction = view.findViewById(R.id.btn_direction);
        AppCompatImageButton img_close = view.findViewById(R.id.btn_close);
        TextView txt_name = view.findViewById(R.id.txt_charge_point_name);
        TextView txt_address = view.findViewById(R.id.txt_address);
        TextView txt_status = view.findViewById(R.id.txt_status);
        RoundedImageView img_charge_point = view.findViewById(R.id.img_charge_point);

        Glide.with(getApplicationContext()).asBitmap().thumbnail(0.02f).load(chargePointModel.getImage()).placeholder(R.drawable.garrage).into(new BitmapImageViewTarget(img_charge_point) {
            @Override
            protected void setResource(Bitmap resource) {
                img_charge_point.setImageBitmap(resource);
            }
        });
        txt_name.setText(Html.fromHtml(chargePointModel.getName()));
        txt_address.setText(Html.fromHtml(chargePointModel.getAddress()));
        if (chargePointModel.getStatus().equals("0")) {
            txt_status.setText("Working Good");
        } else if (chargePointModel.getStatus().equals("1")) {
            txt_status.setText("Under Maintenance");
        } else {
            txt_status.setText("Not Exit");
            img_direction.setVisibility(View.GONE);
        }

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetDialog.dismiss();
            }
        });

        img_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (location != null) {
                    sheetDialog.dismiss();
                    isDirection = true;
                    point_model = chargePointModel;
//                    charge_point_name = chargePointModel.getName();
//                    charge_point_id = chargePointModel.getId();
//                    isReview = chargePointModel.getIsReview();
//                    endCPLatLng = new LatLng(Double.parseDouble(chargePointModel.getLatitude()), Double.parseDouble(chargePointModel.getLongitude()));
                    drawPath(new LatLng(location.getLatitude(), location.getLongitude()),
                            new LatLng(Double.parseDouble(chargePointModel.getLatitude()), Double.parseDouble(chargePointModel.getLongitude())),
                            txt_name.getText().toString());
                }
            }
        });

        sheetDialog.setContentView(view);
        sheetDialog.setCanceledOnTouchOutside(false);
        sheetDialog.show();
    }

    @Override
    protected void onStop() {
        LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, this);
        super.onStop();
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + Constant.API_KEY;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Log.e("Tag Marker", marker.getId());
        if (lin_bottom_direction.getVisibility() == View.GONE) {

            if (markers != null && markers.size() > 0) {
                for (int i = 0; i < markers.size(); i++) {
                    Log.e("Tag Marker", markers.get(i).getId() + " " + marker.getId());
                    if (markers.get(i).getId().equals(marker.getId())) {
                        DetailDialog(charge_point_models.get(i));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";
            try {
                data = downloadUrl(url[0]);
                Log.e("Tag Direction API", data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getString("status").equals("ZERO_RESULTS")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "No Rout Found", Toast.LENGTH_LONG).show();
                                lin_bottom_direction.setVisibility(View.GONE);
                            }
                        });

                    } else if (jsonObject.getString("status").equals("OVER_QUERY_LIMIT")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "your Daily Limit is Over", Toast.LENGTH_LONG).show();
                            }
                        });

                    } else {

                        JSONArray array = jsonObject.getJSONArray("routes");
                        JSONObject route = array.getJSONObject(0);
                        JSONArray legs = route.getJSONArray("legs");
                        JSONObject steps = legs.getJSONObject(0);
                        JSONObject distance = steps.getJSONObject("distance");
                        String distances = distance.getString("text");
                        JSONObject duration = steps.getJSONObject("duration");
                        String durations = duration.getString("text");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                txt_away_time.setText(Html.fromHtml(durations + " Away Charge Point"));
                                Log.e("Tag Second 1", distances);
                            }
                        });

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    PolylineOptions lineOptions = null;
    Polyline polyline = null;

    @SuppressLint("StaticFieldLeak")
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            JSONObject jsonObject = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                if (lineOptions == null) {
                    lineOptions = new PolylineOptions();
                    lin_bottom_direction.setVisibility(View.VISIBLE);
                    lineOptions.addAll(points);
                    lineOptions.width(12);
                    lineOptions.color(Color.RED);
                    lineOptions.geodesic(true);
                } else {
                    if (polyline != null)
                        polyline.setPoints(points);
                }
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                if (polyline == null) {
                    polyline = map.addPolyline(lineOptions);
                }
            }

        }
    }

    Marker startMarker = null;

    private void drawPath(LatLng startLatLng, LatLng endLatLng, String destName) {

        String url = getDirectionsUrl(startLatLng, endLatLng);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);

        if (startMarker == null) {
            startMarker = map.addMarker(new MarkerOptions().position(startLatLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                    .title("your Position"));
        } else {
            MarkerAnimation.animateMarkerToGB(startMarker, startLatLng, new LatLngInterpolator.Spherical());
        }

        /*if (endMarker == null) {
            endMarker = map.addMarker(new MarkerOptions().position(endLatLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                    .title(destName));
        } else {
            MarkerAnimation.animateMarkerToGB(endMarker, endLatLng, new LatLngInterpolator.Spherical());
        }*/

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(startLatLng);
        builder.include(endLatLng);
        LatLngBounds lngBounds = builder.build();
        int width = mapFragment.getResources().getDisplayMetrics().widthPixels;
        int height = mapFragment.getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(lngBounds, width, height, padding);
        int PinToShow = 2000;
        map.animateCamera(cameraUpdate);
    }

    List<Marker> markers = new ArrayList<>();

    public void addMarker(@NotNull List<Charge_Point_Model> models) {

        for (int i = 0; i < models.size(); i++) {
            String latitude = models.get(i).getLatitude();
            String longitude = models.get(i).getLongitude();
            LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

            markers.add(map.addMarker(new MarkerOptions().position(latLng).icon(bitmapDescriptorFromVector(this, R.drawable.ic_charge_pin)).title(models.get(i).getName())));
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, 40, 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

}