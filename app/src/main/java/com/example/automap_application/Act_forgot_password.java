package com.example.automap_application;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Contry_spinner_model;
import com.example.automap_application.Model.Forgot_model;
import com.example.automap_application.RetrofitApi.ApiConfig;
import com.example.automap_application.RetrofitApi.AppConfig;
import com.example.automap_application.extra.API;
import com.example.automap_application.extra.APIResponse;
import com.example.automap_application.utils.AppUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.mukesh.OtpView;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Act_forgot_password extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBack;
    TextView textTitle;
    SharedPreferences sharedpreferences;
    EditText edContactNo, edPwd, edCPwd;
    AutoCompleteTextView edCountryCode;
    CardView card_forgot;
    String[] codes = null;

    LinearLayout lin_send_view;

    View verify_view;
    TextView txt_message, txt_timer;
    OtpView otpView;
    AVLoadingIndicatorView av_loading;
    LinearLayout linBtn;
    Button btn_verify;

    FirebaseAuth auth;
    PhoneAuthProvider.ForceResendingToken token;
    String verificationId;
    private boolean isVerified = false;

    PhoneAuthCredential authCredential = null;
    boolean isInstant = false;
    private String countryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_forgot_password);

        lin_send_view = findViewById(R.id.lin_send_view);
        verify_view = findViewById(R.id.verify_view);
        textTitle = findViewById(R.id.textTitle);
        imgBack = findViewById(R.id.imgBack);

        auth = FirebaseAuth.getInstance();
        imgBack.setOnClickListener(this);

        sharedpreferences = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);

        countryCode = getDeviceCountryCode(this);

        bindVerifyView();
        bindSendView();
        getCountryCode();
    }


    // change oth view and password view
    public void changeView() {

        if (countDownTimer != null){
            countDownTimer.cancel();
        }
        if (lin_send_view.getVisibility() == View.VISIBLE) {
            verify_view.setVisibility(View.VISIBLE);
            lin_send_view.setVisibility(View.GONE);
            textTitle.setText(getString(R.string.veryfy_contact));
            OtpViewShow();
        } else {
            verify_view.setVisibility(View.GONE);
            lin_send_view.setVisibility(View.VISIBLE);
            textTitle.setText(getString(R.string.forgot_password));
        }
    }

    // otp view
    public void OtpViewShow() {
        otpView.setOtpCompletionListener(otp -> {
            AppUtils.hideKeyboard(Act_forgot_password.this);
        });
        txt_message.setText(getString(R.string.please_type_verification_code)+" \n " + edCountryCode.getText().toString() + " " + edContactNo.getText().toString());
        otpView.setTextColor(Color.BLACK);
        startCountDown(txt_timer);

        if (isInstant && authCredential != null){
            auth.signInWithCredential(authCredential)
                    .addOnCompleteListener(Act_forgot_password.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                isInstant = false;
                                PasswordChangeApi(edContactNo.getText().toString(), edPwd.getText().toString());
                                Toast.makeText(Act_forgot_password.this, getString(R.string.varification_successfully), Toast.LENGTH_SHORT).show();
                            } else {
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    Toast.makeText(Act_forgot_password.this, "Verification Failed, Invalid credentials", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(Act_forgot_password.this, "Verification Failed", Toast.LENGTH_SHORT).show();
                                }
                                changeView();
                            }
                            linBtn.setVisibility(View.VISIBLE);
                            av_loading.setVisibility(View.GONE);
                        }
                    });
        }

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isVerified) {
                    PasswordChangeApi(edContactNo.getText().toString(), edPwd.getText().toString());
                } else {
                    linBtn.setVisibility(View.GONE);
                    av_loading.setVisibility(View.VISIBLE);
                    if (otpView.getText().toString().length() < 6) {
                        Toast.makeText(Act_forgot_password.this, "Enter OTP", Toast.LENGTH_SHORT).show();
                        linBtn.setVisibility(View.VISIBLE);
                        av_loading.setVisibility(View.GONE);
                    } else {
                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, otpView.getText().toString());
                        auth.signInWithCredential(credential)
                                .addOnCompleteListener(Act_forgot_password.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            isVerified = true;
                                            PasswordChangeApi(edContactNo.getText().toString(), edPwd.getText().toString());
//                                            Toast.makeText(Act_forgot_password.this, "Verification Successfully", Toast.LENGTH_SHORT).show();
                                        } else {
                                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                                Toast.makeText(Act_forgot_password.this, getString(R.string.verification_fail_invalid_credentials), Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(Act_forgot_password.this, getString(R.string.verification_failed), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        linBtn.setVisibility(View.VISIBLE);
                                        av_loading.setVisibility(View.GONE);
                                    }
                                });
                    }
                }
            }
        });
    }

    // otp timer
    CountDownTimer countDownTimer;
    public void startCountDown(TextView textView) {

        //60_000=60 sec or 1 min and another is interval of count down is 1 sec

        countDownTimer = new CountDownTimer(120_000, 1000) {
            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                String min = String.valueOf(remainedSecs / 60);
                String sec = String.valueOf(remainedSecs % 60);
                if (min.length() == 1) {
                    min = "0" + min;
                }

                if (sec.length() == 1) {
                    sec = "0" + sec;
                }

                textView.setText(getString(R.string.enter_code_within) + (min) + ":" + (sec) );
                textView.setTextColor(Color.BLACK);
                textView.setBackground(null);
            }

            public void onFinish() {
                textView.setText(getString(R.string.resend_otp));
                textView.setBackground(getResources().getDrawable(R.drawable.btm_line_shape));
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PhoneAuthProvider.getInstance().verifyPhoneNumber(edCountryCode.getText().toString() + edContactNo.getText().toString()
                                , 2
                                , TimeUnit.MINUTES
                                , Act_forgot_password.this
                                , new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), "Otp Send", Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        Log.e("tag Error", e.getMessage());
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Failed to send Opt", Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        token = forceResendingToken;
                                        verificationId = s;
                                        startCountDown(textView);
                                    }
                                },
                                token);
                    }
                });
            }
        }.start();
    }

    public void bindVerifyView() {
        txt_message = findViewById(R.id.txt_message);
        txt_timer = findViewById(R.id.txt_timer);
        otpView = findViewById(R.id.otpView);
        av_loading = findViewById(R.id.av_loading);
        btn_verify = findViewById(R.id.btn_verify);
        linBtn = findViewById(R.id.linBtn);
    }

    public void bindSendView() {
        edCountryCode = findViewById(R.id.edCountryCode);
        edContactNo = findViewById(R.id.edContact);
        edPwd = findViewById(R.id.edPwd);
        edCPwd = findViewById(R.id.edCPwd);
        card_forgot = findViewById(R.id.card_forgot);

        edCountryCode.setVisibility(View.GONE);

        edCountryCode.setOnClickListener(view -> {
            if (codes == null) {
                getCountryCode();
            }
        });

        card_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edContactNo.getText().toString())) {
                    edContactNo.setError(getString(R.string.enter_contact));
                } else if (TextUtils.isEmpty(edCountryCode.getText().toString())) {
                    edCountryCode.setError("enter Country Code");
                } else if (TextUtils.isEmpty(edPwd.getText().toString())) {
                    edContactNo.setError(getString(R.string.enter_password));
                } else if (edPwd.getText().toString().length() < 6) {
                    edContactNo.setError(getString(R.string.please_enter6digit_password));
                } else if (!edPwd.getText().toString().matches(edCPwd.getText().toString())) {
                    edCPwd.setError(getString(R.string.does_not_match_password));
                } else if (edContactNo.getText().toString().length() < 6 || edContactNo.getText().toString().length() > 13) {
                    edContactNo.setError(getString(R.string.contact_invalide));
                } else {
                    if (!edCountryCode.getText().toString().substring(0,1).equals("+")){
                        edCountryCode.setText("+"+edCountryCode.getText().toString());
                    }
                    edCountryCode.setTextDirection(View.TEXT_DIRECTION_LTR);
                    checkNumber();
                }
            }
        });
    }

    // password change api call
    private void PasswordChangeApi(String contact, String psw) {

        API api = new API(Act_forgot_password.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    Toast.makeText(Act_forgot_password.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                    sharedpreferences.edit().clear().apply();
                    Intent intent = new Intent(Act_forgot_password.this, Act_Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finishAffinity();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(Act_forgot_password.this, getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        });
        Map<String, String> map = new HashMap<String, String>();
        map.put("contact_no", contact);
        map.put("password", psw);
        api.execute(100, Constant.URL + "user_forgotpassword", map, true);
    }

    // ger country code data
    private void getCountryCode() {
        if (AppUtils.isNetworkAvailable(this)) {

            API api = new API(getApplicationContext(), new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Log.e("Country Response", response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        if (obj.optString("status").equals("true")) {
                            JSONObject object = obj.getJSONObject("result");
                            JSONArray dataArray = object.getJSONArray("country");
                            codes = new String[dataArray.length()];
                            edCountryCode.setVisibility(View.VISIBLE);
                            edCountryCode.setText("+" + GetCountryZipCode());
                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject data_obj = dataArray.getJSONObject(i);
                                codes[i] = data_obj.getString("phone_code");
//                                edCountryCode.setText(codes[0]);

                                if (data_obj.getString( "country_code").toLowerCase().equals(countryCode.toLowerCase())) {
                                    edCountryCode.setText(data_obj.getString("phone_code"));
                                    edCountryCode.setVisibility(View.GONE);
                                }
                            }
                            ArrayAdapter<String> codeAdapter = new ArrayAdapter<String>(Act_forgot_password.this, android.R.layout.simple_spinner_dropdown_item, codes);
                            edCountryCode.setAdapter(codeAdapter);
                            edCountryCode.setThreshold(1);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        edCountryCode.setText("+" + GetCountryZipCode());
                        edCountryCode.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                    edCountryCode.setVisibility(View.VISIBLE);
                    edCountryCode.setText("+" + GetCountryZipCode());
                }
            });

            api.execute(200,Constant.URL + "get_country",null,false);

            /*StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "get_country",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Country Response", response);
                            try {
                                JSONObject obj = new JSONObject(response);
                                if (obj.optString("status").equals("true")) {
                                    JSONObject object = obj.getJSONObject("result");
                                    JSONArray dataArray = object.getJSONArray("country");
                                    codes = new String[dataArray.length()];
                                    for (int i = 0; i < dataArray.length(); i++) {

                                        JSONObject data_obj = dataArray.getJSONObject(i);
                                        codes[i] = data_obj.getString("phone_code");
                                        edCountryCode.setText(codes[0]);
                                    }
                                    ArrayAdapter<String> codeAdapter = new ArrayAdapter<String>(Act_forgot_password.this, android.R.layout.simple_spinner_dropdown_item, codes);
                                    edCountryCode.setAdapter(codeAdapter);
                                    edCountryCode.setThreshold(1);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //  displaying the error in toast if occurs
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            // request queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            requestQueue.add(stringRequest);*/
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
            edCountryCode.setVisibility(View.VISIBLE);
            edCountryCode.setText("+" + GetCountryZipCode());
        }
    }

    // check mobile register or not
    private void checkNumber() {
        ProgressDialog dialog = new ProgressDialog(Act_forgot_password.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        API api = new API(Act_forgot_password.this, new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

                Log.e("Tag Response", response);

                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    if (object.getString("status").equals("false")) {
                        PhoneAuthProvider.getInstance().verifyPhoneNumber(edCountryCode.getText().toString() + edContactNo.getText().toString()
                                , 2
                                , TimeUnit.MINUTES
                                , Act_forgot_password.this
                                , new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                    @Override
                                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.otp_send), Toast.LENGTH_LONG).show();
                                        String code = phoneAuthCredential.getSmsCode();
                                        dialog.dismiss();
                                        authCredential = phoneAuthCredential;
                                        isInstant = true;
                                        changeView();
                                    }

                                    @Override
                                    public void onVerificationFailed(@NonNull FirebaseException e) {
                                        dialog.dismiss();
                                        if (e instanceof FirebaseTooManyRequestsException){
                                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                        }else {
                                            Toast.makeText(getApplicationContext(), getString(R.string.failed_to_send_otp), Toast.LENGTH_LONG).show();
                                        }
                                        edCountryCode.setVisibility(View.VISIBLE);

                                    }

                                    @Override
                                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                        super.onCodeSent(s, forceResendingToken);
                                        Toast.makeText(getApplicationContext(), getString(R.string.send_otp_success), Toast.LENGTH_LONG).show();
                                        token = forceResendingToken;
                                        verificationId = s;
                                        dialog.dismiss();
                                        changeView();
                                    }
                                });
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.this_number_not_register), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        Map<String, String> map = new HashMap<>();
        map.put("contact_no", edContactNo.getText().toString());
        map.put("email", "email");

        api.execute(100, Constant.URL + "checkUserEmailAndContact", map, true);
    }

    // get country zip code based
    public String GetCountryZipCode() {
        String CountryID = "";
        String CountryZipCode = "962";

        CountryID = getDeviceCountryCode(this);
        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    // get country code on device
    private static @NotNull String getDeviceCountryCode(@NotNull Context context) {
        String countryCode;

        // Try to get country code from TelephonyManager service
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            // Query first getSimCountryIso()
            countryCode = tm.getSimCountryIso();
            if (countryCode != null && countryCode.length() == 2)
                return countryCode;

            if (tm.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                // Special case for CDMA Devices
                String network = tm.getNetworkCountryIso();
                if (network != null && network.length() == 2) {
                    countryCode = tm.getNetworkCountryIso();
                }
            } else {
                // For 3G devices (with SIM) query getNetworkCountryIso()
                countryCode = tm.getNetworkCountryIso();
            }

            if (countryCode != null && countryCode.length() == 2)
                return countryCode;
        }

        // If network country not available (tablets maybe), get country code from Locale class
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            countryCode = context.getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            countryCode = context.getResources().getConfiguration().locale.getCountry();
        }

        if (countryCode != null && countryCode.length() == 2)
            return countryCode;

        // General fallback to "us"
        return countryCode;
    }

    @Override
    public void onClick(View view) {
        if (view == imgBack) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        if (verify_view.getVisibility() == View.VISIBLE) {
            changeView();
        } else {
            super.onBackPressed();
        }
    }
}
