package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Act_daily_payment_total extends AppCompatActivity {

    ImageView back;
    EditText ED_Total_date, ED_Total_time, ED_current_month;
    String date;
    private int mHour, mMinute;
    String current_month;

//    String get_monthly_fule_cost = "http://webmobdemo.xyz/automap/api/get_monthly_fule_cost";

    SharedPreferences prefs;
    String car_id;
    EditText ED_total_payment;
    Button btnExit;

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_daily_payment_total);

        back = findViewById(R.id.back_daily_petrol_payment2);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);
        car_id = prefs.getString("CAR_ID", " ");

        ED_Total_date = findViewById(R.id.ED_Total_date);
        ED_Total_time = findViewById(R.id.ED_Total_time);
        ED_current_month = findViewById(R.id.ED_current_month);
        ED_total_payment = findViewById(R.id.ED_total_payment);
        btnExit = findViewById(R.id.btnExit);

        date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(new Date());
        ED_Total_date.setText(date);
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm aaa");
        String strDate = dateFormat.format(currentTime);
        ED_Total_time.setText(strDate);

        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        current_month = dateFormat2.format(date);
        ED_current_month.setText(current_month);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                new Act_daily_petrol_payment().instance.finish();
            }
        });

        ED_Total_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                Locale.setDefault(Locale.ENGLISH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Act_daily_payment_total.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                ED_Total_date.setText(date);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        ED_Total_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                Locale.setDefault(Locale.ENGLISH);
                TimePickerDialog timePickerDialog = new TimePickerDialog(Act_daily_payment_total.this, new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                //ED_Total_time.setText(hourOfDay + ":" + minute);
                                mHour = hourOfDay;
                                minute = minute;

                                updateTime(mHour, minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        getMonthlyFuelCost();
    }

    String total_amount;

    // Add fuel cost
    private void getMonthlyFuelCost() {

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"get_monthly_fule_cost", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);

                    if (obj.optString("status").equals("1")) {

                        JSONObject result = obj.getJSONObject("result");
                        total_amount = result.getString("total_amount");
                        ED_total_payment.setText(total_amount);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Act_daily_payment_total.this, getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("car_id", car_id);
                map.put("month_year", ED_current_month.getText().toString());
                Log.e("params", " " + map);
                return map;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder

        String aTime = new StringBuilder().append(hours).append(':').append(minutes).append(" ").append(timeSet).toString();

        ED_Total_time.setText(aTime);
    }
}
