package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_accessories extends AppCompatActivity {

    ImageView back;
    EditText ED_Total_date,ED_Total_time;
    private int mHour, mMinute,mSec;
    String date;
    String enum_id;

    LinearLayout ll_card_ok;
    String car_id,date_time,str_paid_amount,str_title;
    SharedPreferences prefs;
    EditText ED_paid_amount,ED_title;
//    String url="http://webmobdemo.xyz/automap/api/add_services";
    ArrayList<String> object;

    String currency;
    TextView txt_currency;
    LinearLayout ll_date,ll_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accesories);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<String>) args.getSerializable("ARRAYLIST");
        enum_id = object.get(0);
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        car_id = prefs.getString("CAR_ID"," ");

        back = findViewById(R.id.back_engine_oil_services);
        ll_date = findViewById(R.id.ll_date);
        ll_time = findViewById(R.id.ll_time);
        ED_Total_date = findViewById(R.id.ED_Total_date);
        ED_Total_time = findViewById(R.id.ED_Total_time);
        ll_card_ok = findViewById(R.id.ll_card_ok);
        ED_paid_amount = findViewById(R.id.ED_paid_amount);
        ED_title = findViewById(R.id.ED_title);

        txt_currency = findViewById(R.id.txt_currency);
        currency = prefs.getString("CURRENCY"," ");
        txt_currency.setText(currency);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        date = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH).format(new Date());
        ED_Total_date.setText(date);
        Date currentTime = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat")
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
        String strDate = dateFormat.format(currentTime);
        ED_Total_time.setText(strDate);

        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                Locale.setDefault(Locale.ENGLISH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Act_accessories.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                ED_Total_date.setText(date);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });
        ll_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                Locale.setDefault(Locale.ENGLISH);
                TimePickerDialog timePickerDialog = new TimePickerDialog(Act_accessories.this, new TimePickerDialog.OnTimeSetListener() {

                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                ED_Total_time.setText(hourOfDay + ":" + minute+":"+mSec);
                                mHour   = hourOfDay;
//                                minute = minute;

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });
        ll_card_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_title=ED_title.getText().toString();
                str_paid_amount=ED_paid_amount.getText().toString();

                if(TextUtils.isEmpty(str_title)){
                    Toast.makeText(Act_accessories.this, getString(R.string.please_enter_title), Toast.LENGTH_SHORT).show();
                }else if(TextUtils.isEmpty(str_paid_amount)){
                    Toast.makeText(Act_accessories.this,getString(R.string.please_enter_amount), Toast.LENGTH_SHORT).show();
                }else {
                    addFrontBreakService();
                }
            }
        });

        date_time=ED_Total_date.getText().toString()+ED_Total_time.getText().toString();
    }

    // add service book front break detail
    private void addFrontBreakService() {
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"add_services", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response"," " +response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("1")) {
                        Toast.makeText(Act_accessories.this, getString(R.string.accessories_add_successfully), Toast.LENGTH_LONG).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Act_accessories.this, getString(R.string.somting_wrong_please) + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("car_id", car_id);
                map.put("title", ED_title.getText().toString());
                map.put("amount", ED_paid_amount.getText().toString());
                map.put("date_time", date_time);
                map.put("enum_id", enum_id);
                Log.e("params"," " +map);
                return map;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
