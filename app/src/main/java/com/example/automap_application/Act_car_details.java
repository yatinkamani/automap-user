package com.example.automap_application;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.ClickableViewPager;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.CAR.Car_Model_model;
import com.example.automap_application.Model.CAR.Car_brand_model;
import com.example.automap_application.Model.CAR.Car_speed_model;
import com.example.automap_application.Model.CAR.Car_transmission_Model;
import com.example.automap_application.Model.CAR.Manufacture_Country_Model;
import com.example.automap_application.Model.CAR.car_fule_model;
import com.example.automap_application.Model.VolleyMultipartRequest;
import com.example.automap_application.adapter.MyCustomPagerAdapter;
import com.example.automap_application.interfaces.RecyclerViewItemClickedCallback;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Act_car_details extends AppCompatActivity implements View.OnClickListener {

    public static final int REQ_CODE = 100;

    SearchableSpinner sp_car_brand, sp_car_model, sp_m_country;
    Spinner sp_car_variant, sp_fule_type, sp_car_transmission, sp_speedometer, Spinner_car_year;
    private ProgressDialog dialog = null;
    TextView noImage;
    Button upload, save;
    ArrayList<String> encodedImageList;
    String imageURI;
    private JSONObject jsonObject;

    private Bitmap bitmap;

    ScrollView scroll_view;
    View lin_error;
    TextView txt_error;
    Button btn_retry;

    Car_Model_model car_model_model = new Car_Model_model();
    private ArrayList<Car_brand_model> car_brand_model_Array_List;
    private ArrayList<Car_brand_model> select_brand_model;
    private ArrayList<String> car_brand = new ArrayList<String>();

    private ArrayList<Car_Model_model> car_model_Array_List;
    private ArrayList<String> car_model = new ArrayList<String>();
    private ArrayList<String> modelId = new ArrayList<String>();

    private ArrayList<Manufacture_Country_Model> manufactureCountryModels = new ArrayList<>();

    //    Car_variant_Model car_variant_model = new Car_variant_Model();
    //    private ArrayList<Car_variant_Model> car_variant_models_Array_List;
    //    private ArrayList<String> St_car_variant = new ArrayList<String>();
    //    private ArrayList<String> St_car_variant_id = new ArrayList<String>();String

    Car_speed_model car_speed_model = new Car_speed_model();
    private ArrayList<Car_speed_model> car_speed_models_Array_List;
    private ArrayList<String> car_speed = new ArrayList<String>();

    car_fule_model car_fule_model = new car_fule_model();
    private ArrayList<car_fule_model> car_fule_models_Array_List;
    private ArrayList<String> car_fule = new ArrayList<String>();
//    private ArrayList<String> car_fuel_id = new ArrayList<String>();

    Car_transmission_Model car_transmission_model = new Car_transmission_Model();
    private ArrayList<Car_transmission_Model> car_transmission_Array_List;
    private ArrayList<String> car_transmission = new ArrayList<String>();

    private static final int REQUEST_CODE = 6384;

    private ArrayList<Uri> arrayList;
    EditText ED_Total_date;

    String REGISTRATION_DATE;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String USER_ID;
    EditText ed_van_No;

    String fuel_id = "", transmission_id = "",
            speedometer_id = "", car_brand_id = "",
            car_model_id = "", manufacture_country_id = "";
    ClickableViewPager viewPager;
    MyCustomPagerAdapter myCustomPagerAdapter;

    RadioGroup radio_group;
    ImageView back;

    String date;
    String van_no;

    //    ArrayList<String> car_images_array = new ArrayList<String>();
    ArrayList<String> car_image_ids;
    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
    ArrayList<Uri> mArrayUri2 = new ArrayList<Uri>();
    ArrayList<Uri> mArrayUri3 = new ArrayList<Uri>();

    String car_id = "";
    String selected_year = "";
    String fule_id = "";
    String car_transmission_id = "";
    String vin_no = "";
    String registration_date = "";
    String car_image_id = "";
    String image_name = "";
    ProgressDialog progress;

    int PICK_IMAGE_CAMERA = 100, PICK_IMAGE_GALLERY = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_car_details);

        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.please_wait_updating_car_detail));
        dialog.setCancelable(false);

        jsonObject = new JSONObject();
        encodedImageList = new ArrayList<>();

        arrayList = new ArrayList<>();

        ED_Total_date = (EditText) findViewById(R.id.ED_Total_date);
        radio_group = (RadioGroup) findViewById(R.id.radio_button);
        sp_car_brand = findViewById(R.id.Spinner_car_brand);
        sp_car_model = findViewById(R.id.Spinner_car_model);
        sp_car_variant = findViewById(R.id.Spinner_car_variant);
        sp_m_country = findViewById(R.id.Spinner_m_country);
        sp_fule_type = findViewById(R.id.Spinner_car_fule);
        Spinner_car_year = findViewById(R.id.Spinner_car_year);
        sp_car_transmission = findViewById(R.id.sp_car_transmission);
        sp_speedometer = findViewById(R.id.sp_speedometer);
        noImage = (TextView) findViewById(R.id.noImage);
        upload = findViewById(R.id.upload);
        save = findViewById(R.id.save);
        ed_van_No = (EditText) findViewById(R.id.ed_van_No);
        viewPager = (ClickableViewPager) findViewById(R.id.viewPager);
        back = (ImageView) findViewById(R.id.back);
        scroll_view = (ScrollView) findViewById(R.id.scroll_view);
        lin_error = (View) findViewById(R.id.lin_error);
        txt_error = (TextView) findViewById(R.id.txt_msg);
        btn_retry = (Button) findViewById(R.id.btn_retry);

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);
        USER_ID = prefs.getString(getString(R.string.pref_user_id), "");
        selected_year = prefs.getString(getString(R.string.pref_car_year),"");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                van_no = ed_van_No.getText().toString();
                if (getIntent().getBooleanExtra("vin_no_require", false)) {
                    if (van_no.equals("")) {
                        ed_van_No.requestFocus();
                        ed_van_No.setError(getString(R.string.please_enter_van_number));
                        return;
                    } else if (van_no.length() != 17) {
                        ed_van_No.requestFocus();
                        ed_van_No.setError(getString(R.string.please_enter_17_digit_vin_number));
                        return;
                    }
                }

                /*if(radio_group.getCheckedRadioButtonId() == -1){
                    Toast.makeText(Act_car_details.this,"Please select transmission type...", Toast.LENGTH_LONG).show();
                }*/

                /*if(TextUtils.isEmpty(van_no)){
                    Toast.makeText(Act_car_details.this,"Please enter van number...", Toast.LENGTH_LONG).show();
                } else if(Constant.i == 2){
                    editCarDetail();
                    editCarDetail2();
                } else if(mArrayUri.size() == 0){
                    Toast.makeText(Act_car_details.this,"Please select at least one image...", Toast.LENGTH_LONG).show();
                } else {
                    addDetail();
                }*/

                /*if (TextUtils.isEmpty(van_no)) {
                    Toast.makeText(Act_car_details.this, "Please enter van number...", Toast.LENGTH_LONG).show();
                } else*/ if (!TextUtils.isEmpty(van_no) && van_no.length() != 17) {
                    Toast.makeText(Act_car_details.this, getString(R.string.please_enter_17_digit_vin_number), Toast.LENGTH_LONG).show();
                    ed_van_No.requestFocus();
                } else
                if (!car_id.equals("")) {
                    editCarDetail();
                } else if (Constant.i == 2) {
                    editCarDetail();
//                    editCarDetail2();
                } /*else if (ed_car_year.getText().toString().equals("")) {
                    ed_car_year.requestFocus();
                    ed_car_year.setError("Please Enter Car Year");
                } else if (mArrayUri.size() == 0) {
                    Toast.makeText(Act_car_details.this, "Please select at least one image...", Toast.LENGTH_LONG).show();
                } */ else {
                    addDetail();
                }
            }
        });

        // car image select and view
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Choose application"),PICK_IMAGE_GALLERY);*/
                selectImage();

            }
        });

        /*sp_m_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                manufacture_country_id = manufactureCountryModels.get(i).getMc_Id();

                car_brand = new ArrayList<>();
                select_brand_model = new ArrayList<>();
                for (int j = 0; j < car_brand_model_Array_List.size(); j++) {
                    if (manufacture_country_id.equals(car_brand_model_Array_List.get(j).getManufacture_country_id())) {
                        car_brand.add(car_brand_model_Array_List.get(j).getBrand_nm().toString());
                        select_brand_model.add(car_brand_model_Array_List.get(j));
                    }
                }
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_car_details.this, android.R.layout.simple_spinner_dropdown_item, car_brand);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                sp_car_brand.setAdapter(spinnerArrayAdapter);
                for (int j = 0; j < select_brand_model.size(); j++) {
                    if (car_brand_id.equals(select_brand_model.get(j).getBrand_id())) {
                        sp_car_brand.setSelection(j);
                    }
                }
                if (select_brand_model.size() < 1) {
                    car_model_id = "";
                    car_brand_id = "";
                    sp_car_model.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        // car brand spinner
        sp_car_brand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                car_brand_model_Array_List.get(position).getBrand_id();
                String p_code = car_brand_model_Array_List.get(position).getBrand_id();
                car_brand_id = car_brand_model_Array_List.get(position).getBrand_id();
                car_model = new ArrayList<>();
                modelId = new ArrayList<>();
                for (int i = 0; i < car_model_Array_List.size(); i++) {

                    Car_Model_model car_model_model = car_model_Array_List.get(i);
                    if (p_code.equalsIgnoreCase(car_model_model.getBrand_id())) {
                        car_model.add(car_model_model.getModel_nm());
                        modelId.add(car_model_model.getModel_id().toString());
                    }
                }

                ArrayAdapter<String> spinnerArrayAdapter_c_model = new ArrayAdapter<String>(Act_car_details.this, android.R.layout.simple_spinner_dropdown_item, car_model);
                spinnerArrayAdapter_c_model.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                sp_car_model.setAdapter(spinnerArrayAdapter_c_model);
                for (int i = 0; i < modelId.size(); i++) {
                    if (car_model_id.equals(modelId.get(i))) {
                        sp_car_model.setSelection(i);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // car model spinner
        sp_car_model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                modelId.get(position);
                Log.e("model_id", modelId.get(position));
                car_model_id = modelId.get(position);

                /*St_car_variant = new ArrayList<>();
                St_car_variant_id = new ArrayList<>();

                for (int i = 0; i < car_variant_models_Array_List.size(); i++) {

                    Car_variant_Model car_variant_model = car_variant_models_Array_List.get(i);
                    if (modelId.get(position).equalsIgnoreCase(car_variant_model.getModel_id())) {
                        St_car_variant.add(car_variant_model.getVariant_name().toString());
                        St_car_variant_id.add(car_variant_model.getVariant_id().toString());
                    }
                }*/

                /*if (St_car_variant_id != null && St_car_variant_id.size()>0){
                    sp_car_variant.setVisibility(View.VISIBLE);
                } else {
                    sp_car_variant.setVisibility(View.GONE);
                    variant_id = "1";
                    varient_id = "1";
                }

                ArrayAdapter<String> spinnerArrayAdapter_v_model = new ArrayAdapter<String>(Act_car_details.this, android.R.layout.simple_spinner_dropdown_item, St_car_variant);
                spinnerArrayAdapter_v_model.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                sp_car_variant.setAdapter(spinnerArrayAdapter_v_model);
                for (int i=0;i<St_car_variant_id.size();i++){
                    if (variant_id.equals(St_car_variant_id.get(i))){
                        sp_car_variant.setSelection(i);
                    }
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                car_model_id = "";
            }
        });

        sp_car_variant.setVisibility(View.GONE);

        /*sp_car_variant.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                car_variant_models_Array_List.get(position).getVariant_id();
                varient_id = St_car_variant_id.get(position);
                variant_id = St_car_variant_id.get(position);
                car_model_id = car_variant_models_Array_List.get(position).getModel_id();
                Log.e("Tag Variant ID",St_car_variant_id.get(position));
                Log.e("Tag Variant Name",St_car_variant.get(position));
                Log.e("Tag Variant ID position", ""+position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        // car speedometer spinner
        sp_speedometer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                car_speed_models_Array_List.get(position).getSpeed_id();
                speedometer_id = car_speed_models_Array_List.get(position).getSpeed_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // car fuel type spinner
        sp_fule_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                car_fule_models_Array_List.get(position).getFule_id();
                fuel_id = car_fule_models_Array_List.get(position).getFule_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // car transmeter spinner
        sp_car_transmission.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                car_transmission_Array_List.get(position).getTransmission_id();
                transmission_id = car_transmission_Array_List.get(position).getTransmission_id();
                car_transmission_id = car_transmission_Array_List.get(position).getTransmission_id();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                for(int i=0; i<rg.getChildCount(); i++) {
                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                    if(btn.getId() == checkedId) {
                        transmission_id = car_transmission_Array_List.get(i).getTransmission_id();
                        return;
                    }
                }
            }
        });*/

        date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        ED_Total_date.setText(date);
        if (Constant.i == 2) {
            ED_Total_date.setEnabled(false);
        }

        // date of registration spinner
        ED_Total_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                Locale.setDefault(Locale.ENGLISH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Act_car_details.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                //String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                String date = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
                                ED_Total_date.setText(date);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });

//        retrieveJSON();

        if (myCustomPagerAdapter != null && myCustomPagerAdapter.getCount() > 0) {
            viewPager.setVisibility(View.VISIBLE);
        } else {
            viewPager.setVisibility(View.GONE);
        }
        getCarDetail("0");
//        getSpeedometerDetail();
        CarYearSpinner();
        btn_retry.setOnClickListener(this);
    }

    // spinner of year list
    public void CarYearSpinner() {

        ArrayList<String> years = new ArrayList<>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1950; i <= thisYear+1; i++) {
            years.add(Integer.toString(i));
        }
        Collections.reverse(years);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, years);
        Spinner_car_year.setAdapter(adapter);
        for (int i = 0; i < Spinner_car_year.getAdapter().getCount(); i++) {
            if (Spinner_car_year.getAdapter().getItem(i).equals(selected_year)) {
                Spinner_car_year.setSelection(i);
            }
        }

        Spinner_car_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selected_year = adapter.getItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    // selected car details
    private void getCarDetail(String check) {

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_car_detail",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", " " + response);
                        dialog.dismiss();
                        try {

                            JSONObject mainJsonObject = new JSONObject(response);
                            if (mainJsonObject.optString("status").equals("true")) {

                                JSONArray result = mainJsonObject.getJSONArray("result");

                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject jsonObject = result.getJSONObject(i);

                                    car_id = jsonObject.getString("car_id");
//                                    variant_id = jsonObject.getString("variant_id");
                                    car_model_id = jsonObject.getString("model_id");
                                    car_brand_id = jsonObject.getString("brand_id");
                                    fule_id = jsonObject.getString("fule_id");
                                    car_transmission_id = jsonObject.getString("transmission_id");
                                    speedometer_id = jsonObject.getString("speedometer_id");
                                    manufacture_country_id = jsonObject.getString("manufacture_country_id");
                                    vin_no = jsonObject.getString("vin_no");
                                    registration_date = jsonObject.getString("registration_date");
                                    selected_year = jsonObject.getString("car_year");
                                    JSONArray car_images = jsonObject.optJSONArray("carImage");
                                    car_image_ids = new ArrayList<String>();
                                    for (int j = 0; j < car_images.length(); j++) {
                                        JSONObject jsonObject2 = car_images.getJSONObject(j);
                                        car_image_id = jsonObject2.getString("car_image_id");
                                        image_name = jsonObject2.getString("image_name");

                                        //  mArrayUri.add(Uri.parse(image_name));
                                        mArrayUri2.add(Uri.parse(image_name));
                                        car_image_ids.add(car_image_id);
                                    }
                                    Log.e("Tag car_id", "" + car_image_ids.size());
                                }

                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString(getString(R.string.pref_car_id), car_id);
                                editor.putString(getString(R.string.pref_car_brand_id), car_brand_id);
                                editor.putString(getString(R.string.pref_car_model_id), car_model_id);
                                editor.putString(getString(R.string.pref_car_year), selected_year);
                                editor.putString(getString(R.string.pref_fule_id), fuel_id);
                                editor.putString(getString(R.string.pref_manufacture_country_id), manufacture_country_id);
                                editor.putString(getString(R.string.pref_booking_date), registration_date);
                                editor.putString(getString(R.string.pref_vin_no), vin_no);
                                editor.apply();

                                ed_van_No.setText(vin_no);
                                ED_Total_date.setText(registration_date);

                                if (prefs.getString("image", "").toString().equals("")) {
                                    myCustomPagerAdapter = new MyCustomPagerAdapter(Act_car_details.this, mArrayUri2, new RecyclerViewItemClickedCallback() {
                                        @Override
                                        public void onItemClicked(final int position) {

                                            AlertDialog.Builder builder1 = new AlertDialog.Builder(Act_car_details.this);
                                            builder1.setMessage(getString(R.string.are_you_sure_delete_image));
                                            builder1.setCancelable(true);

                                            builder1.setPositiveButton(getString(R.string.yess), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    Log.e("Tag car_id", "" + car_image_ids.size());
                                                    deleteCarImage(car_image_ids.get(position));
                                                    mArrayUri2.remove(position);
                                                    car_image_ids.remove(position);
                                                    myCustomPagerAdapter.notifyDataSetChanged();
                                                    if (myCustomPagerAdapter != null && myCustomPagerAdapter.getCount() > 0) {
                                                        viewPager.setVisibility(View.VISIBLE);
                                                    } else {
                                                        viewPager.setVisibility(View.GONE);
                                                    }
                                                }
                                            });

                                            builder1.setNegativeButton(getString(R.string.nos), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                                            AlertDialog alert11 = builder1.create();
                                            alert11.show();
                                        }
                                    });
                                } else {

                                    Uri uri = Uri.parse(prefs.getString("image", ""));
                                    mArrayUri2 = new ArrayList<>();
                                    mArrayUri = new ArrayList<>();
                                    mArrayUri2.add(uri);
                                    mArrayUri.add(uri);
                                    myCustomPagerAdapter = new MyCustomPagerAdapter(Act_car_details.this, mArrayUri2, new RecyclerViewItemClickedCallback() {
                                        @Override
                                        public void onItemClicked(final int position) {

                                            mArrayUri2.remove(position);
                                            car_image_ids.remove(position);
                                            myCustomPagerAdapter.notifyDataSetChanged();
                                            if (myCustomPagerAdapter != null && myCustomPagerAdapter.getCount() > 0) {
                                                viewPager.setVisibility(View.VISIBLE);
                                            } else {
                                                viewPager.setVisibility(View.GONE);
                                            }
                                        }
                                    });
                                }

                                if (myCustomPagerAdapter != null && myCustomPagerAdapter.getCount() > 0) {
                                    viewPager.setVisibility(View.VISIBLE);
                                } else {
                                    viewPager.setVisibility(View.GONE);
                                }
                                viewPager.setAdapter(myCustomPagerAdapter);

                            }
                            if (check.equals("1")) {
                                editor = prefs.edit();
                                editor.putString("image", "").apply();
                                Intent intent = new Intent(Act_car_details.this, Navigation_Activity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                retrieveJSON();
                                getSpeedometerDetail();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            scroll_view.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            txt_error.setText(getString(R.string.no_data_found));
                            btn_retry.setVisibility(View.VISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  displaying the error in toast if occurs
                dialog.dismiss();
                scroll_view.setVisibility(View.GONE);
                lin_error.setVisibility(View.VISIBLE);
                txt_error.setText(getString(R.string.no_data_found));
                btn_retry.setVisibility(View.VISIBLE);
                Log.e("TAG", "onErrorResponse: " + error);
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", USER_ID);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    // update api call to edit car details
    private void editCarDetail() {
        progress = new ProgressDialog(Act_car_details.this);
        progress.setMessage(getString(R.string.please_wait_updating_car_detail));
        progress.setCancelable(false);
        progress.show();
        //  our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "edit_car_detail",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        progress.dismiss();

                        Log.e("response", " " + (new String(response.data)));

                        try {
                            JSONObject obj = new JSONObject(new String(response.data));

                            if (obj.getString("status").equals("true")) {
                        /* Toast.makeText(getApplication(), obj.optString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(Act_car_details.this, Navigation_Activity.class);
                        startActivity(intent);*/
                                getCarDetail("1");
//                                Toast.makeText(getApplication(), obj.optString("message"), Toast.LENGTH_SHORT).show();
                                Toast.makeText(getApplication(), getString(R.string.car_detail_update_success), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError) {
                    Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                }/*else if(error instanceof SocketException){
                    Toast.makeText(getApplicationContext(),getString(R.string.internet_not_connect),Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),""+t,Toast.LENGTH_LONG).show();
                }*/
                progress.dismiss();
            }
        }) {
            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                JSONArray jsonArray = new JSONArray();

                params.put("car_id", car_id);
//              params.put("variant_id", varient_id);
                params.put("car_model_id", car_model_id);
                params.put("car_brand_id", car_brand_id);
                params.put("fule_id", fule_id);
                params.put("car_year", selected_year);
                params.put("transmission_id", transmission_id);
                params.put("manufacture_country_id", manufacture_country_id);
                params.put("van_no", ed_van_No.getText().toString());
                params.put("registration_date", ED_Total_date.getText().toString());
                params.put("speedometer_id", speedometer_id);
                params.put("language", prefs.getString(getString(R.string.pref_language), "ar"));


                Log.e("params", " " + params);
            /*for (Uri uri : mArrayUri) {
                jsonArray.put(uri);
            }
            params.put("car_images[]", String.valueOf(jsonArray));*/
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                long image_name = System.currentTimeMillis();
                int i = 0;
                for (Uri object : mArrayUri) {
                    InputStream iStream = null;
                    try {
                        iStream = getContentResolver().openInputStream(object);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    byte[] inputData = new byte[0];
                    try {
                        assert iStream != null;
                        inputData = getBytes(iStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    params.put("car_images[" + (i++) + "]", new DataPart(image_name + ".png", inputData, "image/*"));
                    Log.e("Tag car_images", " " + params);
                    Log.e("Tag car_images", " " + image_name + ".png " + inputData);

                }
                return params;
            }
        };

        Volley.newRequestQueue(this).add(volleyMultipartRequest);
        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 100000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 100000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

    }

    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(Act_car_details.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Choose application"), PICK_IMAGE_GALLERY);
                            /*Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                              startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);*/
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 0);
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 0);
            e.printStackTrace();
        }
    }

    private void deleteCarImage(final String car_image_id) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "delete_car_image", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.getString("message").equalsIgnoreCase("Car image remove successfully !")) {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //displaying the error in toast if occurs
                Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("car_image_id", car_image_id);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //get speedometer data and set spinner
    private void getSpeedometerDetail() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "get_enum_by_type", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("speed_response", " " + response);

                try {

                    car_speed_models_Array_List = new ArrayList<>();

                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.optString("status").equals("1")) {

                        JSONArray result = mainJsonObject.getJSONArray("result");

                        for (int i = 0; i < result.length(); i++) {
                            car_speed_model = new Car_speed_model();
                            JSONObject jsonObject = result.getJSONObject(i);
                            car_speed_model.setSpeed_id(jsonObject.getString("enum_id"));
                            car_speed_model.setSpeed_value(jsonObject.getString("value"));
                            car_speed_models_Array_List.add(car_speed_model);
                        }
                        if (car_speed_models_Array_List != null && car_speed_models_Array_List.size()>0){
                            Collections.reverse(car_speed_models_Array_List);
                        }
                        for (int i = 0; i < car_speed_models_Array_List.size(); i++) {
                            car_speed.add(car_speed_models_Array_List.get(i).getSpeed_value().toString());
                        }
                        ArrayAdapter<String> spinnerArrayAdapter_c_speed = new ArrayAdapter<String>(Act_car_details.this, android.R.layout.simple_spinner_dropdown_item, car_speed);
                        spinnerArrayAdapter_c_speed.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        sp_speedometer.setAdapter(spinnerArrayAdapter_c_speed);

                        for (int i=0; i<car_speed_models_Array_List.size(); i++){
                            Log.e("Tag id", car_speed.get(i)+"  "+speedometer_id);
                            if (car_speed_models_Array_List.get(i).getSpeed_id().toString().equals(speedometer_id))
                                sp_speedometer.setSelection(i);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Tag Car Detail", error.toString());
                        scroll_view.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_error.setText(getString(R.string.no_data_found));
                        btn_retry.setVisibility(View.VISIBLE);

//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("enum_type", "Speedometer");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    // get manufacture country, brand, model, fuel, and transmission data and set spinner
    private void retrieveJSON() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL + "car_detail", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("true")) {

                        car_brand_model_Array_List = new ArrayList<>();
                        car_model_Array_List = new ArrayList<>();
//                      car_variant_models_Array_List = new ArrayList<>();
                        car_fule_models_Array_List = new ArrayList<>();
                        car_transmission_Array_List = new ArrayList<>();
                        JSONObject object_result = obj.getJSONObject("result");

                        JSONArray carBrand = object_result.getJSONArray("car_brand");
                        for (int i = 0; i < carBrand.length(); i++) {

                            Car_brand_model car_brand_model = new Car_brand_model();
                            JSONObject c_brand = carBrand.getJSONObject(i);
                            car_brand_model.setBrand_id(c_brand.getString("brand_id"));
                            car_brand_model.setBrand_nm(c_brand.getString("brand_name"));
                            car_brand_model.setManufacture_country_id(c_brand.getString("manufacture_country_id"));
                            car_brand_model.setCreated_at(c_brand.getString("created_at"));
                            car_brand_model.setUpdated_at(c_brand.getString("updated_at"));
                            car_brand_model_Array_List.add(car_brand_model);
                        }

                        JSONArray car_model = object_result.getJSONArray("car_model");
                        for (int i = 0; i < car_model.length(); i++) {

                            car_model_model = new Car_Model_model();
                            JSONObject c_model = car_model.getJSONObject(i);
                            car_model_model.setModel_id(c_model.getString("model_id"));
                            car_model_model.setBrand_id(c_model.getString("brand_id"));
                            car_model_model.setModel_nm(c_model.getString("model_name"));
                            car_model_model.setManufacture_country_id(c_model.getString("manufacture_country_id"));
                            car_model_model.setModel_year(c_model.getString("model_year"));
                            car_model_model.setCreated_at(c_model.getString("created_at"));
                            car_model_model.setUpdated_at(c_model.getString("updated_at"));
                            car_model_Array_List.add(car_model_model);
                        }

                        /*JSONArray car_variant = object_result.getJSONArray("car_variant");
                                for (int i = 0; i < car_variant.length(); i++) {

                                    car_variant_model = new Car_variant_Model();
                                    JSONObject c_model = car_variant.getJSONObject(i);

//                                    car_variant_model.setVariant_id(c_model.getString("variant_id"));
                                    car_variant_model.setModel_id(c_model.getString("model_id"));
                                    car_variant_model.setVariant_name(c_model.getString("variant_name"));
                                    car_variant_model.setCreated_at(c_model.getString("created_at"));
                                    car_variant_model.setUpdated_at(c_model.getString("updated_at"));
                                    car_variant_models_Array_List.add(car_variant_model);

                                }*/

                        JSONArray car_transmission = object_result.getJSONArray("car_transmission");
                        for (int i = 0; i < car_transmission.length(); i++) {

                            car_transmission_model = new Car_transmission_Model();
                            JSONObject c_model = car_transmission.getJSONObject(i);
                            car_transmission_model.setTransmission_id(c_model.getString("transmission_id"));
                            car_transmission_model.setTransmission_name(c_model.getString("transmission"));
                            car_transmission_Array_List.add(car_transmission_model);

                        }

                        if (car_transmission_Array_List != null && car_transmission_Array_List.size()>0){
                            Collections.reverse(car_transmission_Array_List);
                        }

                        JSONArray car_fuel = object_result.getJSONArray("car_fule");
                        for (int i = 0; i < car_fuel.length(); i++) {

                            car_fule_model = new car_fule_model();
                            JSONObject c_model = car_fuel.getJSONObject(i);

                            car_fule_model.setFule_id(c_model.getString("fule_id"));
                            car_fule_model.setFule_type(c_model.getString("fule_type"));
                            car_fule_model.setCreated_at(c_model.getString("created_at"));
                            car_fule_model.setUpdated_at(c_model.getString("updated_at"));

                            car_fule_models_Array_List.add(car_fule_model);
                        }

                        manufactureCountryModels = new ArrayList<>();
                        JSONArray m_country = object_result.getJSONArray("manufacture_country");
                        for (int i = 0; i < m_country.length(); i++) {
                            JSONObject object = m_country.getJSONObject(i);
                            Manufacture_Country_Model model = new Manufacture_Country_Model();
                            model.setMc_Id(object.getString("manufacture_country_id"));
                            model.setMc_name(object.getString("country_name"));
                            model.setIsChecked("0");
                            manufactureCountryModels.add(model);
                        }

                        for (int i = 0; i < car_fule_models_Array_List.size(); i++) {
                            car_fule.add(car_fule_models_Array_List.get(i).getFule_type().toString());
                        }
                        ArrayAdapter<String> spinnerArrayAdapter_c_fule = new ArrayAdapter<String>(Act_car_details.this, android.R.layout.simple_spinner_dropdown_item, car_fule);
                        spinnerArrayAdapter_c_fule.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        sp_fule_type.setAdapter(spinnerArrayAdapter_c_fule);
                        for (int i = 0; i < car_fule_models_Array_List.size(); i++) {
                            if (fule_id.equals(car_fule_models_Array_List.get(i).getFule_id())) {
                                sp_fule_type.setSelection(i);
                            }
                        }

                        /*ArrayAdapter<Manufacture_Country_Model> adapter = new ArrayAdapter<>(Act_car_details.this, android.R.layout.simple_spinner_dropdown_item, manufactureCountryModels);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sp_m_country.setAdapter(adapter);
                        for (int i = 0; i < manufactureCountryModels.size(); i++) {
                            if (manufacture_country_id.equals(manufactureCountryModels.get(i).getMc_Id())) {
                                sp_m_country.setSelection(i);
                            }
                        }*/

                        for (int i = 0; i < car_brand_model_Array_List.size(); i++) {
//                            if (manufacture_country_id.equals(car_brand_model_Array_List.get(i).getManufacture_country_id())){
                            car_brand.add(car_brand_model_Array_List.get(i).getBrand_nm());
//                            }
                        }
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(Act_car_details.this, android.R.layout.simple_spinner_dropdown_item, car_brand);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        sp_car_brand.setAdapter(spinnerArrayAdapter);
                        for (int i = 0; i < car_brand_model_Array_List.size(); i++) {
                            if (car_brand_id.equals(car_brand_model_Array_List.get(i).getBrand_id())) {
                                sp_car_brand.setSelection(i);
                            }
                        }

                        for (int i = 0; i < car_transmission_Array_List.size(); i++) {
                            Act_car_details.this.car_transmission.add(car_transmission_Array_List.get(i).getTransmission_name().toString());
                        }
                        ArrayAdapter<String> spinnerArrayAdapter_transmission = new ArrayAdapter<String>(Act_car_details.this, android.R.layout.simple_spinner_dropdown_item, Act_car_details.this.car_transmission);
                        spinnerArrayAdapter_transmission.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        sp_car_transmission.setAdapter(spinnerArrayAdapter_transmission);
                        for (int i = 0; i < car_transmission_Array_List.size(); i++) {
//                          Act_car_details.this.car_transmission.add(car_transmission_Array_List.get(i).getTransmission_name().toString());
                            if (car_transmission_id.equals(car_transmission_Array_List.get(i).getTransmission_id())) {
                                sp_car_transmission.setSelection(i);
                            }
                        }

                        scroll_view.setVisibility(View.VISIBLE);
                        lin_error.setVisibility(View.GONE);

                    } else {
                        scroll_view.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_error.setText(getString(R.string.no_data_found));
                        btn_retry.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    scroll_view.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_error.setText(getString(R.string.no_data_found));
                    btn_retry.setVisibility(View.VISIBLE);
                }
                dialog.dismiss();
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurs
                        Log.e("Tag Car Detail", error.toString());
                        dialog.dismiss();
                        scroll_view.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_error.setText("Try Again Car data not get");
                        btn_retry.setVisibility(View.VISIBLE);
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && null != data) {
                // Get the Image from data

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                encodedImageList.clear();

                if (data.getData() != null) {
                    Uri mImageUri = data.getData();
                    mArrayUri = new ArrayList<Uri>();
                    mArrayUri.add(mImageUri);
                    Log.e("Tag Data", data.getDataString());
                    Log.e("Tag Data Path", data.getData().getPath());

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri, filePathColumn, null, null, null);

                    // Move to first row
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageURI = cursor.getString(columnIndex);
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                    mArrayUri3.add(getImageUri(this, bitmap));
                    Log.e("Tag Bitmap", bitmap.toString());
//                    Log.e("Tag URI", imageURI);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                    encodedImageList.add(encodedImage);
                    cursor.close();

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageURI = cursor.getString(columnIndex);

                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                            mArrayUri3.add(getImageUri(this, bitmap));

                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

                            encodedImageList.add(encodedImage);
                            cursor.close();
                        }
                        noImage.setText("Selected Images: " + mArrayUri.size());
                    }
                }

                myCustomPagerAdapter = new MyCustomPagerAdapter(Act_car_details.this, mArrayUri,
                        new RecyclerViewItemClickedCallback() {
                            @Override
                            public void onItemClicked(int position) {
                                mArrayUri.remove(position);
                                mArrayUri3.remove(position);
                                myCustomPagerAdapter.notifyDataSetChanged();
                                if (myCustomPagerAdapter != null && myCustomPagerAdapter.getCount() > 0) {
                                    viewPager.setVisibility(View.VISIBLE);
                                } else {
                                    viewPager.setVisibility(View.GONE);
                                }
                            }
                        });
                if (myCustomPagerAdapter != null && myCustomPagerAdapter.getCount() > 0) {
                    viewPager.setVisibility(View.VISIBLE);
                } else {
                    viewPager.setVisibility(View.GONE);
                }
                viewPager.setAdapter(myCustomPagerAdapter);
            } else if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK && null != data) {
                File destination;
                try {

                    bitmap = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    Uri selectedImage = getImageUri(this, bitmap);
                    editor = prefs.edit();
                    editor.putString("image", "" + selectedImage);
                    editor.apply();
                    mArrayUri = new ArrayList<Uri>();
                    mArrayUri.add(selectedImage);

                    /*Log.e("Activity", "Pick from Camera::>>> ");

                    String timeStamp = new SimpleDateFormat("yyyy MM dd_HH mm ss", Locale.ENGLISH).format(new Date());
                    destination = new File(Environment.getExternalStorageDirectory() + "/" + getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

//                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    // Get the cursor
                    mArrayUri3.add(getImageUri(this, bitmap));
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                    encodedImageList.add(encodedImage);
//                    path = destination.getAbsolutePath();
//                    imageView.setImageBitmap(bitmap);

                    Log.e("Tag Uri 1", "" + mArrayUri3.size());
                    Log.e("Tag Uri 2", "" + mArrayUri.size());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                myCustomPagerAdapter = new MyCustomPagerAdapter(this, mArrayUri, new RecyclerViewItemClickedCallback() {
                    @Override
                    public void onItemClicked(int position) {
                        mArrayUri.remove(position);
                        mArrayUri3.remove(position);
                        myCustomPagerAdapter.notifyDataSetChanged();
                        if (myCustomPagerAdapter != null && myCustomPagerAdapter.getCount() > 0) {
                            viewPager.setVisibility(View.VISIBLE);
                        } else {
                            viewPager.setVisibility(View.GONE);
                        }
                    }
                });

                if (myCustomPagerAdapter != null && myCustomPagerAdapter.getCount() > 0) {
                    viewPager.setVisibility(View.VISIBLE);
                } else {
                    viewPager.setVisibility(View.GONE);
                }

                if (mArrayUri.size() > 0)
                    viewPager.setVisibility(View.VISIBLE);
                viewPager.setAdapter(myCustomPagerAdapter);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.you_have_not_chose_image), Toast.LENGTH_LONG);
            }
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            Log.e("Tag Error", "" + e);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    // first time add car detail
    private void addDetail() {

        progress = new ProgressDialog(Act_car_details.this);
        progress.setMessage(getString(R.string.please_wait_updating_car_detail));
        progress.setCancelable(false);
        progress.show();
        // our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "add_car_detail", new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                Log.e("add_response", "" + new String(response.data));
                progress.dismiss();

                try {
                    JSONObject obj = new JSONObject(new String(response.data));

                    if (obj.getString("status").equals("true")) {
                        Toast.makeText(getApplication(), getString(R.string.upload_successfully), Toast.LENGTH_SHORT).show();
                        getCarDetail("1");
                        /*Intent intent = new Intent(Act_car_details.this, Navigation_Activity.class);
                        startActivity(intent);*/
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> params = new HashMap<>();

                params.put("user_id", USER_ID);
//                params.put("variant_id", varient_id);
                params.put("car_model_id", car_model_id);
                params.put("car_brand_id", car_brand_id);
                params.put("fule_id", fuel_id);
                params.put("manufacture_country_id", manufacture_country_id);
                params.put("transmission_id", transmission_id);
                params.put("van_no", ed_van_No.getText().toString());
                params.put("registration_date", ED_Total_date.getText().toString());
                params.put("speedometer_id", speedometer_id);
                params.put("car_year", selected_year);
                Log.e("params", " " + params);

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                long imageName = System.currentTimeMillis();
                int i = 0;
                for (Uri object : mArrayUri) {
                    InputStream iStream = null;
                    try {

                        iStream = getContentResolver().openInputStream(object);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    byte[] inputData = new byte[0];
                    try {

                        inputData = getBytes(iStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.e("re image", "test data detail " + imageName + ".png " + inputData);
                    params.put("car_images[" + (i++) + "]", new DataPart(imageName + ".png", inputData, "image/*"));
//                    Log.e("re image",(new DataPart(imageName + ".png",inputData)).toString());
//                    params.put("car_images[]", new DataPart(imageName + ".png",inputData));

                }
                Log.e("Tag image param", params.toString());
                return params;
            }
        };

        Volley.newRequestQueue(this).add(volleyMultipartRequest);
        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 100000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 100000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        editor = prefs.edit();
        editor.putString("image", "").apply();
    }

    @Override
    public void onClick(View view) {
        if (view == btn_retry) {
//            getSpeedometerDetail();
            getCarDetail("");
        }
    }

}
