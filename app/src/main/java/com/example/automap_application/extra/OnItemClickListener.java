package com.example.automap_application.extra;


import com.example.automap_application.Model.GARAGE.Garage_Model;

public interface OnItemClickListener {
    void onItemClick(Garage_Model item);
}
