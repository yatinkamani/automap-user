package com.example.automap_application.extra;

/**
 * Created by y34h1a on 1/13/16.
 */
public class Constants {
    public static String PREF_NAME = "preference_data";
    public static String PREF_LANG = "current_language";
}
