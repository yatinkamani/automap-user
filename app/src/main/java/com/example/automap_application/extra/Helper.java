package com.example.automap_application.extra;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/*
 * Created by KCS on 08-Dec-18.
 * Email : info.kaprat@gmail.com
 */
/*For All = 0, Admin = 1, Subadmin = 2, teachers = 3,
 parent = 4, student = 5; */
public class Helper {

    public static String Choosen_Language = "ar";


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

   /* public static boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length();) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }*/

    /*
    private static final String arabic = "\u06f0\u06f1\u06f2\u06f3\u06f4\u06f5\u06f6\u06f7\u06f8\u06f9";

    private static String arabicToDecimal(String number) {
        char[] chars = new char[number.length()];
        for(int i=0;i<number.length();i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        return new String(chars);
    }*/

    public static String decimalToArbic(String number){
        number = number.replace("1","١").replace("2","٢").
                replace("3","٣").replace("4","٤").replace("5","٥").
                replace("6","٦").replace("7","٧").replace("8","٨")
                .replace("9","٩").replace("0","٠");
        return number;
    }
}
