package com.example.automap_application.extra;



public interface APIResponse {
    public void onAPISuccess(int requestCode, boolean isSuccess, String response);
    public void onAPIError(int requestCode, boolean isError, String error);
}
