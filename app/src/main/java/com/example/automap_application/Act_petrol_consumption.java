package com.example.automap_application;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.backgroundServices.GasStationAlertService;
import com.example.automap_application.utils.AppUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

public class Act_petrol_consumption extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks {

    ImageView back;
    LinearLayout lin_main;
    private FusedLocationProviderClient fusedLocationClient;
    Location location1 = null;
    LocationManager manager;
    Spinner sp_speedometer;
    EditText ED_amount;
    String mk_status = "";
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    CardView card_ok, card_Edit_amount;
    double kilometer = 0;
    double kilometer_new = 0;

    LinearLayout lin_old, lin_new;
    EditText ED_litter_amount, ED_the_amount, ED_odo_meter;
    Spinner sp_speedometer_new;
    CardView card_ok_new;
    DecimalFormat decimalFormat;
    String gas_station = "";

    public int PERMISSION = 202;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_petrol_cunsuption);

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);
        editor = prefs.edit();
        back = findViewById(R.id.back_daily_petrol_payment2);
        lin_main = findViewById(R.id.lin_main);
        ED_amount = findViewById(R.id.ED_amount);
        sp_speedometer = findViewById(R.id.sp_speedometer);
        card_ok = findViewById(R.id.card_ok);
        card_Edit_amount = findViewById(R.id.card_Edit_amount);
        lin_new = findViewById(R.id.lin_new);
        lin_old = findViewById(R.id.lin_old);
        ED_litter_amount = findViewById(R.id.ED_litter_amount);
        ED_the_amount = findViewById(R.id.ED_the_amount);
        ED_odo_meter = findViewById(R.id.ED_odo_meter);
        sp_speedometer_new = findViewById(R.id.sp_speedometer_new);
        card_ok_new = findViewById(R.id.card_ok_new);

        lin_old.setVisibility(View.VISIBLE);
        lin_new.setVisibility(View.GONE);
        decimalFormat = new DecimalFormat("#.##");

        manager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = manager.getBestProvider(criteria, false);
        Intent intent = getIntent();
        if (intent != null) {
            gas_station = intent.getStringExtra("gas_station");
        }

        back.setOnClickListener(view -> {onBackPressed();});

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION);


            return;
        } else {
            Location location = manager.getLastKnownLocation(provider);

            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1, this);
            manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);

            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showGPSDisabledAlertToUser();
            }
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(createLocationRequest());
            LocationSettingsRequest locationSettingsRequest = builder.build();
            SettingsClient settingsClient = LocationServices.getSettingsClient(Act_petrol_consumption.this);
            settingsClient.checkLocationSettings(locationSettingsRequest);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            fusedLocationClient.getLastLocation().addOnSuccessListener(Act_petrol_consumption.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        location1 = location;
                    }
                }
            });
        }
        ArrayList<String> strings = new ArrayList<>();
        strings.add(getString(R.string.mile));
        strings.add(getString(R.string.kilometer));
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, strings);
        sp_speedometer.setAdapter(adapter);

        sp_speedometer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    if (!ED_amount.getText().toString().equals("")) {
                        mk_status = parent.getItemAtPosition(position).toString();
                        if (mk_status.equals(getString(R.string.mile)))
                            kilometer = Double.parseDouble("" + (Double.parseDouble((ED_amount.getText().toString())) / (0.62137)));
                        else
                            kilometer = Double.parseDouble("" + (Double.parseDouble((ED_amount.getText().toString())))); /*  (0.62137)));*/
                    } else {
                        sp_speedometer.setSelection(0);
                        ED_amount.setError(getString(R.string.enter_patrol_amount));
                        ED_amount.requestFocus();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_speedometer_new.setAdapter(adapter);

        sp_speedometer_new.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    if (ED_litter_amount.getText().toString().equals("")) {
                        ED_litter_amount.setError(getString(R.string.please_enter_filled_litter_amount));
                        ED_litter_amount.requestFocus();
                    } else if (ED_the_amount.getText().toString().equals("")) {
                        ED_the_amount.setError(getString(R.string.please_enter_paid_amount));
                        ED_the_amount.requestFocus();
                    } else if (ED_odo_meter.getText().toString().equals("")) {
                        ED_odo_meter.setError(getString(R.string.please_enter_new_odo_meter));
                        ED_odo_meter.requestFocus();
                    } else {
                        mk_status = parent.getItemAtPosition(position).toString();
                        if (mk_status.equals("Mile"))
                            kilometer_new = Double.parseDouble("" + (Double.parseDouble((ED_odo_meter.getText().toString())) / (0.62137)));
                        else
                            kilometer_new = Double.parseDouble("" + (Double.parseDouble((ED_odo_meter.getText().toString())))); /* (0.62137)))*/
                        ;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        card_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ED_amount.getText().equals("")) {
                    ED_amount.setError(getString(R.string.please_enter_new_odo_meter));
                } else if (location1 != null){
                    Toast.makeText(getApplicationContext(), getString(R.string.location_not_available),Toast.LENGTH_LONG).show();
                }else {

                    ProgressDialog progressDialog = new ProgressDialog(Act_petrol_consumption.this);
                    progressDialog.setMessage(getString(R.string.petrol_consumption_rate_test_is_in_progress));
                    progressDialog.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            lin_old.setVisibility(View.GONE);
                            lin_new.setVisibility(View.VISIBLE);
                            progressDialog.dismiss();
                        }
                    }, 5000);
                    editor.putString("oldodometer_old", "" + decimalFormat.format(kilometer));
                    editor.putString("old_odo_lat", "" + location1.getLatitude());
                    editor.putString("old_odo_log", "" + location1.getLongitude());
                    editor.apply();
                }
            }
        });

        card_ok_new.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (ED_litter_amount.getText().toString().equals("")) {
                    ED_litter_amount.setError(getString(R.string.please_enter_filled_litter_amount));
                } else if (ED_the_amount.getText().toString().equals("")) {
                    ED_litter_amount.setError(getString(R.string.please_enter_paid_amount));
                } else if (ED_odo_meter.getText().toString().equals("")) {
                    ED_odo_meter.setError(getString(R.string.please_enter_new_odo_meter));
                } else if (Float.parseFloat("" + kilometer_new) < Float.parseFloat(Objects.requireNonNull(prefs.getString("oldodometer_old", "0")))) {
                    ED_odo_meter.setError(getString(R.string.please_enter_new_odo_meter));
                    ED_odo_meter.requestFocus();
                    sp_speedometer_new.setSelection(0);
                    ED_odo_meter.setText("" + (Float.parseFloat(prefs.getString("oldodometer_old", "0")) + 10));
                } else {

                    Log.e("TAG 1", prefs.getString("oldodometer_old", "0") + " " + "" + kilometer_new);
                    float new_odometer = Float.parseFloat("" + kilometer_new) - Float.parseFloat(prefs.getString("oldodometer_old", "0"));
                    Log.e("TAG 2", ED_the_amount.getText().toString() + " " + new_odometer);
                    float cok = Float.parseFloat(ED_the_amount.getText().toString()) / new_odometer;
                    float aol = new_odometer / Float.parseFloat(ED_litter_amount.getText().toString());
                    Log.e("TAG 3", "" + aol);
                    float galon = 20 * aol;
                    editor.putString("pcr", "" + cok);
                    editor.apply();
                    AlertDialog.Builder builder = new AlertDialog.Builder(Act_petrol_consumption.this);
                    builder.setTitle("Results");
                    builder.setMessage(getString(R.string.the_end_result_leads) + " " + decimalFormat.format(galon));
                    builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(Act_petrol_consumption.this);
                            builder1.setMessage(getString(R.string.do_you_want_to_test_gasoline_consumption));
                            builder1.setPositiveButton(getString(R.string.yess), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), Act_petrol_consumption.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            builder1.setNegativeButton(getString(R.string.nos), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });

                            AlertDialog dialogs = builder1.create();
                            dialogs.setCanceledOnTouchOutside(false);
                            dialogs.setCancelable(false);
                            dialogs.show();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setCancelable(false);
                    dialog.show();
                }
            }
        });

        if (gas_station != null) {
            finishTest();
        } else {
            TestConditionAlert();
        }
    }

    public void finishTest() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_petrol_consumption.this);
        builder.setTitle(getString(R.string.test_condition));
        builder.setMessage(getString(R.string.did_you_like_to_finish_petrol_consumption_rate_test_now));

        builder.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), GasStationAlertService.class);
                stopService(intent);
                finish();
            }
        });
        builder.setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                TestConditionAlert();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    // show dialog to ask must patrol station
    public void TestConditionAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_petrol_consumption.this);
        builder.setTitle(getString(R.string.test_condition));
        builder.setMessage(getString(R.string.you_must_be_at_the_petrol_station));

        builder.setNeutralButton(getString(R.string.remind_me_in_a_petrol_station), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!isMyServiceRunning(GasStationAlertService.class)) {

                    Intent intent = new Intent(getApplicationContext(), GasStationAlertService.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startService(intent);
                        Log.e("Tag", "Big");
                    } else {
                        startService(intent);
                        Log.e("Tag", "Small");
                    }
                }
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.im_at_the_petrol_staion_now), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ConsumptionRateAlert();
            }
        });
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    // show dialog consumption rate now
    public void ConsumptionRateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_petrol_consumption.this);
        builder.setMessage(getString(R.string.did_you_like_make_petrol_consumption_rate_test_now));

        builder.setPositiveButton(getString(R.string.yess), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                PetrolStationAlert();
            }
        });
        builder.setNegativeButton(getString(R.string.nos), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    // show dialog patrol station alert
    public void PetrolStationAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_petrol_consumption.this);
        builder.setMessage(getString(R.string.are_you_now_at_the_petrol_station));

        builder.setPositiveButton(getString(R.string.yess), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (location1 != null){
                    dialog.dismiss();
                    check_petrol_station(location1);
                }else {
                    Toast.makeText(getApplicationContext(), getString(R.string.location_not_available), Toast.LENGTH_LONG).show();
                }
            }
        });

        builder.setNegativeButton(getString(R.string.nos), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                PetrolStationNoAlert();

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    // show dialog not patrol station
    public void PetrolStationNoAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_petrol_consumption.this);
        builder.setMessage(getString(R.string.you_must_abide_by_the_conditions));

        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        location1 = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Act_petrol_consumption.this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false);
        alertDialogBuilder.setPositiveButton("Goto Settings Page To Enable GPS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                callGPSSettingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callGPSSettingIntent);
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    boolean message = false;

    // check curent location aroun 30 yard radio patroll station
    public void check_petrol_station(Location location) {

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Loading...");
        progressDialog.show();
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + location.getLatitude() + "," + location.getLongitude());
        googlePlacesUrl.append("&radius=30");
        googlePlacesUrl.append("&type=" + "gas_station");
        googlePlacesUrl.append("&key=" + getString(R.string.google_api_key));

        Log.e("TAG URL", googlePlacesUrl.toString());
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest request = new StringRequest(Request.Method.GET, googlePlacesUrl.toString(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("TAG", response);
                    progressDialog.dismiss();
                    if (!response.isEmpty()) {
                        try {
                            JSONObject mainObject = new JSONObject(response);
                            Log.e("TAG", mainObject.getString("status"));
//                        if (mainObject != null){
                            if (mainObject.getString("status").equals("OK")) {
                                message = true;
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.you_must_abide_by_the_conditions), Toast.LENGTH_LONG).show();
                                finish();
                            }
//                        }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("TAG", error.toString());
                    if (error instanceof com.android.volley.NoConnectionError) {
                        PetrolStationAlert();
                        Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(request);
        } else {
            PetrolStationAlert();
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    private boolean isMyServiceRunning(@NonNull Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION){
            if (grantResults.length > 0){
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED){
                    recreate();
                }
                if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ||
                        !shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)){
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_access_to_device_setting), Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), getString(R.string.app_not_work_proper), Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
