package com.example.automap_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.GarageNotificationModel;
import com.example.automap_application.backgroundServices.LocationUpdateService;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Act_Mobile_Request_Offers_List extends AppCompatActivity implements View.OnClickListener {

    ImageView back_mobile_request_list;
    RecyclerView rl_garage;
    String user_id = "", mobile_request_id = "";
    ProgressDialog progressDialog;
    ArrayList<GarageNotificationModel> garage_list = new ArrayList<>();
    LinearLayout lin_bottom;
    View no_data;
    TextView txt_msg;
    Button btn_retry;
    SharedPreferences preferences;
    Adapter_Garage_offer_List act_garage_offer_list;
    SharedPreferences sharedPreferences;
    ImageView img_menu;
    SharedPreferences.Editor editor;
    Boolean SortPrice = false, SortTime = false;

    @SuppressLint("StaticFieldLeak")
    public Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__mobile__request__offers__list);

        instance = this;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));

        preferences = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id), "");

        rl_garage = (RecyclerView) findViewById(R.id.rl_garage);
        back_mobile_request_list = (ImageView) findViewById(R.id.back_garage_list);
        img_menu = (ImageView) findViewById(R.id.img_menu);
        no_data = (View) findViewById(R.id.no_data);
        lin_bottom = (LinearLayout) findViewById(R.id.lin_bottom);
        txt_msg = (TextView) findViewById(R.id.txt_msg);
        btn_retry = (Button) findViewById(R.id.btn_retry);

        back_mobile_request_list.setOnClickListener(this);
        btn_retry.setOnClickListener(this);
        img_menu.setOnClickListener(this);
        img_menu.setVisibility(View.GONE);

        getIntentData();
        getMobileRequestOffer();
        InitializeBroadcast();
    }

    @Override
    public void onClick(View view) {
        if (view == btn_retry) {
            getMobileRequestOffer();
        } else if (view == back_mobile_request_list) {
            onBackPressed();
        }
        // mobile request offer filter popup menu
        else if (view == img_menu) {

            PopupMenu menu = new PopupMenu(this, img_menu);
            menu.getMenuInflater().inflate(R.menu.sort_menu, menu.getMenu());
            menu.getMenu().findItem(R.id.price).setTitle(getString(R.string.sort_by_price));
            menu.getMenu().findItem(R.id.time).setTitle(getString(R.string.sort_by_time));

            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if (menuItem.getItemId() == R.id.price) {
                        SortPrice();
                        SortPrice = true;
                        SortTime = false;

                    } else if (menuItem.getItemId() == R.id.time) {
                        SortTime();
                        SortPrice = false;
                        SortTime = true;

                    }
                    return true;
                }
            });
            menu.show();
        }
    }

    // get request id
    public void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            mobile_request_id = intent.getStringExtra("request_id");
        }
    }

    // get all offer of particular id and lintierz
    private void getMobileRequestOffer() {
        img_menu.setVisibility(View.VISIBLE);
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            ProgressDialog dialog = new ProgressDialog(Act_Mobile_Request_Offers_List.this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "user_mobile_request_list", new Response.Listener<String>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(String response) {
                    Log.e("notification_list", " " + response);
                    try {

                        JSONObject mainJsonObject = new JSONObject(response);
                        garage_list = new ArrayList<>();
                        if (mainJsonObject.getString("status").equals("1")) {
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            if (result.length() > 0) {
                                lin_bottom.setVisibility(View.VISIBLE);
                                no_data.setVisibility(View.GONE);
                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject jsonObject = result.getJSONObject(i);

                                    GarageNotificationModel notification_model = new GarageNotificationModel();
                                    notification_model.setGarage_appointment_id(jsonObject.getString("mobile_request_id"));
                                    notification_model.setName(jsonObject.getString("garage_name"));
                                    notification_model.setGarage_id(jsonObject.getString("garage_id"));
                                    notification_model.setStatus(jsonObject.getString("status"));
                                    notification_model.setEmail(jsonObject.getString("email"));
                                    notification_model.setCity(jsonObject.getString("city_name"));
                                    notification_model.setAddress(jsonObject.getString("address"));
                                    notification_model.setContact_no(jsonObject.getString("contact_no"));
//                                    notification_model.setPinCode(jsonObject.optString("pinCode",""));
//                                    notification_model.setState(jsonObject.getString("state"));
                                    notification_model.setPrice(jsonObject.optString("price", ""));
                                    notification_model.setTime(jsonObject.optString("arrival_time", ""));
                                    notification_model.setUser_image(jsonObject.getString("owner_profile_img"));
                                    notification_model.setAppointment_date(jsonObject.getString("request_date"));
                                    notification_model.setAppointment_time(jsonObject.getString("request_time"));

                                    if (jsonObject.getString("latitude") != null && !(jsonObject.getString("latitude").equalsIgnoreCase("null"))) {
                                        if (0 < jsonObject.getString("latitude").length())
                                            notification_model.setLatitude(jsonObject.getString("latitude"));
                                        else
                                            notification_model.setLatitude("0.0");
                                    } else {
                                        notification_model.setLatitude("0.0");
                                    }

                                    if (jsonObject.getString("longitude") != null && !(jsonObject.getString("longitude").equalsIgnoreCase("null"))) {
                                        if (0 < jsonObject.getString("longitude").length())
                                            notification_model.setLongitude(jsonObject.getString("longitude"));
                                        else
                                            notification_model.setLongitude("0.0");
                                    } else {
                                        notification_model.setLongitude("0.0");
                                    }

                                    Log.e("TAG", jsonObject.getString("status"));
                                    if (!jsonObject.getString("status").equals("2") && !jsonObject.getString("status").equals("0") && !jsonObject.getString("status").equals("4")) {
                                        garage_list.add(notification_model);
                                        Log.e("TAG IND", jsonObject.getString("status"));
                                    }
                                }
                                if (garage_list.size() > 0) {
                                    if (act_garage_offer_list == null) {

                                        act_garage_offer_list = new Adapter_Garage_offer_List(Act_Mobile_Request_Offers_List.this, garage_list);
                                        rl_garage.setAdapter(act_garage_offer_list);
                                        rl_garage.setLayoutManager(new LinearLayoutManager(Act_Mobile_Request_Offers_List.this));
                                    } else {

                                        if (SortPrice) {
                                            SortPrice();
                                        } else if (SortTime) {
                                            SortTime();
                                        } else {
                                            act_garage_offer_list.setData(garage_list);
                                        }
                                    }
                                    lin_bottom.setVisibility(View.VISIBLE);
                                    no_data.setVisibility(View.GONE);
                                    img_menu.setVisibility(View.VISIBLE);
                                } else {
                                    lin_bottom.setVisibility(View.GONE);
                                    no_data.setVisibility(View.VISIBLE);
                                    btn_retry.setVisibility(View.GONE);
                                    txt_msg.setText(getString(R.string.no_garage_appointment_found));
                                }
                            } else {
                                lin_bottom.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                                btn_retry.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_garage_appointment_found));
                            }
                        } else {
                            lin_bottom.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                            btn_retry.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.no_data_found));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        lin_bottom.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                    }

                    dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    lin_bottom.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            }) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("mobile_request_id", mobile_request_id);
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {

            lin_bottom.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.internet_not_connect));
        }
    }

    public void InitializeBroadcast(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(Act_Mobile_Request_Offers_List.this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }

    // new offer come to refress api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (mobile_request_badge > 0 && !(Act_Mobile_Request_Offers_List.this.isFinishing())) {
                    getMobileRequestOffer();
                }
                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);

            }
        }
    };

    public void SortPrice() {

        Collections.sort(garage_list, new Comparator<GarageNotificationModel>() {
            @Override
            public int compare(GarageNotificationModel t0, GarageNotificationModel t1) {

                String tt1 = t0.getPrice().toString().replaceAll("\\D+", "");
                String tt2 = t1.getPrice().toString().replaceAll("\\D+", "");
                if (tt1.equals("")) {
                    tt1 = "-1";
                } else {
                    tt1 = tt1;
                }

                if (tt2.equals("")) {
                    tt2 = "-1";
                } else {
                    tt2 = tt2;
                }

                if (Integer.parseInt(tt1) > Integer.parseInt(tt2)) {
                    return 1;
                } else if (Integer.parseInt(tt1) < Integer.parseInt(tt2)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        if (act_garage_offer_list != null) {
            act_garage_offer_list.setData(garage_list);
        }
    }

    public void SortTime() {

        Collections.sort(garage_list, new Comparator<GarageNotificationModel>() {
            @Override
            public int compare(GarageNotificationModel t0, GarageNotificationModel t1) {

                String tt1 = t0.getTime().toString().replaceAll("\\D+", "");
                String tt2 = t1.getTime().toString().replaceAll("\\D+", "");
                Log.e("Tag Sort 1", tt1);
                Log.e("Tag Sort 2", tt2);
                if (tt1.equals("")) {
                    tt1 = "-1";
                } else {
                    tt1 = tt1;
                }

                if (tt2.equals("")) {
                    tt2 = "-1";
                } else {
                    tt2 = tt2;
                }

                if (Integer.parseInt(tt1) > Integer.parseInt(tt2)) {
                    return 1;
                } else if (Integer.parseInt(tt1) < Integer.parseInt(tt2)) {
                    return -1;
                } else {
                    return 0;
                }

            }
        });
        if (act_garage_offer_list != null) {
            act_garage_offer_list.setData(garage_list);
        }
    }

    @Override
    protected void onResume() {
        getMobileRequestOffer();
        super.onResume();
    }

    class Adapter_Garage_offer_List extends RecyclerView.Adapter<Adapter_Garage_offer_List.MyViewHolder> {

        private Activity activity;
        private ArrayList<GarageNotificationModel> booking_models;
        Location location;

        public Adapter_Garage_offer_List(Activity activity, ArrayList<GarageNotificationModel> booking_models) {
            this.activity = activity;
            this.booking_models = booking_models;
            location = LocationUpdateService.locations;
        }

        public void setData(ArrayList<GarageNotificationModel> data) {
            Log.e("Tag data 1", "" + data.size());
            booking_models = new ArrayList<>();
            booking_models.addAll(data);
            notifyDataSetChanged();
        }

        @SuppressLint("SetTextI18n")
        @NonNull
        @Override
        public Adapter_Garage_offer_List.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_mobile_request_offer_list, viewGroup, false);

            return new Adapter_Garage_offer_List.MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull Adapter_Garage_offer_List.MyViewHolder holder, int i) {
            GarageNotificationModel model = booking_models.get(i);

            holder.txt_name.setText("  " + model.getName());
            holder.txt_city.setText(   getString(R.string.city2)+" "+ model.getCity());
            if (!model.getPrice().equals("")) {
                holder.txt_price.setVisibility(View.VISIBLE);
                holder.txt_price.setText(getString(R.string.price)+" : " + model.getPrice());
            } else {
                holder.txt_price.setVisibility(View.GONE);
            }

            Glide.with(getApplicationContext()).asBitmap().load(model.getUser_image())
                    .placeholder(getResources().getDrawable(R.drawable.user))
                    .thumbnail(0.01f)
                    .into(new BitmapImageViewTarget(holder.user_image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            holder.user_image.setImageBitmap(resource);
                        }
                    });

            if (!model.getTime().equals("")) {
                holder.txt_arrival_time.setVisibility(View.VISIBLE);
                holder.txt_arrival_time.setText(getString(R.string.arrival_time)+" : " + (model.getTime() + " Hour"));
            } else {
                holder.txt_arrival_time.setVisibility(View.GONE);
            }

            if (model.getStatus().equals("2")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"     : "+getString(R.string.ignore_your_request));
            } else if (model.getStatus().equals("3")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"     : "+getString(R.string.you_confirm_this_workshop_offers));
            } else if (model.getStatus().equals("5")) {
                holder.txt_status.setVisibility(View.GONE);
//                holder.txt_status.setText("Status      :  pending ");
            } else if (model.getStatus().equals("6")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"     : "+getString(R.string.complete_request));
            } else if (model.getStatus().equals("7")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.you_complained_to_this_service_provider));
            } else if (model.getStatus().equals("8")) {
                holder.txt_status.setVisibility(View.VISIBLE);
                holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.the_service_provider_complained_to_you));
            } else {
                holder.txt_status.setVisibility(View.GONE);
            }

            Glide.with(activity).load(model.getUser_image()).into(holder.user_image);

            holder.llDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (model.getStatus().equals("2")) {
                        Toast.makeText(activity, getString(R.string.ignore_your_request), Toast.LENGTH_LONG).show();
                    } else if (model.getStatus().equals("4")) {
                        Toast.makeText(activity, getString(R.string.cancel_request), Toast.LENGTH_LONG).show();
                    } else if (model.getStatus().equals("6")) {
                        Toast.makeText(activity, getString(R.string.complete_request), Toast.LENGTH_LONG).show();
                    } else if (model.getStatus().equals("7") || model.getStatus().equals("8")) {
                        Toast.makeText(activity, getString(R.string.complained_request), Toast.LENGTH_LONG).show();
                    } else {
                        Intent in = new Intent(activity, Act_Mobile_Request_Workshop_Detail.class);
                        Constant.model = null;
                        Constant.model = new GarageNotificationModel();
                        Constant.model = model;
                        in.putExtra(getString(R.string.pref_garage_id), model.getGarage_id());
                        in.putExtra(getString(R.string.name), model.getName());
                        in.putExtra("garage_offer", true);
                        in.putExtra(getString(R.string.pref_request_id), model.getGarage_appointment_id());
                        activity.startActivity(in);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return booking_models.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView txt_name, txt_city, txt_arrival_time, txt_price, txt_status;
            private ImageView user_image;
            private LinearLayout llDetail;

            MyViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_name = itemView.findViewById(R.id.txt_name);
                user_image = itemView.findViewById(R.id.user_image);
                txt_city = itemView.findViewById(R.id.txt_city);
                txt_price = itemView.findViewById(R.id.txt_price);
                txt_status = itemView.findViewById(R.id.txt_status);
                txt_arrival_time = itemView.findViewById(R.id.txt_arraival_time);
                txt_price = itemView.findViewById(R.id.txt_price);
                llDetail = itemView.findViewById(R.id.llDetail);

            }
        }
    }

}
