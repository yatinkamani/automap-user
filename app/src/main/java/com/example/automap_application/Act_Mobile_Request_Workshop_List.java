package com.example.automap_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.Garage_Model;
import com.example.automap_application.Model.VolleyMultipartRequest;
import com.example.automap_application.backgroundServices.RemainderBroadcast;
import com.example.automap_application.extra.EqualSpacingItemDecoration;
import com.example.automap_application.firebase.NotificationUtils;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Mobile_Request_Workshop_List extends AppCompatActivity implements View.OnClickListener {

    ImageView back_garage_list;
    RecyclerView rl_garage;
    GarageListAdapter garageListAdapter;
    String garage_name;
    String garage_banner;
    String garage_id;
    String facebook_link, rating = "";
    ArrayList<Garage_Model> garage_list = new ArrayList<>();
    ProgressDialog progressDialog;
    String date = "";
    String time = "", type = "";
    String appointment_type = "", come_now = "", amount = "", oil_sae = "", oil_brand = "", leather_seat = "",
            lat = "", log = "", location = "", change_filter = "", service_id = "";

    String user_id = "";
    RelativeLayout lin_bottom;
    View no_data_error;
    Button btn_retry;
    TextView txt_msg;
    Button btn_send;
    String model_id = "", brand_id = "", fule_id = "";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String facility_json = null;
    String service_json = null;

    public Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__moblie__request__workshop__list);

        instance = this;
        rl_garage = (RecyclerView) findViewById(R.id.rl_garage);
        back_garage_list = (ImageView) findViewById(R.id.back_garage_list);
        btn_send = (Button) findViewById(R.id.btn_send);
        no_data_error = (LinearLayout) findViewById(R.id.no_data_error);
        lin_bottom = (RelativeLayout) findViewById(R.id.lin_bottom);
        txt_msg = (TextView) findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);

        preferences = getSharedPreferences(LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id), "");
        model_id = preferences.getString(getString(R.string.pref_car_model_id), "");
        brand_id = preferences.getString(getString(R.string.pref_car_brand_id), "");
        fule_id = preferences.getString(getString(R.string.pref_fule_id),"");

        back_garage_list.setOnClickListener(this);
        btn_send.setOnClickListener(this);
        btn_retry.setOnClickListener(this);

        createNotificationChannel();
        getIntentData();
        try {
            JSONArray jsonArray = new JSONArray(facility_json);
            JSONArray array = new JSONArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                JSONObject object1 = new JSONObject();
                service_id = object.getString("service_id");
                object1.put("service_id", service_id);
                array.put(object1);
            }
            service_json = array.toString();


        } catch (JSONException e) {
            e.printStackTrace();
//            sendRequest();
        }
        getBookingGarageList();
        if (facility_json != null) {
            if (come_now != null && come_now.equals("1")) {
                btn_send.setVisibility(View.GONE);
            } else {
                btn_send.setVisibility(View.VISIBLE);
            }
        }
    }

    // get previous screen send data
    public void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            facility_json = intent.getStringExtra("service_json");
            time = intent.getStringExtra("time");
            date = intent.getStringExtra("date");
            appointment_type = intent.getStringExtra("appointment_type");
            come_now = intent.getStringExtra("come_now");
//            tire_size_number = intent.getStringExtra("tire_size_number");
//            tire_brand = intent.getStringExtra("tire_brand");
//            battery_brand = intent.getStringExtra("battery_brand");
            oil_sae = intent.getStringExtra("oil_sae");
            oil_brand = intent.getStringExtra("oil_brand");
            amount = intent.getStringExtra("amount");
            lat = intent.getStringExtra("lat");
            log = intent.getStringExtra("log");
            location = intent.getStringExtra("location");
            type = intent.getStringExtra("type");
            change_filter = intent.getStringExtra("change_filter");
            leather_seat = intent.getStringExtra("leather_seat");
            if (leather_seat != null && !leather_seat.equals("null")){
            }else {
                leather_seat = "";
            }
        }
    }

    // mobile request send dialog
    public void send_dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Mobile_Request_Workshop_List.this);
        builder.setMessage(R.string.send_request_workshop);
        builder.setPositiveButton(getString(R.string.confirm_and_send), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendRequest();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void getMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Mobile_Request_Workshop_List.this);
        builder.setMessage(msg);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showThankDialog("");
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // get workshop list display listview
    private void getBookingGarageList() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "get_garage_mobile_request", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("garage booking response", response);
                    try {
                        ArrayList<Garage_Model> garage_list_in = new ArrayList<>();
                        JSONObject mainJsonObject = new JSONObject(response);
                        if (mainJsonObject.getString("status").equals("true")) {
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject jsonObject = result.getJSONObject(i);
                                    garage_name = jsonObject.getString("garage_name");
                                    garage_banner = jsonObject.getString("garage_banner");
                                    garage_id = jsonObject.getString("garage_id");
                                    facebook_link = jsonObject.getString("facebook_link");

                                    if (jsonObject.has("ratting")){
                                        rating = jsonObject.getString("ratting");
                                    }

                                    garage_list_in.add(new Garage_Model(garage_name, garage_banner, garage_id, facebook_link,rating));
                                }
                                if (garage_list_in.size() > 0) {

                                    for (Garage_Model garage_model : garage_list_in) {
                                        boolean isFound = false;
                                        for (Garage_Model garage_model1 : garage_list) {
                                            if (garage_model1.getGarage_id().equals(garage_model.getGarage_id())) {
                                                isFound = true;
                                                break;
                                            }
                                        }
                                        if (!isFound) {
                                            garage_list.add(garage_model);
                                        }
                                    }

                                    no_data_error.setVisibility(View.GONE);
                                    lin_bottom.setVisibility(View.VISIBLE);
                                    garageListAdapter = new GarageListAdapter(Act_Mobile_Request_Workshop_List.this, garage_list);
                                    rl_garage.setAdapter(garageListAdapter);
                                    rl_garage.setLayoutManager(new GridLayoutManager(Act_Mobile_Request_Workshop_List.this, 2));
                                    rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16)); // 16px. In practice, you'll want to use getDimensionPixelSize
                                    rl_garage.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
                                } else {
                                    lin_bottom.setVisibility(View.GONE);
                                    no_data_error.setVisibility(View.VISIBLE);
                                    txt_msg.setText(getString(R.string.no_service_provider_found));
                                    btn_retry.setVisibility(View.GONE);
                                }
                            } else {
                                lin_bottom.setVisibility(View.GONE);
                                no_data_error.setVisibility(View.VISIBLE);
                                txt_msg.setText(getString(R.string.no_service_provider_found));
                                btn_retry.setVisibility(View.GONE);
                            }
                        } else {
                            lin_bottom.setVisibility(View.GONE);
                            no_data_error.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.no_service_provider_found));
                            btn_retry.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        lin_bottom.setVisibility(View.GONE);
                        no_data_error.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                        btn_retry.setVisibility(View.VISIBLE);
                    }
                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    lin_bottom.setVisibility(View.GONE);
                    no_data_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                    Log.e("response", " " + error);
                    progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("user_id", user_id);
                    map.put("service_id", service_id);
                    map.put("fule_id", fule_id);
                    Log.e("Tag param", map.toString());
                    return map;

                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    3000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(request);
        } else {
            lin_bottom.setVisibility(View.GONE);
            no_data_error.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.internet_not_connect));
            btn_retry.setVisibility(View.GONE);
        }
    }

    // send mobile request for visible workshop
    private void sendRequest() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();

            VolleyMultipartRequest request = new VolleyMultipartRequest(Request.Method.POST, Constant.URL + "send_mobile_request",
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            Log.e("send response", new String(response.data));
                            dialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(new String(response.data));
                                if (object.getString("status").equals("1")) {

                                    /*if (CheckDate((date + " " + time), 3)) {
                                        Date Date = new Date();
                                        try {
                                            Date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date + " " + time);
                                            Calendar calendar = Calendar.getInstance();
                                            calendar.setTimeInMillis(Date.getTime());
                                            calendar.add(Calendar.HOUR, -2);

                                            Calendar calendar1 = Calendar.getInstance();
                                            calendar1.setTimeInMillis(Date.getTime());

                                            Intent intent = new Intent(Act_Mobile_Request_Workshop_List.this, RemainderBroadcast.class);

                                            String msg = "Your Mobile Auto Request 2 hour Remaining \n Time : " + new SimpleDateFormat("hh:mm aa").format(calendar1.getTime());
                                            intent.putExtra("title", "Mobile Request");
                                            intent.putExtra("messageFor", "mobile_request");
                                            intent.putExtra("msg", msg);
                                            intent.putExtra("id", Integer.parseInt(service_id));
                                            PendingIntent pendingIntent = PendingIntent.getBroadcast(Act_Mobile_Request_Workshop_List.this, Integer.parseInt(service_id), intent, Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                            Log.e("Tag Date Time", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(calendar.getTime()));
                                            manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            Log.e("Tag Date Time", e.toString());
                                        }
                                    }*/
                                    getMessage(object.getString("message"));
//                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                    btn_send.setVisibility(View.GONE);
                                    SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(service_id, new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date()));
                                    editor.apply();
                                } else {
                                    showThankDialog(object.getString("message"));
//                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response Error", "" + error);
                    if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                    } else if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                    }
//                    Toast.makeText(getApplicationContext(),""+error,Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", user_id);
                    map.put("request_date", date);
                    map.put("request_time", time);
                    map.put("model_id", model_id);
                    map.put("brand_id", brand_id);
                    map.put("facility_id_json", "" + facility_json);
                    map.put("request_type", appointment_type);
                    map.put("oil_sae", oil_sae);
                    map.put("oil_brand", oil_brand);
                    map.put("amount", amount);
                    map.put("leather_seat", leather_seat);
                    map.put("change_filter", change_filter);
                    map.put("location_address", location);
                    map.put("user_latitude", lat);
                    map.put("user_longitude", log);
                    JSONArray json = new JSONArray();
                    for (int i = 0; i < garage_list.size(); i++) {
                        JSONObject object = new JSONObject();
                        try {
                            object.put("garage_id", garage_list.get(i).getGarage_id());
                            json.put(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("garage_id_json", "" + json);

                    Log.e("Tag Parameter", map.toString());
                    return map;
                }
            };

            Volley.newRequestQueue(this).add(request);
            request.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 100000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 100000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    public void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NotificationUtils.CHANNEL_ID, NotificationUtils.CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("Notify Me");
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

    @SuppressLint("SetTextI18n")
    public void showThankDialog(String msg) {
        try {
            final Dialog dialog = new Dialog(this);
            Objects.requireNonNull(dialog.getWindow()).clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND); // for dialog shadow
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.popup_thank);
            dialog.setCancelable(false);
            // set values for custom dialog components - text, image and button
            //  final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
            TextView tvMessageTwo = (TextView) dialog.findViewById(R.id.tvMessageTwo);
            if (msg.equals("")){
                tvMessageTwo.setText(R.string.thank_you_for_request_mobile);
            } else {
                tvMessageTwo.setText(msg);
            }
            TextView tvOK = (TextView) dialog.findViewById(R.id.tvOk);

            dialog.show();

            tvOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    if (new Act_Mobile_Request_Booking().instance != null) {
                        new Act_Mobile_Request_Booking().instance.finish();
                    }
                    if (new Act_Mobile_Request().instance != null) {
                        new Act_Mobile_Request().instance.finish();
                    }
                    Intent i = new Intent(getApplicationContext(), Navigation_Activity.class);
                    startActivity(i);
                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btn_send) {
            send_dialog();
        } else if (view == btn_retry) {
            getBookingGarageList();
        } else if (view == back_garage_list) {
            onBackPressed();
        }
    }

    public class GarageListAdapter extends RecyclerView.Adapter<GarageListAdapter.MyViewHolder> {

        private Activity activity;
        private ArrayList<Garage_Model> garageList;
        private com.example.automap_application.adapter.GarageListAdapter.customButtonListener customListener;

        GarageListAdapter(Activity activity, ArrayList<Garage_Model> garageList) {
            this.activity = activity;
            this.garageList = garageList;
        }

        @Override
        public int getItemCount() {
            return garageList.size();
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_garage_list, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
            Garage_Model garage_model = garageList.get(position);
            holder.garage_name.setText(garage_model.getGarage_name());
//            Glide.with(activity).load(garage_model.getGarage_banner()).error(R.drawable.car1).apply(RequestOptions.skipMemoryCacheOf(true)).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)).dontAnimate().into(holder.garage_banner);

            if (garage_model.getGarage_banner().startsWith("http://") || garage_model.getGarage_banner().startsWith("https://")) {

                Log.e("Tag image", garage_model.getGarage_banner());
                Glide.with(activity).asBitmap().load(garage_model.getGarage_banner()).thumbnail(0.01f).placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(holder.garage_banner) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.garage_banner.setImageBitmap(resource);
                    }
                });
            } else {
                String url = Constant.IMAGE_URL+"Garage_Banner_Img/";
                Glide.with(activity).asBitmap().load(url + garage_model.getGarage_banner()).thumbnail(0.01f).placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(holder.garage_banner) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.garage_banner.setImageBitmap(resource);
                    }
                });
                Log.e("Tag image", url + garage_model.getGarage_banner());
            }

            holder.garage_banner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constant.model = null;
                    Intent in = new Intent(getApplicationContext(), Act_Workshop.class);
                    in.putExtra("workshop_id", garage_list.get(position).getGarage_id());
                    in.putExtra("workshop_name", garage_list.get(position).getGarage_name());
                    in.putExtra("isMobileRequest", true);
                    if (come_now != null) {
                        in.putExtra("come_now", come_now);
                        in.putExtra("request_date", date);
                        in.putExtra("request_time", time);
                        in.putExtra("model_id", model_id);
                        in.putExtra("brand_id", brand_id);
                        in.putExtra("facility_id_json", facility_json);
                        in.putExtra("request_type", appointment_type);
                        in.putExtra("oil_sae", oil_sae);
                        in.putExtra("oil_brand", oil_brand);
                        in.putExtra("amount", amount);
                        in.putExtra("leather_seat", leather_seat);
                        in.putExtra("change_filter", change_filter);
                        in.putExtra("location_address", location);
                        in.putExtra("user_latitude", lat);
                        in.putExtra("user_longitude", log);
                        JSONArray json = new JSONArray();
                        for (int i = 0; i < 1; i++) {
                            JSONObject object = new JSONObject();
                            try {
                                object.put("garage_id", garage_list.get(position).getGarage_id());
                                json.put(object);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        in.putExtra("garage_id_json", "" + json);

                    }

                    if (garage_list.get(position).getWebsite() != null) {
                        in.putExtra("website", garage_list.get(position).getWebsite());
                    }
                    startActivity(in);
                }
            });
        }

        public void setFilter(List<Garage_Model> studentListModel) {
            garageList = new ArrayList<>();
            garageList.addAll(studentListModel);
            notifyDataSetChanged();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView garage_name;
            ImageView garage_banner;

            MyViewHolder(View itemView) {
                super(itemView);
                garage_name = itemView.findViewById(R.id.garage_name);
                garage_banner = itemView.findViewById(R.id.garage_banner);
            }
        }
    }

    public boolean CheckDate(String date1, int number) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        try {
            Date date = dateFormat.parse(date1);
            Date current_date = dateFormat.parse(dateFormat.format(new Date()));
            if (current_date.before(date)) {
                long diff = date.getTime() - current_date.getTime();
                long second = diff / 1000;
                long minute = second / 60;
                long hour = minute / 60;
                long day = (hour / 24) + 1;
                Log.e("Tag days", "" + hour + ":" + minute + ":" + second);

                if (hour > number) {
                    return true;
                }

            } else {
                Log.e("Tag days", "" + date + "  " + current_date);
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("Tag days", "" + e);
        }

        return false;
    }

}