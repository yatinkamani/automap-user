package com.example.automap_application.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.automap_application.Act_Garage_List;
import com.example.automap_application.Act_Map;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.GarageNotificationModel;
import com.example.automap_application.Model.RejectReason;
import com.example.automap_application.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class GarageNotificationListAdapter extends RecyclerView.Adapter<GarageNotificationListAdapter.MyViewHolder> {

    private static final String TAG = "GarageNotification";
    private Activity activity;
    private ArrayList<GarageNotificationModel> garageList;
    private List<RejectReason> rejectReasons;
    String url="http://webmobdemo.xyz/automap/api/user_garage_appointment_list";

    public GarageNotificationListAdapter(Activity activity, ArrayList<GarageNotificationModel> garageList) {
        this.activity = activity;
        this.garageList = garageList;
    }

    @Override
    public int getItemCount() {
        return garageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public GarageNotificationListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification_list_garage, parent, false);
        return new GarageNotificationListAdapter.MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@Nullable final GarageNotificationListAdapter.MyViewHolder holder, int position) {

        final GarageNotificationModel notification_model = garageList.get(position);
        Log.d("Tag",notification_model.toString());
        assert holder != null;
        holder.garage_name.setText(notification_model.getName());
        holder.user_city.setText("City :             " + notification_model.getCity());
        holder.user_state.setText("State :           " + notification_model.getState());
        holder.user_address.setText(notification_model.getAddress());
        holder.user_contact_no.setText(notification_model.getContact_no());
        holder.lin_price.setVisibility(View.GONE);
        holder.lin_time.setVisibility(View.GONE);

        Glide.with(activity).load(notification_model.getUser_image()).into(holder.user_image);
        Log.e(TAG, "status==> " + notification_model.getStatus());

        if (notification_model.getStatus().equals("1")) {
            holder.img_msg.setImageResource(R.drawable.ic_check_circle_black_24dp);
            holder.txt_msg.setText("Your request for garage appointment has been accepted");
            holder.lin_price.setVisibility(View.VISIBLE);
            holder.lin_time.setVisibility(View.VISIBLE);
            holder.lin_agreement.setVisibility(View.VISIBLE);
            holder.lin_agree_status.setVisibility(View.VISIBLE);
            holder.user_price.setText(notification_model.getPrice() +" JD");
            holder.user_time.setText(notification_model.getPrice()+" Hour");
            if (notification_model.getUser_agree().equals("1")){
                holder.lin_agreement.setVisibility(View.GONE);
                holder.lin_agree_status.setVisibility(View.VISIBLE);
                holder.txt_agree_msg.setText("Your Work Is Progress");
                holder.img_agree_msg.setImageResource(R.drawable.ic_working);
                /*if (notification_model.getWork_done().equals("2")){
//                    RattingDialog(notification_model.getName(),notification_model);
                }else */if (notification_model.getGarage_complain().equals("1")) {
                    holder.lin_agreement.setVisibility(View.GONE);
                    holder.lin_agree_status.setVisibility(View.VISIBLE);
                    holder.txt_agree_msg.setText("Your Work Is Done");
                    holder.img_agree_msg.setImageResource(R.drawable.ic_check_circle_black_24dp);
                    if (notification_model.getDistance().equals("2")){
                        RattingDialog(notification_model);
                        notification_model.setRatting("1.5");
                        notifyDataSetChanged();
                    }
                }
            }else if(notification_model.getUser_agree().equals("2")){
                holder.lin_agreement.setVisibility(View.GONE);
                holder.lin_agree_status.setVisibility(View.VISIBLE);
                holder.txt_agree_msg.setText("Not Agree with the Garage");
                holder.img_agree_msg.setImageResource(R.drawable.ic_cancel_black_24dp);
            }else {
                holder.lin_agreement.setVisibility(View.VISIBLE);
                holder.lin_agree_status.setVisibility(View.GONE);
            }
        } else if (notification_model.getStatus().equals("2")) {
            holder.img_msg.setImageResource(R.drawable.ic_cancel_black_24dp);
            holder.txt_msg.setText("Your request for garage appointment has been rejected");
            holder.lin_agreement.setVisibility(View.GONE);
            holder.lin_agree_status.setVisibility(View.GONE);
        }else {
            holder.lin_agreement.setVisibility(View.GONE);
            holder.lin_agree_status.setVisibility(View.GONE);
        }

        /*if (notification_model.getUser_agree().equals("1")){
            holder.lin_agreement.setVisibility(View.GONE);
            holder.txt_agree_msg.setText("Agree with the Garage");
            holder.img_agree_msg.setImageResource(R.drawable.ic_check_circle_black_24dp);
        }else if(notification_model.getUser_agree().equals("2")){
            holder.lin_agreement.setVisibility(View.GONE);
            holder.lin_agree_status.setVisibility(View.GONE);
            holder.txt_agree_msg.setText("Not Agree with the Garage");
            holder.img_agree_msg.setImageResource(R.drawable.ic_cancel_black_24dp);
        }else {
            holder.lin_agreement.setVisibility(View.VISIBLE);
            holder.lin_agree_status.setVisibility(View.VISIBLE);
        }*/

        /*if (notification_model.getWork_done().equals("2")){
            RattingDialog(notification_model.getName(),notification_model);
        }*/

        holder.btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Tag", "Ok");
                holder.lin_agreement.setVisibility(View.GONE);
                holder.lin_agree_status.setVisibility(View.VISIBLE);
                holder.img_agree_msg.setImageResource(R.drawable.ic_working);
                holder.txt_agree_msg.setText("Your Work Is Progress");
                AgreementUser(notification_model.getUser_id(),notification_model.getStatus(),notification_model.getGarage_id(),"1",notification_model.getGarage_appointment_id(),"");
//                Toast.makeText(activity,"Agree With Garage",Toast.LENGTH_LONG).show();
            }
        });

        holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRejectionReason(notification_model.getUser_id(),notification_model.getStatus(),notification_model.getGarage_id(),"2",notification_model.getGarage_appointment_id());
                holder.lin_agreement.setVisibility(View.GONE);
                holder.lin_agree_status.setVisibility(View.VISIBLE);
                holder.img_agree_msg.setImageResource(R.drawable.ic_cancel_black_24dp);
                notification_model.setGarage_complain("0");
                notifyDataSetChanged();
            }
        });

        holder.llDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, Act_Map.class);
                i.putExtra("notificationLatitude", notification_model.getLatitude());
                i.putExtra("notificationLongitude", notification_model.getLongitude());
                activity.startActivity(i);
            }
        });

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView garage_name, user_city, user_state, user_address, user_contact_no, user_price, user_time, txt_msg, txt_agree_msg;
        CardView card_msg, agree_card_msg;
        CircleImageView user_image;
        ImageView img_msg, img_agree_msg;
        LinearLayout llDetail, lin_agreement, lin_price, lin_time;
        Button btn_ok, btn_cancel;
        RelativeLayout lin_agree_status;

        MyViewHolder(View itemView) {
            super(itemView);
            garage_name = itemView.findViewById(R.id.name);
            user_city = itemView.findViewById(R.id.user_city);
            user_state = itemView.findViewById(R.id.user_state);
            user_address = itemView.findViewById(R.id.user_address);
            user_contact_no = itemView.findViewById(R.id.user_contact_no);
            user_price = itemView.findViewById(R.id.user_price);
            user_time = itemView.findViewById(R.id.user_time);
            card_msg = itemView.findViewById(R.id.card_msg);
            agree_card_msg = itemView.findViewById(R.id.agree_card_msg);
            user_image = itemView.findViewById(R.id.user_image);
            img_msg = itemView.findViewById(R.id.img_msg);
            img_agree_msg = itemView.findViewById(R.id.img_agree_msg);
            txt_msg = itemView.findViewById(R.id.txt_msg);
            txt_agree_msg = itemView.findViewById(R.id.txt_agree_msg);
            llDetail = itemView.findViewById(R.id.llDetail);
            lin_agreement = itemView.findViewById(R.id.lin_agreement);
            lin_time = itemView.findViewById(R.id.lin_time);
            lin_price = itemView.findViewById(R.id.lin_price);
            btn_ok = itemView.findViewById(R.id.btn_ok);
            btn_cancel = itemView.findViewById(R.id.btn_cancel);
            lin_agree_status = itemView.findViewById(R.id.lin_agree_status);
        }
    }

    ArrayList<String> strings = new ArrayList<>();
    private void CancelReasonDialog(String user_id, String status, String garage_id, String garage_appointment_id,List<RejectReason> rejectReasons){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View view = LayoutInflater.from(activity).inflate(R.layout.dailog_garage_no_agree_reason,null);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        txt_dialog_title.setText("Reason For Cancel Agreement");

        CheckBox chk_prise = view.findViewById(R.id.chk_price);
        CheckBox chk_time = view.findViewById(R.id.chk_time);
        CheckBox chk_trust = view.findViewById(R.id.chk_trust);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        LinearLayout linearLayout = view.findViewById(R.id.lin_dynamic_checkbox);
        btn_cancel.setVisibility(View.GONE);
        chk_prise.setVisibility(View.GONE);
        chk_time.setVisibility(View.GONE);
        chk_trust.setVisibility(View.GONE);
//        chk_trust.setText("Car requirements are not available by my Garage");

        strings = new ArrayList<>();

        linearLayout.removeAllViews();
        for (int i=0;i<rejectReasons.size();i++){
            if (rejectReasons.get(i).getStatus().equals("0") || rejectReasons.get(i).getStatus().equals("3")){

                CheckBox checkBox = new CheckBox(activity);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                checkBox.setText(rejectReasons.get(i).getReason());
                checkBox.setTextColor(Color.BLACK);
                checkBox.setLayoutParams(params);
                final int finalI = i;
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            rejectReasons.get(finalI).setIs_checked("1");
                            strings.add(rejectReasons.get(finalI).getId());
                        }else {
                            rejectReasons.get(finalI).setIs_checked("0");
                            strings.remove(rejectReasons.get(finalI).getId());
                        }
                    }
                });
                linearLayout.addView(checkBox);
            }
        }
        /*chk_price.setOnCheckedChangeListener((buttonView, isChecked) -> {
            price[0] = isChecked;
            if (isChecked){
                checkReason.add(buttonView.getText().toString());
            }else {
                checkReason.remove(buttonView.getText().toString());
            }
        });
        chk_time.setOnCheckedChangeListener((buttonView, isChecked) -> {
            time[0] = isChecked;
            if (isChecked){
                checkReason.add(buttonView.getText().toString());
            }else {
                checkReason.remove(buttonView.getText().toString());
            }
        });
        chk_trust.setOnCheckedChangeListener((buttonView, isChecked) -> {
            trust[0] = isChecked;
            if (isChecked){
                checkReason.add(buttonView.getText().toString());
            }else {
                checkReason.remove(buttonView.getText().toString());
            }
        });*/

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onClick(View v) {
                Toast.makeText(activity,strings.toString(), Toast.LENGTH_LONG);
                if (strings.size()>0){
                    String data = strings.toString().replaceAll("\\[|\\]","");
                    AgreementUser(user_id,status,garage_id,"2",garage_appointment_id,data);
                    Log.e("TAG",strings.toString());
                    dialog.dismiss();
                    activity.finish();
                }else {
                    Toast.makeText(activity,"Please Select Reason",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private float ratings = 0;
    private void RattingDialog( GarageNotificationModel notificationModel){

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(activity).inflate(R.layout.dailog_garage_no_agree_reason,null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText("Please Rate "+notificationModel.getName());
        btn_ok.setText(activity.getString(R.string.yess));
        btn_cancel.setText(activity.getString(R.string.nos));
        ratingBar.setVisibility(View.VISIBLE);
        btn_cancel.setVisibility(View.GONE);

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        ratingBar.setOnRatingBarChangeListener((ratingBar1, rating, fromUser) -> ratings = rating);
        btn_ok.setOnClickListener(v -> {
            dialog.dismiss();
            notificationModel.setRatting("1");
            notifyDataSetChanged();
            RatingUser(notificationModel.getUser_id(), notificationModel.getStatus(), notificationModel.getGarage_id(),
                    notificationModel.getUser_agree(), notificationModel.getGarage_complain(),"1", ""+ratings, notificationModel.getGarage_appointment_id());
//            Intent intent = new Intent(activity, Act_Service_book.class);
//            activity.startActivity(intent);
            if (new Act_Garage_List().instance != null){
                new Act_Garage_List().instance.finish();
            }
            /* Intent intents = new Intent(getApplicationContext(), AlertDialogWorkDoneService.class);
               stopService(intents); */
//            activity.finish();
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity,"will be notified after 24 hours",Toast.LENGTH_LONG).show();
                /* Intent intent = new Intent(getApplicationContext(), AlertDialogWorkDoneService.class);
                   intent.putExtra("time",30000);
                   startService(intent);  */
            }
        });
    }

    private void AgreementUser(String user_id, String status,String garage_id,String user_agree,String garage_appointment_id, String reason){
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(activity.getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL+"response_garage_appointment", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Agreement_Response"," " +response);
                try {
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.getString("status").equals("1")){
                        Toast.makeText(activity,"Send Agreement",Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(activity,"Reject Agreement",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // displaying the error in toast if occur
//                 Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(activity,"Please Try Again",Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        }){
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("garage_id", garage_id);
                params.put("garage_appointment_id", garage_appointment_id);
                params.put("status", status);
                params.put("user_agree", user_agree);
                params.put("user_reason_id", reason);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(6000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    private void RatingUser(String user_id, String status, String garage_id, String user_agree, String work_status, String is_ratting, String ratting, String garage_appointment_id){
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(activity.getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL+"response_garage_appointment", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Agreement_Response"," " +response);
                try {
                    JSONObject mainJsonObject = new JSONObject(response);
                    if (mainJsonObject.getString("status").equals("1")){
                        Toast.makeText(activity,"Send Agreement",Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(activity,"Reject Agreement",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // displaying the error in toast if occur
//                 Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(activity,"Please Try Again",Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        }){
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",user_id);
                params.put("garage_id",garage_id);
                params.put("garage_appointment_id",garage_appointment_id);
                params.put("status",status);
                params.put("user_agree",user_agree);
                params.put("work_status",work_status);
                params.put("is_rating",is_ratting);
                params.put("rating",ratting);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(6000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    private void getRejectionReason(final String User_id, final String status, final String garage_id, final String agree_status, final String Garage_Appointment_Id){
        final ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(activity.getString(R.string.loading));
        dialog.show();
        rejectReasons = new ArrayList<>();
//        rejectReasons.add(new RejectReason("1","0","Price NOt Good","-1"));
//        rejectReasons.add(new RejectReason("2","0","Time NOt Good","-1"));
//        rejectReasons.add(new RejectReason("3","1","Car requirements are not available by my Garage","-1"));
//        rejectReasons.add(new RejectReason("4","1","We Are Not Provided This Facility","-1"));

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL+"get_rejection_reason", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("notification_list", " " + response);
                        dialog.dismiss();
                        rejectReasons = new ArrayList<>();
                        JSONObject mainJsonObject = null;
                        try {
                            mainJsonObject = new JSONObject(response);
                            JSONArray result = mainJsonObject.getJSONArray("result");

                            for (int i=0; i<result.length();i++){
                                JSONObject object = result.getJSONObject(i);
                                String id = object.getString("rejection_reson_id");
                                String status = object.getString("status");
                                String reason = object.getString("reason");

                                rejectReasons.add(new RejectReason(id,status,reason,"-1"));
                            }
                            CancelReasonDialog(User_id,status,garage_id,Garage_Appointment_Id,rejectReasons);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(activity);
        queue.add(stringRequest);

    }
}
