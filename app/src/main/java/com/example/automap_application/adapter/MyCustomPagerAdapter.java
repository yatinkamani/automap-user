package com.example.automap_application.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.example.automap_application.R;
import com.example.automap_application.interfaces.RecyclerViewItemClickedCallback;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

public class MyCustomPagerAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<Uri> images;
    private LayoutInflater layoutInflater;
    private ArrayList<String> car_image_ids;

    private RecyclerViewItemClickedCallback recyclerViewItemClickedCallback;

    public MyCustomPagerAdapter(Context context,ArrayList<Uri> images, RecyclerViewItemClickedCallback recyclerViewItemClickedCallback) {
        this.context = context;
        this.images = images;
        this.car_image_ids = car_image_ids ;
        this.recyclerViewItemClickedCallback = recyclerViewItemClickedCallback;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
       return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.item, container, false);
        RoundedImageView imageView = (RoundedImageView) itemView.findViewById(R.id.imageView);
        ImageView img_delete=(ImageView)itemView.findViewById(R.id.img_delete);

        //      imageView.setImageURI(images.get(position));
        /*      Glide.with(context).load(car_image_ids.get(position)).into(imageView);*/

        Glide.with(context).asBitmap().thumbnail(0.01f).load(images.get(position)).listener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                Log.e("Tag exception",e.toString());
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                Log.e("Tag Success", model.toString());
                imageView.setImageBitmap(resource);
                return false;
            }
        }).into(new BitmapImageViewTarget(imageView){
            @Override
            protected void setResource(Bitmap resource) {
                imageView.setImageBitmap(resource);
            }
        });

        container.addView(itemView);
        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewItemClickedCallback.onItemClicked(position);

            }
        });
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }

    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}