package com.example.automap_application.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.Garage_Model;
import com.example.automap_application.R;

import java.util.ArrayList;
import java.util.List;

public class GarageListAdapter extends RecyclerView.Adapter<GarageListAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<Garage_Model> garageList;
    private customButtonListener customListener;

    public GarageListAdapter(Activity activity, ArrayList<Garage_Model> garageList) {
        this.activity = activity;
        this.garageList = garageList;
    }

    public interface customButtonListener {
         void onButtonClickListener(int position);
    }

    public void setCustomButtonListener(customButtonListener listener) {
        this.customListener = listener;
    }

    @Override
    public int getItemCount() {
        return garageList.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_garage_list, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        Garage_Model garage_model = garageList.get(position);
        holder.garage_name.setText(garage_model.getGarage_name());

        /*Glide.with(activity).asBitmap().load(garage_model.getGarage_banner()).error(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(holder.garage_banner){
            @Override
            protected void setResource(Bitmap resource) {
                holder.garage_banner.setImageBitmap(resource);
            }
        });*/



        holder.garage_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (garage_model.getGarage_banner().startsWith("http://") || garage_model.getGarage_banner().startsWith("https://")) {

                Log.e("Tag image", garage_model.getGarage_banner());
                Glide.with(activity).asBitmap().load(garage_model.getGarage_banner()).thumbnail(0.01f).placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(holder.garage_banner) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.garage_banner.setImageBitmap(resource);
                    }
                });
            } else {
                String url = Constant.IMAGE_URL+"Garage_Banner_Img/";
                Glide.with(activity).asBitmap().load(url + garage_model.getGarage_banner()).thumbnail(0.01f).placeholder(R.mipmap.ic_launcher).into(new BitmapImageViewTarget(holder.garage_banner) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        holder.garage_banner.setImageBitmap(resource);
                    }
                });
                Log.e("Tag image", url + garage_model.getGarage_banner());
            }
                if (customListener != null) customListener.onButtonClickListener(holder.getAdapterPosition());
            }
        });
    }

    public void setFilter(List<Garage_Model> studentListModel) {
        garageList = new ArrayList<>();
        garageList.addAll(studentListModel);
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView garage_name;
        ImageView garage_banner;
        MyViewHolder(View itemView) {
            super(itemView);
            garage_name = itemView.findViewById(R.id.garage_name);
            garage_banner = itemView.findViewById(R.id.garage_banner);
        }
    }

}
