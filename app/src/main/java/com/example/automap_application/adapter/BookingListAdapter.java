package com.example.automap_application.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.automap_application.Act_Garage_Offer_List;
import com.example.automap_application.Model.GARAGE.GarageNotificationModel;
import com.example.automap_application.Model.GARAGE.Request_Model;
import com.example.automap_application.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class BookingListAdapter extends RecyclerView.Adapter<BookingListAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<Request_Model> booking_models;

    public BookingListAdapter(Activity activity, ArrayList<Request_Model> booking_models) {
        this.activity = activity;
        this.booking_models = booking_models;
    }

    public void setFilter(ArrayList<Request_Model> filter){
        this.booking_models = new ArrayList<>();
        booking_models.addAll(filter);
        notifyDataSetChanged();
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_booking_list, viewGroup, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Request_Model model = booking_models.get(i);

//        txt_name.setText("  "+model.getName());
        txt_appointment_date.setText(activity.getString(R.string.appointment_date) +" : "+DateFormat(model.getAppointment_date()));
        if (TimeFormat(model.getAppointment_time()).equals("1")){
            txt_appointment_time.setVisibility(View.GONE);
        }else {
            txt_appointment_time.setText(activity.getString(R.string.appointment_time)+" : "+TimeFormat(model.getAppointment_time()));
        }

        if (model.getAppointment_date().equals("") || model.getAppointment_date().equals("0000-00-00")){
            txt_appointment_date.setVisibility(View.GONE);
        } else {
            txt_appointment_date.setVisibility(View.VISIBLE);
        }

        if (model.getAppointment_time().equals("")){
            txt_appointment_time.setVisibility(View.GONE);
        } else {
            txt_appointment_time.setVisibility(View.VISIBLE);
        }

        if (model.getCar_problem().equals("")){
            txt_problem.setVisibility(View.GONE);
        }else {
            txt_problem.setVisibility(View.VISIBLE);
            txt_problem.setText(activity.getString(R.string.your_problem)+"  : "+(model.getCar_problem()));
        }

        if (model.getAppointment_type().equals("") || model.getAppointment_type().contains("null")){
            txt_appointment_type.setVisibility(View.GONE);
        }else {
            txt_appointment_type.setVisibility(View.VISIBLE);
            txt_appointment_type.setText(activity.getString(R.string.your_request)+"   : "+(model.getAppointment_type()));
        }

        if (model.getOil_brand().equals("")){
            txt_oil_brand.setVisibility(View.GONE);
        }else {
            txt_oil_brand.setVisibility(View.VISIBLE);
            txt_oil_brand.setText(activity.getString(R.string.oil_brand)+"   : "+model.getOil_brand());
        }

        if (model.getOil_sae().equals("")){
            txt_oil_sae.setVisibility(View.GONE);
        }else {
            txt_oil_sae.setVisibility(View.VISIBLE);
            txt_oil_sae.setText(activity.getString(R.string.oil_sae)+"   :" +model.getOil_sae());
        }

        if (model.getRims_brand().equals("")){
            txt_rims_brand.setVisibility(View.GONE);
        }else {
            txt_rims_brand.setVisibility(View.VISIBLE);
            txt_rims_brand.setText(activity.getString(R.string.oil_brand)+"  :" +model.getRims_brand());
        }

        if (model.getRims_size().equals("")){
            txt_rims_size.setVisibility(View.GONE);
        }else {
            txt_rims_size.setVisibility(View.VISIBLE);
            txt_rims_size.setText(activity.getString(R.string.rims_size)+"  :" +model.getRims_size());
        }

        if (model.getTire_brand().equals("")){
            txt_tire_brand.setVisibility(View.GONE);
        }else {
            txt_tire_brand.setVisibility(View.VISIBLE);
            txt_tire_brand.setText(activity.getString(R.string.tire_brand)+"  :" +model.getTire_brand());
        }

        if (model.getTire_size_number().equals("")){
            txt_tire_size.setVisibility(View.GONE);
        }else {
            txt_tire_size.setVisibility(View.VISIBLE);
            txt_tire_size.setText(activity.getString(R.string.tire_size)+"  :" +model.getTire_size_number());
        }

        if (model.getBattery_brand().equals("")){
            txt_battery_brand.setVisibility(View.GONE);
        }else {
            txt_battery_brand.setVisibility(View.VISIBLE);
            txt_battery_brand.setText(activity.getString(R.string.battery_brand)+"  :" +model.getBattery_brand());
        }

        llDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(activity, Act_Garage_Offer_List.class);
                in.putExtra("garage_offer", true);
                in.putExtra("request_id",model.getGarage_appointment_id());
                activity.startActivity(in);
            }
        });
    }

    @Override
    public int getItemCount() {
        return booking_models.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private TextView txt_name, txt_appointment_date, txt_problem, txt_appointment_time, txt_tire_size, txt_tire_brand,txt_oil_brand,
    txt_oil_sae, txt_rims_size, txt_rims_brand,txt_battery_brand, txt_appointment_type;
    private ImageView img_go_detail;
    private LinearLayout llDetail;

    class MyViewHolder extends RecyclerView.ViewHolder {
        private MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_appointment_date = itemView.findViewById(R.id.txt_appointment_date);
            txt_problem = itemView.findViewById(R.id.txt_problem);
            txt_appointment_time = itemView.findViewById(R.id.txt_appointment_time);
            llDetail = itemView.findViewById(R.id.llDetail);
            txt_tire_size = itemView.findViewById(R.id.txt_tire_size);
            txt_tire_brand = itemView.findViewById(R.id.txt_tire_brand);
            txt_rims_size = itemView.findViewById(R.id.txt_rims_size);
            txt_rims_brand = itemView.findViewById(R.id.txt_rims_brand);
            txt_oil_brand = itemView.findViewById(R.id.txt_oil_brand);
            txt_oil_sae = itemView.findViewById(R.id.txt_oil_sae);
            txt_battery_brand = itemView.findViewById(R.id.txt_battery_brand);
            txt_appointment_type = itemView.findViewById(R.id.txt_appointment_type);
//            txt_price = itemView.findViewById(R.id.txt_price);
//            img_go_detail = itemView.findViewById(R.id.img_go_detail);

        }
    }

    private String TimeFormat(String time){
        String aTime = "1";
        if (time.split(":").length > 1){
            int hour = Integer.parseInt(time.split(":")[0]);
            int minute = Integer.parseInt(time.split(":")[1]);
            int minutes = minute;
            String timeSet = "";
            if (hour > 12) {
                hour -= 12;
                timeSet = "PM";
            } else if (hour == 0) {
                hour += 12;
                timeSet = "AM";
            } else if (hour == 12){
                timeSet = "PM";
            }else{
                timeSet = "AM";
            }

            String min = "";
            if (minutes < 10)
                min = "0" + minutes ;
            else
                min = String.valueOf(minutes);
            Log.e("TAG AM PM", timeSet);
            Log.e("TAG Hour ", ""+hour);
            Log.e("TAG Minute", ""+min);

            aTime = new StringBuilder().append(hour).append(':')
                    .append(min ).append(" ").append(timeSet).toString();
            Log.e("TAG ATime", ""+aTime);
        }

        return aTime;
    }

    private String DateFormat(String Date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat newFormat = new SimpleDateFormat("dd, MMM yyyy", Locale.ENGLISH);
        java.util.Date date = null;
        try {
            date = format.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newFormat.format(date);

    }
}
