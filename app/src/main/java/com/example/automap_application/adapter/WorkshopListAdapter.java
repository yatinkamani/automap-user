package com.example.automap_application.adapter;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.automap_application.Model.Workshop.Workshop;
import com.example.automap_application.R;

import java.util.ArrayList;
import java.util.List;

public class WorkshopListAdapter extends RecyclerView.Adapter<WorkshopListAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<Workshop> workshopList;
    private customButtonListener customListener;

    public WorkshopListAdapter(Activity activity, ArrayList<Workshop> workshopList) {
        this.activity = activity;
        this.workshopList = workshopList;
    }

    public interface customButtonListener {
        public void onButtonClickListener(String workshop_id, String workshop_name);
    }

    public void setCustomButtonListener(customButtonListener listener) {
        this.customListener = listener;
    }

    @Override
    public int getItemCount() {
        return workshopList.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_workshop_list, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        Workshop workshop_model = workshopList.get(position);
        holder.workshop_name.setText(workshop_model.getWorkshop_name());
        Glide.with(activity).load(workshop_model.getWorkshop_banner()).error(R.drawable.amr_icon).apply(RequestOptions.skipMemoryCacheOf(true)).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)).dontAnimate().into(holder.workshop_banner);
        holder.workshop_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customListener != null){
                    customListener.onButtonClickListener(workshop_model.getWorkshop_id(),workshop_model.getWorkshop_name());

                    /*Intent in = new Intent(activity, Act_Workshop.class);
                    in.putExtra("workshop_id", workshop_model.getWorkshop_id());
                    in.putExtra("workshop_name", workshop_model.getWorkshop_name());
                    if(workshop_model.getWebsite() != null){
                        in.putExtra("website", workshop_model.getWebsite());
                    }
                    activity.startActivity(in);*/
                }
            }
        });
    }

    public void setFilter(List<Workshop> studentListModel) {
        workshopList = new ArrayList<>();
        workshopList.addAll(studentListModel);
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView workshop_name;
        ImageView workshop_banner;

        MyViewHolder(View itemView) {
            super(itemView);
            workshop_name=itemView.findViewById(R.id.garage_name);
            workshop_banner=itemView.findViewById(R.id.garage_banner);

        }
    }

}
