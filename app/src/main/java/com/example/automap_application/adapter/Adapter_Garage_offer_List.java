package com.example.automap_application.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.automap_application.Act_Garage;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.GarageNotificationModel;
import com.example.automap_application.R;
import com.example.automap_application.backgroundServices.LocationUpdateService;

import java.util.ArrayList;

public class Adapter_Garage_offer_List extends RecyclerView.Adapter<Adapter_Garage_offer_List.MyViewHolder> {

    private Activity activity;
    private ArrayList<GarageNotificationModel> booking_models;
    private Location location;

    public Adapter_Garage_offer_List(Activity activity, ArrayList<GarageNotificationModel> booking_models) {
        this.activity = activity;
        this.booking_models = booking_models;
        location = LocationUpdateService.locations;
    }

    public void setData(ArrayList<GarageNotificationModel> models){
        booking_models = new ArrayList<>();
        booking_models.addAll(models);
        notifyDataSetChanged();
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_garage_offer_list, viewGroup, false);

        return new MyViewHolder(view);
    }

    @SuppressLint({"SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        GarageNotificationModel model = booking_models.get(i);

        holder.txt_name.setText("  "+model.getName());
        holder.txt_city.setText(activity.getString(R.string.city2) +model.getCity());
        holder.txt_address.setText(activity.getString(R.string.Address)+"  :" +model.getAddress());
        if (!model.getPrice().equals("")){
            holder.txt_price.setVisibility(View.VISIBLE);
            holder.txt_price.setText(activity.getString(R.string.price)+"  :" +model.getPrice());
        }else {
            holder.txt_price.setVisibility(View.GONE);
        }
        if (!model.getTime().equals("")){
            holder.txt_working_hour.setVisibility(View.VISIBLE);
            holder.txt_working_hour.setText(activity.getString(R.string.estimate_time)+"  :" + (model.getTime())+" Hour");
        }else {
            holder.txt_working_hour.setVisibility(View.GONE);
        }

        if(model.getStatus().equals("3")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.you_have_select_this_service_provider));
        }else if (model.getStatus().equals("4")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.you_hav_not_select_this_service_provider));
        }else if (model.getStatus().equals("5")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.cancel_order));
        }else if (model.getStatus().equals("7")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.you_are_not_agree));
        }else if (model.getStatus().equals("9")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.service_provider_not_agree));
        }else if (model.getStatus().equals("8")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.work_in_progress));
        }else if (model.getStatus().equals("10")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.work_done));
        }else if (model.getStatus().equals("11")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.your_order_complete));
        }else if (model.getStatus().equals("12")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.your_order_complete));
        }else if (model.getStatus().equals("13")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.you_complained_to_this_service_provider));
        }else if (model.getStatus().equals("14")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.the_service_provider_complained_to_you));
        }else {
            holder.txt_status.setVisibility(View.GONE);
        }

        if (model.getUser_complain() != null && model.getUser_complain().equals("1")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.you_have_made_a_complaint_to_this_service_provider));
        }

        if (model.getGarage_complain() != null && model.getGarage_complain().equals("1")){
            holder.txt_status.setText(activity.getString(R.string.status)+"     : "+activity.getString(R.string.this_service_provider_complaind_again_you));
        }

        /*if (location != null){
            holder.txt_distance.setVisibility(View.VISIBLE);
            if (model.getLatitude() != null && model.getLongitude() != null){
                Location locationA = new Location("");
                locationA.setLatitude(Double.parseDouble(""+model.getLatitude()));
                locationA.setLongitude(Double.parseDouble(""+model.getLongitude()));

                Location locationB = new Location("");
                locationB.setLatitude(Double.parseDouble(""+location.getLatitude()));
                locationB.setLongitude(Double.parseDouble(""+location.getLongitude()));

                double mDistance = locationA.distanceTo(locationB) / 1000;
//                double mDistance = Double.parseDouble(""+AppUtils.distance(Float.parseFloat(""+location.getLatitude()),Float.parseFloat(""+location.getLongitude()),Float.parseFloat(model.getLatitude()),Float.parseFloat(model.getLongitude()))) / 1000;
                holder.txt_distance.setText("Distance : " + String.format("%.2f",mDistance)  + "KM");
            }else {
                holder.txt_distance.setVisibility(View.GONE);
            }
        }else {
            holder.txt_distance.setVisibility(View.GONE);
        }*/

        if (model.getDistance() != null && !model.getDistance().equals("")){
//            holder.txt_distance.setText("Distance : "+ String.format("%.2f",Double.parseDouble(model.getDistance())) +" KM");
            holder.txt_distance.setText(activity.getString(R.string.disatance)+" : "+ model.getDistance() +" KM");
            holder.txt_distance.setVisibility(View.VISIBLE);
        }else {
            holder.txt_distance.setVisibility(View.GONE);
        }

        Glide.with(activity).load(model.getUser_image()).into(holder.user_image);

        holder.llDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (model.getUser_complain() != null && model.getUser_complain().equals("1") ||
                    model.getGarage_complain() != null && model.getGarage_complain().equals("1")){
                    Toast.makeText(activity, activity.getString(R.string.the_service_is_terminated_due_to_complaint_each_other),Toast.LENGTH_LONG).show();
                }else if (model.getStatus().equals("4")){
                    Toast.makeText(activity, activity.getString(R.string.you_have_this_provider),Toast.LENGTH_LONG).show();
                } else if (model.getStatus().equals("5")){
                    Toast.makeText(activity, activity.getString(R.string.you_have_cancel_appointment),Toast.LENGTH_LONG).show();
                } else if (model.getStatus().equals("7")){
                    Toast.makeText(activity, activity.getString(R.string.you_are_not_aree_for_this_appointment),Toast.LENGTH_LONG).show();
                } else if (model.getStatus().equals("9")) {
                    Toast.makeText(activity, activity.getString(R.string.workshop_not_agree_for_this_appointment),Toast.LENGTH_LONG).show();
                } else if (model.getStatus().equals("13") || model.getStatus().equals("14")){
                    Toast.makeText(activity, activity.getString(R.string.complained_appointment),Toast.LENGTH_LONG).show();
                }  else{
                    Intent in = new Intent(activity, Act_Garage.class);
                    Constant.model = null;
                    Constant.model = new GarageNotificationModel();
                    Constant.model = model;
                    in.putExtra("garage_id", model.getGarage_id());
                    in.putExtra("garage_name", model.getName());
                    in.putExtra("garage_offer", true);
                    in.putExtra("request_id",model.getGarage_appointment_id());
                    activity.startActivity(in);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return booking_models.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_name, txt_city, txt_address, txt_working_hour, txt_price, txt_status, txt_distance;
        private ImageView user_image;
        private LinearLayout llDetail;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_name = itemView.findViewById(R.id.txt_name);
            user_image = itemView.findViewById(R.id.user_image);
            txt_city = itemView.findViewById(R.id.txt_city);
            txt_address = itemView.findViewById(R.id.txt_address);
            txt_price = itemView.findViewById(R.id.txt_price);
            txt_status = itemView.findViewById(R.id.txt_status);
            txt_working_hour = itemView.findViewById(R.id.txt_working_hour);
            txt_price = itemView.findViewById(R.id.txt_price);
            llDetail = itemView.findViewById(R.id.llDetail);
            txt_distance = itemView.findViewById(R.id.txt_distance);

        }
    }

}
