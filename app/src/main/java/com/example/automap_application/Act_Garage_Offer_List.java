package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;

import android.location.Location;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.GarageNotificationModel;
import com.example.automap_application.adapter.Adapter_Garage_offer_List;
import com.example.automap_application.backgroundServices.LocationService;
import com.example.automap_application.backgroundServices.LocationUpdateService;
import com.example.automap_application.utils.AppUtils;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Garage_Offer_List extends AppCompatActivity implements View.OnClickListener {

    ImageView back_garage_list,img_menu;
    RecyclerView rl_garage;
    String user_id = "", garage_appointment_id = "";
    ProgressDialog progressDialog;
    ArrayList<GarageNotificationModel> garage_list = new ArrayList<>();
    LinearLayout lin_bottom ;
    View no_data;
    TextView txt_msg;
    Button btn_retry;
    SharedPreferences preferences;
    Adapter_Garage_offer_List act_garage_offer_list;
    Location location = null;
    Boolean SortTime = false, SortPrice = false, SortDistance = false;

    @SuppressLint("StaticFieldLeak")
    public static Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__garage__offer__list);

        instance = this;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));

        rl_garage = (RecyclerView)findViewById(R.id.rl_garage);
        back_garage_list = (ImageView)findViewById(R.id.back_garage_list);
        img_menu = (ImageView)findViewById(R.id.img_menu);
        no_data = (View) findViewById(R.id.no_data);
        lin_bottom = (LinearLayout) findViewById(R.id.lin_bottom);
        txt_msg = (TextView) findViewById(R.id.txt_msg);
        btn_retry = (Button) findViewById(R.id.btn_retry);

        preferences = getSharedPreferences(LOGIN_PREF, MODE_PRIVATE);
        user_id = preferences.getString(getString(R.string.pref_user_id),"");
        location = LocationUpdateService.locations;

        btn_retry.setOnClickListener(this);
        back_garage_list.setOnClickListener(this);
        img_menu.setOnClickListener(this);
        img_menu.setVisibility(View.GONE);
        getIntentData();
        getGarageOffer();
        InitializeBroadcast();
    }

    // notification click to open this screen
    public void getIntentData(){

        Intent intent = getIntent();
        if (intent != null){
            garage_appointment_id = intent.getStringExtra("request_id");
        }
    }

    // get workshop offer list
    private void getGarageOffer() {
        img_menu.setVisibility(View.VISIBLE);
        if (AppUtils.isNetworkAvailable(getApplicationContext())){
            ProgressDialog dialog = new ProgressDialog(Act_Garage_Offer_List.this);
            dialog.setMessage(getString(R.string.loading));
            dialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL+"user_garage_list", new Response.Listener<String>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(String response) {
                    Log.e("notification_list"," " +response);
                    try {

                        JSONObject mainJsonObject = new JSONObject(response);
                        garage_list = new ArrayList<>();
                        if (mainJsonObject.getString("status").equals("1")){
                            JSONArray result = mainJsonObject.getJSONArray("result");
                            if (result.length()>0){
                                lin_bottom.setVisibility(View.VISIBLE);
                                no_data.setVisibility(View.GONE);
                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject jsonObject = result.getJSONObject(i);

                                    GarageNotificationModel notification_model = new GarageNotificationModel();
                                    notification_model.setGarage_appointment_id(jsonObject.getString("garage_appointment_id"));
                                    notification_model.setName(jsonObject.getString("garage_name"));
                                    notification_model.setGarage_id(jsonObject.getString("garage_id"));
                                    notification_model.setStatus(jsonObject.getString("status"));
                                    notification_model.setEmail(jsonObject.getString("email"));
                                    notification_model.setCity(jsonObject.getString("city_name"));
                                    notification_model.setAddress(jsonObject.getString("address"));
                                    notification_model.setContact_no(jsonObject.getString("contact_no"));
                                    notification_model.setPrice(jsonObject.optString("price",""));
                                    notification_model.setTime(jsonObject.optString("time",""));
                                    notification_model.setUser_image(jsonObject.getString("owner_profile_img"));
                                    notification_model.setAppointment_date(jsonObject.optString("appointment_date",""));
                                    notification_model.setAppointment_time(jsonObject.optString("appointment_time",""));

                                    if (jsonObject.has("user_complain")){
                                        notification_model.setUser_complain(jsonObject.getString("user_complain"));
                                    }

                                    if (jsonObject.has("garage_complain")){
                                        notification_model.setUser_complain(jsonObject.getString("garage_complain"));
                                    }
                                    if (jsonObject.getString("latitude") != null && !(jsonObject.getString("latitude").equalsIgnoreCase("null"))){
                                        if (0 < jsonObject.getString("latitude").length())
                                            notification_model.setLatitude(jsonObject.getString("latitude"));
                                        else
                                            notification_model.setLatitude("0.0");
                                    } else {
                                        notification_model.setLatitude("0.0");
                                    }

                                    if (jsonObject.getString("longitude") != null && !(jsonObject.getString("longitude").equalsIgnoreCase("null"))){
                                        if (0 < jsonObject.getString("longitude").length())
                                            notification_model.setLongitude(jsonObject.getString("longitude"));
                                        else
                                            notification_model.setLongitude("0.0");
                                    } else {
                                        notification_model.setLongitude("0.0");
                                    }

                                    if (location != null){
                                        if (notification_model.getLatitude() != null && notification_model.getLongitude() != null){
                                            Location locationA = new Location("");
                                            locationA.setLatitude(Double.parseDouble(""+notification_model.getLatitude()));
                                            locationA.setLongitude(Double.parseDouble(""+notification_model.getLongitude()));

                                            Location locationB = new Location("");
                                            locationB.setLatitude(Double.parseDouble(""+location.getLatitude()));
                                            locationB.setLongitude(Double.parseDouble(""+location.getLongitude()));

                                            double mDistance = locationA.distanceTo(locationB) / 1000;
                                            notification_model.setDistance(String.format("%.2f",mDistance));
                                        }else {
                                            notification_model.setDistance("");
                                        }
                                    } else {
                                        notification_model.setDistance("");
                                    }

                                    Log.e("TAG",jsonObject.getString("status"));
                                    if (jsonObject.getString("status").equals("2") || jsonObject.getString("status").equals("0")){

                                    }else {
                                        garage_list.add(notification_model);
                                        Log.e("TAG IND",jsonObject.getString("status"));
                                    }
                                }
                                if (garage_list.size() > 0){
                                    if (act_garage_offer_list == null){

                                        act_garage_offer_list = new Adapter_Garage_offer_List(Act_Garage_Offer_List.this, garage_list);
                                        rl_garage.setAdapter(act_garage_offer_list);
                                        rl_garage.setLayoutManager(new LinearLayoutManager(Act_Garage_Offer_List.this));

                                        if (garage_list.size()>1){
                                            img_menu.setVisibility(View.VISIBLE);
                                        }else {
                                            img_menu.setVisibility(View.GONE);
                                        }

                                    } else {

                                        if (SortPrice){
                                            SortPrice();
                                        } else if (SortTime){
                                            SortTime();
                                        } else if (SortDistance && location != null){
                                            SortTime();
                                        } else {
                                            act_garage_offer_list.setData(garage_list);
                                        }
                                    }
                                    lin_bottom.setVisibility(View.VISIBLE);
                                    no_data.setVisibility(View.GONE);
                                    img_menu.setVisibility(View.VISIBLE);
                                }else {
                                    lin_bottom.setVisibility(View.GONE);
                                    no_data.setVisibility(View.VISIBLE);
                                    btn_retry.setVisibility(View.GONE);
                                    txt_msg.setText(getString(R.string.no_garage_appointment_found));
                                }
                            }else {
                                lin_bottom.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                                btn_retry.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_garage_appointment_found));
                            }
                        }else {
                            lin_bottom.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                            btn_retry.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.somting_wrong_please));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        lin_bottom.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                    }

                    dialog.dismiss();
                }
            },  new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    lin_bottom.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            }){
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params=new HashMap<String, String>();
                    params.put("user_id",user_id);
                    params.put("garage_appointment_id",garage_appointment_id);
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }else {

            lin_bottom.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.internet_not_connect));
        }
    }

    // new offfer come to refress api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (getQuotes_badge != 0 && !(Act_Garage_Offer_List.this.isFinishing())){
                    getGarageOffer();
                }

                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);
            }
        }
    };

    public void InitializeBroadcast(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onResume() {
        getGarageOffer();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry){
            getGarageOffer();
        } else if (v == back_garage_list){
            onBackPressed();
        }
        // offers filter popup menu
        else if (v == img_menu){
            PopupMenu menu = new PopupMenu(this, img_menu);
            menu.getMenuInflater().inflate(R.menu.sort_menu, menu.getMenu());
            menu.getMenu().findItem(R.id.price).setTitle("Sort by Price");
            menu.getMenu().findItem(R.id.time).setTitle("Sort by Time");
            if (location != null){
                menu.getMenu().findItem(R.id.distance).setTitle("Sort by Distance");
                menu.getMenu().findItem(R.id.distance).setVisible(true);
            } else {
                menu.getMenu().findItem(R.id.distance).setVisible(false);
            }

            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if (menuItem.getItemId() == R.id.price){
                        SortPrice();
                        SortPrice = true;
                        SortTime = false;
                        SortDistance = false;

                    } else if (menuItem.getItemId() == R.id.time){
                        SortTime();
                        SortPrice = false;
                        SortDistance = false;
                        SortTime = true;

                    } else if (menuItem.getItemId() == R.id.distance) {
                        SortDistance();
                        SortDistance = true;
                        SortPrice = false;
                        SortTime = false;
                    }
                    return true;
                }
            });
            menu.show();
        }
    }

    public void SortPrice() {

        Collections.sort(garage_list, new Comparator<GarageNotificationModel>() {
            @Override
            public int compare(GarageNotificationModel t0, GarageNotificationModel t1) {

                String tt1 = t0.getPrice().toString().replaceAll("\\D+","");
                String tt2 = t1.getPrice().toString().replaceAll("\\D+","");
                if (tt1.equals("")){
                    tt1 = "-1" ;
                }else {
                    tt1 = tt1;
                }

                if (tt2.equals("")){
                    tt2 = "-1" ;
                }else {
                    tt2 = tt2;
                }

                if (Integer.parseInt(tt1) > Integer.parseInt(tt2)){
                    return 1;
                } else if (Integer.parseInt(tt1) < Integer.parseInt(tt2)){
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        if (act_garage_offer_list != null){
            act_garage_offer_list.setData(garage_list);
        }
    }

    public void SortTime() {

        Collections.sort(garage_list, new Comparator<GarageNotificationModel>() {
            @Override
            public int compare(GarageNotificationModel t0, GarageNotificationModel t1) {

                String tt1 = t0.getTime().toString().replaceAll("\\D+","");
                String tt2 = t1.getTime().toString().replaceAll("\\D+","");
                Log.e("Tag Sort 1",tt1);
                Log.e("Tag Sort 2",tt2);
                if (tt1.equals("")){
                    tt1 = "-1" ;
                }else {
                    tt1 = tt1;
                }

                if (tt2.equals("")){
                    tt2 = "-1" ;
                }else {
                    tt2 = tt2;
                }

                if (Integer.parseInt(tt1) > Integer.parseInt(tt2)){
                    return 1;
                } else if (Integer.parseInt(tt1) < Integer.parseInt(tt2)){
                    return -1;
                } else {
                    return 0;
                }

            }
        });
        if (act_garage_offer_list != null){
            act_garage_offer_list.setData(garage_list);
        }
    }

    public void SortDistance() {

        Collections.sort(garage_list, new Comparator<GarageNotificationModel>() {
            @Override
            public int compare(GarageNotificationModel t0, GarageNotificationModel t1) {

                String tt1 = t0.getDistance().toString().replaceAll("\\D+","")/*.substring(0,t0.getDistance().indexOf("."))*/;
                String tt2 = t1.getDistance().toString().replaceAll("\\D+","")/*.substring(0,t1.getDistance().indexOf("."))*/;
                Log.e("Tag Sort 1",tt1);
                Log.e("Tag Sort 2",tt2);
                if (tt1.equals("")){
                    tt1 = "-1" ;
                }else {
                    tt1 = tt1;
                }

                if (tt2.equals("")){
                    tt2 = "-1" ;
                }else {
                    tt2 = tt2;
                }

                if (Float.parseFloat(tt1) > Float.parseFloat(tt2)){
                    return 1;
                } else if (Float.parseFloat(tt1) < Float.parseFloat(tt2)){
                    return -1;
                } else {
                    return 0;
                }

            }
        });
        if (act_garage_offer_list != null){
            act_garage_offer_list.setData(garage_list);
        }
    }
}
