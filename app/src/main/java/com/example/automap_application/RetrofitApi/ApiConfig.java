package com.example.automap_application.RetrofitApi;

import com.example.automap_application.RetrofitApi.UpdateCarDetail.CarUpdateResponse;
import com.example.automap_application.RetrofitApi.UpdateProfile.ServerResponse;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiConfig {

    //  Update Profile //
    @Multipart
    @POST("user_profile_update")
    Call<ServerResponse> UpdateProfile(@Part("user_id") RequestBody user_id,
                                       @Part("name") RequestBody name,
                                       @Part("email") RequestBody email,
                                       @Part("contact_no") RequestBody contact_no,
                                       @Part("address") RequestBody address,
                                       @Part MultipartBody.Part file
//                                       @Part("country_id") RequestBody country_id,
//                                       @Part("state") RequestBody state,
//                                       @Part("city") RequestBody city,
//                                       @Part("pincode") RequestBody pincode,
//                                       @Part("latitude") RequestBody latitude,
//                                       @Part("longitude") RequestBody longitude,
    );

    @FormUrlEncoded
    @POST("user_profile_update")
    Call<ServerResponse> UpdateLocation(@Field("user_id") String user_id,
                                       @Field("name") String name,
                                       @Field("email") String email,
                                       @Field("contact_no") String contact_no,
                                       @Field("address") String address,
                                       @Field("latitude") String latitude,
                                       @Field("longitude") String longitude
    );

    // car Detail Update //

    @Multipart
    @POST("edit_car_detail")
    Call<CarUpdateResponse> CAR_UPDATE_RESPONSE_CALL(@Part("car_id") RequestBody car_id,
//                                                     @Part("brand_id") RequestBody brand_id,
//                                                     @Part("model_id") RequestBody model_id,
                                                     @Part("variant_id") RequestBody variant_id,
                                                     @Part("fule_id") RequestBody fule_id,
                                                     @Part("transmission_id") RequestBody transmission_id,
                                                     @Part("van_no") RequestBody van_no,
                                                     @Part("registration_date") RequestBody registration_date,
                                                     @Part("speedometer_id") RequestBody speedometer_id,
                                                     @Part MultipartBody.Part[] file);

    @FormUrlEncoded
    @POST("user_login")
    Call<ResponseBody> CALL (@Field("email") String email,
                             @Field("password") String password,
                             @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST("check_user_status")
    Call<ResponseBody> BODY_CALL(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("get_car_detail")
    Call<ResponseBody> CAR_DETAIL(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("user_forgotpassword")
    Call<Object> FORGET_PASSWORD(@FieldMap Map<String,String> map);
}
