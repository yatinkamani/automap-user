package com.example.automap_application.RetrofitApi.UpdateCarDetail;

public class CarUpdateResponse {

    private String status;
    private String message;
    private Object result;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "CarUpdateResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", result=" + result +
                '}';
    }
}
