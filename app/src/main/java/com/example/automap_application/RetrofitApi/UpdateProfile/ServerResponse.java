package com.example.automap_application.RetrofitApi.UpdateProfile;

import com.google.gson.annotations.SerializedName;

public class ServerResponse {

    private String status;

    private String message;

    private Result result;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Result getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ServerResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", result=" + result +
                '}';
    }
}
