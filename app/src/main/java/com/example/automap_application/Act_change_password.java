package com.example.automap_application;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.extra.API;
import com.example.automap_application.extra.APIResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Act_change_password extends AppCompatActivity {

    ImageView imgBack;
    SharedPreferences sharedpreferences;
    EditText password, confirm_password, ED_OldPwd;
    TextView TV_forgot;
    Button btn_change;
    String ed_pass, ed_c_pass, ed_otp;

    String status;
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_change_password);

        imgBack = findViewById(R.id.imgBack);
        password = findViewById(R.id.ed_change_password);
        confirm_password = findViewById(R.id.ed_Confirm_change_password);
        ED_OldPwd = findViewById(R.id.ED_OldPwd);
        TV_forgot = findViewById(R.id.TV_forgot);

        btn_change = findViewById(R.id.btn_change);

        imgBack.setOnClickListener(view -> {onBackPressed();});
        TV_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Act_change_password.this, Act_forgot_password.class);
                startActivity(intent1);
            }
        });

        // check validation and change password
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedpreferences = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);
//                String st_otp = sharedPreferences.getString(getString(R.string.pref_user_pwd), "");
                String id = sharedpreferences.getString(getString(R.string.pref_user_id), "");
                Log.e("ID", ""+id);
//                Log.e("OTP", ""+st_otp);
                ed_otp = ED_OldPwd.getText().toString();
                ed_pass = password.getText().toString();
                ed_c_pass = confirm_password.getText().toString();

                if (TextUtils.isEmpty(ED_OldPwd.getText().toString())) {
                    ED_OldPwd.setError(getString(R.string.enter_current_password));
                } else if (TextUtils.isEmpty(password.getText().toString())) {
                    password.setError(getString(R.string.enter_password));
                } else if (TextUtils.isEmpty(confirm_password.getText().toString())) {
                    confirm_password.setError(getString(R.string.confirm_password));
                } else if (password.getText().toString().length() < 6) {
                    password.setError(getString(R.string.please_enter6digit_password));
                } else if (!password.getText().toString().trim().equalsIgnoreCase(confirm_password.getText().toString().trim())) {
                    confirm_password.setError(getString(R.string.does_not_match_password));
                } else {

                    API api = new API(Act_change_password.this, new APIResponse() {
                        @Override
                        public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                            try {
                                JSONObject obj = new JSONObject(response);
                                status = obj.getString("status");
                                message = obj.getString("message");
                                Toast.makeText(Act_change_password.this, message, Toast.LENGTH_SHORT).show();
                                if (status.equals("true")){
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onAPIError(int requestCode, boolean isError, String error) {
                            Toast.makeText(Act_change_password.this, getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                            Log.i("My error", "" + error);
                        }
                    });
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("user_id", id);
                    map.put("password", ed_pass);
                    map.put("old_password", ED_OldPwd.getText().toString());
                    api.execute(100,Constant.URL+"user_change_password", map, true);

                }
            }
        });
    }
}



