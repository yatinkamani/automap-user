package com.example.automap_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.backgroundServices.RemainderBroadcast;
import com.example.automap_application.utils.AppUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class Act_Mobile_Request_Workshop_Detail extends AppCompatActivity implements View.OnClickListener {

    ImageView back;
    TextView title;
    ScrollView scroll_view;
    ImageView img_banner;
    TextView txt_address, phone_no, txt_waiting_time, txt_distance, txt_rate;
    CardView card_call, card_confirm_order, card_back, card_track, card_rate, card_complain;
    private FusedLocationProviderClient mFusedLocationClient;
    View no_data_error;
    TextView txt_msg;
    Button btn_retry;
    LinearLayout lin_rate;
    RatingBar ratingBar;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String address = "", contact_no = "", garage_name = "", garage_banner = "", latitude = "", longitude = "", status = "";
    String garage_id = "", request_id = "", user_id = "", provider_lat = "", provider_lng = "", rating = "", request_date = "", request_time = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__mobile__request__workshop__detail);

        back = findViewById(R.id.back);
        title = findViewById(R.id.title);
        scroll_view = findViewById(R.id.scroll_view);
        img_banner = findViewById(R.id.img_banner);
        txt_address = findViewById(R.id.txt_address);
        phone_no = findViewById(R.id.phone_no);
        txt_waiting_time = findViewById(R.id.txt_waiting_time);
        txt_distance = findViewById(R.id.txt_distance);
        card_call = findViewById(R.id.card_call);
        card_confirm_order = findViewById(R.id.card_confirm_order);
        card_back = findViewById(R.id.card_back);
        card_track = findViewById(R.id.card_track);
        card_rate = findViewById(R.id.card_rate);
        card_complain = findViewById(R.id.card_complain);
        no_data_error = findViewById(R.id.no_data_error);
        txt_msg = findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);

        lin_rate = findViewById(R.id.lin_rate);
        ratingBar = findViewById(R.id.ratingBar);
        txt_rate = findViewById(R.id.txt_rate);

        card_call.setOnClickListener(this);
        card_confirm_order.setOnClickListener(this);
        card_back.setOnClickListener(this);
        card_track.setOnClickListener(this);
        card_rate.setOnClickListener(this);
        btn_retry.setOnClickListener(this);

        pref = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        editor = pref.edit();

        user_id = pref.getString(getString(R.string.pref_user_id), "");
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        back.setOnClickListener(this);
        getIntentData();

        getGarageInfo();
//        getLastLocation();
        getStatus();
        InitializeBroadcast();
    }

    public void InitializeBroadcast() {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(Act_Mobile_Request_Workshop_Detail.this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }

    // new offer come to refress api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (mobile_request_badge > 0 && !(Act_Mobile_Request_Workshop_Detail.this.isFinishing())) {
                    getStatus();
                }
                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);

            }
        }
    };

    public void getIntentData() {
        garage_id = getIntent().getStringExtra(getString(R.string.pref_garage_id));
        request_id = getIntent().getStringExtra(getString(R.string.pref_request_id));
        garage_name = getIntent().getStringExtra(getString(R.string.name));
        title.setText(garage_name);

    }

    @Override
    public void onClick(View view) {

        if (view == card_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_no));
            startActivity(intent);
        } else if (view == card_back) {
            onBackPressed();
        } else if (view == card_confirm_order) {
            timer_dialog();
        } else if (view == card_track) {
            Intent intent = new Intent(getApplicationContext(), Act_Map.class);
            intent.putExtra(getString(R.string.pref_request_id), request_id);
            intent.putExtra("garage_name", garage_name);
            intent.putExtra(getString(R.string.pref_garage_id), garage_id);
            intent.putExtra("notificationLatitude", provider_lat);
            intent.putExtra("notificationLongitude", provider_lng);
            intent.putExtra(getString(R.string.contact_no), contact_no);
            intent.putExtra(getString(R.string.pref_user_id), user_id);
            startActivity(intent);
        } else if (view == card_rate) {
            VerifyDialog();
        } else if (view == card_complain) {
//            Toast.makeText(getApplicationContext(), "no complain", Toast.LENGTH_LONG).show();
            ComplainDialog();
        } else if (view == back) {
            onBackPressed();
        } else if (view == btn_retry) {
            getGarageInfo();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    // get location data to distance find
    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (AppUtils.checkGPSPermissions(this)) {
            if (AppUtils.isLocationEnabled(getApplicationContext())) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    latitude = String.valueOf(location.getLatitude());
                                    longitude = String.valueOf(location.getLongitude());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } else {
            AppUtils.requestGPSPermissions(this);
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            latitude = String.valueOf(mLastLocation.getLatitude());
            longitude = String.valueOf(mLastLocation.getLongitude());
            getDistanceInfo();
            Log.e("Tag Location", mLastLocation.toString());
        }
    };

    // get garage detail set view
    private void getGarageInfo() {

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "check_garage_status",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("status_response", " " + response);
                        progressDialog.dismiss();
                        try {
                            JSONObject mainJsonObject = new JSONObject(response);
                            if (mainJsonObject.getString("status").equals("true")) {
                                JSONArray result = mainJsonObject.getJSONArray("result");
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject jsonObject = result.getJSONObject(i);
                                    contact_no = jsonObject.getString("contact_no");
                                    garage_name = jsonObject.getString("garage_name");
                                    garage_banner = jsonObject.getString("garage_banner");
                                    address = jsonObject.getString("address");
                                    provider_lat = jsonObject.optString("latitude", "0.00");
                                    provider_lng = jsonObject.optString("longitude", "0.00");
                                    if (jsonObject.has("ratting")) {
                                        rating = jsonObject.getString("ratting");
                                    }
                                }

                                if (!address.equals("")) {
                                    String s = address.split(",")[0];
                                    txt_address.setText(s + ", \n" + address.replaceAll(s + ",", ""));
                                } else {
                                    txt_address.setText(address);
                                }

                                if (!rating.equals("") || !rating.isEmpty()) {
                                    lin_rate.setVisibility(View.VISIBLE);
                                    txt_rate.setText(Html.fromHtml(rating));
                                    ratingBar.setRating(Float.parseFloat(rating));
                                } else {
                                    lin_rate.setVisibility(View.GONE);
                                }

                                phone_no.setText(contact_no);
                                Glide.with(getApplicationContext()).asBitmap().load(garage_banner)
                                        .placeholder(getResources().getDrawable(R.drawable.amr_icon))
                                        .thumbnail(0.01f)
                                        .into(new BitmapImageViewTarget(img_banner) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                img_banner.setImageBitmap(resource);
                                            }
                                        });
                                if (result.length() > 0) {
                                    no_data_error.setVisibility(View.GONE);
                                    scroll_view.setVisibility(View.VISIBLE);

                                } else {
                                    no_data_error.setVisibility(View.VISIBLE);
                                    scroll_view.setVisibility(View.GONE);
                                    txt_msg.setText(getString(R.string.no_detail_found));
                                    btn_retry.setVisibility(View.GONE);
                                }

                                getStatus();
                            } else {
                                no_data_error.setVisibility(View.VISIBLE);
                                scroll_view.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_detail_found));
                                btn_retry.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            no_data_error.setVisibility(View.VISIBLE);
                            scroll_view.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.somting_wrong_please));
                            btn_retry.setVisibility(View.VISIBLE);
                        }
//                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
                no_data_error.setVisibility(View.VISIBLE);
                scroll_view.setVisibility(View.GONE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("garage_id", garage_id);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    // get mobile request status
    private void getStatus() {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "mobile_request_detail",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    JSONArray result = mainJsonObject.getJSONArray("result");
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject jsonObject = result.getJSONObject(i);
                                        status = jsonObject.getString("status");
                                        request_date = jsonObject.getString("request_date");
                                        request_time = jsonObject.getString("request_time");

                                        if (!jsonObject.getString("garage_latitude").equals("")) {
                                            provider_lat = jsonObject.optString("garage_latitude", "0.00");
                                            provider_lng = jsonObject.optString("garage_longitude", "0.00");
                                        }
                                    }

                                    // workshop participate
                                    if (status.equalsIgnoreCase("1")) {
                                        card_confirm_order.setVisibility(View.VISIBLE);
                                        card_track.setVisibility(View.GONE);
                                        card_complain.setVisibility(View.GONE);
                                    }
                                    // customer confirm
                                    else if (status.equalsIgnoreCase("3")) {
                                        card_track.setVisibility(View.VISIBLE);
                                        card_confirm_order.setVisibility(View.GONE);
                                        card_complain.setVisibility(View.VISIBLE);
                                        card_rate.setVisibility(View.VISIBLE);
                                    }
                                    // costomer declain
                                    else if (status.equalsIgnoreCase("4")) {
                                        card_rate.setVisibility(View.GONE);
                                        card_confirm_order.setVisibility(View.GONE);
                                        card_complain.setVisibility(View.VISIBLE);
                                    }
                                    // work done
                                    else if (status.equalsIgnoreCase("5")) {
                                        card_rate.setVisibility(View.GONE);
                                        card_track.setVisibility(View.GONE);
                                        card_confirm_order.setVisibility(View.GONE);
                                    }

                                    getDistanceInfo();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile_request_id", request_id);
                    params.put("garage_id", garage_id);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    // confirm mobile request offers
    private void changeStatus(String _status) {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "change_mobile_request_status",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.confirm_the_request), Toast.LENGTH_LONG).show();
                                    getStatus();
                                    NotificationRemainder(request_date + " " + request_time, Integer.parseInt(request_id));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile_request_id", request_id);
                    params.put("status", "3");
                    params.put("garage_id", garage_id);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    // set reminder before 5 hours
    public void NotificationRemainder(String datetime, int id) {
        Date Date = new Date();
        Date CurDate = new Date();
        try {
            Date = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH).parse(datetime);
            CurDate = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH).parse(new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH).format(new Date()));

            if (Date.after(CurDate) && AppUtils.printDifference(CurDate, Date, "h") > 5) {

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Date.getTime());
                calendar.add(Calendar.HOUR, -2);

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTimeInMillis(Date.getTime());

                Log.e("Tag Date 1", new SimpleDateFormat("hh:mm aa", Locale.ENGLISH).format(calendar1.getTime()));
                Log.e("Tag Date 2", new SimpleDateFormat("hh:mm aa", Locale.ENGLISH).format(calendar.getTime()));
                Intent intent = new Intent(getApplicationContext(), RemainderBroadcast.class);

                String msg = "Your Mobile Request Time 2 hour Remaining \n Time : " + new SimpleDateFormat("hh:mm aa", Locale.ENGLISH).format(calendar1.getTime());
                intent.putExtra("title", "Auto Mobile Request");
                intent.putExtra("messageFor", "mobile_request");
                intent.putExtra("msg", msg);
                intent.putExtra("id", id);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), new Random().nextInt(), intent, Intent.FLAG_ACTIVITY_CLEAR_TOP);
                AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                Log.e("Tag Date Time", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa", Locale.ENGLISH).format(calendar.getTime()));
                manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ratting workshop base on work
    private void ratting(String _status, String ratting) {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "user_ratting",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    Intent intent = new Intent(getApplicationContext(), Act_Service_book.class);
                                    if (new Act_Mobile_Request_Offers_List().instance != null) {
                                        new Act_Mobile_Request_Offers_List().instance.finish();
                                    }

                                    if (new Act_Mobile_Request_List().instance != null) {
                                        new Act_Mobile_Request_List().instance.finish();
                                    }
                                    finish();
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile_request_id", request_id);
                    params.put("status", _status);
                    params.put("garage_id", garage_id);
                    params.put("user_id", user_id);
                    params.put("ratting", ratting);
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    int count = 10;
    Handler handler;
    Runnable runnable;

    // timer set 10 count after confirm request
    @SuppressLint("SetTextI18n")
    public void timer_dialog() {
        count = 10;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);
        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        TextView txt_message = view.findViewById(R.id.txt_message);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText(getString(R.string.confirm_the_request));
        txt_message.setVisibility(View.VISIBLE);
        txt_message.setGravity(Gravity.CENTER);
        txt_message.setText("" + count);
        txt_message.setTextSize(20);
        txt_message.setTextColor(Color.RED);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                count--;
                txt_message.setText(String.valueOf(count));
                btn_ok.setText(getString(R.string.confirm) + "(" + count + ")");
                if (count == 0) {
                    dialog.dismiss();
                    changeStatus("3");
                } else {
                    handler.postDelayed(runnable, 1000);
                }
            }
        };

//        btn_ok.setText(getString(R.string.confirm));
        handler.postDelayed(runnable, 1000);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                changeStatus("3");
                dialog.dismiss();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                changeStatus("4");
                handler.removeCallbacks(runnable);
                dialog.dismiss();
            }
        });
    }

    float ratings = 0;

    public void RattingDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        TextView txt_message = view.findViewById(R.id.txt_message);
        RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText(getString(R.string.please_rate_the_service));
        btn_ok.setText(getString(R.string.yess));
        btn_cancel.setText(getString(R.string.nos));
        btn_cancel.setVisibility(View.GONE);
        ratingBar.setVisibility(View.VISIBLE);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2)
                .setColorFilter(getResources().getColor(R.color.black),
                        PorterDuff.Mode.SRC_ATOP); // for filled stars
        stars.getDrawable(1)
                .setColorFilter(getResources().getColor(R.color.black),
                        PorterDuff.Mode.SRC_ATOP); // for half filled stars
        stars.getDrawable(0)
                .setColorFilter(getResources().getColor(R.color.gray),
                        PorterDuff.Mode.SRC_ATOP); // for empty stars
        txt_message.setVisibility(View.VISIBLE);
        txt_message.setText(getString(R.string.ratting_service_time));

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (v == 1) {
                    Toast.makeText(getApplicationContext(), "Bad", Toast.LENGTH_LONG).show();
                } else if (v == 2) {
                    Toast.makeText(getApplicationContext(), "Bad Good", Toast.LENGTH_LONG).show();
                } else if (v == 3) {
                    Toast.makeText(getApplicationContext(), "Good", Toast.LENGTH_LONG).show();
                } else if (v == 4) {
                    Toast.makeText(getApplicationContext(), "Excellent Good", Toast.LENGTH_LONG).show();
                } else if (v == 5) {
                    Toast.makeText(getApplicationContext(), "Excellent", Toast.LENGTH_LONG).show();
                }
                ratings = v;
            }
        });

        btn_ok.setOnClickListener(v -> {
            if (ratings != 0) {
                ratting("5", "" + ratings);
                dialog.dismiss();
            } else {
                Toast.makeText(getApplicationContext(), "Please rate the Service", Toast.LENGTH_LONG).show();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "will be notified after 24 hours", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == AppUtils.PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            } else {
                Toast.makeText(getApplicationContext(), "Please Grant Permission Use Features", Toast.LENGTH_LONG).show();
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppUtils.checkGPSPermissions(this)) {
            getLastLocation();
        }
    }

    // get original distance of google api
    private void getDistanceInfo() {
        final StringBuilder[] stringBuilder = {new StringBuilder()};

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latitude + "," + longitude + "&destination=" + provider_lat + "," + provider_lng + "&mode=driving&sensor=false&key=" + getString(R.string.google_maps_key);
                    Log.e("Tag Url", url);
                    HttpPost http_post = new HttpPost(url);
                    HttpClient client = new DefaultHttpClient();
                    HttpResponse response;
                    stringBuilder[0] = new StringBuilder();
                    response = client.execute(http_post);
                    HttpEntity entity = (response.getEntity());
                    InputStream stream = entity.getContent();
                    int b;
                    while ((b = stream.read()) != -1) {
                        stringBuilder[0].append((char) b);
                    }
                    Log.e("Tag Daat", "" + stringBuilder[0]);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        String dura = "0.0";
                        String dist = "0.0";

                        jsonObject = new JSONObject(stringBuilder[0].toString());

                        if (jsonObject.getString("status").equals("ZERO_RESULTS")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    txt_distance.setText(getString(R.string.no_get_distance));
                                    txt_waiting_time.setText(getString(R.string.not_found_timming));
                                }
                            });

                        } else if (jsonObject.getString("status").equals("OVER_QUERY_LIMIT")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    txt_distance.setText("Fail");
                                    txt_waiting_time.setText("Fail");
                                }
                            });

                        } else {

                            JSONArray array = jsonObject.getJSONArray("routes");
                            JSONObject routes = array.getJSONObject(0);
                            JSONArray legs = routes.getJSONArray("legs");
                            JSONObject steps = legs.getJSONObject(0);
                            JSONObject distance = steps.getJSONObject("distance");
                            JSONObject duration = steps.getJSONObject("duration");
                            Log.i("Distance", distance.toString());
                            Log.i("Duration", duration.toString());
                            dist = (distance.getString("text"));
                            dura = (duration.getString("text"));

                            String finalDist = dist;
                            String finalDura = dura;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    txt_distance.setText(String.valueOf(finalDist));
                                    txt_waiting_time.setText(String.valueOf(finalDura));
                                }
                            });
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void AlertDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Mobile_Request_Workshop_Detail.this);
        builder.setMessage(msg);
        builder.setTitle("Message");

        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // verify complete work and redirect service book screen
    private void VerifyDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Mobile_Request_Workshop_Detail.this);
        builder.setTitle(getString(R.string.confirm));
        builder.setMessage(R.string.comapete_work_or_servie_service_book);

        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                RattingDialog();
            }
        });

        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void ComplainDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Mobile_Request_Workshop_Detail.this);
        View view = LayoutInflater.from(Act_Mobile_Request_Workshop_Detail.this).inflate(R.layout.dialog_complain, null, false);
        EditText ed_complain = view.findViewById(R.id.ed_complain);
        Button btn_complain = view.findViewById(R.id.btn_complain);
        builder.setView(view);

        AlertDialog dialog = builder.create();
        dialog.show();

        btn_complain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed_complain.getText().toString().equals("")) {
                    ed_complain.requestFocus();
                    ed_complain.setError(getString(R.string.enter_detial));
                } else {
                    StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "user_garage_complain", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Tag Response", response);

                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("1")) {
                                    dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Tag error", error.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("garage_id", garage_id);
                            map.put("user_id", user_id);
                            map.put("mobile_request_id", request_id);
                            map.put("status", "7");
                            map.put("complain", ed_complain.getText().toString());
                            Log.e("Tag Params", map.toString());
                            return map;
                        }
                    };

                    RequestQueue queue = Volley.newRequestQueue(Act_Mobile_Request_Workshop_Detail.this);
                    queue.add(request);
                }
            }
        });
    }

}
