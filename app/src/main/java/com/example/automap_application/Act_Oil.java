package com.example.automap_application;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class Act_Oil extends AppCompatActivity {

    LinearLayout c_engine_oil, c_gear_oil, c_break_oil, c_power_oil;
    ImageView back;
    ArrayList<String> object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__oil);

        back = findViewById(R.id.back_oil);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<String>) args.getSerializable("ARRAYLIST");

        c_engine_oil = findViewById(R.id.card_engine_oil_services);
        c_gear_oil = findViewById(R.id.card_gear_oil_service);
        c_break_oil = findViewById(R.id.card_break_oil_service);
        c_power_oil = findViewById(R.id.card_power_of_service);

        c_engine_oil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (object != null && object.size() > 0) {

                    String string = object.get(0);
                    Intent intent = new Intent(Act_Oil.this, Act_Engine_oil_service.class);
                    intent.putExtra("string", string);
                    startActivity(intent);
                }
            }
        });

        c_gear_oil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (object != null && object.size() > 0) {
                    String string = object.get(1);
                    Intent intent = new Intent(Act_Oil.this, Act_Gear_oil_service.class);
                    intent.putExtra("string", string);
                    startActivity(intent);

                }
            }
        });

        c_break_oil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (object != null && object.size() > 0) {
                    String string = object.get(2);
                    Intent intent = new Intent(Act_Oil.this, Act_Breake_oil_service.class);
                    intent.putExtra("string", string);
                    startActivity(intent);
                }
            }
        });

        c_power_oil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (object != null && object.size() > 0) {
                    String string = object.get(3);
                    Intent intent = new Intent(Act_Oil.this, Act_Power_oil_service.class);
                    intent.putExtra("string", string);
                    startActivity(intent);
                }
            }
        });
    }
}
