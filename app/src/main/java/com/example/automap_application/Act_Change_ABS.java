package com.example.automap_application;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.backgroundServices.RemainderBroadcast;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class Act_Change_ABS extends AppCompatActivity {

    String enum_id;
    ImageView back;
    EditText ED_Total_date, ED_Total_time;
    private int mHour, mMinute, mSec;
    String date;

    LinearLayout ll_card_ok;
    String car_id, date_time, str_paid_amount, str_gurantee_days;
    SharedPreferences prefs;
    EditText ED_grantee_days, ED_paid_amount;
//    String url = "http://webmobdemo.xyz/automap/api/add_services";

    String currency;
    TextView txt_currency;
    LinearLayout ll_date, ll_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__change__abs);

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);
        car_id = prefs.getString("CAR_ID", " ");

        Bundle bundle = getIntent().getExtras();
        enum_id = bundle.getString("string");

        back = findViewById(R.id.back);
        ll_date = findViewById(R.id.ll_date);
        ll_time = findViewById(R.id.ll_time);
        ED_Total_date = findViewById(R.id.ED_Total_date);
        ED_Total_time = findViewById(R.id.ED_Total_time);
        ED_grantee_days = findViewById(R.id.ED_gurantee_days);
        ll_card_ok = findViewById(R.id.ll_card_ok);
        ED_paid_amount = findViewById(R.id.ED_paid_amount);

        txt_currency = findViewById(R.id.txt_currency);
        currency = prefs.getString("CURRENCY", " ");

        txt_currency.setText(currency);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        ED_Total_date.setText(date);
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
        String strDate = dateFormat.format(currentTime);
        ED_Total_time.setText(strDate);

        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                Locale.setDefault(Locale.ENGLISH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Act_Change_ABS.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                ED_Total_date.setText(date);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });
        ll_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                Locale.setDefault(Locale.ENGLISH);
                TimePickerDialog timePickerDialog = new TimePickerDialog(Act_Change_ABS.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                ED_Total_time.setText(hourOfDay + ":" + minute + ":" + mSec);
                                mHour = hourOfDay;
                                mMinute = minute;

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        ll_card_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_gurantee_days = ED_grantee_days.getText().toString();
                str_paid_amount = ED_paid_amount.getText().toString();

                if (TextUtils.isEmpty(str_gurantee_days)) {
                    Toast.makeText(Act_Change_ABS.this, getString(R.string.please_enter_guranty_day), Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(str_paid_amount)) {
                    Toast.makeText(Act_Change_ABS.this, getString(R.string.please_enter_amount), Toast.LENGTH_SHORT).show();
                } else {
                    addABSService();
                }
            }
        });

        date_time = ED_Total_date.getText().toString() + ED_Total_time.getText().toString();

    }

    // Add abs detail in service book
    private void addABSService() {

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "add_services", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);

                    if (obj.optString("status").equals("1")) {

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        try {
                            Date date = dateFormat.parse(date_time);
                            calendar.setTime(date);
                            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + Integer.parseInt(ED_grantee_days.getText().toString()));
                            NotificationRemainder("" + dateFormat.format(calendar.getTime()),  new Random().nextInt());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(Act_Change_ABS.this, getString(R.string.change_abs_added_successfully), Toast.LENGTH_LONG).show();
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Act_Change_ABS.this, getString(R.string.internet_not_connect) + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("car_id", car_id);
                map.put("guarantee_by_days", ED_grantee_days.getText().toString());
                map.put("amount", ED_paid_amount.getText().toString());
                map.put("date_time", date_time);
                map.put("enum_id", enum_id);
                Log.e("params", " " + map);
                return map;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public void NotificationRemainder(String datetime, int id) {
        Date Date = new Date();
        Date CurDate = new Date();
        try {
            Date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(datetime);
            CurDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));

            if (Date.after(CurDate) && AppUtils.printDifference(CurDate, Date, "d") > 3) {

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Date.getTime());
                calendar.add(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH) - 2));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTimeInMillis(Date.getTime());

                Log.e("Tag Date 1", new SimpleDateFormat("hh:mm aa").format(calendar1.getTime()));
                Log.e("Tag Date 2", new SimpleDateFormat("hh:mm aa").format(calendar.getTime()));
                Intent intent = new Intent(getApplicationContext(), RemainderBroadcast.class);

                String msg = "Your ABS Grantee Expire 2 days to go \n Date : " + new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime());
                intent.putExtra("title", "Service Book");
                intent.putExtra("messageFor", "service_book");
                intent.putExtra("msg", msg);
                intent.putExtra("id", id);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), new Random().nextInt(), intent, Intent.FLAG_ACTIVITY_CLEAR_TOP);
                AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                Log.e("Tag Date Time", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(calendar.getTime()));
                manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
