package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.Services_Model;
import com.example.automap_application.Model.Sub_Service_Model;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Act_Booking_service_select extends AppCompatActivity implements View.OnClickListener {

    ImageView back_garage_list;
    LinearLayout lin_services;
//    List<Services_Model> services_models = new ArrayList<>();
    List<Sub_Service_Model> sub_service_models = new ArrayList<>();
    View no_date_error;
    Button btn_retry;
    TextView txt_msg;
    RecyclerView rl_service;
    AdapterService adapterService;

    @SuppressLint("StaticFieldLeak")
    public Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__booking_service_select);

        instance = this;
        lin_services = findViewById(R.id.lin_services);
        no_date_error = findViewById(R.id.no_date_error);
        btn_retry = findViewById(R.id.btn_retry);
        txt_msg = findViewById(R.id.txt_msg);
        rl_service = findViewById(R.id.rl_service);
        back_garage_list = findViewById(R.id.back_garage_list);

        back_garage_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_retry.setOnClickListener(this);

        get_garage_facility();
    }

    // get all appointment service data set adapter
    private void get_garage_facility() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
            StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "getServiceData", new Response.Listener<String>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(String response) {
                    Log.e("garage_facility", " " + response);
                    try {

                        JSONObject obj = new JSONObject(response);
                        if (obj.getString("status").equals("true")) {
                            JSONArray jsonArray = obj.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String sub_service_id = jsonObject.optString("subservice_id", " ");
                                String sub_service_name = jsonObject.optString("subservice_name", " ");
                                String service_image = jsonObject.optString("service_image", " ");
                                String service_id = jsonObject.optString("service_id", " ");
                                String service_name = jsonObject.optString("service_name", " ");
                                String service_provider = jsonObject.optString("service_provider", " ");

                                if (service_provider.contains(getString(R.string.parametr_get_quote_and_booking))){
                                    sub_service_models.add(new Sub_Service_Model(service_id,service_name,sub_service_id,sub_service_name,service_image,service_provider));
                                }
                            }

                            if (sub_service_models!= null && sub_service_models.size() > 0) {
                                lin_services.setVisibility(View.VISIBLE);
                                adapterService = new AdapterService(sub_service_models, getApplicationContext());
                                rl_service.setLayoutManager(new LinearLayoutManager(Act_Booking_service_select.this));
                                rl_service.setAdapter(adapterService);
                            } else {
                                rl_service.setVisibility(View.GONE);
                                no_date_error.setVisibility(View.VISIBLE);
                                btn_retry.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_data_found));
                            }
                        } else {
                            rl_service.setVisibility(View.GONE);
                            no_date_error.setVisibility(View.VISIBLE);
                            btn_retry.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.no_data_found));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        rl_service.setVisibility(View.GONE);
                        no_date_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.somting_wrong_please));
                    }
                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("My error", "" + error);
                    rl_service.setVisibility(View.GONE);
                    no_date_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("service_provider", getString(R.string.parametr_get_quote_and_booking));
                    Log.e("Tag params",map.toString());
                    return map;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    6000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(getApplicationContext()).add(request);
        } else {

            no_date_error.setVisibility(View.VISIBLE);
            rl_service.setVisibility(View.GONE);
            btn_retry.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.internet_not_connect));
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry) {
            get_garage_facility();
        }
    }

    int[] Image = new int[]{R.drawable.change_abs, R.drawable.dry_clean, R.drawable.b_break, R.drawable.b_wash, R.drawable.r_wheel};

    // adpter of service
    public class AdapterService extends RecyclerView.Adapter<AdapterService.MyViewHolder> {

        List<Sub_Service_Model> services_models;
        Context context;

        AdapterService(List<Sub_Service_Model> services_models, Context context) {
            this.services_models = services_models;
            this.context = context;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_service_list, viewGroup, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int i) {

            holder.service_name.setText(Html.fromHtml(services_models.get(i).getSub_service_name()));

            // any service image not found when static image display
            if (services_models.get(i).getService_img() != null) {
                if (i == 0)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.c_ngk).into(holder.service_image);
                else if (i == 1)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.dry_clean).into(holder.service_image);
                else if (i == 2)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.b_break).into(holder.service_image);
                else if (i == 3)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.b_wash).into(holder.service_image);
                else if (i == 4)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.change_abs).into(holder.service_image);
                else if (i == 5)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.cleaning_spray).into(holder.service_image);
                else if (i == 6)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.engine_oil_service).into(holder.service_image);
                else if (i == 7)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.gear_oil_service).into(holder.service_image);
                else if (i == 8)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.f_break).into(holder.service_image);
                else if (i == 9)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.polish).into(holder.service_image);
                else if (i == 10)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.power_oil_service).into(holder.service_image);
                else if (i == 11)
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(R.drawable.r_wheel).into(holder.service_image);
                else
                    Glide.with(context).load(services_models.get(i).getService_img()).placeholder(Image[(int) (Math.random() * Image.length)]).into(holder.service_image);
            } else {
                holder.service_image.setImageResource(Image[(int) (Math.random() * Image.length)]);
            }

            //redirect booking appointment screen
            holder.lin_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sp = getSharedPreferences(getString(R.string.pref_disable_service), MODE_PRIVATE);
                    String sendDate = sp.getString(getString(R.string.pref_isSend), "");
                    @SuppressLint("SimpleDateFormat")
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
                    Log.e("Tag Date", sendDate + "\n" + date);

                    JSONArray array = new JSONArray();
                    for (int j = 0; j < 1; j++) {
                        JSONObject obj = new JSONObject();

                        try {
                            obj.put("service_id", services_models.get(i).getService_id());
                            obj.put("service_name", services_models.get(i).getSub_service_name());
                            array.put(obj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // service is towtruck
                        if (services_models.get(i).getSub_service_name().toLowerCase().matches(".*tow truck*.") ||
                            services_models.get(i).getSub_service_name().toLowerCase().equalsIgnoreCase("tow truck")){
                            Intent intent = new Intent(getApplicationContext(), Act_Tow_track.class);
                            startActivity(intent);
                        }else {

                            Intent intent = new Intent(getApplicationContext(), Act_Booking_appointment.class);
                            intent.putExtra("array", array.toString());
                            intent.putExtra("sub_service_id",services_models.get(i).getSub_service_id());
                            intent.putExtra("sub_service_name", services_models.get(i).getSub_service_name());
                            intent.putExtra("type", "Special Service");
                            startActivity(intent);
                        }
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return services_models.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView service_name;
            private ImageView service_image;
            LinearLayout lin_main;

            MyViewHolder(@NonNull View itemView) {
                super(itemView);

                service_image = itemView.findViewById(R.id.service_image);
                lin_main = itemView.findViewById(R.id.lin_main);
                service_name = itemView.findViewById(R.id.service_name);
            }

        }
    }
}
