package com.example.automap_application;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.view.View;
import android.widget.TextView;

public class Act_Login_first extends AppCompatActivity {

    CardView btn_login;
    TextView btn_reg;
    public static Activity instance = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__login_first);

        instance = this;

        btn_login = findViewById(R.id.BT_LOGIN);
        btn_reg = findViewById(R.id.BT_SIGN_UP);

        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Act_Login_first.this, Act_Registration.class);
                startActivity(intent);
                finish();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Act_Login_first.this, Act_Login.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
