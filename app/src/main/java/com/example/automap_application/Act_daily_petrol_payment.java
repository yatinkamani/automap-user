package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Act_daily_petrol_payment extends AppCompatActivity {

    CardView btn_ok, btn_edit;
    ImageView back;
    EditText ED_Total_date, ED_Total_time;
    private int mHour, mMinute, mSec;
    String date, str_ED_amount;
    EditText ED_amount;

    SharedPreferences prefs;
    String car_id, date_time;

    LinearLayout ll_card_ok, ll_card_edit;
    LinearLayout ll_date, ll_time;

    String fuel_payment_id;
    String fuel_payment_amount;
    Handler handler;
    Runnable runnable;

    @SuppressLint("StaticFieldLeak")
    public static Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_daily_petrol_payment);

        instance = this;
        btn_ok = findViewById(R.id.card_ok);
        btn_edit = findViewById(R.id.card_Edit_amount);
        back = findViewById(R.id.back_daily_petrol_payment);
        ED_amount = findViewById(R.id.ED_amount);
        ED_Total_date = findViewById(R.id.ED_Total_date);
        ED_Total_time = findViewById(R.id.ED_Total_time);
        ll_card_ok = findViewById(R.id.ll_card_ok);
        ll_card_edit = findViewById(R.id.ll_card_edit);
        ll_date = findViewById(R.id.ll_date);
        ll_time = findViewById(R.id.ll_time);

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);
        car_id = prefs.getString("CAR_ID", " ");
        fuel_payment_id = prefs.getString("FULE_PAYMENT_ID", " ");
        fuel_payment_amount = prefs.getString("FULE_PAYMENT_AMOUNT", "");

        if (fuel_payment_amount == null) {

        } else {
            ED_amount.setText(fuel_payment_amount);
        }
        if (fuel_payment_id == null) {
//            ll_card_ok.setVisibility(View.VISIBLE);
            ll_card_edit.setVisibility(View.GONE);
        } else if (fuel_payment_id.equals("")){
//            ll_card_ok.setVisibility(View.GONE);
            ll_card_edit.setVisibility(View.GONE);
        } else {
//            ll_card_ok.setVisibility(View.GONE);
            ll_card_edit.setVisibility(View.VISIBLE);
        }

        ED_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handler.removeCallbacks(runnable);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        runnable = new Runnable() {
            @Override
            public void run() {
                alert(true);
            }
        };
        handler = new Handler();
        handler.postDelayed(runnable, 6000 * 10 * 1);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_ED_amount = ED_amount.getText().toString();

                if (TextUtils.isEmpty(str_ED_amount)) {
                    Toast.makeText(Act_daily_petrol_payment.this, getString(R.string.please_enter_amount), Toast.LENGTH_SHORT).show();
                } else {
                    addDailyFuelPayment();
                    Intent intent = new Intent(Act_daily_petrol_payment.this, Act_daily_payment_total.class);
                    startActivity(intent);
                }
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_ED_amount = ED_amount.getText().toString();

                if (TextUtils.isEmpty(str_ED_amount)) {
                    Toast.makeText(Act_daily_petrol_payment.this, "Please enter your amount", Toast.LENGTH_SHORT).show();
                } else {
                    editDailyFuelPayment();
                }
            }
        });

        date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        ED_Total_date.setText(date);
        Date currentTime = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat")
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
        String strDate = dateFormat.format(currentTime);
        ED_Total_time.setText(strDate);

        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                Locale.setDefault(Locale.ENGLISH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Act_daily_petrol_payment.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                ED_Total_date.setText(date);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        ll_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                mSec = c.get(Calendar.SECOND);

                // Launch Time Picker Dialog
                Locale.setDefault(Locale.ENGLISH);
                TimePickerDialog timePickerDialog = new TimePickerDialog(Act_daily_petrol_payment.this, new TimePickerDialog.OnTimeSetListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ED_Total_time.setText(hourOfDay + ":" + minute + ":" + mSec);
                        mHour = hourOfDay;
//                                minute = minute;

                        //updateTime(mHour,minute);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        date_time = ED_Total_date.getText().toString() + ED_Total_time.getText().toString();

    }

    // edit petrol payment detail in service book
    private void editDailyFuelPayment() {
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "add_daily_fule_payment", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("1")) {
                        Toast.makeText(Act_daily_petrol_payment.this, "Your amount edited successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("FULE_PAYMENT_ID", fuel_payment_id);
                    editor.putString("FULE_PAYMENT_AMOUNT", ED_amount.getText().toString());
                    editor.apply();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Act_daily_petrol_payment.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("fule_payment_id", fuel_payment_id);
                map.put("car_id", car_id);
                map.put("amount", ED_amount.getText().toString());
                map.put("date_time", date_time);
                Log.e("params", " " + map);
                return map;

            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void addDailyFuelPayment() {
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "add_daily_fule_payment", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("status").equals("1")) {
                        Toast.makeText(Act_daily_petrol_payment.this, getString(R.string.your_amount_added_successfully), Toast.LENGTH_SHORT).show();
                        JSONObject dataObject = obj.getJSONObject("result");
                        fuel_payment_id = dataObject.getString("fule_payment_id");
                    }

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("FULE_PAYMENT_ID", fuel_payment_id);
                    editor.putString("FULE_PAYMENT_AMOUNT", ED_amount.getText().toString());
                    editor.apply();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Act_daily_petrol_payment.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("car_id", car_id);
                map.put("amount", ED_amount.getText().toString());
                map.put("date_time", date_time);
                Log.e("params", " " + map);
                return map;

            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void alert(boolean isShow) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_daily_petrol_payment.this);

        builder.setMessage(getString(R.string.do_you_want_to_enter_petrol_amount));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ED_amount.setText("");
                ED_amount.requestFocus();
            }
        });
        AlertDialog alertDialog = builder.create();
        if (isShow)
            alertDialog.show();
        else
            alertDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        handler.removeCallbacks(runnable);
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        handler.removeCallbacks(runnable);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }
}
