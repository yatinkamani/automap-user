package com.example.automap_application.Constant;

import android.net.Uri;

import com.example.automap_application.Model.GARAGE.GarageNotificationModel;

import java.util.ArrayList;
import java.util.List;

public class Constant {

   public static int i;
   public static int i2;
   public static ArrayList<Uri> mUri = new ArrayList<>();

   public static String DIRECTION_URL = "https://maps.googleapis.com/maps/api/directions/";

//    public static String API_KEY = "AIzaSyBhd3izOpoMltZ81aDZtSpoaMthmyUzD0M";
//   public static String API_KEY = "AIzaSyBBIiuqET41PjtO4SvCiA6T4BYJzS3OSy0";
   public static String API_KEY = "AIzaSyAnOLvdoU9vDZoQbnXepxEIEcQt9uN0-iI";

   public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

//   public static final String URL = "http://mobwebdemo.xyz/automap/api/";
//   public static final String URL = "http://arccus.in/webmobdemo/automap/api/";
   public static final String BASE_URL = "http://kaprat.com/dev/automap/";
//   public static final String BASE_URL = "http://ec2-3-129-16-59.us-east-2.compute.amazonaws.com/admin/";
//   public static final String BASE_URL = "http://amrgroup.online/admin/";
   public static final String URL = BASE_URL+"api_v1_0_0/" ;
   public static final String IMAGE_URL = BASE_URL+"assets/uploads/";

/*
 *    The constant FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS.
 */

   public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

   public static GarageNotificationModel model = new GarageNotificationModel();

}
