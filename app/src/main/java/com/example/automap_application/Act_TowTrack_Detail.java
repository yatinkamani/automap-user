package com.example.automap_application;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Act_TowTrack_Detail extends AppCompatActivity implements View.OnClickListener {

    CardView ll_call;
    TextView txt_address,txt_rate,phone_no,txt_country,txt_city;
    RatingBar ratingBar;
    View lin_error;
    TextView txt_error;
    Button btn_retry;
    ScrollView scrollView;
    Toolbar toolbar;
    ImageView image_banner;
    RelativeLayout lin_rating;

    String contact_no, towtrack_name, towtrack_id, city_name  = "", address = "", image = "", ratting = "", country_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__tow_track__detail);

        ll_call = findViewById(R.id.ll_call);
        txt_address = findViewById(R.id.txt_address);
        txt_rate = findViewById(R.id.txt_rate);
        txt_city = findViewById(R.id.txt_city);
        txt_country = findViewById(R.id.txt_country);
        phone_no = findViewById(R.id.phone_no);
        ratingBar = findViewById(R.id.ratingBar);
        lin_error = findViewById(R.id.no_data_error);
        scrollView = findViewById(R.id.scroll_view);
        image_banner = findViewById(R.id.img_banner);
        toolbar = findViewById(R.id.toolbar);
        lin_rating = findViewById(R.id.lin_rating);

        txt_error = findViewById(R.id.txt_msg);
        btn_retry = findViewById(R.id.btn_retry);
        getIntentData();
        getTowTrackDetail(towtrack_id);
        ll_call.setOnClickListener(this);
        btn_retry.setOnClickListener(this);
        ActionBar();
    }

    public void getIntentData(){

        towtrack_id = getIntent().getStringExtra(getString(R.string.pref_towtrack_id));
        towtrack_name = getIntent().getStringExtra(getString(R.string.pref_name));
    }

    // get towtruck detail set all detail view
    private void getTowTrackDetail(final String id) {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"check_towtrack_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("check_towTruck_response"," " +response);
                    JSONObject obj = new JSONObject(response);
                    obj.getString("request");
                    if (obj.optString("status").equals("true")) {
                        JSONArray dataArray = obj.getJSONArray("result");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data_obj = dataArray.getJSONObject(i);
                            contact_no = data_obj.getString("contact_no");
                            city_name = (data_obj.optString("city_name",""));
                            if (!data_obj.getString("towtrack_name").equals("")){
                                towtrack_name = data_obj.getString("towtrack_name");
                            }
                            country_name = data_obj.getString("country_name");
                            address = data_obj.getString("address");
                            image = (data_obj.getString("towtrack_profile_img"));
                            ratting = data_obj.optString("rating","");
                        }

                        Glide.with(getApplicationContext()).asBitmap().load(image).placeholder(R.drawable.amr_icon).into(new BitmapImageViewTarget(image_banner) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                image_banner.setImageBitmap(resource);
                            }
                        });

                        txt_address.setText(Html.fromHtml(address));
                        phone_no.setText(Html.fromHtml(contact_no));

                        if (!ratting.equalsIgnoreCase("")){
                            lin_rating.setVisibility(View.VISIBLE);
                            ratingBar.setRating(Float.parseFloat(ratting));
                            txt_rate.setText(ratting);
                        }

                        if (!country_name.equals("")){
                            txt_country.setVisibility(View.VISIBLE);
                            txt_country.setText(getString(R.string.country2)+" "+country_name);
                        }

                        if (!city_name.equals("")){
                            txt_city.setVisibility(View.VISIBLE);
                            txt_city.setText(getString(R.string.city2)+" "+city_name);
                        }

                        scrollView.setVisibility(View.VISIBLE);
                        lin_error.setVisibility(View.GONE);
                    } else {
                        scrollView.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        txt_error.setText(getString(R.string.no_data_found));
                        btn_retry.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    scrollView.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    txt_error.setText(getString(R.string.somting_wrong_please));
                    btn_retry.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Act_Login.this, "my error :" + error, Toast.LENGTH_LONG).show();
                Log.e("Tag error", "" + error);
                scrollView.setVisibility(View.GONE);
                lin_error.setVisibility(View.VISIBLE);
                txt_error.setText(getString(R.string.somting_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("towtrack_id", id);

                return map;
            }
        };
        queue.add(request);
    }

    public void ActionBar(){
//        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(towtrack_name);
            actionBar.setDisplayShowHomeEnabled(true);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == ll_call){
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+contact_no));
            startActivity(intent);
        } else if (btn_retry == view){
            getTowTrackDetail(towtrack_id);
        }
    }

}
