package com.example.automap_application.newPackage;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.annotation.NonNull;

import com.example.automap_application.listeners.PermissionCallback;
import com.example.automap_application.listeners.PermissionChecker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class Helper {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String DIRECTION_API = "https://maps.googleapis.com/maps/api/directions/json?origin=";
//    private static final String API_KEY = "AIzaSyBhd3izOpoMltZ81aDZtSpoaMthmyUzD0M";
    private static final String API_KEY = "AIzaSyBBIiuqET41PjtO4SvCiA6T4BYJzS3OSy0";

    public static final int MY_SOCKET_TIMEOUT_MS = 5000;
    public static String getUrl(String originLat, String originLon, String destinationLat, String destinationLon){
        return Helper.DIRECTION_API + originLat+","+originLon+"&destination="+destinationLat+","+destinationLon+"&key="+API_KEY;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(activity);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(activity, result, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }

    public static void permissionChecker(@NonNull Activity context, String[] permissions, PermissionCallback permissionCallback) {
        PermissionChecker.init(context);
        PermissionChecker.askForPermission(context, permissions, permissionCallback);
    }
}
