package com.example.automap_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.utils.AppUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Act_Tow_Track_Offers_Detail extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back, tow_image;
    TextView txt_title, txt_distance, txt_time, txt_address, txt_city, txt_country;
    ScrollView scroll_view;
    RatingBar ratingBar;
    CardView card_call, card_confirm_order, card_rate, card_back, card_track, card_complain;
    View lin_error;
    Button btn_retry;
    TextView txt_msg;
    FusedLocationProviderClient mFusedLocationClient;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String address = "", contact_no = "", towtrack_name = "", towtrack_profile_img = "", latitude = "", longitude = "", status = "", country_name = "", city_name = "";
    String towtrack_id = "", towtrack_request_id = "", user_id = "", rating = "", tow_lat, tow_lng, final_lat = "", final_lng = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__tow__track__offers__detail);

        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        tow_image = findViewById(R.id.tow_image);
        txt_distance = findViewById(R.id.txt_distance);
        txt_time = findViewById(R.id.txt_time);
        txt_address = findViewById(R.id.txt_address);
        txt_city = findViewById(R.id.txt_city);
        txt_country = findViewById(R.id.txt_country);
        scroll_view = findViewById(R.id.scroll_view);
        ratingBar = findViewById(R.id.ratingBar);
        card_call = findViewById(R.id.card_call);
        card_confirm_order = findViewById(R.id.card_confirm_order);
        card_rate = findViewById(R.id.card_rate);
        card_back = findViewById(R.id.card_back);
        card_track = findViewById(R.id.card_track);
        card_complain = findViewById(R.id.card_complain);
        lin_error = findViewById(R.id.lin_error);
        btn_retry = findViewById(R.id.btn_retry);
        txt_msg = findViewById(R.id.txt_msg);

        pref = getSharedPreferences(Act_Login.LOGIN_PREF, MODE_PRIVATE);
        user_id = pref.getString(getString(R.string.pref_user_id), "");

        img_back.setOnClickListener(this);
        btn_retry.setOnClickListener(this);
        card_back.setOnClickListener(this);
        card_call.setOnClickListener(this);
        card_confirm_order.setOnClickListener(this);
        card_rate.setOnClickListener(this);
        card_track.setOnClickListener(this);
        card_complain.setOnClickListener(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getIntentData();
        getTowTrackInfo();
        InitializeBroadcast();
    }

    public void getIntentData() {
        towtrack_id = getIntent().getStringExtra(getString(R.string.pref_towtrack_id));
        towtrack_request_id = getIntent().getStringExtra(getString(R.string.pref_request_id));
        towtrack_name = getIntent().getStringExtra(getString(R.string.name));
        txt_title.setText(towtrack_name);

    }

    // get towtruck detail and set view
    private void getTowTrackInfo() {

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "check_towtrack_status",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("status_response", " " + response);
                        progressDialog.dismiss();
                        try {
                            JSONObject mainJsonObject = new JSONObject(response);
                            if (mainJsonObject.getString("status").equals("true")) {
                                JSONArray result = mainJsonObject.getJSONArray("result");
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject jsonObject = result.getJSONObject(i);
                                    contact_no = jsonObject.getString("contact_no");
                                    towtrack_name = jsonObject.getString("towtrack_name");
                                    towtrack_profile_img = jsonObject.getString("towtrack_profile_img");
                                    address = jsonObject.getString("address");
                                    country_name = jsonObject.getString("country_name");
                                    city_name = jsonObject.optString("city_name", "");
                                    tow_lat = jsonObject.getString("latitude");
                                    tow_lng = jsonObject.getString("longitude");

                                    if (jsonObject.has("ratting")) {
                                        rating = jsonObject.getString("ratting");
                                    }
                                }

                                if (!rating.equals("") || !rating.isEmpty()) {
                                    ratingBar.setVisibility(View.VISIBLE);
                                    ratingBar.setRating(Float.parseFloat(rating));
                                } else {
                                    ratingBar.setVisibility(View.GONE);
                                }

                                if (address.equals("")) {
                                    txt_address.setVisibility(View.GONE);
                                } else {
                                    txt_address.setVisibility(View.VISIBLE);
                                    txt_address.setText(Html.fromHtml(address));
                                }

                                if (city_name.equals("")) {
                                    txt_city.setVisibility(View.GONE);
                                } else {
                                    txt_city.setVisibility(View.VISIBLE);
                                    txt_city.setText(Html.fromHtml("<b>"+getString(R.string.city2) +"  </b>" + city_name));
                                }

                                if (country_name.equals("")) {
                                    txt_country.setVisibility(View.GONE);
                                } else {
                                    txt_country.setVisibility(View.VISIBLE);
                                    txt_country.setText(Html.fromHtml("<b>"+getString(R.string.country2) +"  </b>" + country_name));
                                }

                                Glide.with(getApplicationContext()).asBitmap().load(towtrack_profile_img)
                                        .placeholder(getResources().getDrawable(R.drawable.amr_icon))
                                        .thumbnail(0.01f)
                                        .into(new BitmapImageViewTarget(tow_image) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                tow_image.setImageBitmap(resource);
                                            }
                                        });

                                if (result.length() > 0) {
                                    lin_error.setVisibility(View.GONE);
                                    scroll_view.setVisibility(View.VISIBLE);

                                } else {
                                    lin_error.setVisibility(View.VISIBLE);
                                    scroll_view.setVisibility(View.GONE);
                                    txt_msg.setText(getString(R.string.no_detail_found_tow));
                                    btn_retry.setVisibility(View.GONE);
                                }

                                getStatus();
                            } else {
                                lin_error.setVisibility(View.VISIBLE);
                                scroll_view.setVisibility(View.GONE);
                                txt_msg.setText(getString(R.string.no_detail_found_tow));
                                btn_retry.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            lin_error.setVisibility(View.VISIBLE);
                            scroll_view.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.somting_wrong_please));
                            btn_retry.setVisibility(View.VISIBLE);
                        }
//                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
                lin_error.setVisibility(View.VISIBLE);
                scroll_view.setVisibility(View.GONE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                btn_retry.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("towtrack_id", towtrack_id);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    // get towtruck offer status
    private void getStatus() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "towtrack_request_detail",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("true")) {
                                    JSONArray result = mainJsonObject.getJSONArray("result");
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject jsonObject = result.getJSONObject(i);
                                        status = jsonObject.getString("status");
                                        final_lat = jsonObject.getString("towtrack_latitude");
                                        final_lng = jsonObject.getString("towtrack_longitude");
                                    }

                                    // towtrcuk participate
                                    if (status.equalsIgnoreCase("1")) {
                                        card_confirm_order.setVisibility(View.VISIBLE);
                                        card_track.setVisibility(View.GONE);
                                        card_complain.setVisibility(View.GONE);
                                    }
                                    // confirm request
                                    else if (status.equalsIgnoreCase("3") ||
                                            status.equalsIgnoreCase("4")) {
                                        card_track.setVisibility(View.VISIBLE);
                                        card_confirm_order.setVisibility(View.GONE);
                                        card_complain.setVisibility(View.VISIBLE);
                                        card_rate.setVisibility(View.VISIBLE);
                                    }
                                    // cancel request
                                    else if (status.equalsIgnoreCase("6")) {
                                        card_rate.setVisibility(View.GONE);
                                        card_confirm_order.setVisibility(View.GONE);
                                        card_complain.setVisibility(View.VISIBLE);
                                    }
                                    // complete towtruck request
                                    else if (status.equalsIgnoreCase("7")) {
                                        card_rate.setVisibility(View.GONE);
                                        card_track.setVisibility(View.GONE);
                                        card_confirm_order.setVisibility(View.GONE);
                                    }

                                    getDistanceInfo();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("towtrack_request_id", towtrack_request_id);
                    params.put("towtrack_id", towtrack_id);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    public void InitializeBroadcast(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(Act_Tow_Track_Offers_Detail.this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }

    // status change to refress status api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (tow_track_badge > 0 && !(Act_Tow_Track_Offers_Detail.this.isFinishing())) {
                    getStatus();
                }
                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);

            }
        }
    };

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (AppUtils.checkGPSPermissions(this)) {
            if (AppUtils.isLocationEnabled(getApplicationContext())) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    latitude = String.valueOf(location.getLatitude());
                                    longitude = String.valueOf(location.getLongitude());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } else {
            AppUtils.requestGPSPermissions(this);
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            latitude = String.valueOf(mLastLocation.getLatitude());
            longitude = String.valueOf(mLastLocation.getLongitude());
            getDistanceInfo();
            Log.e("Tag Location", mLastLocation.toString());
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == AppUtils.PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            } else {
                Toast.makeText(getApplicationContext(), "Please Grant Permission Use Features", Toast.LENGTH_LONG).show();
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume() {
        if (AppUtils.checkGPSPermissions(this)) {

            getLastLocation();
        }
        super.onResume();
    }

    // get distance info for google api
    private void getDistanceInfo() {
        final StringBuilder[] stringBuilder = {new StringBuilder()};

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latitude + "," + longitude + "&destination=" + tow_lat + "," + tow_lng + "&mode=driving&sensor=false&key=" + getString(R.string.google_maps_key);
                    Log.e("Tag Url", url);
                    HttpPost http_post = new HttpPost(url);
                    HttpClient client = new DefaultHttpClient();
                    HttpResponse response;
                    stringBuilder[0] = new StringBuilder();
                    response = client.execute(http_post);
                    HttpEntity entity = (response.getEntity());
                    InputStream stream = entity.getContent();
                    int b;
                    while ((b = stream.read()) != -1) {
                        stringBuilder[0].append((char) b);
                    }
                    Log.e("Tag Daat", "" + stringBuilder[0]);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        String dura = "0.0";
                        String dist = "0.0";

                        jsonObject = new JSONObject(stringBuilder[0].toString());

                        if (jsonObject.getString("status").equals("ZERO_RESULTS")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    txt_distance.setText(getString(R.string.out_of_range));
                                    txt_time.setText(getString(R.string.no_result));
                                }
                            });

                        } else if (jsonObject.getString("status").equals("OVER_QUERY_LIMIT")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    txt_distance.setText(getString(R.string.fail));
                                    txt_time.setText(getString(R.string.fail));
                                }
                            });

                        } else {

                            JSONArray array = jsonObject.getJSONArray("routes");
                            JSONObject routes = array.getJSONObject(0);
                            JSONArray legs = routes.getJSONArray("legs");
                            JSONObject steps = legs.getJSONObject(0);
                            JSONObject distance = steps.getJSONObject("distance");
                            JSONObject duration = steps.getJSONObject("duration");
                            Log.i("Distance", distance.toString());
                            Log.i("Duration", duration.toString());
//                        dist = Double.parseDouble(distance.getString("text").replaceAll("[^\\.0123456789]","") );
//                        duran = Double.parseDouble(duration.getString("text").replaceAll("[^\\.0123456789]","") );
                            dist = (distance.getString("text"));
                            dura = (duration.getString("text"));

                            String finalDist = dist;
                            String finalDura = dura;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    txt_distance.setText(String.valueOf(finalDist));
                                    txt_time.setText(String.valueOf(finalDura));
                                }
                            });
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onClick(View view) {

        if (view == btn_retry) {
            onBackPressed();
        } else if (view == card_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel" + contact_no));
            startActivity(intent);
        } else if (view == card_confirm_order) {
            changeStatus("3");
        } else if (view == card_track) {
            Intent intent = new Intent(getApplicationContext(), Act_Tow_Track_Maps.class);
            intent.putExtra("latitude", tow_lat);
            intent.putExtra("longitude", tow_lng);
            intent.putExtra("final_lat", final_lat);
            intent.putExtra("final_lng", final_lng);
            intent.putExtra("towtrack_name", towtrack_name);
            intent.putExtra(getString(R.string.contact_no), contact_no);
            intent.putExtra(getString(R.string.request_tow_truck), towtrack_request_id);
            intent.putExtra(getString(R.string.pref_user_id), user_id);
            intent.putExtra("towtrack_id", towtrack_id);
            startActivity(intent);
        } else if (view == card_rate) {
            VerifyDialog();
        } else if (view == card_back) {
            onBackPressed();
        } else if (view == img_back) {
            onBackPressed();
        } else if (view == card_complain){
            ComplainDialog();
        }
    }

    // confirm towtruck offers
    private void changeStatus(String _status) {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "confirm_cancel_towtrack_request",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.confirm_the_request), Toast.LENGTH_LONG).show();
                                    if (_status.equals("3")) {
                                        timer_dialog();
                                    } else if (_status.equals("5")){
                                        finish();
                                    } else {
                                        getStatus();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("towtrack_request_id", towtrack_request_id);
                    params.put("status", _status);
                    params.put("towtrack_id", towtrack_id);
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    int count = 10;
    Handler handler;
    Runnable runnable;

    // timer dialog 10 count after confirm offer
    @SuppressLint("SetTextI18n")
    public void timer_dialog() {
        count = 10;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);
        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        TextView txt_message = view.findViewById(R.id.txt_message);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText(R.string.confirm_the_request);
        txt_message.setVisibility(View.VISIBLE);
        txt_message.setGravity(Gravity.CENTER);
        txt_message.setText("" + count);
        txt_message.setTextSize(20);
        txt_message.setTextColor(Color.RED);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                count--;
                txt_message.setText(String.valueOf(count));
                btn_ok.setText(getString(R.string.confirm) + "(" + count + ")");
                if (count == 0) {
                    dialog.dismiss();
                    changeStatus("4");
                } else {
                    handler.postDelayed(runnable, 1000);
                }
            }
        };

//        btn_ok.setText(getString(R.string.confirm));
        handler.postDelayed(runnable, 1000);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnable);
                changeStatus("4");
                dialog.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatus("5");
                handler.removeCallbacks(runnable);
                dialog.dismiss();
            }
        });
    }

    float ratings = 0;

    public void RattingDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dailog_garage_no_agree_reason, null);
        builder.setView(view);

        TextView txt_dialog_title = view.findViewById(R.id.txt_dialog_title);
        TextView txt_message = view.findViewById(R.id.txt_message);
        RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        txt_dialog_title.setText(getString(R.string.please_rate_the_service));
        btn_ok.setText(getString(R.string.yess));
        btn_cancel.setText(getString(R.string.nos));
        btn_cancel.setVisibility(View.GONE);
        ratingBar.setVisibility(View.VISIBLE);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2)
                .setColorFilter(getResources().getColor(R.color.black),
                        PorterDuff.Mode.SRC_ATOP); // for filled stars
        stars.getDrawable(1)
                .setColorFilter(getResources().getColor(R.color.black),
                        PorterDuff.Mode.SRC_ATOP); // for half filled stars
        stars.getDrawable(0)
                .setColorFilter(getResources().getColor(R.color.gray),
                        PorterDuff.Mode.SRC_ATOP); // for empty stars
        txt_message.setVisibility(View.VISIBLE);
        txt_message.setText(getString(R.string.ratting_service_time));

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (v == 1) {
                    Toast.makeText(getApplicationContext(), "Bad", Toast.LENGTH_LONG).show();
                } else if (v == 2) {
                    Toast.makeText(getApplicationContext(), "Bad Good", Toast.LENGTH_LONG).show();
                } else if (v == 3) {
                    Toast.makeText(getApplicationContext(), "Good", Toast.LENGTH_LONG).show();
                } else if (v == 4) {
                    Toast.makeText(getApplicationContext(), "Excellent Good", Toast.LENGTH_LONG).show();
                } else if (v == 5) {
                    Toast.makeText(getApplicationContext(), "Excellent", Toast.LENGTH_LONG).show();
                }
                ratings = v;
            }
        });

        btn_ok.setOnClickListener(v -> {
            if (ratings != 0) {
                ratting("7", "" + ratings);
                dialog.dismiss();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.please_rate_service), Toast.LENGTH_LONG).show();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "will be notified after 24 hours", Toast.LENGTH_LONG).show();
            }
        });
    }

    // ratting towtruck offers
    private void ratting(String _status, String ratting) {
        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "user_towtrack_ratting",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Request_status_response", " " + response);
                            try {
                                JSONObject mainJsonObject = new JSONObject(response);
                                if (mainJsonObject.getString("status").equals("1")) {
                                    Intent intent = new Intent(getApplicationContext(), Act_Service_book.class);
                                    if (new Act_Tow_Track_Offer_List().instance != null) {
                                        new Act_Mobile_Request_Offers_List().instance.finish();
                                    }

                                    if (new Act_Tow_Track_Request_List().instance != null) {
                                        new Act_Mobile_Request_List().instance.finish();
                                    }
                                    finish();
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Tag Error", "" + error);
                }
            }) {

                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("towtrack_request_id", towtrack_request_id);
                    params.put("status", _status);
                    params.put("towtrack_id", towtrack_id);
                    params.put("user_id", user_id);
                    params.put("ratting", ratting);
                    Log.e("Tag Params", params.toString());
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
        }
    }

    // verify work after complete order/request
    private void VerifyDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Tow_Track_Offers_Detail.this);
        builder.setTitle(getString(R.string.confirm));
        builder.setMessage(R.string.reach_the_finel_dest_complet_order);

        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                RattingDialog();
            }
        });

        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void ComplainDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Act_Tow_Track_Offers_Detail.this);
        View view = LayoutInflater.from(Act_Tow_Track_Offers_Detail.this).inflate(R.layout.dialog_complain, null, false);
        EditText ed_complain = view.findViewById(R.id.ed_complain);
        Button btn_complain = view.findViewById(R.id.btn_complain);
        builder.setView(view);

        AlertDialog dialog = builder.create();
        dialog.show();

        btn_complain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed_complain.getText().toString().equals("")) {
                    ed_complain.requestFocus();
                    ed_complain.setError(getString(R.string.enter_detial));
                } else {
                    StringRequest request = new StringRequest(Request.Method.POST, Constant.URL + "user_garage_complain", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Tag Response", response);

                            try {
                                JSONObject object = new JSONObject(response);
                                if (object.getString("status").equals("1")) {
                                    dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Tag error", error.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.somting_wrong_please), Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("towtrack_id", towtrack_id);
                            map.put("user_id", user_id);
                            map.put("towtrack_request_id", towtrack_request_id);
                            map.put("status","9");
                            map.put("complain", ed_complain.getText().toString());
                            Log.e("Tag Params", map.toString());
                            return map;
                        }
                    };

                    RequestQueue queue = Volley.newRequestQueue(Act_Tow_Track_Offers_Detail.this);
                    queue.add(request);
                }
            }
        });
    }

}
