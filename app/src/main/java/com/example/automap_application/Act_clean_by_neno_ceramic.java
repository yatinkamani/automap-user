package com.example.automap_application;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.automap_application.extra.API;
import com.example.automap_application.extra.APIResponse;
import com.example.automap_application.extra.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class Act_clean_by_neno_ceramic extends AppCompatActivity {

    ImageView back;
    String enum_id;
    EditText ED_Total_date, ED_Total_time;
    private int mHour, mMinute, mSec;
    String date;
    LinearLayout btn_ok;


    String car_id, date_time;
    SharedPreferences prefs;
    String str_ED_amount;
    EditText ED_amount;

    String currency;
    TextView txt_currency;
    LinearLayout ll_date, ll_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_clean_by_neno_ceramic);

        back = findViewById(R.id.back_body_wash);
        ED_Total_date = findViewById(R.id.ED_Total_date);
        ED_Total_time = findViewById(R.id.ED_Total_time);
        btn_ok = findViewById(R.id.btn_ok);
        ED_amount = findViewById(R.id.ED_amount);
        ll_date = findViewById(R.id.ll_date);
        ll_time = findViewById(R.id.ll_time);

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);
        car_id = prefs.getString("CAR_ID", " ");

        Bundle bundle = getIntent().getExtras();
        enum_id = bundle.getString("string");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txt_currency = findViewById(R.id.txt_currency);
        currency = prefs.getString("CURRENCY", " ");
        txt_currency.setText(currency);


        date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        ED_Total_date.setText(date);
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
        String strDate = dateFormat.format(currentTime);
        ED_Total_time.setText(strDate);


        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                Locale.setDefault(Locale.ENGLISH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Act_clean_by_neno_ceramic.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                ED_Total_date.setText(date);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });
        ll_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                mSec = c.get(Calendar.SECOND);

                // Launch Time Picker Dialog
                Locale.setDefault(Locale.ENGLISH);
                TimePickerDialog timePickerDialog = new TimePickerDialog(Act_clean_by_neno_ceramic.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                ED_Total_time.setText(hourOfDay + ":" + minute + ":" + mSec);
                                mHour = hourOfDay;
                                minute = minute;
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        date_time = ED_Total_date.getText().toString() + ED_Total_time.getText().toString();

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_ED_amount = ED_amount.getText().toString();

                if (TextUtils.isEmpty(str_ED_amount)) {
                    Toast.makeText(Act_clean_by_neno_ceramic.this, getString(R.string.please_enter_amount), Toast.LENGTH_SHORT).show();
                } else {
                    addDetail();
                }
            }
        });
    }

    // Add neno ceramic detail in service book
    private void addDetail() {

        APIResponse apiResponse = new APIResponse() {
            @Override
            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {

                Log.e("response", " " + response);


                try {
                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        Toast.makeText(Act_clean_by_neno_ceramic.this, getString(R.string.detail_addes_successfully), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getApplicationContext(), Navigation_Activity.class);
                        startActivity(i);
                        finish();
                    }
//                    JSONArray result=object.getJSONArray("result");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onAPIError(int requestCode, boolean isError, String error) {
                Toast.makeText(getApplicationContext(), getString(R.string.internet_not_connect), Toast.LENGTH_LONG).show();
                Log.e("Tag Error", "" + error);
            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("car_id", car_id);
        params.put("enum_id", enum_id);
        params.put("amount", ED_amount.getText().toString());
        params.put("date_time", date_time);
        Log.e("params", " " + params);


        API api = new API(Act_clean_by_neno_ceramic.this, apiResponse);
        api.execute(1, Services.ADD_SERVICE, params, true);


    }
}
