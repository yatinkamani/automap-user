package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.Request_Model;
import com.example.automap_application.adapter.BookingListAdapter;
import com.example.automap_application.backgroundServices.LocationUpdateService;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Booking_List extends AppCompatActivity implements View.OnClickListener {

    ImageView back_garage_list, img_menu;
    RecyclerView rl_book;
    BookingListAdapter bookingListAdapter;
    ArrayList<Request_Model> arrayList = new ArrayList<>();
    SharedPreferences sharedpreferences;
    String user_id;
    View no_data_error;
    Button btn_retry;
    TextView txt_msg;

    @SuppressLint("StaticFieldLeak")
    public static Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking__list);

        instance = this;

        sharedpreferences = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        user_id = sharedpreferences.getString(getString(R.string.pref_user_id), "");

        back_garage_list = findViewById(R.id.back_garage_list);
        rl_book = findViewById(R.id.rl_book);
        no_data_error = findViewById(R.id.no_data_error);
        btn_retry = findViewById(R.id.btn_retry);
        img_menu = findViewById(R.id.img_menu);
        txt_msg = findViewById(R.id.txt_msg);

        back_garage_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (AppUtils.checkGPSPermissions(this)) {
            startService(new Intent(getApplicationContext(), LocationUpdateService.class));
        } else {
            AppUtils.requestGPSPermissions(this);
        }

        // view 3dot menu to filter request
        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu menu = new PopupMenu(Act_Booking_List.this, img_menu);
                menu.getMenuInflater().inflate(R.menu.sort_menu, menu.getMenu());
                menu.getMenu().findItem(R.id.price).setTitle(getString(R.string.all_request));
                menu.getMenu().findItem(R.id.distance).setTitle(getString(R.string.last_7_day_request)).setVisible(true);
                menu.getMenu().findItem(R.id.time).setTitle(getString(R.string.last_30_day_request));
                menu.getMenu().findItem(R.id.item1).setTitle(getString(R.string.last_1_day_request)).setVisible(true);

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        if (menuItem.getItemId() == R.id.price) {
                            bookingListAdapter.setFilter(arrayList);
                        } else if (menuItem.getItemId() == R.id.distance) {
                            bookingListAdapter.setFilter(FilterDate(-7, arrayList));
                        } else if (menuItem.getItemId() == R.id.time) {
                            bookingListAdapter.setFilter(FilterDate(-30, arrayList));
                        } else if (menuItem.getItemId() == R.id.item1) {
                            bookingListAdapter.setFilter(FilterDate(-1, arrayList));
                        }
                        return true;
                    }
                });
                menu.show();
            }
        });

        GetIntentData();
        getBookingList();
        InitializeBroadcast();
        btn_retry.setOnClickListener(this);
    }

    // notification click to redirect this screen
    public void GetIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            String id = intent.getStringExtra("id");
            if (id != null) {
                Intent in = new Intent(getApplicationContext(), Act_Garage_Offer_List.class);
                in.putExtra("garage_offer", true);
                in.putExtra("request_id", id);
                startActivity(in);
            }
        }
    }

    // get booking appointment list
    private void getBookingList() {

        ProgressDialog dialog = new ProgressDialog(Act_Booking_List.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "user_garage_appointment_list", new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                Log.e("response Data", " " + response);
                try {

                    JSONObject mainJsonObject = new JSONObject(response);
                    Object object = mainJsonObject.get("result");
                    if (object instanceof JSONArray) {
                        JSONArray result = mainJsonObject.getJSONArray("result");
                        arrayList = new ArrayList<>();
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject jsonObject = result.getJSONObject(i);
                            Request_Model request_model = new Request_Model();

                            request_model.setGarage_appointment_id(jsonObject.getString("garage_appointment_id"));
                            request_model.setUser_id(jsonObject.getString("user_id"));
                            request_model.setAppointment_time(jsonObject.getString("appointment_time"));
                            request_model.setAppointment_date(jsonObject.getString("appointment_date"));
                            request_model.setCar_problem(jsonObject.getString("car_problem"));
                            request_model.setBattery_brand(jsonObject.getString("battery_brand"));
                            request_model.setRims_size(jsonObject.getString("rims_size"));
                            request_model.setRims_brand(jsonObject.getString("rims_brand"));
                            request_model.setOil_brand(jsonObject.getString("oil_brand"));
                            request_model.setOil_sae(jsonObject.getString("oil_sae"));
                            request_model.setTire_size_number(jsonObject.getString("tire_size_number"));
                            request_model.setTire_brand(jsonObject.getString("tire_brand"));
                            request_model.setAppointment_type(jsonObject.optString("garage_appointment_type", ""));
                            request_model.setCreated_at(jsonObject.optString("created_at", ""));
                            arrayList.add(request_model);
                        }
                    }

                    Log.e("size==>", " " + arrayList.size());

                    if (arrayList != null && arrayList.size() > 0) {

                        Collections.reverse(arrayList);

                        bookingListAdapter = new BookingListAdapter(Act_Booking_List.this, arrayList);
                        rl_book.setLayoutManager(new GridLayoutManager(Act_Booking_List.this, 1));
                        rl_book.setAdapter(bookingListAdapter);
                        no_data_error.setVisibility(View.GONE);
                        rl_book.setVisibility(View.VISIBLE);
                        if (arrayList.size() > 1) {
                            img_menu.setVisibility(View.VISIBLE);
                        }

                        SharedPreferences notification_badge = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                        SharedPreferences.Editor editor = notification_badge.edit();
                        editor.putInt(getString(R.string.pref_get_quote_badge_count), 0);
                        editor.apply();
                    } else {
                        no_data_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.GONE);
                        txt_msg.setText(getString(R.string.no_any_appointment_found));
                        rl_book.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    no_data_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    rl_book.setVisibility(View.GONE);
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //              displaying the error in toast if occur
//              Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                no_data_error.setVisibility(View.VISIBLE);
                btn_retry.setVisibility(View.VISIBLE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                rl_book.setVisibility(View.GONE);
                Log.e("TAG", error.toString());
                dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry) {
            getBookingList();
        }
    }

    @Override
    public void onBackPressed() {
        stopService(new Intent(getApplicationContext(), LocationUpdateService.class));
        super.onBackPressed();
    }

    public ArrayList<Request_Model> FilterDate(int day, ArrayList<Request_Model> models) {

        ArrayList<Request_Model> filter_models = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH) + day));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(calendar.getTime());

        Date lastDate = null;
        try {
            lastDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("Tag Date", " Last Date 0" + lastDate);

        for (int i = 0; i < models.size(); i++) {

            if (lastDate != null) {
                if (!models.get(i).getCreated_at().equals("") && !models.get(i).getCreated_at().equals("null")) {
                    try {
                        Date created_at = dateFormat.parse(AppUtils.ConvertLocalTimeSame(models.get(i).getCreated_at()));

                        if (created_at.equals(lastDate) || created_at.after(lastDate)) {
                            filter_models.add(models.get(i));
                        }
                        Log.e("Tag Date", "" + created_at + " Last Date " + lastDate);
                        Log.e("Tag Date", "" + created_at.after(lastDate));
                        Log.e("Tag Date", "" + created_at.before(lastDate));

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return filter_models;
    }

    // effect of booking request to refress api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (getQuotes_badge != 0 && !(Act_Booking_List.this.isFinishing())){
                    getBookingList();
                }

                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);
            }
        }
    };

    public void InitializeBroadcast() {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }
}
