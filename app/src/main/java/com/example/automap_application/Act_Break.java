package com.example.automap_application;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class Act_Break extends AppCompatActivity {

    LinearLayout card_front_break,card_back_break,card_change_abs,card_xxx_break;
    ImageView back1;

    ArrayList<String> object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__break);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<String>) args.getSerializable("ARRAYLIST");

        back1 = findViewById(R.id.back_break);
        card_front_break = findViewById(R.id.card_front_break);
        card_back_break = findViewById(R.id.card_back_break);
        card_change_abs = findViewById(R.id.card_change_abs);
        card_xxx_break = findViewById(R.id.card_xxx_break);


        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        card_front_break.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(0);
                Intent intent = new Intent(Act_Break.this, Act_Front_break.class);
                intent.putExtra("string",string);
                startActivity(intent);
            }
        });

        card_back_break.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(1);
                Intent intent = new Intent(Act_Break.this, Act_back_break.class);
                intent.putExtra("string",string);
                startActivity(intent);
            }
        });

        card_change_abs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(2);
                Intent intent = new Intent(Act_Break.this, Act_Change_ABS.class);
                intent.putExtra("string",string);
                startActivity(intent);
            }
        });

        card_xxx_break.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = object.get(3);
                Intent intent = new Intent(Act_Break.this, Act_XXX_break.class);
                intent.putExtra("string",string);
                startActivity(intent);
            }
        });
    }
}
