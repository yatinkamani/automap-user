package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

// not use
public class Act_Petrol_Station extends AppCompatActivity {

    ImageView back;
    EditText ED_Total_date,ED_Total_time;
    private int mHour, mMinute,mSec;
    String date;
    Handler handler;
    AlertDialog.Builder builder;
    private Map map = null;

    // map fragment embedded in this activity
    LinearLayout ll_date,ll_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__petrol__station);

        ED_Total_date = findViewById(R.id.ED_Total_date);
        back = findViewById(R.id.back_gas_payment);
        ED_Total_time = findViewById(R.id.ED_Total_time);
        ll_time = findViewById(R.id.ll_time);
        ll_date = findViewById(R.id.ll_date);

        date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(new Date());
        ED_Total_date.setText(date);
        Date currentTime = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat")
        DateFormat dateFormat = new SimpleDateFormat("hh:mm aaa", Locale.ENGLISH);
        String strDate = dateFormat.format(currentTime);
        ED_Total_time.setText(strDate);

        back.setOnClickListener(view -> {onBackPressed();});

        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                Locale.setDefault(Locale.ENGLISH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Act_Petrol_Station.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                ED_Total_date.setText(date);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });
        ll_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                mSec = c.get(Calendar.SECOND);

                // Launch Time Picker Dialog
                Locale.setDefault(Locale.ENGLISH);
                TimePickerDialog timePickerDialog = new TimePickerDialog(Act_Petrol_Station.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                ED_Total_time.setText(hourOfDay + ":" + minute+":"+mSec);
                                mHour   = hourOfDay;
                                minute = minute;

                                //updateTime(mHour,minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

       /* handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                builder = new AlertDialog.Builder(Act_Petrol_Station.this);
                builder.setTitle(R.string.enter_amount);
                builder.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Act_Petrol_Station.this, Act_daily_petrol_payment.class);
                        startActivity(intent);
                    }
                });
                builder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Act_Petrol_Station.this, Navigation_Activity.class);
                        startActivity(intent);
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

        },60000);*/
    }
}
