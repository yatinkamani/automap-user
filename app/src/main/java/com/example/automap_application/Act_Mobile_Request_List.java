package com.example.automap_application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.GARAGE.GarageNotificationModel;
import com.example.automap_application.Model.GARAGE.Request_Model;
import com.example.automap_application.backgroundServices.LocationUpdateService;
import com.example.automap_application.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.view.View.GONE;
import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Mobile_Request_List extends AppCompatActivity implements View.OnClickListener {

    ImageView back_garage_list, img_menu;
    RecyclerView rl_book;
    BookingListAdapter bookingListAdapter;
    ArrayList<GarageNotificationModel> arrayList = new ArrayList<>();
    SharedPreferences sharedpreferences;
    String user_id;
    View no_data_error;
    Button btn_retry;
    TextView txt_msg;

    @SuppressLint("StaticFieldLeak")
    public Activity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__mobile__request__list);

        instance = this;

        sharedpreferences = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        user_id = sharedpreferences.getString(getString(R.string.pref_user_id), "");

        back_garage_list = findViewById(R.id.back_garage_list);
        img_menu = findViewById(R.id.img_menu);
        rl_book = findViewById(R.id.rl_book);
        no_data_error = findViewById(R.id.no_data_error);
        btn_retry = findViewById(R.id.btn_retry);
        txt_msg = findViewById(R.id.txt_msg);

        GetIntentData();
        btn_retry.setOnClickListener(this);
        back_garage_list.setOnClickListener(this);
        img_menu.setOnClickListener(this);
        startService(new Intent(getApplicationContext(), LocationUpdateService.class));
        getMobileRequestList();
        InitializeBroadcast();
    }

    public void GetIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            String id = intent.getStringExtra("id");
            if (id != null) {

                Intent in = new Intent(getApplicationContext(), Act_Mobile_Request_Offers_List.class);
                in.putExtra("mobile_offer", true);
                in.putExtra("request_id", id);
                startActivity(in);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry) {
            getMobileRequestList();
        } else if (v == back_garage_list) {
            onBackPressed();
        }
        // mobile request filter popup menu
        else if (v == img_menu) {
            PopupMenu menu = new PopupMenu(Act_Mobile_Request_List.this, img_menu);
            menu.getMenuInflater().inflate(R.menu.sort_menu, menu.getMenu());
            menu.getMenu().findItem(R.id.price).setTitle(getString(R.string.all_request));
            menu.getMenu().findItem(R.id.distance).setTitle(getString(R.string.last_7_day_request)).setVisible(true);
            menu.getMenu().findItem(R.id.time).setTitle(getString(R.string.last_30_day_request));
            menu.getMenu().findItem(R.id.item1).setTitle(getString(R.string.last_1_day_request)).setVisible(true);

            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    if (menuItem.getItemId() == R.id.price) {
                        bookingListAdapter.setFilter(arrayList);
                    } else if (menuItem.getItemId() == R.id.distance) {
                        bookingListAdapter.setFilter(FilterDate(-7, arrayList));
                    } else if (menuItem.getItemId() == R.id.time) {
                        bookingListAdapter.setFilter(FilterDate(-30, arrayList));
                    } else if (menuItem.getItemId() == R.id.item1) {
                        bookingListAdapter.setFilter(FilterDate(-1, arrayList));
                    }
                    return true;
                }
            });
            menu.show();
        }
    }

    // get all sanded mobile request and list view and clear notification
    private void getMobileRequestList() {

        ProgressDialog dialog = new ProgressDialog(Act_Mobile_Request_List.this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL + "mobile_request_service", new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                Log.e("Mobile Request Response", " " + response);
                response = Html.fromHtml(response).toString();
                response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                try {

                    JSONObject mainJsonObject = new JSONObject(response);

                    if (mainJsonObject.getString("status").equalsIgnoreCase("1")) {

                        JSONArray result = mainJsonObject.getJSONArray("result");
                        arrayList = new ArrayList<>();
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject jsonObject = result.getJSONObject(i);
                            GarageNotificationModel notification_model = new GarageNotificationModel();

                            notification_model.setGarage_appointment_id(jsonObject.getString("mobile_request_id"));
                            notification_model.setName(jsonObject.getString("service_name"));
                            notification_model.setAppointment_time(jsonObject.getString("request_time"));
                            notification_model.setAppointment_date(jsonObject.getString("request_date"));
                            notification_model.setTimeout_status(jsonObject.getString("timeout_status"));
                            notification_model.setCreated_at(jsonObject.getString("created_at"));

                            arrayList.add(notification_model);
                        }

                        Log.e("size==>", " " + arrayList.size());

                        if (arrayList != null) {
                            if (arrayList.size() > 0) {

                                bookingListAdapter = new BookingListAdapter(Act_Mobile_Request_List.this, arrayList);
                                rl_book.setLayoutManager(new GridLayoutManager(Act_Mobile_Request_List.this, 1));
                                rl_book.setAdapter(bookingListAdapter);
                                no_data_error.setVisibility(GONE);
                                rl_book.setVisibility(View.VISIBLE);

                                SharedPreferences notification_badge = getSharedPreferences(getString(R.string.pref_Badge), MODE_PRIVATE);
                                SharedPreferences.Editor editor = notification_badge.edit();
                                editor.putInt(getString(R.string.pref_mobile_request_badge_count), 0);
                                editor.apply();

                                if (arrayList.size() > 1) {
                                    img_menu.setVisibility(View.VISIBLE);
                                }

                            } else {
                                no_data_error.setVisibility(View.VISIBLE);
                                btn_retry.setVisibility(GONE);
                                txt_msg.setText(getString(R.string.no_data_found));
                                rl_book.setVisibility(GONE);
                            }
                        }
                    } else {
                        no_data_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(GONE);
                        txt_msg.setText(getString(R.string.no_data_found));
                        rl_book.setVisibility(GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    no_data_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.somting_wrong_please));
                    rl_book.setVisibility(GONE);
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onErrorResponse(VolleyError error) {
                no_data_error.setVisibility(View.VISIBLE);
                btn_retry.setVisibility(View.VISIBLE);
                txt_msg.setText(getString(R.string.somting_wrong_please));
                rl_book.setVisibility(GONE);
                Log.e("TAG", error.toString());
                dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        stopService(new Intent(getApplicationContext(), LocationUpdateService.class));
        super.onBackPressed();
    }

    // adapter of booking adapter
    class BookingListAdapter extends RecyclerView.Adapter<BookingListAdapter.MyViewHolder> {

        private Activity activity;
        private ArrayList<GarageNotificationModel> booking_models;

        public BookingListAdapter(Activity activity, ArrayList<GarageNotificationModel> booking_models) {
            this.activity = activity;
            this.booking_models = booking_models;
        }

        public void setFilter(ArrayList<GarageNotificationModel> models) {
            booking_models = new ArrayList<>();
            booking_models.addAll(models);
            notifyDataSetChanged();
        }

        @SuppressLint("SetTextI18n")
        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_mobile_request_list, viewGroup, false);

            return new MyViewHolder(view);

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
            GarageNotificationModel model = booking_models.get(i);

            txt_name.setText("  " + model.getName());
            txt_request_date.setText(getString(R.string.request_date) + DateFormat(model.getAppointment_date()));
            if (TimeFormat(model.getAppointment_time()).equals("1")) {
                txt_request_time.setVisibility(GONE);
            } else {
                txt_request_time.setText(getString(R.string.request_time) + TimeFormat(model.getAppointment_time()));
            }

            if (model.getTimeout_status().equals("0")) {
                txt_timeout_status.setVisibility(GONE);
            } else {
                txt_timeout_status.setText(" Status   :  your request has been timeout");
            }

            llDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(activity, Act_Mobile_Request_Offers_List.class);
                    in.putExtra("mobile_offer", true);
                    in.putExtra("request_id", model.getGarage_appointment_id());
                    activity.startActivity(in);
                }
            });
        }

        @Override
        public int getItemCount() {
            return booking_models.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private TextView txt_name, txt_request_date, txt_request_time, txt_timeout_status;
        private LinearLayout llDetail;

        class MyViewHolder extends RecyclerView.ViewHolder {
            private MyViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_name = itemView.findViewById(R.id.txt_name);
                txt_request_date = itemView.findViewById(R.id.txt_request_date);
                txt_request_time = itemView.findViewById(R.id.txt_request_time);
                txt_timeout_status = itemView.findViewById(R.id.txt_timeout_status);
                llDetail = itemView.findViewById(R.id.llDetail);

            }
        }

        private String TimeFormat(String time) {
            String aTime = "1";
            if (time.split(":").length > 1) {
                int hour = Integer.parseInt(time.split(":")[0]);
                int minute = Integer.parseInt(time.split(":")[1]);
                int minutes = minute;
                String timeSet = "";
                if (hour > 12) {
                    hour -= 12;
                    timeSet = "PM";
                } else if (hour == 0) {
                    hour += 12;
                    timeSet = "AM";
                } else if (hour == 12) {
                    timeSet = "PM";
                } else {
                    timeSet = "AM";
                }

                String min = "";
                if (minutes < 10)
                    min = "0" + minutes;
                else
                    min = String.valueOf(minutes);
                Log.e("TAG AM PM", timeSet);
                Log.e("TAG Hour ", "" + hour);
                Log.e("TAG Minute", "" + min);

                aTime = new StringBuilder().append(hour).append(':')
                        .append(min).append(" ").append(timeSet).toString();
                Log.e("TAG ATime", "" + aTime);
            }

            return aTime;
        }

        private String DateFormat(String Date) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd, MMM yyyy", Locale.ENGLISH);
            java.util.Date date = null;
            try {
                date = format.parse(Date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return newFormat.format(date);

        }
    }

    public ArrayList<GarageNotificationModel> FilterDate(int day, ArrayList<GarageNotificationModel> models) {

        ArrayList<GarageNotificationModel> filter_models = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH) + day));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(calendar.getTime());

        Date lastDate = null;
        try {
            lastDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("Tag Date", " Last Date 0" + lastDate);

        for (int i = 0; i < models.size(); i++) {

            if (lastDate != null) {
                if (!models.get(i).getCreated_at().equals("") && !models.get(i).getCreated_at().equals("null")) {
                    try {
                        Date created_at = dateFormat.parse(AppUtils.ConvertLocalTimeSame(models.get(i).getCreated_at()));

                        if (created_at.equals(lastDate) || created_at.after(lastDate)) {
                            filter_models.add(models.get(i));
                        }
                        Log.e("Tag Date", "" + created_at + " Last Date " + lastDate);
                        Log.e("Tag Date", "" + created_at.after(lastDate));
                        Log.e("Tag Date", "" + created_at.before(lastDate));


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return filter_models;
    }

    public void InitializeBroadcast(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(Act_Mobile_Request_List.this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("send_data");
        manager.registerReceiver(broadcastReceiver, filter);
    }

    // new request come to refress api
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("send_data")) {
                int getQuotes_badge = intent.getIntExtra(getString(R.string.pref_get_quote_badge_count), 0);
                int mobile_request_badge = intent.getIntExtra(getString(R.string.pref_mobile_request_badge_count), 0);
                int tow_track_badge = intent.getIntExtra(getString(R.string.pref_towtrack_request_badge_count), 0);
                int admin_count = intent.getIntExtra(getString(R.string.pref_admin_msg_badge_count), 0);

                if (mobile_request_badge > 0 && !(Act_Mobile_Request_List.this.isFinishing())) {
                   getMobileRequestList();
                }
                Log.e("Tag getQuotes", "" + getQuotes_badge);
                Log.e("Tag Mobile Request", "" + mobile_request_badge);
                Log.e("Tag Tow Truck", "" + tow_track_badge);
                Log.e("Tag Admin Message", "" + admin_count);

            }
        }
    };
}
