package com.example.automap_application;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.automap_application.Constant.Constant;
import com.example.automap_application.Model.CAR.Enum_id_model;
import com.example.automap_application.utils.AppUtils;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_Service_book extends AppCompatActivity {

    LinearLayout c_oil, c_carwash, c_NGK, c_break, c_battery, c_filter, c_sensors, c_suspenssion, c_wheels, c_engine, c_glass, c_keys, c_accessory, c_expenssion;
    //  String url = "http://webmobdemo.xyz/automap/api/get_enum";
    ArrayList<Enum_id_model> enum_id_list = new ArrayList<Enum_id_model>();

    ArrayList<String> oil_enum = new ArrayList<String>();
    ArrayList<String> Car_wash_and_polishing_list = new ArrayList<String>();
    ArrayList<String> NGK_and_Sprays = new ArrayList<String>();
    ArrayList<String> Breaks_ABS = new ArrayList<String>();
    ArrayList<String> Battery = new ArrayList<String>();
    ArrayList<String> Filters = new ArrayList<String>();
    ArrayList<String> Sensors = new ArrayList<String>();
    ArrayList<String> Suspension_system = new ArrayList<String>();
    ArrayList<String> Wheels = new ArrayList<String>();
    ArrayList<String> Engine_Gear_AC = new ArrayList<String>();
    ArrayList<String> Glass = new ArrayList<String>();
    ArrayList<String> Key_Winsh = new ArrayList<String>();
    ArrayList<String> Accessories = new ArrayList<String>();
    ArrayList<String> Varient_expenses = new ArrayList<String>();
    ArrayList<String> Car_seats = new ArrayList<String>();
    SharedPreferences prefs;
    ProgressDialog progressDialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
//        applyOverrideConfiguration(new Configuration());
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            //Use you logic to update overrideConfiguration locale
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String lng = preferences.getString(getString(R.string.pref_language), "ar");
            Log.e("Tag LANG+++\n", lng);
            Locale locale = new Locale(lng);//your own implementation here
            overrideConfiguration.setLocale(locale);
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);

        String language = prefs.getString(getString(R.string.pref_language), "ar").toString().trim();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_act__service_book);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));

        c_oil = findViewById(R.id.Card_oil);
        c_carwash = findViewById(R.id.card_carWash);
        c_NGK = findViewById(R.id.card_NGK);
        c_break = findViewById(R.id.card_break);
        c_battery = findViewById(R.id.card_bettery);
        c_filter = findViewById(R.id.card_filter);
        c_sensors = findViewById(R.id.card_sensors);
        c_suspenssion = findViewById(R.id.card_suspension);
        c_wheels = findViewById(R.id.card_wheels);
        c_engine = findViewById(R.id.card_Engine);
        c_glass = findViewById(R.id.card_glass);
        c_keys = findViewById(R.id.card_keys);
        c_accessory = findViewById(R.id.card_accessories);
        c_expenssion = findViewById(R.id.card_variant);

        ImageView back = findViewById(R.id.back_service_book);

        RoundedImageView oil = findViewById(R.id.img_oil);
        RoundedImageView car_wash = findViewById(R.id.img_car_wash);
        RoundedImageView ngk = findViewById(R.id.img_ngk);
        RoundedImageView Break = findViewById(R.id.img_break);
        RoundedImageView filter = findViewById(R.id.img_filter);
        RoundedImageView sensor = findViewById(R.id.img_sensor);
        RoundedImageView suspension = findViewById(R.id.img_suspenssion);
        RoundedImageView wheels = findViewById(R.id.img_wheel);
        RoundedImageView engine = findViewById(R.id.img_engine);
        RoundedImageView glass = findViewById(R.id.img_glass);
        RoundedImageView key = findViewById(R.id.img_keys);
        RoundedImageView accessory = findViewById(R.id.img_accessories);
        RoundedImageView exception = findViewById(R.id.img_variant);

        Glide.with(this).load((R.drawable.oil)).into(oil);
        Glide.with(this).load((R.drawable.car_wash)).into(car_wash);
        Glide.with(this).load((R.drawable.ngk_spray)).into(ngk);
        Glide.with(this).load((R.drawable.battery)).into(Break);
        Glide.with(this).load((R.drawable.filters)).into(filter);
        Glide.with(this).load((R.drawable.sensor)).into(sensor);
        Glide.with(this).load((R.drawable.car_suspension)).into(suspension);
        Glide.with(this).load((R.drawable.wheels)).into(wheels);
        Glide.with(this).load((R.drawable.engins)).into(engine);
        Glide.with(this).load((R.drawable.glass)).into(glass);
        Glide.with(this).load((R.drawable.key)).into(key);
        Glide.with(this).load((R.drawable.accessory)).into(accessory);
        Glide.with(this).load((R.drawable.variant_expenceve)).into(exception);

        getEnum();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        c_oil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Act_Service_book.this, Act_Oil.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) oil_enum);
                intent.putExtra("BUNDLE", args);
                startActivity(intent);
            }
        });

        c_carwash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Act_Service_book.this, Act_car_wash_and_polishing.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) Car_wash_and_polishing_list);
                intent.putExtra("BUNDLE", args);
                startActivity(intent);
            }
        });

        c_NGK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Act_Service_book.this, Act_NGK_Sprays.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) NGK_and_Sprays);
                intent.putExtra("BUNDLE", args);
                startActivity(intent);
            }
        });

        c_break.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Act_Service_book.this, Act_Break.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) Breaks_ABS);
                intent.putExtra("BUNDLE", args);
                startActivity(intent);
            }
        });

        c_battery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Battery != null) {
                    if (Battery.size() > 0) {
                        Intent intent = new Intent(Act_Service_book.this, Act_battery_type.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Battery);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Filters != null) {
                    if (Filters.size() > 0) {

                        Intent intent = new Intent(Act_Service_book.this, Act_filter.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Filters);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_sensors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Sensors != null) {
                    if (Sensors.size() > 0) {

                        Intent intent = new Intent(Act_Service_book.this, Act_sensors.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Sensors);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_suspenssion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Suspension_system != null) {
                    if (Suspension_system.size() > 0) {
                        Intent intent = new Intent(Act_Service_book.this, Act_suspension_system.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Suspension_system);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_wheels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Wheels != null) {
                    if (Wheels.size() > 0) {
                        Intent intent = new Intent(Act_Service_book.this, Act_wheels.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Wheels);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_engine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Engine_Gear_AC != null) {
                    if (Engine_Gear_AC.size() > 0) {

                        Intent intent = new Intent(Act_Service_book.this, Act_engine.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Engine_Gear_AC);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_keys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Key_Winsh != null) {
                    if (Key_Winsh.size() > 0) {
                        Intent intent = new Intent(Act_Service_book.this, Act_key_winsh.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Key_Winsh);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_glass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Glass != null) {
                    if (Glass.size() > 0) {

                        Intent intent = new Intent(Act_Service_book.this, Act_glass.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Glass);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_accessory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Accessories != null) {
                    if (Accessories.size() > 0) {

                        Intent intent = new Intent(Act_Service_book.this, Act_accessories.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Accessories);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });

        c_expenssion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Varient_expenses != null) {
                    if (Varient_expenses.size() > 0) {

                        Intent intent = new Intent(Act_Service_book.this, Act_varient_expenses.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST", (Serializable) Varient_expenses);
                        intent.putExtra("BUNDLE", args);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    String enum_id;
    String type;

    // all service book option and display
    private void getEnum() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            StringRequest request = new StringRequest(Request.Method.GET, Constant.URL + "get_enum", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("response", " " + response);
                    try {

                        JSONObject obj = new JSONObject(response);
                        JSONArray result = obj.getJSONArray("result");
                        if (obj.optString("status").equals("1")) {

                            for (int i = 0; i < result.length(); i++) {
                                JSONObject jsonObject = result.getJSONObject(i);

                                enum_id = jsonObject.getString("enum_id");
                                type = jsonObject.getString("type");
                                String value = jsonObject.getString("value");

                                enum_id_list.add(new Enum_id_model(enum_id, type));

                                if (type.equals("Oils")) {
                                    oil_enum.add(enum_id);
                                    Log.e("enum_id_list", " " + oil_enum.size());
                                }
                                if (type.equals("Car wash and polishing")) {
                                    Car_wash_and_polishing_list.add(enum_id);
                                    Log.e("Car_wash_and_polishing", " " + Car_wash_and_polishing_list.size());
                                }
                                if (type.equals("NGK and Sprays")) {
                                    NGK_and_Sprays.add(enum_id);
                                    Log.e("NGK_and_Sprays", " " + NGK_and_Sprays.size());
                                }
                                if (type.equals("Breaks(ABS)")) {
                                    Breaks_ABS.add(enum_id);
                                    Log.e("Breaks(ABS)", " " + Breaks_ABS.size());
                                }
                                if (type.equals("Battery")) {
                                    Battery.add(enum_id);
                                    Log.e("Battery", " " + Battery.size());
                                }
                                if (type.equals("Filters")) {
                                    Filters.add(enum_id);
                                    Log.e("Filters", " " + Filters.size());
                                }
                                if (type.equals("Sensors")) {
                                    Sensors.add(enum_id);
                                    Log.e("Sensors", " " + Sensors.size());
                                }
                                if (type.equals("Suspension system")) {
                                    Suspension_system.add(enum_id);
                                    Log.e("Suspension system", " " + Suspension_system.size());
                                }
                                if (type.equals("Wheels")) {
                                    Wheels.add(enum_id);
                                    Log.e("Wheels", " " + Wheels.size());
                                }
                                if (type.equals("Engine/Gear/AC")) {
                                    Engine_Gear_AC.add(enum_id);
                                    Log.e("Engine/Gear/AC", " " + Engine_Gear_AC.size());
                                }
                                if (type.equals("Glass")) {
                                    Glass.add(enum_id);
                                    Log.e("Glass", " " + Glass.size());
                                }
                                if (type.equals("Key/Winsh")) {
                                    Key_Winsh.add(enum_id);
                                    Log.e("Key/Winsh", " " + Key_Winsh.size());
                                }
                                if (type.equals("Accessories")) {
                                    Accessories.add(enum_id);
                                    Log.e("Accessories", " " + Accessories.size());
                                }
                                if (type.equals("Varient expenses")) {
                                    Varient_expenses.add(enum_id);
                                    Log.e("Variant expenses", " " + Varient_expenses.size());
                                }
                                if (type.equals("Car seats")) {
                                    Car_seats.add(enum_id);
                                    Log.e("Variant expenses", " " + Car_seats.size());
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(Act_Service_book.this, "my error :" + error, Toast.LENGTH_LONG).show();
                    Log.i("My error", "" + error);
                    progressDialog.dismiss();
                }
            });

            Volley.newRequestQueue(getApplicationContext()).add(request);
             progressDialog.show();
            request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {
            showNetDisabledAlertToUser(getApplicationContext());
        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
//        getEnum();
        super.onResume();
    }

    public void showNetDisabledAlertToUser(final Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Would you like to enable it?")
                .setTitle("No Internet Connection")
                .setPositiveButton(" Enable Internet ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(dialogIntent);
                    }
                });

        alertDialogBuilder.setNegativeButton(" Cancel ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}
