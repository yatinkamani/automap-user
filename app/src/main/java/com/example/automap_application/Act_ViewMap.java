package com.example.automap_application;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.automap_application.newPackage.DirectionObject;
import com.example.automap_application.newPackage.GsonRequest;
import com.example.automap_application.newPackage.Helper;
import com.example.automap_application.newPackage.LegsObject;
import com.example.automap_application.newPackage.PolylineObject;
import com.example.automap_application.newPackage.RouteObject;
import com.example.automap_application.newPackage.StepsObject;
import com.example.automap_application.utils.VolleySingleton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Act_ViewMap extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks {

    private MarkerOptions place1, place2;
    private GoogleMap mMap;
    private Polyline currentPolyline;

    String latitude1;
    String longitude1;
    String latitude2;
    String longitude2;
    Button btn_direction;

    private List<LatLng> latLngList;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private static final int PERMISSION_LOCATION_REQUEST_CODE = 100;
    private double latitudeValue = 0.0;
    private double longitudeValue = 0.0;
    private MarkerOptions yourLocationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__view_map);
        latLngList = new ArrayList<LatLng>();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addApi(LocationServices.API).build();
        }

        mLocationRequest = createLocationRequest();
        latitude1 = getIntent().getStringExtra("latitude1");
        longitude1 = getIntent().getStringExtra("longitude1");
        latitude2 = getIntent().getStringExtra("latitude2");
        longitude2 = getIntent().getStringExtra("longitude2");

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Geocoder geocoder;
        List<Address> addresses = null;
        List<Address> addresses2 = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(latitude1), Double.parseDouble(longitude1), 1);

//          Here 1 represent max location result to returned, by documents it recommended 1 to 5

            addresses2 = geocoder.getFromLocation(Double.parseDouble(latitude2), Double.parseDouble(longitude2), 1);

        } catch (IOException e) {
            e.printStackTrace();
        }

        String address1 = addresses.get(0).getAddressLine(0);
        String address2 = addresses2.get(0).getAddressLine(0);

        place1 = new MarkerOptions().position(new LatLng(Double.parseDouble(latitude1), Double.parseDouble(longitude1))).title(address1);
        place2 = new MarkerOptions().position(new LatLng(Double.parseDouble(latitude2), Double.parseDouble(longitude2))).title(address2);

        btn_direction = (Button) findViewById(R.id.btn_direction);
    }

    // diaplay map path on trip
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d("my log", "Added Markers");
        mMap.addMarker(place1);
        mMap.addMarker(place2);

        /*CameraUpdate cameraUpdate =CameraUpdateFactory.newLatLngBounds(builder.build(), 100);
            mMap.moveCamera(cameraUpdate);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(place1.getPosition()));

            LatLng nrLatLng = mMap.getProjection().getVisibleRegion().nearRight;
            LatLng frLatLng = mMap.getProjection().getVisibleRegion().farRight;
            LatLng nlLatLng = mMap.getProjection().getVisibleRegion().nearLeft;
            LatLng flLatLng = mMap.getProjection().getVisibleRegion().farLeft;

            builder.include(nrLatLng);
            builder.include(frLatLng);
            builder.include(nlLatLng);
            builder.include(flLatLng);

            cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), 100);
            mMap.animateCamera(cameraUpdate);*/

        String directionApiPath = Helper.getUrl(latitude1, longitude1, latitude2, longitude2);
        getDirectionFromDirectionApiServer(directionApiPath);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                            if (mLastLocation != null) {
                                assignLocationValues(mLastLocation);
                                setDefaultMarkerOption(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                            }
                        } else {
                            ActivityCompat.requestPermissions(Act_ViewMap.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION_REQUEST_CODE);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void assignLocationValues(Location currentLocation) {
        if (currentLocation != null) {
            latitudeValue = currentLocation.getLatitude();
            longitudeValue = currentLocation.getLongitude();
            markStartingLocationOnMap(mMap, new LatLng(latitudeValue, longitudeValue));
            addCameraToMap(new LatLng(latitudeValue, longitudeValue));
        }
    }

    private void addCameraToMap(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void markStartingLocationOnMap(GoogleMap mapObject, LatLng location) {
        mapObject.addMarker(new MarkerOptions().position(location).title("Current location"));
        mapObject.moveCamera(CameraUpdateFactory.newLatLng(location));
    }

    private void refreshMap(GoogleMap mapInstance) {
        mapInstance.clear();
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    private void setDefaultMarkerOption(LatLng location) {
        if (yourLocationMarker == null) {
            yourLocationMarker = new MarkerOptions();
        }
        yourLocationMarker.position(location);
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    public void getDirectionFromDirectionApiServer(String url) {
        GsonRequest<DirectionObject> serverRequest = new GsonRequest<DirectionObject>(Request.Method.GET, url, DirectionObject.class, createRequestSuccessListener(), createRequestErrorListener());
        serverRequest.setRetryPolicy(new DefaultRetryPolicy(Helper.MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(serverRequest);
    }

    private Response.Listener<DirectionObject> createRequestSuccessListener() {
        return new Response.Listener<DirectionObject>() {

            @Override
            public void onResponse(DirectionObject response) {
                try {
                    Log.d("JSON Response", response.toString());
                    Log.e("RESPONSE", " " + response.getStatus());

                    if (response.getStatus().equals("OK")) {
                        List<LatLng> mDirections = getDirectionPolyLines(response.getRoutes());
                        drawRouteOnMap(mMap, mDirections);
                    } else {
                        Toast.makeText(Act_ViewMap.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private List<LatLng> getDirectionPolyLines(List<RouteObject> routes) {
        List<LatLng> directionList = new ArrayList<LatLng>();
        for (RouteObject route : routes) {
            List<LegsObject> legs = route.getLegs();
            for (LegsObject leg : legs) {
                List<StepsObject> steps = leg.getSteps();
                for (StepsObject step : steps) {
                    PolylineObject polyline = step.getPolyline();
                    String points = polyline.getPoints();
                    List<LatLng> singlePolyline = decodePoly(points);
                    for (LatLng direction : singlePolyline) {
                        directionList.add(direction);
                    }
                }
            }
        }
        return directionList;
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private void drawRouteOnMap(GoogleMap map, List<LatLng> positions) {
        PolylineOptions options = new PolylineOptions().width(10).color(Color.RED).geodesic(true);
        options.addAll(positions);
        Polyline polyline = map.addPolyline(options);
       /* LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(place1.getPosition());
        builder.include(place2.getPosition());
        LatLngBounds bounds = builder.build();

        Toast.makeText(getApplicationContext(),""+positions,Toast.LENGTH_LONG ).show();

        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,20));*/
        MakeCameraFocus(place1, place2);
        //      CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(positions.get(1).latitude, positions.get(1).longitude)).zoom(20).build()
        //      map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    /*
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    private void MakeCameraFocus(/*String userFromLat, String userFromLong, String userToLat, String userToLong*/ MarkerOptions place1, MarkerOptions place2) {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        final LatLng srcLatLng = new LatLng(Double.parseDouble(userFromLat), Double.parseDouble(userFromLong));
//        final LatLng destLatLng = new LatLng(Double.parseDouble(userToLat), Double.parseDouble(userToLong));
        builder.include(place1.getPosition());
        builder.include(place2.getPosition());
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        int pinShowTime = getResources().getInteger(R.integer.map_pin_time);
        mMap.animateCamera(cameraUpdate, pinShowTime, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
//                mDistance = getDistance(srcLatLng, destLatLng);
                //     mTotalDistance.setText("Distance" + mDistance + " Meters");
            }

            @Override
            public void onCancel() {
            }
        });
    }

}
/*  mMap.addPolyline(new PolylineOptions().add(new LatLng(Double.parseDouble(latitude1), Double.parseDouble(longitude1)), new LatLng(Double.parseDouble(latitude2), Double.parseDouble(longitude2))).width(5).color(Color.RED));*/






