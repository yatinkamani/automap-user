package com.example.automap_application;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.automap_application.backgroundServices.LocationService;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.DecimalFormat;

import static com.example.automap_application.Act_Login.LOGIN_PREF;

public class Act_start_trip extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks {

    public CardView card_start_trip;
    public CardView card_end_trip;
    Button btn_view_map;

    private FusedLocationProviderClient fusedLocationClient;
    Location location1 = null;
    Location location2;
    TextView txt_distance, txt_total, txt_cost_of_1_km;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    boolean start = false;
    boolean end = false;
    //    float p_c_r = 0;
    float distance = 0;

    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_start_trip);
        prefs = getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE);
        editor = prefs.edit();

        LocationManager manager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = manager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = manager.getLastKnownLocation(provider);

        manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1, this);
        manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        card_start_trip = (CardView) findViewById(R.id.card_start_trip);
        card_end_trip = (CardView) findViewById(R.id.card_end_trip);
        txt_distance = (TextView) findViewById(R.id.txt_distance);
        txt_total = (TextView) findViewById(R.id.txt_total);
        txt_cost_of_1_km = (TextView) findViewById(R.id.txt_cost_of_1_km);
        btn_view_map = (Button) findViewById(R.id.btn_view_map);

        String pcr = (prefs.getString("pcr", "0"));
        txt_cost_of_1_km.setText(pcr);

        // start trip and store preferance current location
        card_start_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!prefs.getBoolean("start", false)) {
                    start = true;
                    end = false;
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        showGPSDisabledAlertToUser();
                    }
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                    builder.addLocationRequest(createLocationRequest());
                    LocationSettingsRequest locationSettingsRequest = builder.build();

                    SettingsClient settingsClient = LocationServices.getSettingsClient(Act_start_trip.this);
                    settingsClient.checkLocationSettings(locationSettingsRequest);
                    fusedLocationClient.getLastLocation().addOnSuccessListener(Act_start_trip.this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                Toast.makeText(Act_start_trip.this, getString(R.string.your_trip_is_start_now), Toast.LENGTH_SHORT).show();
                                location1 = location;
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("LATITUDE1", String.valueOf(location1.getLatitude()));
                                editor.putString("LONGITUDE1", String.valueOf(location1.getLongitude()));
                                editor.apply();
                                // Logic to handle location object
                            } else {
                                if (ActivityCompat.checkSelfPermission(Act_start_trip.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Act_start_trip.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    ActivityCompat#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for ActivityCompat#requestPermissions for more details.
                                    return;
                                }
                                Location loc = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (loc != null) {
                                    Toast.makeText(Act_start_trip.this, getString(R.string.your_trip_is_start_now)+" 2", Toast.LENGTH_SHORT).show();
                                    location1 = loc;
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putString("LATITUDE1", String.valueOf(location1.getLatitude()));
                                    editor.putString("LONGITUDE1", String.valueOf(location1.getLongitude()));
                                    editor.apply();
                                } else {
                                    loc = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    if (loc != null) {
                                        Toast.makeText(Act_start_trip.this, getString(R.string.your_trip_is_start_now)+" 3", Toast.LENGTH_SHORT).show();
                                        location1 = loc;
                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.putString("LATITUDE1", String.valueOf(location1.getLatitude()));
                                        editor.putString("LONGITUDE1", String.valueOf(location1.getLongitude()));
                                        editor.apply();
                                    }
                                }
                            }
                            Intent intent = new Intent(getApplicationContext(), LocationService.class);
                            intent.putExtra("latitude", location1.getLatitude());
                            intent.putExtra("longitude", location1.getLatitude());
                            intent.putExtra("start", start);
                            intent.putExtra("end", end);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("LATITUDE1", String.valueOf(location1.getLatitude()));
                            editor.putString("LONGITUDE1", String.valueOf(location1.getLongitude()));
                            editor.putBoolean("start", start);
                            editor.putBoolean("end", end);
                            editor.apply();
                            startService(intent);
                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.already_start_end_the_trip_after_start), Toast.LENGTH_LONG).show();
                }
            }
        });

        // if trip is started then end trip
        card_end_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!prefs.getBoolean("start", false)) {
                    Toast.makeText(Act_start_trip.this, getString(R.string.please_start_your_trip), Toast.LENGTH_SHORT).show();
                } else {
                    end = true;
//                    if (!start) {
//                        Toast.makeText(Act_start_trip.this, "Please start your trip", Toast.LENGTH_SHORT).show();
//                    } else {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        showGPSDisabledAlertToUser();
                    }
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                    builder.addLocationRequest(createLocationRequest());
                    LocationSettingsRequest locationSettingsRequest = builder.build();

                    SettingsClient settingsClient = LocationServices.getSettingsClient(Act_start_trip.this);
                    settingsClient.checkLocationSettings(locationSettingsRequest);

                    double lat = Double.parseDouble(prefs.getString("LATITUDE1", "0.00"));
                    double log = Double.parseDouble(prefs.getString("LONGITUDE1", "0.00"));
                    Log.e("TAG", "" + lat);
                    Log.e("TAG", "" + log);
                    location1 = new Location("gps");
                    location1.setLatitude(lat);
                    location1.setLongitude(log);

                    fusedLocationClient.getLastLocation().addOnSuccessListener(Act_start_trip.this, new OnSuccessListener<Location>() {
                        @SuppressLint({"DefaultLocale", "SetTextI18n"})
                        @Override
                        public void onSuccess(Location location) {
                            Toast.makeText(Act_start_trip.this, getString(R.string.your_trip_is_ended), Toast.LENGTH_SHORT).show();
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                location2 = new Location("gps");
                                location2.setLatitude(location.getLatitude());
                                location2.setLongitude(location.getLongitude());
                                // Logic to handle location object
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("LATITUDE2", String.valueOf(location.getLatitude()));
                                editor.putString("LONGITUDE2", String.valueOf(location.getLongitude()));
                                editor.apply();
                                distance = (location1.distanceTo(location2)) / 1000;
                                txt_distance.setText(new DecimalFormat("#.##").format(distance) + "km");
                            } else {
                                if (ActivityCompat.checkSelfPermission(Act_start_trip.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Act_start_trip.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    ActivityCompat#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for ActivityCompat#requestPermissions for more details.
                                    return;
                                }
                                Location loc = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (loc != null) {
                                    location2 = new Location("");
                                    location2.setLatitude(loc.getLatitude());
                                    location2.setLongitude(loc.getLongitude());
                                    // Logic to handle location object
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putString("LATITUDE2", String.valueOf(loc.getLatitude()));
                                    editor.putString("LONGITUDE2", String.valueOf(loc.getLongitude()));
                                    editor.apply();
                                    distance = (location1.distanceTo(location2)) / 1000;
                                    txt_distance.setText(new DecimalFormat("#.##").format(distance) + "km");
                                } else {
                                    loc = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    if (loc != null) {
                                        location2 = new Location("");
                                        location2.setLatitude(loc.getLatitude());
                                        location2.setLongitude(loc.getLongitude());
                                        // Logic to handle location object
                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.putString("LATITUDE2", String.valueOf(loc.getLatitude()));
                                        editor.putString("LONGITUDE2", String.valueOf(loc.getLongitude()));
                                        editor.apply();
                                        distance = (location1.distanceTo(location2)) / 1000;
                                        txt_distance.setText(new DecimalFormat("#.##").format(distance) + "km");
                                    }
                                }
                            }
                            Log.e("TAG distance", "" + distance);
                            txt_total.setText("" + (distance) * (Float.parseFloat(txt_cost_of_1_km.getText().toString())));
                            Intent intent = new Intent(getApplicationContext(), LocationService.class);
                            intent.putExtra("latitude", location2.getLatitude());
                            intent.putExtra("longitude", location2.getLongitude());
                            intent.putExtra("start", start);
                            intent.putExtra("end", end);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("LATITUDE2", "" + location2.getLatitude());
                            editor.putString("LONGITUDE2", "" + location2.getLongitude());
                            editor.putBoolean("start", false);
                            editor.putBoolean("end", false);
                            editor.apply();
//                            startService(intent);
                            stopService(intent);
                        }
                    });

//                    }
                }
            }
        });

        btn_view_map.setVisibility(View.GONE);

        // complete trip to view map and display path of trip
        btn_view_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!start) {
                    Toast.makeText(Act_start_trip.this, getString(R.string.please_start_your_trip), Toast.LENGTH_SHORT).show();
                } else if (!end) {
                    Toast.makeText(Act_start_trip.this, getString(R.string.please_end_your_trip), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(Act_start_trip.this, Act_ViewMap.class);
                    intent.putExtra("latitude1", prefs.getString("LATITUDE1", ""));
                    intent.putExtra("longitude1", prefs.getString("LONGITUDE1", ""));
                    intent.putExtra("latitude2", prefs.getString("LATITUDE2", ""));
                    intent.putExtra("longitude2", prefs.getString("LONGITUDE2", ""));
                    startActivity(intent);
                }
            }
        });

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

//              Intent intent = new Intent(getApplicationContext(), Act_start_trip.class);

                /*PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(Act_start_trip.this)
                        .setAutoCancel(true)
                        .setTicker("Stop Your Trip!!!")
                        .setContentTitle("Stop Your Trip!!!")
                        .setContentText("Please end Your Trip, you can't move your current location in last 10 minute")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pendingIntent)
                        .setSound(RingtoneManager.getActualDefaultRingtoneUri(getApplicationContext(),RingtoneManager.TYPE_RINGTONE));

                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(0,builder.build());*/
                AlertMessage();
//                        Toast.makeText(getApplicationContext(),"INSIDE",Toast.LENGTH_LONG).show();
            }
        };
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {

//            Toast.makeText(getApplicationContext(),"on Change "+location,Toast.LENGTH_LONG).show();
            if (location1 != null) {
                float distance = location1.distanceTo(location);

//                Toast.makeText(getApplicationContext(),"Default "+distance,Toast.LENGTH_LONG).show();

                if (distance > 100) {
//                    Toast.makeText(getApplicationContext(), "100" + handler, Toast.LENGTH_LONG).show();
                    if (runnable != null)
                        handler.removeCallbacks(runnable);
                } else {
                    if (!end) {
                        handler.postDelayed(runnable, 60000 * 10);
                    }
                }
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
//        Toast.makeText(getApplicationContext(),""+status,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
//        Toast.makeText(getApplicationContext(),""+provider,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
//        Toast.makeText(getApplicationContext(),""+provider,Toast.LENGTH_LONG).show();
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Act_start_trip.this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false);
        alertDialogBuilder.setPositiveButton("Goto Settings Page To Enable GPS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                callGPSSettingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callGPSSettingIntent);
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    // end trip aler dialog
    public void AlertMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Act_start_trip.this);
        builder.setMessage("Please End Your Trip, and show results");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        end = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}