package com.example.automap_application;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.automap_application.Constant.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Act_Engine_oil_service extends AppCompatActivity {

    ImageView back;
    EditText ED_Total_date, ED_Total_time;
    private int mHour, mMinute, mSec;
    String date;
//    String url = "http://webmobdemo.xyz/automap/api/add_services";

    LinearLayout ll_card_ok;
    String car_id, date_time, str_current_reading, str_travelling_distance, str_paid_amount;
    SharedPreferences prefs;

    LayoutInflater layoutInflater;
    PopupWindow popupWindow;

    EditText ED_station_name, ED_kind_name, ED_travelling_distance, ED_paid_amount;
    TextView ED_current_reading;

    String enum_id;
    String currency;
    TextView txt_currency;
    LinearLayout ll_date, ll_time;
    RVAdapterKm adapterKm;
    ArrayList Kmarraylist = new ArrayList();

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__engine_oil_service);

        prefs = getSharedPreferences(Act_Login.LOGIN_PREF, Context.MODE_PRIVATE);

        car_id = prefs.getString("CAR_ID", " ");
        currency = prefs.getString("CURRENCY", " ");

        Bundle bundle = getIntent().getExtras();
        enum_id = bundle.getString("string");

        back = findViewById(R.id.back_engine_oil_services);
        ll_date = findViewById(R.id.ll_date);
        ll_time = findViewById(R.id.ll_time);
        ED_Total_date = findViewById(R.id.ED_Total_date);
        ED_Total_time = findViewById(R.id.ED_Total_time);
        ll_card_ok = findViewById(R.id.ll_card_ok);
        ED_station_name = findViewById(R.id.ED_station_name);
        ED_kind_name = findViewById(R.id.ED_kind_name);
        ED_current_reading = findViewById(R.id.ED_current_reading);
        ED_travelling_distance = findViewById(R.id.ED_travelling_distance);
        ED_paid_amount = findViewById(R.id.ED_paid_amount);
        txt_currency = findViewById(R.id.txt_currency);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ED_current_reading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiatePopupWindow(v);
            }
        });

        date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
        ED_Total_date.setText(date);
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
        String strDate = dateFormat.format(currentTime);
        ED_Total_time.setText(strDate);

        txt_currency.setText(currency);

        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                Locale.setDefault(Locale.ENGLISH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(Act_Engine_oil_service.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                ED_Total_date.setText(date);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        ll_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                mSec = c.get(Calendar.SECOND);

                // Launch Time Picker Dialog
                Locale.setDefault(Locale.ENGLISH);
                TimePickerDialog timePickerDialog = new TimePickerDialog(Act_Engine_oil_service.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                ED_Total_time.setText(hourOfDay + ":" + minute + ":" + mSec);
                                mHour = hourOfDay;
//                                minute = minute;

                                //updateTime(mHour,minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        ll_card_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_current_reading = ED_current_reading.getText().toString();
                str_travelling_distance = ED_travelling_distance.getText().toString();
                str_paid_amount = ED_paid_amount.getText().toString();

                if (TextUtils.isEmpty(str_current_reading)) {
                    Toast.makeText(Act_Engine_oil_service.this, getString(R.string.please_enter_current_reading), Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(str_travelling_distance)) {
                    Toast.makeText(Act_Engine_oil_service.this, getString(R.string.please_enter_travaling_distance), Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(str_paid_amount)) {
                    Toast.makeText(Act_Engine_oil_service.this, getString(R.string.please_enter_amount), Toast.LENGTH_SHORT).show();
                } else {
                    addEngineOilService();
                }
            }
        });
        date_time = ED_Total_date.getText().toString() + ED_Total_time.getText().toString();
    }

    // add Engine Oil detail in service book
    private void addEngineOilService() {

        StringRequest request = new StringRequest(Request.Method.POST, Constant.URL+"add_services", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", " " + response);
                try {

                    JSONObject obj = new JSONObject(response);

                    if (obj.optString("status").equals("1")) {
                        showRemindDialog();
                        //Toast.makeText(Act_Engine_oil_service.this, "Engine oil service added successfully", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Act_Engine_oil_service.this, getString(R.string.internet_not_connect) + error, Toast.LENGTH_LONG).show();
                Log.i("My error", "" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<String, String>();
                map.put("car_id", car_id);
                map.put("station_name", ED_station_name.getText().toString());
                map.put("kind", ED_kind_name.getText().toString());
                map.put("current_reading", ED_current_reading.getText().toString());
                map.put("travelling_distance", ED_travelling_distance.getText().toString());
                map.put("amount", ED_paid_amount.getText().toString());
                map.put("date_time", date_time);
                map.put("enum_id", enum_id);
                Log.e("params", " " + map);
                return map;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @SuppressLint("InflateParams")
    private PopupWindow initiatePopupWindow(View v) {

        try {
            layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             View layout = layoutInflater.inflate(R.layout.pop_up_km, null);

            layout.measure(View.MeasureSpec.UNSPECIFIED,View.MeasureSpec.UNSPECIFIED);
            popupWindow = new PopupWindow(layout, FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT, true);
            popupWindow.showAsDropDown(v, -4, -4);

            final RecyclerView rvKm = layout.findViewById(R.id.rvKm);
            rvKm.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
            Kmarraylist.clear();

            Kmarraylist.add("3000 km");
            Kmarraylist.add("4000 km");
            Kmarraylist.add("5000 km");
            Kmarraylist.add("6000 km");
            Kmarraylist.add("7000 km");
            Kmarraylist.add("8000 km");
            Kmarraylist.add("9000 km");
            Kmarraylist.add("10000 km");
            Kmarraylist.add("11000 km");
            Kmarraylist.add("12000 km");
            Kmarraylist.add("13000 km");
            Kmarraylist.add("14000 km");
            Kmarraylist.add("15000 km");
            Kmarraylist.add("16000 km");

            adapterKm = new RVAdapterKm(Kmarraylist, popupWindow);
            rvKm.setAdapter(adapterKm);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return popupWindow;
    }

    @SuppressLint("SetTextI18n")
    public void showRemindDialog() {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND); // for dialog shadow
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
            dialog.setContentView(R.layout.popup_thank);
            dialog.setCancelable(false);
            // set values for custom dialog components - text, image and button
            TextView tvMessageTwo = (TextView) dialog.findViewById(R.id.tvMessageTwo);
            tvMessageTwo.setText(getString(R.string.your_next_change_oil_should_be) + ED_current_reading.getText().toString() + getString(R.string.for_the_meter_to_the_expected_travelling_distance) + ED_travelling_distance.getText().toString());
//            final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
            TextView tvOK = (TextView) dialog.findViewById(R.id.tvOk);

            dialog.show();

            tvOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    Intent i = new Intent(getApplicationContext(), Navigation_Activity.class);
                    startActivity(i);
                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class RVAdapterKm extends RecyclerView.Adapter<RVAdapterKm.VersionViewHolder> {

        ArrayList<String> mArrayList;
        PopupWindow popupWindow;

        RVAdapterKm(ArrayList<String> arrayList, PopupWindow popupWindow) {
            this.mArrayList = arrayList;
            this.popupWindow = popupWindow;
        }

        @NonNull
        @Override
        public RVAdapterKm.VersionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_km, viewGroup, false);
            RVAdapterKm.VersionViewHolder viewHolder = new RVAdapterKm.VersionViewHolder(view);
            return viewHolder;
        }

        @SuppressLint("RestrictedApi")
        @Override
        public void onBindViewHolder(@NonNull final RVAdapterKm.VersionViewHolder versionViewHolder, final int position) {
            try {
                versionViewHolder.tvKm.setText(mArrayList.get(position));
                versionViewHolder.tvKm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ED_current_reading.setText(versionViewHolder.tvKm.getText().toString());
                        popupWindow.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return mArrayList.size();
        }

        class VersionViewHolder extends RecyclerView.ViewHolder {

            TextView tvKm;
            VersionViewHolder(View itemView) {
                super(itemView);
                tvKm = itemView.findViewById(R.id.tvKm);
            }
        }
    }
}
